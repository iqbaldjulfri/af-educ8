package com.labtekv.educ8.util;

import java.io.IOException;
import java.util.Collection;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.labtekv.educ8.model.Administration;
import com.labtekv.educ8.model.Role;
import com.labtekv.educ8.model.School;
import com.labtekv.educ8.model.Teacher;
import com.labtekv.educ8.model.User;
import com.labtekv.educ8.service.AdministrationService;
import com.labtekv.educ8.service.SchoolService;
import com.labtekv.educ8.service.TeacherService;
import com.labtekv.educ8.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 *
 * @author Iqbal Djulfri
 */
@Component
public class SchoolLoader {
  @Autowired @Qualifier("simpleSchoolService")
  private SchoolService schoolService;

  @Autowired @Qualifier("autoFetchUserService_administration")
  private UserService userService;

  @Autowired @Qualifier("autoFetchAdministrationService_schools")
  private AdministrationService administrationService;

  @Autowired @Qualifier("autoFetchTeacherService_schools")
  private TeacherService teacherService;

  public Collection<School> loadAdministrationSchools(HttpServletRequest req, HttpServletResponse res) throws IOException {
    boolean f = false;

    Set<Role> roles = (Set<Role>) req.getAttribute("ROLES");

    for (Role r : roles) {
      if (r.getAuthority().equals("DEV") || r.getAuthority().equals("ADMIN")) f = true;
      if (f) break;
    }

    if (f) {
      return schoolService.getAll();
    } else {
      User user = (User) req.getAttribute("USER");
      if (user == null) {
        res.sendError(HttpServletResponse.SC_BAD_REQUEST, "No user. Please relogin");
        return null;
      }

      user = userService.getById(user.getId());
      Administration a = user.getAdministration();
      if (a == null) {
        res.sendError(HttpServletResponse.SC_BAD_REQUEST, "Not an Administrator");
        return null;
      }

      a = administrationService.getById(a.getId());
      Set schools = a.getSchools();
      if (schools == null) {
        res.sendError(HttpServletResponse.SC_BAD_REQUEST, "No schools registered for your account. Contact your System Administrator");
        return null;
      }

      return schools;
    }
  }

  public Collection<School> loadTeacherSchools(HttpServletRequest req, HttpServletResponse res) throws IOException {
    boolean f = false;

    Set<Role> roles = (Set<Role>) req.getAttribute("ROLES");

    for (Role r : roles) {
      if (r.getAuthority().equals("DEV") || r.getAuthority().equals("ADMIN")) f = true;
      if (f) break;
    }

    if (f) {
      return schoolService.getAll();
    } else {
      User user = (User) req.getAttribute("USER");
      if (user == null) {
        res.sendError(HttpServletResponse.SC_BAD_REQUEST, "No user. Please relogin");
        return null;
      }

      user = userService.getById(user.getId());
      Teacher a = user.getTeacher();
      if (a == null) {
        res.sendError(HttpServletResponse.SC_BAD_REQUEST, "Not an Administrator");
        return null;
      }

      a = teacherService.getById(a.getId());
      Set schools = a.getSchools();
      if (schools == null) {
        res.sendError(HttpServletResponse.SC_BAD_REQUEST, "No schools registered for your account. Contact your System Administrator");
        return null;
      }

      return schools;
    }
  }
}
