package com.labtekv.educ8.util;

import com.labtekv.educ8.dao.BaseDao;

import org.springframework.stereotype.Component;

/**
 *
 * @author Iqbal Djulfri
 */
@Component("pageCounter")
public class PageCounter {
  public int getMaxPage(long count) {
    return getMaxPage(count, BaseDao.FETCH_SIZE);
  }

  public int getMaxPage(long count, int fetchSize) {
    return Long.valueOf(count / fetchSize + 1).intValue();
  }
}
