package com.labtekv.educ8.beans.propertyeditors;

import java.beans.PropertyEditorSupport;

import com.labtekv.educ8.model.Room;
import com.labtekv.educ8.service.RoomService;

/**
 *
 * @author Iqbal Djulfri
 */
public class RoomPropertyEditor extends PropertyEditorSupport {
  private RoomService roomService;

  public RoomPropertyEditor(RoomService roomService) {
    this.roomService = roomService;
  }

  @Override
  public String getAsText() {
    if (getValue() == null) return "";
    return ((Room) getValue()).getId().toString();
  }

  @Override
  public void setAsText(String text) throws IllegalArgumentException {
    try {
      long id = Long.valueOf(text);
      setValue(roomService.getById(id));
    } catch (Exception e) {
      setValue(null);
    }
  }

}
