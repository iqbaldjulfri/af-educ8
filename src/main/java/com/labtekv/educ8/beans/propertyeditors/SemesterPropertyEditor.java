package com.labtekv.educ8.beans.propertyeditors;

import java.beans.PropertyEditorSupport;

import com.labtekv.educ8.model.Semester;
import com.labtekv.educ8.service.SemesterService;

/**
 *
 * @author Iqbal Djulfri
 */
public class SemesterPropertyEditor  extends PropertyEditorSupport {
  private SemesterService semesterService;

  public SemesterPropertyEditor(SemesterService semesterService) {
    this.semesterService = semesterService;
  }

  @Override
  public String getAsText() {
    if (getValue() == null) return "";
    return ((Semester) getValue()).getId().toString();
  }

  @Override
  public void setAsText(String text) throws IllegalArgumentException {
    try {
      long id = Long.valueOf(text);
      setValue(semesterService.getById(id));
    } catch (Exception e) {
      setValue(null);
    }
  }

}
