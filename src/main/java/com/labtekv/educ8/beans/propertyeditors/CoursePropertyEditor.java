package com.labtekv.educ8.beans.propertyeditors;

import java.beans.PropertyEditorSupport;

import com.labtekv.educ8.model.Course;
import com.labtekv.educ8.service.CourseService;

/**
 *
 * @author Iqbal Djulfri
 */
public class CoursePropertyEditor extends PropertyEditorSupport {

  private CourseService courseService;

  public CoursePropertyEditor(CourseService courseService) {
    this.courseService = courseService;
  }

  @Override
  public String getAsText() {
    return getValue() == null ? "" : ((Course) getValue()).getId().toString();
  }

  @Override
  public void setAsText(String text) throws IllegalArgumentException {
    try {
      long id = Long.valueOf(text);
      setValue(courseService.getById(id));
    } catch (Exception e) {
      setValue(null);
    }
  }

}
