package com.labtekv.educ8.beans.propertyeditors;

import java.beans.PropertyEditorSupport;

import com.labtekv.educ8.model.Exam;
import com.labtekv.educ8.service.ExamService;

/**
 *
 * @author Iqbal Djulfri
 */
public class ExamPropertyEditor extends PropertyEditorSupport {
  private ExamService examService;

  public ExamPropertyEditor(ExamService examService) {
    this.examService = examService;
  }

  @Override
  public String getAsText() {
    return getValue() == null ? "" : ((Exam) getValue()).getId().toString();
  }

  @Override
  public void setAsText(String text) throws IllegalArgumentException {
    try {
      long id = Long.valueOf(text);
      setValue(examService.getById(id));
    } catch (Exception e) {
      setValue(null);
    }
  }
}
