package com.labtekv.educ8.beans.propertyeditors;

import java.beans.PropertyEditorSupport;

import com.labtekv.educ8.model.Clazz;
import com.labtekv.educ8.service.ClazzService;

/**
 *
 * @author Iqbal Djulfri
 */
public class ClazzPropertyEditor extends PropertyEditorSupport {

  private ClazzService clazzService;

  public ClazzPropertyEditor(ClazzService clazzService) {
    this.clazzService = clazzService;
  }

  @Override
  public String getAsText() {
    return getValue() == null ? "" : ((Clazz) getValue()).getId().toString();
  }

  @Override
  public void setAsText(String text) throws IllegalArgumentException {
    try {
      long id = Long.valueOf(text);
      setValue(clazzService.getById(id));
    } catch (Exception e) {
      setValue(null);
    }
  }
}
