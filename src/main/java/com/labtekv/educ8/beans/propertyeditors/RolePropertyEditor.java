package com.labtekv.educ8.beans.propertyeditors;

import java.beans.PropertyEditorSupport;

import com.labtekv.educ8.model.Role;
import com.labtekv.educ8.service.RoleService;

/**
 *
 * @author Iqbal Djulfri
 */
public class RolePropertyEditor extends PropertyEditorSupport {

  private RoleService roleService;

  public RolePropertyEditor(RoleService roleService) {
    this.roleService = roleService;
  }

  @Override
  public void setAsText(String text) throws IllegalArgumentException {
    try {
      long id = Long.valueOf(text);

      Role r = roleService.getById(id);
      setValue(r);
    } catch (Exception e) {
      setValue(null);
    }
  }

  @Override
  public String getAsText() {
    if (getValue() == null) return "";
    return "" + ((Role) getValue()).getId();
  }

}
