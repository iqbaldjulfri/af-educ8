package com.labtekv.educ8.beans.propertyeditors;

import java.beans.PropertyEditorSupport;

import com.labtekv.educ8.model.ExamType;
import com.labtekv.educ8.service.ExamTypeService;

/**
 *
 * @author Iqbal Djulfri
 */
public class ExamTypePropertyEditor extends PropertyEditorSupport {
  private ExamTypeService examTypeService;

  public ExamTypePropertyEditor(ExamTypeService examTypeService) {
    this.examTypeService = examTypeService;
  }

  @Override
  public String getAsText() {
    return getValue() == null ? "" : ((ExamType) getValue()).getId().toString();
  }

  @Override
  public void setAsText(String text) throws IllegalArgumentException {
    try {
      long id = Long.valueOf(text);
      setValue(examTypeService.getById(id));
    } catch (Exception e) {
      setValue(null);
    }
  }
}
