package com.labtekv.educ8.beans.propertyeditors;

import java.beans.PropertyEditorSupport;

import com.labtekv.educ8.model.Address;
import com.labtekv.educ8.service.AddressService;

/**
 *
 * @author Iqbal Djulfri
 */
public class AddressPropertyEditor extends PropertyEditorSupport {
  private AddressService addressService;

  public AddressPropertyEditor(AddressService addressService) {
    this.addressService = addressService;
  }

  @Override
  public void setAsText(String text) throws IllegalArgumentException {
    try {
      long id = Long.valueOf(text);
      setValue(addressService.getById(id));
    } catch (Exception e) {
      setValue(null);
    }
  }

  @Override
  public String getAsText() {
    if (getValue() == null) return "";
    return "" + ((Address) getValue()).getId();
  }

}
