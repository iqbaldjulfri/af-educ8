package com.labtekv.educ8.beans.propertyeditors;

import java.beans.PropertyEditorSupport;

import com.labtekv.educ8.model.Schedule;
import com.labtekv.educ8.service.ScheduleService;

/**
 *
 * @author Iqbal Djulfri
 */
public class SchedulePropertyEditor extends PropertyEditorSupport {
  private ScheduleService scheduleService;

  public SchedulePropertyEditor(ScheduleService scheduleService) {
    this.scheduleService = scheduleService;
  }

  @Override
  public String getAsText() {
    if (getValue() == null) return "";
    return ((Schedule) getValue()).getId().toString();
  }

  @Override
  public void setAsText(String text) throws IllegalArgumentException {
    try {
      long id = Long.valueOf(text);
      setValue(scheduleService.getById(id));
    } catch (Exception e) {
      setValue(null);
    }
  }

}
