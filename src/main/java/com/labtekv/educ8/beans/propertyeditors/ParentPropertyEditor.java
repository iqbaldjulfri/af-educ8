/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.labtekv.educ8.beans.propertyeditors;

import com.labtekv.educ8.model.Parent;
import com.labtekv.educ8.service.ParentService;
import java.beans.PropertyEditorSupport;

/**
 *
 * @author Andika
 */
public class ParentPropertyEditor extends PropertyEditorSupport {
  private ParentService parentService;

  public ParentPropertyEditor(ParentService parentService) {
    this.parentService = parentService;
  }

  @Override
  public void setAsText(String text) throws IllegalArgumentException {
    try {
      long id = Long.valueOf(text);
      setValue(parentService.getById(id));
    } catch (Exception e) {
      setValue(null);
    }
  }

  @Override
  public String getAsText() {
    if (getValue() == null) return "";
    return "" + ((Parent) getValue()).getId();
  }
}
