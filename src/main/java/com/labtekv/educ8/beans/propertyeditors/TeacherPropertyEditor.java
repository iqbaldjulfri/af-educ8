package com.labtekv.educ8.beans.propertyeditors;

import java.beans.PropertyEditorSupport;

import com.labtekv.educ8.model.Teacher;
import com.labtekv.educ8.service.TeacherService;

/**
 *
 * @author Iqbal Djulfri
 */
public class TeacherPropertyEditor extends PropertyEditorSupport {
  private TeacherService teacherService;

  public TeacherPropertyEditor(TeacherService teacherService) {
    this.teacherService = teacherService;
  }

  @Override
  public void setAsText(String text) throws IllegalArgumentException {
    try {
      long id = Long.valueOf(text);
      setValue(teacherService.getById(id));
    } catch (Exception e) {
      setValue(null);
    }
  }

  @Override
  public String getAsText() {
    if (getValue() == null) return "";
    return "" + ((Teacher) getValue()).getId();
  }

}
