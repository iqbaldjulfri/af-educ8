package com.labtekv.educ8.beans.propertyeditors;

import java.beans.PropertyEditorSupport;

import com.labtekv.educ8.model.School;
import com.labtekv.educ8.service.SchoolService;

/**
 *
 * @author Iqbal Djulfri
 */
public class SchoolPropertyEditor extends PropertyEditorSupport {

  private SchoolService schoolService;

  public SchoolPropertyEditor(SchoolService schoolService) {
    this.schoolService = schoolService;
  }

  @Override
  public void setAsText(String text) throws IllegalArgumentException {
    try {
      long id = Long.valueOf(text);

      School s = schoolService.getById(id);
      setValue(s);
    } catch (Exception e) {
      setValue(null);
    }
  }

  @Override
  public String getAsText() {
    if (getValue() == null) return "";
    return "" + ((School) getValue()).getId();
  }

}
