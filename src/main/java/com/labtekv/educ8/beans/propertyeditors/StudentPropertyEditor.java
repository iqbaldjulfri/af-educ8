/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.labtekv.educ8.beans.propertyeditors;

import com.labtekv.educ8.model.Student;
import com.labtekv.educ8.service.StudentService;
import java.beans.PropertyEditorSupport;

/**
 *
 * @author Andika
 */
public class StudentPropertyEditor extends PropertyEditorSupport {
  private StudentService studentService;

  public StudentPropertyEditor(StudentService studentService) {
    this.studentService = studentService;
  }

  @Override
  public void setAsText(String text) throws IllegalArgumentException {
    try {
      long id = Long.valueOf(text);
      setValue(studentService.getById(id));
    } catch (Exception e) {
      setValue(null);
    }
  }

  @Override
  public String getAsText() {
    if (getValue() == null) return "";
    return "" + ((Student) getValue()).getId();
  }
}
