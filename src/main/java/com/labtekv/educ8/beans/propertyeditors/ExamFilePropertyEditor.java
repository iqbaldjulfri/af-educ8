package com.labtekv.educ8.beans.propertyeditors;

import java.beans.PropertyEditorSupport;

import com.labtekv.educ8.model.ExamFile;
import com.labtekv.educ8.service.ExamFileService;

/**
 *
 * @author Iqbal Djulfri
 */
public class ExamFilePropertyEditor extends PropertyEditorSupport {
  private ExamFileService examFileService;

  public ExamFilePropertyEditor(ExamFileService examFileService) {
    this.examFileService = examFileService;
  }

  @Override
  public String getAsText() {
    return getValue() == null ? "" : ((ExamFile) getValue()).getId().toString();
  }

  @Override
  public void setAsText(String text) throws IllegalArgumentException {
    try {
      long id = Long.valueOf(text);
      setValue(examFileService.getById(id));
    } catch (Exception e) {
      setValue(null);
    }
  }
}
