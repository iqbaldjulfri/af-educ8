package com.labtekv.educ8.beans.propertyeditors;

import java.beans.PropertyEditorSupport;

import com.labtekv.educ8.model.Menu;
import com.labtekv.educ8.service.MenuService;

/**
 *
 * @author Iqbal Djulfri
 */
public class MenuPropertyEditor extends PropertyEditorSupport {

  private MenuService menuService;

  public MenuPropertyEditor(MenuService menuService) {
    this.menuService = menuService;
  }

  @Override
  public void setAsText(String text) throws IllegalArgumentException {
    try {
      long id = Long.valueOf(text);
      setValue(menuService.getById(id));
    } catch (Exception e) {
      setValue(null);
    }
  }

  @Override
  public String getAsText() {
    if (getValue() == null) return "";
    return "" + ((Menu) getValue()).getId();
  }

}
