/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.labtekv.educ8.beans.propertyeditors;

import com.labtekv.educ8.model.Specialization;
import com.labtekv.educ8.service.SpecializationService;
import java.beans.PropertyEditorSupport;

/**
 *
 * @author Andika
 */
public class SpecializationPropertyEditor extends PropertyEditorSupport {
  private SpecializationService specializationService;

  public SpecializationPropertyEditor(SpecializationService specializationService) {
    this.specializationService = specializationService;
  }

  @Override
  public void setAsText(String text) throws IllegalArgumentException {
    try {
      long id = Long.valueOf(text);
      setValue(specializationService.getById(id));
    } catch (Exception e) {
      setValue(null);
    }
  }

  @Override
  public String getAsText() {
    if (getValue() == null) return "";
    return "" + ((Specialization) getValue()).getId();
  }

}
