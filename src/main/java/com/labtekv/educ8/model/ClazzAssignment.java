/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.labtekv.educ8.model;

import javax.validation.constraints.NotNull;

/**
 *
 * @author Andika
 */
public class ClazzAssignment extends BaseModel<Long>{
  @NotNull
  private Clazz clazz;
  
  @NotNull
  private Student student;
  
  @NotNull
  private Semester semester;

  public Clazz getClazz() {
    return clazz;
  }

  public void setClazz(Clazz clazz) {
    this.clazz = clazz;
  }

  public Semester getSemester() {
    return semester;
  }

  public void setSemester(Semester semester) {
    this.semester = semester;
  }

  public Student getStudent() {
    return student;
  }

  public void setStudent(Student student) {
    this.student = student;
  }
  
}
