package com.labtekv.educ8.model;

import java.util.Date;

import org.hibernate.validator.constraints.NotBlank;

/**
 *
 * @author Iqbal Djulfri
 */
public class Message extends BaseModel<Long> {
  private User user;
  private User sender;

  @NotBlank
  private String subject;
  
  @NotBlank
  private String message;
  private Boolean isRead = false;
  private Boolean isDeleted = false;
  private Date time;

  public Boolean getIsDeleted() {
    return isDeleted;
  }

  public void setIsDeleted(Boolean isDeleted) {
    this.isDeleted = isDeleted;
  }

  public Boolean getIsRead() {
    return isRead;
  }

  public void setIsRead(Boolean isRead) {
    this.isRead = isRead;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public User getSender() {
    return sender;
  }

  public void setSender(User sender) {
    this.sender = sender;
  }

  public String getSubject() {
    return subject;
  }

  public void setSubject(String subject) {
    this.subject = subject;
  }

  public Date getTime() {
    return time;
  }

  public void setTime(Date time) {
    this.time = time;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  @Override
  public Message clone() {
    Message m = new Message();
    m.message = message;
    m.sender = sender;
    m.subject = subject;
    m.time = time;

    return m;
  }

}
