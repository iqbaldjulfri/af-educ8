package com.labtekv.educ8.model;

import java.util.Date;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Iqbal Djulfri
 */
public class Student extends Person {
  public static final int LIVE_WITH_PARENT = 0;
  public static final int LIVE_WITH_FAMILY = 1;
  public static final int LIVE_WITH_DORM = 2;
  public static final int LIVE_WITH_KOST = 3;
  
  private String idNumber;
  private String testNumber;
  private String nickname;
  private int kidsOrder = 1;
  private int bloodBrother = 0;
  private int stepBrother = 0;
  private int fosterBrother = 0;
  private Boolean fatherDeceased = Boolean.FALSE;
  private Boolean motherDeceased = Boolean.FALSE;
  private int liveWith;
  private String homeDistance;
  private String dailyLanguage;
  //HEALTH
  private String diseaseSuffered;
  private String physicalAbnormalities;
  private String heightWeight;
  //LAST EDUCATION
  private String graduateFrom;
  private String sttbDateNumberFrom;
  private String lengthOfStudy;
  //TRANSFER STUDENT
  private String transferFrom;
  private String transferReason;
  //ACCEPTANCE DETAIL
  private String acceptanceLevel;
  private String acceptanceGroup;
  private Specialization specialization;
  private Date acceptanceDate;
  //HOBBIES
  private String art;
  private String sport;
  private String social;
  private String otherHobby;
  //DEVELOPMENT
  private String scholarship;
  private String yearLeaveSchool = null;
  private String reasonLeaveSchool = null;
  private String yearGraduate = null;
  private String sttbDateNumberGraduate;
  //AFTER GRADUATE
  private String continueAt;
  private String workAt;
  private Date dateStartWork = null;
  private String salary;
  
  private School school;
  private Parent father;
  private Parent mother;
  private Parent guardian;
  
  @NotNull
  private Boolean isActive = Boolean.TRUE;

  public Date getAcceptanceDate() {
    return acceptanceDate;
  }

  public void setAcceptanceDate(Date acceptanceDate) {
    this.acceptanceDate = acceptanceDate;
  }

  public String getAcceptanceGroup() {
    return acceptanceGroup;
  }

  public void setAcceptanceGroup(String acceptanceGroup) {
    this.acceptanceGroup = acceptanceGroup;
  }

  public String getAcceptanceLevel() {
    return acceptanceLevel;
  }

  public void setAcceptanceLevel(String acceptanceLevel) {
    this.acceptanceLevel = acceptanceLevel;
  }

  public String getArt() {
    return art;
  }

  public void setArt(String art) {
    this.art = art;
  }

  public int getBloodBrother() {
    return bloodBrother;
  }

  public void setBloodBrother(int bloodBrother) {
    this.bloodBrother = bloodBrother;
  }

  public String getContinueAt() {
    return continueAt;
  }

  public void setContinueAt(String continueAt) {
    this.continueAt = continueAt;
  }

  public String getDailyLanguage() {
    return dailyLanguage;
  }

  public void setDailyLanguage(String dailyLanguage) {
    this.dailyLanguage = dailyLanguage;
  }

  public Date getDateStartWork() {
    return dateStartWork;
  }

  public void setDateStartWork(Date dateStartWork) {
    this.dateStartWork = dateStartWork;
  }

  public String getDiseaseSuffered() {
    return diseaseSuffered;
  }

  public void setDiseaseSuffered(String diseaseSuffered) {
    this.diseaseSuffered = diseaseSuffered;
  }

  public Parent getFather() {
    return father;
  }

  public void setFather(Parent father) {
    this.father = father;
  }

  public Boolean getFatherDeceased() {
    return fatherDeceased;
  }

  public void setFatherDeceased(Boolean fatherDeceased) {
    this.fatherDeceased = fatherDeceased;
  }

  public int getFosterBrother() {
    return fosterBrother;
  }

  public void setFosterBrother(int fosterBrother) {
    this.fosterBrother = fosterBrother;
  }

  public String getGraduateFrom() {
    return graduateFrom;
  }

  public void setGraduateFrom(String graduateFrom) {
    this.graduateFrom = graduateFrom;
  }

  public Parent getGuardian() {
    return guardian;
  }

  public void setGuardian(Parent guardian) {
    this.guardian = guardian;
  }

  public String getHeightWeight() {
    return heightWeight;
  }

  public void setHeightWeight(String heightWeight) {
    this.heightWeight = heightWeight;
  }

  public String getHomeDistance() {
    return homeDistance;
  }

  public void setHomeDistance(String homeDistance) {
    this.homeDistance = homeDistance;
  }

  public String getIdNumber() {
    return idNumber;
  }

  public void setIdNumber(String idNumber) {
    this.idNumber = idNumber;
  }

  public int getKidsOrder() {
    return kidsOrder;
  }

  public void setKidsOrder(int kidsOrder) {
    this.kidsOrder = kidsOrder;
  }

  public String getLengthOfStudy() {
    return lengthOfStudy;
  }

  public void setLengthOfStudy(String lengthOfStudy) {
    this.lengthOfStudy = lengthOfStudy;
  }

  public int getLiveWith() {
    return liveWith;
  }

  public void setLiveWith(int liveWith) {
    this.liveWith = liveWith;
  }

  public Parent getMother() {
    return mother;
  }

  public void setMother(Parent mother) {
    this.mother = mother;
  }

  public Boolean getMotherDeceased() {
    return motherDeceased;
  }

  public void setMotherDeceased(Boolean motherDeceased) {
    this.motherDeceased = motherDeceased;
  }

  public String getNickname() {
    return nickname;
  }

  public void setNickname(String nickname) {
    this.nickname = nickname;
  }

  public String getOtherHobby() {
    return otherHobby;
  }

  public void setOtherHobby(String otherHobby) {
    this.otherHobby = otherHobby;
  }

  public String getPhysicalAbnormalities() {
    return physicalAbnormalities;
  }

  public void setPhysicalAbnormalities(String physicalAbnormalities) {
    this.physicalAbnormalities = physicalAbnormalities;
  }

  public String getReasonLeaveSchool() {
    return reasonLeaveSchool;
  }

  public void setReasonLeaveSchool(String reasonLeaveSchool) {
    this.reasonLeaveSchool = reasonLeaveSchool;
  }

  public String getSalary() {
    return salary;
  }

  public void setSalary(String salary) {
    this.salary = salary;
  }

  public String getScholarship() {
    return scholarship;
  }

  public void setScholarship(String scholarship) {
    this.scholarship = scholarship;
  }

  public School getSchool() {
    return school;
  }

  public void setSchool(School school) {
    this.school = school;
  }

  public String getSocial() {
    return social;
  }

  public void setSocial(String social) {
    this.social = social;
  }

  public Specialization getSpecialization() {
    return specialization;
  }

  public void setSpecialization(Specialization specialization) {
    this.specialization = specialization;
  }

  public String getSport() {
    return sport;
  }

  public void setSport(String sport) {
    this.sport = sport;
  }

  public int getStepBrother() {
    return stepBrother;
  }

  public void setStepBrother(int stepBrother) {
    this.stepBrother = stepBrother;
  }

  public String getSttbDateNumberFrom() {
    return sttbDateNumberFrom;
  }

  public void setSttbDateNumberFrom(String sttbDateNumberFrom) {
    this.sttbDateNumberFrom = sttbDateNumberFrom;
  }

  public String getSttbDateNumberGraduate() {
    return sttbDateNumberGraduate;
  }

  public void setSttbDateNumberGraduate(String sttbDateNumberGraduate) {
    this.sttbDateNumberGraduate = sttbDateNumberGraduate;
  }

  public String getTestNumber() {
    return testNumber;
  }

  public void setTestNumber(String testNumber) {
    this.testNumber = testNumber;
  }

  public String getTransferFrom() {
    return transferFrom;
  }

  public void setTransferFrom(String transferFrom) {
    this.transferFrom = transferFrom;
  }

  public String getTransferReason() {
    return transferReason;
  }

  public void setTransferReason(String transferReason) {
    this.transferReason = transferReason;
  }

  public String getWorkAt() {
    return workAt;
  }

  public void setWorkAt(String workAt) {
    this.workAt = workAt;
  }

  public String getYearGraduate() {
    return yearGraduate;
  }

  public void setYearGraduate(String yearGraduate) {
    this.yearGraduate = yearGraduate;
  }

  public String getYearLeaveSchool() {
    return yearLeaveSchool;
  }

  public void setYearLeaveSchool(String yearLeaveSchool) {
    this.yearLeaveSchool = yearLeaveSchool;
  }

  public Boolean getIsActive() {
    return isActive;
  }

  public void setIsActive(Boolean isActive) {
    this.isActive = isActive;
  }
  
}
