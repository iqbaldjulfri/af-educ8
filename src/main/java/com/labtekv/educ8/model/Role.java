package com.labtekv.educ8.model;

import java.util.Objects;

import org.hibernate.validator.constraints.NotBlank;

import org.springframework.security.core.GrantedAuthority;

/**
 *
 * @author Iqbal Djulfri
 */
public class Role extends BaseModel<Long> implements GrantedAuthority {

	@NotBlank
  private String authority;

	@NotBlank
  private String description;

  @Override
  public String getAuthority() {
    return this.authority;
  }

  public void setAuthority(String authority) {
    this.authority = authority;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  @Override
  public int hashCode() {
    int hash = 5;
    return hash;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final Role other = (Role) obj;
    if (!Objects.equals(this.authority, other.authority)) {
      return false;
    }
    return true;
  }

}
