/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.labtekv.educ8.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Andika
 */
public class TuitionPayment extends BaseModel<Long> {
  @JsonIgnore
  private Student student;
  private int month;
  private int year;
  
  @NotNull
  private Boolean paymentStatus = Boolean.FALSE;

  public int getMonth() {
    return month;
  }

  public void setMonth(int month) {
    this.month = month;
  }

  public Boolean getPaymentStatus() {
    return paymentStatus;
  }

  public void setPaymentStatus(Boolean paymentStatus) {
    this.paymentStatus = paymentStatus;
  }

  public Student getStudent() {
    return student;
  }

  public void setStudent(Student student) {
    this.student = student;
  }

  public int getYear() {
    return year;
  }

  public void setYear(int year) {
    this.year = year;
  }
}
