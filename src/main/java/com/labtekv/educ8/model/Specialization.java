package com.labtekv.educ8.model;

/**
 *
 * @author Andika
 */
public class Specialization extends BaseModel<Long> {
  private String name;
  private String description;
  private Boolean isActive;
  private School school;

  public Boolean getIsActive() {
    return isActive;
  }

  public void setIsActive(Boolean isActive) {
    this.isActive = isActive;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public School getSchool() {
    return school;
  }

  public void setSchool(School school) {
    this.school = school;
  }
}
