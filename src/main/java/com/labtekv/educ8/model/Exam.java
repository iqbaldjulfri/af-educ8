package com.labtekv.educ8.model;

import java.util.Date;
import java.util.List;
import java.util.Set;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Iqbal Djulfri
 */
public class Exam extends BaseModel<Long> {

  private String name;
  private String description;
  private Date start;
  private Date deadline;
  private Exam remedialFor;
  @NotNull
  private Boolean isSubmitable = false;
  @NotNull
  private Boolean isActive = true;
  @NotNull
  private Course course;
  @NotNull
  private Semester semester;
  private ExamType type;
  private Set<Clazz> clazzes;
  private List<Exam> remedials;
  private List<ExamFile> files;

  public List<ExamFile> getFiles() {
    return files;
  }

  public void setFiles(List<ExamFile> files) {
    this.files = files;
  }

  public Set<Clazz> getClazzes() {
    return clazzes;
  }

  public void setClazzes(Set<Clazz> clazzes) {
    this.clazzes = clazzes;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Date getStart() {
    return start;
  }

  public void setStart(Date start) {
    this.start = start;
  }

  public Date getDeadline() {
    return deadline;
  }

  public void setDeadline(Date deadline) {
    this.deadline = deadline;
  }

  public Boolean getIsSubmitable() {
    return isSubmitable;
  }

  public void setIsSubmitable(Boolean isSubmitable) {
    this.isSubmitable = isSubmitable;
  }

  public Boolean getIsActive() {
    return isActive;
  }

  public void setIsActive(Boolean isActive) {
    this.isActive = isActive;
  }

  public Course getCourse() {
    return course;
  }

  public void setCourse(Course course) {
    this.course = course;
  }

  public Semester getSemester() {
    return semester;
  }

  public void setSemester(Semester semester) {
    this.semester = semester;
  }

  public ExamType getType() {
    return type;
  }

  public void setType(ExamType type) {
    this.type = type;
  }

  public Exam getRemedialFor() {
    return remedialFor;
  }

  public void setRemedialFor(Exam remedialFor) {
    this.remedialFor = remedialFor;
  }

  public List<Exam> getRemedials() {
    return remedials;
  }

  public void setRemedials(List<Exam> remedials) {
    this.remedials = remedials;
  }
}
