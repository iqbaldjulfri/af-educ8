
package com.labtekv.educ8.model;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author Iqbal Djulfri
 */
public class BaseModel<ID extends Serializable> implements Serializable {
  protected ID id;

  public ID getId() {
    return id;
  }

  public void setId(ID id) {
    this.id = id;
  }

  public boolean isNew() {
		return (this.id == null);
	}

  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final BaseModel<ID> other = (BaseModel<ID>) obj;
    if (!Objects.equals(this.id, other.id)) {
      return false;
    }
    return true;
  }

  @Override
  public int hashCode() {
    int hash = 3;
    hash = 11 * hash + Objects.hashCode(this.id);
    return hash;
  }

}
