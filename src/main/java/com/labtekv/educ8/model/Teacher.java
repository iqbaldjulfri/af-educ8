package com.labtekv.educ8.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Set;

/**
 *
 * @author Iqbal Djulfri
 */
public class Teacher extends Person {
  @JsonIgnore
  private Set<School> schools;

  public Set<School> getSchools() {
    return schools;
  }

  public void setSchools(Set<School> schools) {
    this.schools = schools;
  }
}
