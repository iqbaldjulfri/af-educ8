package com.labtekv.educ8.model;

import java.util.Set;

/**
 *
 * @author Iqbal Djulfri
 */
public class Administration extends Person {
  private Set<School> schools;

  public Set<School> getSchools() {
    return schools;
  }

  public void setSchools(Set<School> schools) {
    this.schools = schools;
  }
}
