package com.labtekv.educ8.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.List;
import java.util.Set;

import org.hibernate.validator.constraints.NotBlank;

/**
 *
 * @author Iqbal Djulfri
 */
public class Menu extends BaseModel<Long> {

  @NotBlank
  private String name;

  @NotBlank
  private String url;

  @NotBlank
  private String description;

  private int priority = 500;

  private Menu parent;

  @JsonIgnore
  private Set<Role> roles;

  @JsonIgnore
  private List<Menu> children;

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Set<Role> getRoles() {
    return roles;
  }

  public void setRoles(Set<Role> roles) {
    this.roles = roles;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public int getPriority() {
    return priority;
  }

  public void setPriority(int priority) {
    this.priority = priority;
  }

  public Menu getParent() {
    return parent;
  }

  public void setParent(Menu parent) {
    this.parent = parent;
  }

  public List<Menu> getChildren() {
    return children;
  }

  public void setChildren(List<Menu> children) {
    this.children = children;
  }

}
