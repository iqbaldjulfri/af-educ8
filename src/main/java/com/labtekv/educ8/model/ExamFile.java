package com.labtekv.educ8.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.labtekv.educ8.EntityNotSavedException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Objects;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author Iqbal Djulfri
 */
public class ExamFile extends AbstractFile {
  public static final String BASE_LOCATION = "C:\\af-educ8\\files\\exam\\";
  private Exam exam;

  @JsonIgnore
  public Exam getExam() {
    return exam;
  }

  public void setExam(Exam exam) {
    this.exam = exam;
  }

  @Override
  public void loadFile() throws EntityNotSavedException, FileNotFoundException {
    if (isNew()) throw new EntityNotSavedException("Entity is not saved in the database");
    File f  = new File(BASE_LOCATION + id);
    if (!f.exists()) throw new FileNotFoundException("No file found");
    setFile(f);
  }

  @Override
  public void saveFile() throws EntityNotSavedException, IOException {
    if (isNew()) throw new EntityNotSavedException("Entity is not saved in the database");
    file.renameTo(new File(BASE_LOCATION + id));
  }

  @Override
  public int hashCode() {
    int hash = 5;
    hash = 89 * hash + Objects.hashCode(this.id);
    return hash;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final ExamFile other = (ExamFile) obj;
    if (!Objects.equals(this.id, other.id)) {
      return false;
    }
    return true;
  }
}
