package com.labtekv.educ8.model;

import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotBlank;

/**
 *
 * @author Iqbal Djulfri
 */
public class Address extends BaseModel<Long> {
  @NotBlank
  private String street;

  @NotBlank
  private String city;

  @NotBlank
  private String region;

  @NotBlank
  private String country;

  @NotNull
  private String rt = "";
  
  @NotNull
  private String rw = "";
  
  @NotBlank
  private String postalCode;

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public String getPostalCode() {
    return postalCode;
  }

  public void setPostalCode(String postalCode) {
    this.postalCode = postalCode;
  }

  public String getRegion() {
    return region;
  }

  public void setRegion(String region) {
    this.region = region;
  }

  public String getStreet() {
    return street;
  }

  public void setStreet(String street) {
    this.street = street;
  }

  public String getRt() {
    return rt;
  }

  public void setRt(String rt) {
    this.rt = rt;
  }

  public String getRw() {
    return rw;
  }

  public void setRw(String rw) {
    this.rw = rw;
  }

}
