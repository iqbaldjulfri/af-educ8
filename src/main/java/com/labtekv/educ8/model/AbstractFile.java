package com.labtekv.educ8.model;

import java.io.File;

/**
 *
 * @author Iqbal Djulfri
 */
public abstract class AbstractFile extends BaseModel<Long> implements BaseFileInterface {
  protected File file;
  protected String name;
  protected long size;

  public File getFile() {
    return file;
  }

  public void setFile(File file) {
    this.file = file;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public long getSize() {
    return size;
  }

  public void setSize(long size) {
    this.size = size;
  }
}
