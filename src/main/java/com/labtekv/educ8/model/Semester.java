package com.labtekv.educ8.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Date;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

/**
 *
 * @author Iqbal Djulfri
 */
public class Semester extends BaseModel<Long> {
  @NotBlank
  private String name;

  @NotNull
  private Date start;

  @NotNull
  private Date end;

  @NotNull
  @JsonIgnore
  private School school;

  public Date getEnd() {
    return end;
  }

  public void setEnd(Date end) {
    this.end = end;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public School getSchool() {
    return school;
  }

  public void setSchool(School school) {
    this.school = school;
  }

  public Date getStart() {
    return start;
  }

  public void setStart(Date start) {
    this.start = start;
  }
}
