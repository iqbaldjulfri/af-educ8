package com.labtekv.educ8.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Set;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

/**
 *
 * @author Iqbal Djulfri
 */
public class Course extends BaseModel<Long> {
  @NotBlank
  private String name;

  @NotBlank
  private String description;

  @NotBlank
  private String code;

  @NotBlank
  private String syllabus;

  @NotNull
  private Boolean isActive;

  private int maxSchedulePerWeek = 10;

  private int maxSchedulePerDay = 2;

  private int maxConsecutivePerDay = 2;

  private Room defaultRoom;

  @NotNull
  private String grade = "";

  @NotNull
  private School school;

  @JsonIgnore
  private Set<Teacher> teachers;

  public Set<Teacher> getTeachers() {
    return teachers;
  }

  public void setTeachers(Set<Teacher> teachers) {
    this.teachers = teachers;
  }

  public String getGrade() {
    return grade;
  }

  public void setGrade(String grade) {
    this.grade = grade.toUpperCase().trim();
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public int getMaxSchedulePerWeek() {
    return maxSchedulePerWeek;
  }

  public void setMaxSchedulePerWeek(int maxSchedulePerWeek) {
    this.maxSchedulePerWeek = maxSchedulePerWeek;
  }

  public int getMaxSchedulePerDay() {
    return maxSchedulePerDay;
  }

  public void setMaxSchedulePerDay(int maxSchedulePerDay) {
    this.maxSchedulePerDay = maxSchedulePerDay;
  }

  public int getMaxConsecutivePerDay() {
    return maxConsecutivePerDay;
  }

  public void setMaxConsecutivePerDay(int maxConsecutivePerDay) {
    this.maxConsecutivePerDay = maxConsecutivePerDay;
  }

  public Room getDefaultRoom() {
    return defaultRoom;
  }

  public void setDefaultRoom(Room defaultRoom) {
    this.defaultRoom = defaultRoom;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Boolean getIsActive() {
    return isActive;
  }

  public void setIsActive(Boolean isActive) {
    this.isActive = isActive;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public School getSchool() {
    return school;
  }

  public void setSchool(School school) {
    this.school = school;
  }

  public String getSyllabus() {
    return syllabus;
  }

  public void setSyllabus(String syllabus) {
    this.syllabus = syllabus;
  }

}
