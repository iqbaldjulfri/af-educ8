package com.labtekv.educ8.model;

import java.util.Set;

/**
 *
 * @author Iqbal Djulfri
 */
public class Parent extends Person {
  public static final int TYPE_FATHER = 0;  
  public static final int TYPE_MOTHER = 1;  
  public static final int TYPE_GUARDIAN = 2;  
    
  //DETAIL
  private String education;
  private String job;
  private String monthlyIncome;
  private Boolean isDeceased = Boolean.FALSE;
  private String deathDate = null;
  
  private Set<Student> children;
  private int parentType = 0;

  public Set<Student> getChildren() {
    return children;
  }

  public void setChildren(Set<Student> children) {
    this.children = children;
  }

  public String getDeathDate() {
    return deathDate;
  }

  public void setDeathDate(String deathDate) {
    this.deathDate = deathDate;
  }

  public String getEducation() {
    return education;
  }

  public void setEducation(String education) {
    this.education = education;
  }

  public Boolean getIsDeceased() {
    return isDeceased;
  }

  public void setIsDeceased(Boolean isDeceased) {
    this.isDeceased = isDeceased;
  }

  public String getJob() {
    return job;
  }

  public void setJob(String job) {
    this.job = job;
  }

  public String getMonthlyIncome() {
    return monthlyIncome;
  }

  public void setMonthlyIncome(String monthlyIncome) {
    this.monthlyIncome = monthlyIncome;
  }

  public int getParentType() {
    return parentType;
  }

  public void setParentType(int parentType) {
    this.parentType = parentType;
  }
}
