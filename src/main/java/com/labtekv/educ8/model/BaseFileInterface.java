package com.labtekv.educ8.model;

import com.labtekv.educ8.EntityNotSavedException;
import java.io.FileNotFoundException;
import java.io.IOException;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author Iqbal Djulfri
 */
public interface BaseFileInterface {
  public void loadFile() throws EntityNotSavedException, FileNotFoundException;
  public void saveFile() throws EntityNotSavedException, IOException;
}
