package com.labtekv.educ8.model;

/**
 *
 * @author Iqbal Djulfri
 */
public class Schedule extends BaseModel<Long> {
  private ScheduleSlot slot;
  private Clazz clazz;
  private Semester semester;
  private Room room;
  private Course course;
  private Teacher teacher;
  private Boolean isLocked = false;

  public Teacher getTeacher() {
    return teacher;
  }

  public void setTeacher(Teacher teacher) {
    this.teacher = teacher;
  }

  public Boolean getIsLocked() {
    return isLocked;
  }

  public void setIsLocked(Boolean isLocked) {
    this.isLocked = isLocked;
  }

  public ScheduleSlot getSlot() {
    return slot;
  }

  public void setSlot(ScheduleSlot slot) {
    this.slot = slot;
  }

  public Clazz getClazz() {
    return clazz;
  }

  public void setClazz(Clazz clazz) {
    this.clazz = clazz;
  }

  public Semester getSemester() {
    return semester;
  }

  public void setSemester(Semester semester) {
    this.semester = semester;
  }

  public Room getRoom() {
    return room;
  }

  public void setRoom(Room room) {
    this.room = room;
  }

  public Course getCourse() {
    return course;
  }

  public void setCourse(Course course) {
    this.course = course;
  }

  @Override
  public Schedule clone() {
    Schedule temp = new Schedule();
    temp.clazz = clazz;
    temp.course = course;
    temp.isLocked = isLocked;
    temp.room = room;
    temp.semester = semester;
    temp.slot = slot;
    temp.teacher = teacher;

    return temp;
  }
}
