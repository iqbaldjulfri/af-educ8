package com.labtekv.educ8.model;

import java.util.Set;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Iqbal Djulfri
 */
public class Room extends BaseModel<Long> {
  private String name;
  private String location;
  @NotNull
  private Boolean isActive = true;
  private Set<School> schools;

  public Boolean getIsActive() {
    return isActive;
  }

  public void setIsActive(Boolean isActive) {
    this.isActive = isActive;
  }

  public String getLocation() {
    return location;
  }

  public void setLocation(String location) {
    this.location = location;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Set<School> getSchools() {
    return schools;
  }

  public void setSchools(Set<School> schools) {
    this.schools = schools;
  }
}
