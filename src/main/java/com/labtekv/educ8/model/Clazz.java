package com.labtekv.educ8.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

/**
 *
 * @author Iqbal Djulfri
 */
public class Clazz extends BaseModel<Long> {
  @NotBlank
  private String name;

  @NotNull
  private Boolean isActive;

  @NotNull
  @JsonIgnore
  private School school;

  @JsonIgnore
  private Teacher homeroomTeacher;

  private Room defaultRoom;

  @NotNull
  private String grade = "";

  public String getGrade() {
    return grade;
  }

  public void setGrade(String grade) {
    this.grade = grade.toUpperCase().trim();
  }

  public Room getDefaultRoom() {
    return defaultRoom;
  }

  public void setDefaultRoom(Room defaultRoom) {
    this.defaultRoom = defaultRoom;
  }

  public Boolean getIsActive() {
    return isActive;
  }

  public void setIsActive(Boolean isActive) {
    this.isActive = isActive;
  }

  public Teacher getHomeroomTeacher() {
    return homeroomTeacher;
  }

  public void setHomeroomTeacher(Teacher homeroomTeacher) {
    this.homeroomTeacher = homeroomTeacher;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public School getSchool() {
    return school;
  }

  public void setSchool(School school) {
    this.school = school;
  }
}
