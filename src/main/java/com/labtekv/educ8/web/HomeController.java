package com.labtekv.educ8.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author Iqbal Djulfri
 */
@Controller
public class HomeController {

  @RequestMapping("/")
  public String setupHomePage() {
    return "/index";
  }
}
