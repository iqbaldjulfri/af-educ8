package com.labtekv.educ8.web.dev;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import com.labtekv.educ8.beans.propertyeditors.MenuPropertyEditor;
import com.labtekv.educ8.beans.propertyeditors.RolePropertyEditor;
import com.labtekv.educ8.model.Menu;
import com.labtekv.educ8.model.Role;
import com.labtekv.educ8.service.MenuService;
import com.labtekv.educ8.service.RoleService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;

/**
 *
 * @author Iqbal Djulfri
 */
@Controller
@RequestMapping(value = "/dev/menu", method = RequestMethod.GET)
@SessionAttributes("menu")
public class MenuAdminController {

  @Autowired
  @Qualifier("autoFetchMenuService_all")
  private MenuService menuService;

  @Autowired
  @Qualifier("simpleRoleService")
  private RoleService roleService;

  private final String defaultView = "/dev/menu";

  public void setMenuService(MenuService menuService) {
    this.menuService = menuService;
  }

  public void setRoleService(RoleService roleService) {
    this.roleService = roleService;
  }

  @InitBinder
  public void setAllowedFields(WebDataBinder dataBinder) {
    dataBinder.setDisallowedFields("id");
    dataBinder.registerCustomEditor(Role.class, new RolePropertyEditor(roleService));
    dataBinder.registerCustomEditor(Menu.class, new MenuPropertyEditor(menuService));
  }

  @RequestMapping("/")
  public String setupSearchPage() {
    return defaultView + "/index";
  }

  @RequestMapping(value = "/search", method = RequestMethod.POST)
  public String searchMenu(
      Model model,
      @RequestParam(defaultValue = "") String name,
      @RequestParam(defaultValue = "1") int page) {

    List menus = menuService.getByName(name, page);
    model.addAttribute("menus", menus);

    return defaultView + "/list";
  }

  @RequestMapping("/new")
  public String setupNewForm(Model model) {
    model.addAttribute("roles", roleService.getAll());
    model.addAttribute("menu", new Menu());
    return defaultView + "/form";
  }

  @RequestMapping("/{id}")
  public String setupDetailPage(Model model, @PathVariable("id") long id) {
    prepareDetailPage(model, id);
    model.addAttribute("readOnly", true);

    return defaultView + "/form";
  }

  @RequestMapping("/edit-{id}")
  public String setupEditForm(Model model, @PathVariable("id") long id) {
    prepareDetailPage(model, id);
    return defaultView + "/form";
  }

  @RequestMapping(value = "/submit", method = RequestMethod.POST)
  public String updateMenu(
      @Valid @ModelAttribute("menu") Menu menu,
      BindingResult result,
      SessionStatus status,
      Model model) {

    if (result.hasErrors()) {
      model.addAttribute("roles", roleService.getAll());
      return defaultView + "/form";
    } else {
      if (menu.isNew())
        menuService.create(menu);
      else
        menuService.update(menu);

      status.setComplete();
      return "redirect:" + defaultView + "/";
    }
  }

  @RequestMapping(value = "/delete-{id}", method = RequestMethod.DELETE)
  @ResponseBody
  public Map deleteMenu(@PathVariable("id") long id, HttpServletResponse res) throws IOException {
    //Menu menu = null;
    Map map = new HashMap();
    try {
      //menu = menuService.getById(id);
      menuService.deleteById(id);
      map.put("status", true);
    } catch (Exception e) {
      //res.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
      map.put("status", false);
      map.put("message", e.getMessage());
    }
    return map;
  }

  private void prepareDetailPage(Model model, long id) {
    Menu m = menuService.getById(id);
    model.addAttribute("menu", m);
    model.addAttribute("roles", roleService.getAll());
  }

}
