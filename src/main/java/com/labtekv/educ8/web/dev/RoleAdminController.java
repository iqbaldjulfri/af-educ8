
package com.labtekv.educ8.web.dev;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;

import com.labtekv.educ8.model.Role;
import com.labtekv.educ8.service.RoleService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;

/**
 *
 * @author Alienware
 */

@Controller
@RequestMapping(value = "/dev/role", method = RequestMethod.GET)
@SessionAttributes("role")
public class RoleAdminController {

	@Autowired
  @Qualifier("simpleRoleService")
  private RoleService roleService;
  private final String defaultView = "/dev/role";

  public void setRoleService(RoleService roleService) {
    this.roleService = roleService;
  }

  @InitBinder
  public void setAllowedFields(WebDataBinder dataBinder) {
    dataBinder.setDisallowedFields("id");
  }

	@RequestMapping("/")
  public String setupSearchPage() {
    return defaultView + "/index";
  }
  
  @RequestMapping(value = "/search", method = RequestMethod.POST)
  public String searchRole(
      Model model,
      @RequestParam(defaultValue = "") String authority,
      @RequestParam(defaultValue = "1") int page) {

    List roles = roleService.getByAuthority(authority, page);
    model.addAttribute("roles", roles);

    return defaultView + "/list";
  }

	@RequestMapping("/new")
  public String setupNewForm(Model model) {
    model.addAttribute("role", new Role());
    return defaultView + "/form";
  }

  @RequestMapping("/{id}")
  public String setupDetailPage(Model model, @PathVariable("id") long id) {
    model.addAttribute("role", roleService.getById(id));
    model.addAttribute("readOnly", true);
    return defaultView + "/form";
  }

  @RequestMapping("/edit-{id}")
  public String setupEditForm(Model model, @PathVariable("id") long id) {
    model.addAttribute("role", roleService.getById(id));
    return defaultView + "/form";
  }

	@RequestMapping(value = "/submit", method = RequestMethod.POST)
  public String updateRole(
      @Valid @ModelAttribute("role") Role role, 
      BindingResult result, 
      SessionStatus status) {
		/*
    if (role.isNew() && role.getAuthority().isEmpty())
      result.addError(new ObjectError("authority", "Must be filled"));
	  */

    if (result.hasErrors()) {
      return defaultView + "/form";
    } else {
      if (role.isNew())
        roleService.create(role);
      else
        roleService.update(role);

      status.setComplete();
      return "redirect:" + defaultView + "/";
    }
  }

  @RequestMapping(value = "/delete-{id}", method = RequestMethod.DELETE)
  @ResponseBody
  public Map deleteRole(@PathVariable("id") long id) {
    Map map = new HashMap();
    try {
      roleService.deleteById(id);
      map.put("status", true);
    } catch (Exception e) {
      map.put("status", false);
      map.put("message", e.getMessage());
    }
    return map;
  }
}
