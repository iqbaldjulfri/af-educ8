package com.labtekv.educ8.web.lookups;

import java.util.List;

import com.labtekv.educ8.service.MenuService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Iqbal Djulfri
 */
@Controller
@RequestMapping("/lookups/menu")
public class MenuLookupController {

  @Autowired
  @Qualifier("simpleMenuService")
  private MenuService menuService;

  public void setMenuService(MenuService menuService) {
    this.menuService = menuService;
  }

  @RequestMapping("/by-name")
  @ResponseBody
  public List lookupAllMenuByName(
      @RequestParam String term,
      @RequestParam(defaultValue = "1") int page) {

    List result = menuService.getByName(term, page);
    return result;
  }
}
