package com.labtekv.educ8.web.lookups;

import java.util.ArrayList;
import java.util.List;

import com.labtekv.educ8.service.CourseService;
import com.labtekv.educ8.service.SchoolService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Iqbal Djulfri
 */
@Controller
@RequestMapping("/lookups/course")
public class CourseLookupController {

  @Autowired
  @Qualifier("simpleCourseService")
  private CourseService courseService;

  @Autowired
  @Qualifier("simpleSchoolService")
  private SchoolService schoolService;

  public void setCourseService(CourseService courseService) {
    this.courseService = courseService;
  }

  public void setSchoolService(SchoolService schoolService) {
    this.schoolService = schoolService;
  }

  @RequestMapping("/by-name")
  @ResponseBody
  public List lookupByName(@RequestParam String term,
      @RequestParam(required = false) Long schoolId,
      @RequestParam(defaultValue = "1") int page) {

    List schools = new ArrayList();
    schools.add(schoolService.getById(schoolId));
    return courseService.getByName(term, schools, page);
  }
}
