package com.labtekv.educ8.web.lookups;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.labtekv.educ8.service.UserService;
import com.labtekv.educ8.util.PageCounter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Iqbal Djulfri
 */
@Controller
@RequestMapping("/lookups/user")
public class UserLookupController {

  @Autowired @Qualifier("simpleUserService")
  private UserService userService;

  @Autowired
  private PageCounter pageCounter;

  public void setPageCounter(PageCounter pageCounter) {
    this.pageCounter = pageCounter;
  }

  public void setUserService(UserService userService) {
    this.userService = userService;
  }

  @RequestMapping("/by-name")
  @ResponseBody
  public Map<String, Object> lookupAllUser(
      @RequestParam String term,
      @RequestParam(defaultValue = "1") int page) {

    List users = userService.getByName(term, page);
    long count = userService.countByName(term);
    Map result = new HashMap();
    result.put("list", users);
    result.put("pageCount", pageCounter.getMaxPage(count));

    return result;
  }

}
