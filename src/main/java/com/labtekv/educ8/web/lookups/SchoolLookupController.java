/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.labtekv.educ8.web.lookups;

import com.labtekv.educ8.service.SchoolService;
import com.labtekv.educ8.util.PageCounter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Andika
 */
@Controller
@RequestMapping("/lookups/school")
public class SchoolLookupController {

  @Autowired
  @Qualifier("simpleSchoolService")
  private SchoolService schoolService;

  @Autowired
  private PageCounter pageCounter;

  public void setPageCounter(PageCounter pageCounter) {
    this.pageCounter = pageCounter;
  }

  public void setSchoolService(SchoolService schoolService) {
    this.schoolService = schoolService;
  }

  @RequestMapping("/by-name")
  @ResponseBody
  public Map<String, Object> lookupAllSchoolByName(
      @RequestParam String term,
      @RequestParam(defaultValue = "1") int page) {

    List schools = schoolService.getByName(term, page);
    long count = schoolService.getPageCountByName(term);
    Map result = new HashMap();
    result.put("list", schools);
    result.put("pageCount", pageCounter.getMaxPage(count));

    return result;
  }

}
