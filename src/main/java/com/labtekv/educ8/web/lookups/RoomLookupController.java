package com.labtekv.educ8.web.lookups;

import java.util.ArrayList;
import java.util.List;
import com.labtekv.educ8.service.RoomService;
import com.labtekv.educ8.service.SchoolService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Iqbal Djulfri
 */
@Controller
@RequestMapping("/lookups/room")
public class RoomLookupController {

  @Autowired
  @Qualifier("simpleRoomService")
  private RoomService roomService;

  @Autowired
  @Qualifier("simpleSchoolService")
  private SchoolService schoolService;

  public void setRoomService(RoomService roomService) {
    this.roomService = roomService;
  }

  public void setSchoolService(SchoolService schoolService) {
    this.schoolService = schoolService;
  }

  @RequestMapping("/by-name")
  @ResponseBody
  public List lookupByName(@RequestParam String term,
      @RequestParam(required=false) Long schoolId,
      @RequestParam(defaultValue = "1") int page) {

    List schools = new ArrayList();
    schools.add(schoolService.getById(schoolId));
    return roomService.getByName(term, schools, page);
  }
}
