package com.labtekv.educ8.web.lookups;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.labtekv.educ8.model.School;
import com.labtekv.educ8.service.TeacherService;
import com.labtekv.educ8.util.SchoolLoader;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Iqbal Djulfri
 */
@Controller
@RequestMapping("/lookups/teacher")
public class TeacherLookupController {

  @Autowired @Qualifier("simpleTeacherService")
  private TeacherService teacherService;

  @Autowired
  private SchoolLoader schoolLoader;

  public void setSchoolLoader(SchoolLoader schoolLoader) {
    this.schoolLoader = schoolLoader;
  }

  public void setTeacherService(TeacherService teacherService) {
    this.teacherService = teacherService;
  }

  @RequestMapping("/by-name")
  @ResponseBody
  public Map<String, Object> lookupAllUser(
      @RequestParam String term,
      @RequestParam(required=false) Long schoolId,
      @RequestParam(defaultValue = "1") int page,
      HttpServletRequest req,
      HttpServletResponse res) throws IOException {

    List temp = (List) schoolLoader.loadAdministrationSchools(req, res),
        schools = new ArrayList();
    if (schoolId != null) {
      for (Object object : temp) {
        School s = (School) object;
        if (s.getId().equals(schoolId)) schools.add(s);
      }
    }
    List users = teacherService.getByName(term, schools, page);
    Map result = new HashMap();
    result.put("list", users);

    return result;
  }

}
