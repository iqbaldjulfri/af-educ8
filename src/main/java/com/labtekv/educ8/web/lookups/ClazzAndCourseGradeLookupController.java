package com.labtekv.educ8.web.lookups;

import java.util.List;

import com.labtekv.educ8.service.ClazzAndCourseGradeService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Iqbal Djulfri
 */
@Controller
@RequestMapping("/lookups/clazz-and-course-grades")
public class ClazzAndCourseGradeLookupController {

  @Autowired
  private ClazzAndCourseGradeService service;

  public void setDao(ClazzAndCourseGradeService service) {
    this.service = service;
  }

  @RequestMapping("/by-name")
  @ResponseBody
  public List searchScholsClazzAndCourseGrade(@RequestParam String term,
      @RequestParam(required = false) Long schoolId,
      @RequestParam(defaultValue = "1") int page) {
    return service.getClazzAndCourseGrade(term, schoolId);
  }
}
