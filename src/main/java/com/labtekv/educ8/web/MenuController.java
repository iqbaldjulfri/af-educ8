package com.labtekv.educ8.web;

import com.labtekv.educ8.model.Menu;
import com.labtekv.educ8.service.MenuService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author Iqbal Djulfri
 */
@Controller
@RequestMapping("/menu")
public class MenuController {

  private MenuService menuService;

  @Autowired
  @Qualifier("autoFetchMenuService_children")
  public void setMenuService(MenuService menuService) {
    this.menuService = menuService;
  }

  @RequestMapping(method = RequestMethod.GET)
  public String setupMenu(@RequestParam Long parentId,
      Model model) {
    Menu menu = menuService.getById(parentId);
    model.addAttribute("MENU_ITEM", menu);

    return "/menu";
  }
}
