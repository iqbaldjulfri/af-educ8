package com.labtekv.educ8.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author Iqbal Djulfri
 */
@Controller
@RequestMapping
public class LoginController {

  @RequestMapping("/login")
	public String login(Model model, @RequestParam(required=false) String err) {
		model.addAttribute("err", err);
		return "login";
	}

	@RequestMapping("/logout/success")
 	public String logoutSuccess() {
		return "redirect:/";
	}
}
