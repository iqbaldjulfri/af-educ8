package com.labtekv.educ8.web.secure.administration;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import com.labtekv.educ8.beans.propertyeditors.SchoolPropertyEditor;
import com.labtekv.educ8.model.Room;
import com.labtekv.educ8.model.School;
import com.labtekv.educ8.service.RoomService;
import com.labtekv.educ8.service.SchoolService;
import com.labtekv.educ8.util.SchoolLoader;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;

/**
 *
 * @author Iqbal Djulfri
 */
@Controller
@RequestMapping("/secure/administration/room")
@SessionAttributes({"room", "schools"})
public class RoomAdminController {

  @Autowired @Qualifier("autoFetchRoomService_schools")
  private RoomService roomService;

  @Autowired @Qualifier("simpleSchoolService")
  private SchoolService schoolService;

  @Autowired
  private SchoolLoader schoolLoader;

  private final String defaultView = "/secure/administration/room";

  public void setSchoolLoader(SchoolLoader schoolLoader) {
    this.schoolLoader = schoolLoader;
  }

  public void setRoomService(RoomService roomService) {
    this.roomService = roomService;
  }

  public void setSchoolService(SchoolService schoolService) {
    this.schoolService = schoolService;
  }

  @InitBinder
  public void initBinder(WebDataBinder dataBinder) {
    dataBinder.setDisallowedFields("id");
    dataBinder.registerCustomEditor(School.class, new SchoolPropertyEditor(schoolService));
  }

  @RequestMapping("/")
  public String setupSearchPage() {
    return defaultView + "/index";
  }

  @RequestMapping(value = "/search", method = RequestMethod.POST)
  public String searchUser(Model model,
      @RequestParam(defaultValue = "") String name,
      @RequestParam(defaultValue = "1") int page,
      HttpServletResponse res,
      HttpServletRequest req) throws IOException {
    List rooms;

    Collection schools = schoolLoader.loadAdministrationSchools(req, res);
    rooms = roomService.getByName(name, schools, page);

    model.addAttribute("rooms", rooms);

    return defaultView + "/list";
  }

  @RequestMapping("/new")
  public String setupNewForm(Model model,
      HttpServletRequest req, HttpServletResponse res) throws IOException {
    model.addAttribute("room", new Room());
    model.addAttribute("schools", schoolLoader.loadAdministrationSchools(req, res));
    return defaultView + "/form";
  }

  @RequestMapping("/{id}")
  public String setupDetailPage(Model model,
      @PathVariable("id") long id,
      HttpServletRequest req,
      HttpServletResponse res) throws IOException {
    model.addAttribute("room", roomService.getById(id));
    model.addAttribute("schools", schoolLoader.loadAdministrationSchools(req, res));
    model.addAttribute("readOnly", true);
    return defaultView + "/form";
  }

  @RequestMapping("/edit-{id}")
  public String setupEditForm(Model model,
      @PathVariable("id") long id,
      HttpServletRequest req,
      HttpServletResponse res) throws IOException {
    model.addAttribute("room", roomService.getById(id));
    model.addAttribute("schools", schoolLoader.loadAdministrationSchools(req, res));
    return defaultView + "/form";
  }

  @RequestMapping(value = "/submit", method = RequestMethod.POST)
  public String update(
      @Valid @ModelAttribute("room") Room room,
      BindingResult result,
      SessionStatus status) {

    if (result.hasErrors()) {
      return defaultView + "/form";
    } else {
      if (room.isNew())
        roomService.create(room);
      else
        roomService.update(room);

      status.setComplete();
      return "redirect:" + defaultView + "/";
    }
  }

  @RequestMapping(value = "/delete-{id}", method = RequestMethod.DELETE)
  @ResponseBody
  public Map delete(@PathVariable("id") long id) {
    Map map = new HashMap();
    try {
      roomService.deleteById(id);
      map.put("status", true);
    } catch (Exception e) {
      map.put("status", false);
      map.put("message", e.getMessage());
    }
    return map;
  }

}
