package com.labtekv.educ8.web.secure.headmaster;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.labtekv.educ8.beans.propertyeditors.CoursePropertyEditor;
import com.labtekv.educ8.beans.propertyeditors.RoomPropertyEditor;
import com.labtekv.educ8.beans.propertyeditors.SchedulePropertyEditor;
import com.labtekv.educ8.beans.propertyeditors.SemesterPropertyEditor;
import com.labtekv.educ8.beans.propertyeditors.TeacherPropertyEditor;
import com.labtekv.educ8.model.Clazz;
import com.labtekv.educ8.model.Course;
import com.labtekv.educ8.model.Role;
import com.labtekv.educ8.model.Room;
import com.labtekv.educ8.model.Schedule;
import com.labtekv.educ8.model.ScheduleSlot;
import com.labtekv.educ8.model.Semester;
import com.labtekv.educ8.model.Teacher;
import com.labtekv.educ8.model.User;
import com.labtekv.educ8.service.ClazzService;
import com.labtekv.educ8.service.CourseService;
import com.labtekv.educ8.service.RoleService;
import com.labtekv.educ8.service.RoomService;
import com.labtekv.educ8.service.ScheduleService;
import com.labtekv.educ8.service.ScheduleSlotService;
import com.labtekv.educ8.service.SemesterService;
import com.labtekv.educ8.service.TeacherService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Iqbal Djulfri
 */
@Controller
@RequestMapping(value="/secure/headmaster/schedule", method=RequestMethod.GET)
public class ScheduleController {

  private final String defaultView = "/secure/headmaster/schedule";

  @Autowired
  @Qualifier("autoFetchScheduleService_all")
  private ScheduleService scheduleService;

  @Autowired
  @Qualifier("autoFetchSemesterService_school")
  private SemesterService semesterService;

  @Autowired
  @Qualifier("simpleClazzService")
  private ClazzService clazzService;

  @Autowired
  @Qualifier("simpleScheduleSlotService")
  private ScheduleSlotService scheduleSlotService;

  @Autowired
  @Qualifier("simpleRoomService")
  private RoomService roomService;

  @Autowired
  @Qualifier("autoFetchCourseService_teachers")
  private CourseService courseService;

  @Autowired
  @Qualifier("simpleRoleService")
  private RoleService roleService;

  @Autowired
  @Qualifier("simpleTeacherService")
  private TeacherService teacherService;

  public void setScheduleService(ScheduleService scheduleService) {
    this.scheduleService = scheduleService;
  }

  public void setSemesterService(SemesterService semesterService) {
    this.semesterService = semesterService;
  }

  public void setClazzService(ClazzService clazzService) {
    this.clazzService = clazzService;
  }

  public void setScheduleSlotService(ScheduleSlotService scheduleSlotService) {
    this.scheduleSlotService = scheduleSlotService;
  }

  public void setRoomService(RoomService roomService) {
    this.roomService = roomService;
  }

  public void setCourseService(CourseService courseService) {
    this.courseService = courseService;
  }

  public void setRoleService(RoleService roleService) {
    this.roleService = roleService;
  }

  public void setTeacherService(TeacherService teacherService) {
    this.teacherService = teacherService;
  }

  @InitBinder
  public void setAllowedFields(ServletRequestDataBinder binder) {
    binder.registerCustomEditor(Schedule.class, new SchedulePropertyEditor(scheduleService));
    binder.registerCustomEditor(Room.class, new RoomPropertyEditor(roomService));
    binder.registerCustomEditor(Course.class, new CoursePropertyEditor(courseService));
    binder.registerCustomEditor(Semester.class, new SemesterPropertyEditor(semesterService));
    binder.registerCustomEditor(Teacher.class, new TeacherPropertyEditor(teacherService));
  }

  @RequestMapping("/")
  public String setupIndexPage(Model model, HttpServletRequest req) {

    User headmaster = (User) req.getAttribute("USER");
    Collection schools = null;

    if (headmaster.getTeacher() == null) {
      Collection roles = (Collection) req.getAttribute("ROLES");
      Role role = roleService.getByAuthority("DEV");
      if (!roles.contains(role)) return "redirect:/";
    } else {
      schools = headmaster.getTeacher().getSchools();
    }

    List semesters = semesterService.getAll(schools);
    model.addAttribute("semesters", semesters);

    return defaultView + "/index";
  }

  @RequestMapping("/details")
  public String setupMatrixPage(
      Model model,
      @RequestParam("semesterId") Semester semester,
      HttpServletResponse res) throws IOException {

    if (semester == null) res.sendError(400, "No semester found");
    Clazz clazz = null;

    scheduleService.createAllForSemesterClazz(semester, clazz);

    List<Clazz> clazzes;
    List schools = new ArrayList();
    schools.add(semester.getSchool());

    if (clazz == null) {
      clazzes = clazzService.getAll(schools);
    } else {
      clazzes = new ArrayList();
      clazzes.add(clazz);
    }

    List<ScheduleSlot> slots = scheduleSlotService.getAll(schools);
    List<Schedule> schedules = scheduleService.getBySemesterClazz(semester.getId(), null);
    List rooms = roomService.getAll(schools);
    List courses = courseService.getAll(schools);

    /**
     * holder[clazz][day]["slots"] -> list semua slot yang ada di hari x untuk kelas y
     * holder[clazz][day][slot] -> objek schedule utk slot z
     * - clazz
     * |- day
     * ||- "slots"
     * |||> LIST:[slots untuk hari day dan kelas clazz]
     * ||- slot
     * |||> OBJ:schedule dengan schslot slot
     */

    Map holder = new HashMap(); // global holder
    List days = new ArrayList();

    for (Clazz c : clazzes) { // untuk setiap kelas
      Map cHolder = new HashMap(); // buat holder kelas
      holder.put(c, cHolder); // masukin holder kelas ke global holder

      for (int day = 0; day < 7; day++) { // untuk setiap hari
        Map dHolder = new HashMap(); // buat holder hari
        cHolder.put(day, dHolder); // masukin holder hari ke holder kelas

        List slots2 = new ArrayList(); // buat list untuk slot pada hari day

        for (ScheduleSlot slot : slots) { // untuk setiap slot
          if (slot.getDay() == day) { // cari yang harinya sama dengan day
            slots2.add(slot); // tambah ke list

            for (Schedule s : schedules) { // untuk setiap sch
              if (s.getSlot().getId() == slot.getId()) { // cari sch yang pakai slot
                dHolder.put(slot, s); // masukin ke holder hari
                schedules.remove(s); // buang sch dari list schedule
                break; // keluar dari loop karena cuma cari 1
              }
            } // end loop sch
          }
        } // end loop slot

        dHolder.put("slots", slots2); // masukin list slots2 ke holder hari
      } // end loop hari
    } // end loop kelas

    model.addAttribute("semester", semester);
    model.addAttribute("clazzes", clazzes);
    model.addAttribute("days", new int[] {0, 1, 2, 3, 4, 5, 6});
    model.addAttribute("holder", holder);
    model.addAttribute("rooms", rooms);
    model.addAttribute("courses", courses);

    return defaultView + "/details";
  }

  @RequestMapping(value="/update", method=RequestMethod.POST)
  @ResponseBody
  public Map update(
      @RequestParam("id") Schedule schedule,
      @RequestParam(value="courseId", required=false) Course course,
      @RequestParam(value="roomId", required=false) Room room,
      @RequestParam(value="teacherId", required=false) Teacher teacher,
      @RequestParam boolean lock) {

    Map<String, Object> retval = new HashMap<>();
    if (schedule == null) {
      retval.put("status", false);
      retval.put("message", "Schedule not found");
      return retval;
    }

    scheduleService.update(schedule, course, room, teacher, lock);

    retval.put("status", true);
    retval.put("schedule", schedule);
    if (course != null) {
      retval.put("teacher", teacher);
    }

    return retval;
  }

  @RequestMapping(value="/clear", method=RequestMethod.POST)
  @ResponseBody
  public Map clear(@RequestParam("id") Schedule schedule) {
    Map<String, Object> retval = new HashMap<>();
    if (schedule == null) {
      retval.put("status", false);
      retval.put("message", "Schedule not found");
      return retval;
    }

    scheduleService.clear(schedule);
    retval.put("status", true);
    retval.put("schedule", schedule);

    return retval;
  }

  @RequestMapping(value="/copy", method=RequestMethod.POST)
  @ResponseBody
  public Map copy(
      @RequestParam("source") Schedule source,
      @RequestParam("target") Schedule target) {
    Map<String, Object> retval = new HashMap<>();
    if (source == null) {
      retval.put("status", false);
      retval.put("message", "Source not found");
      return retval;
    }
    if (target == null) {
      retval.put("status", false);
      retval.put("message", "Target not found");
      return retval;
    }

    scheduleService.copy(source, target);
    retval.put("status", true);
    retval.put("source", source);
    retval.put("target", target);

    return retval;
  }

  @RequestMapping(value="/move", method=RequestMethod.POST)
  @ResponseBody
  public Map move(
      @RequestParam("source") Schedule source,
      @RequestParam("target") Schedule target) {
    Map<String, Object> retval = new HashMap<>();
    if (source == null) {
      retval.put("status", false);
      retval.put("message", "Source not found");
      return retval;
    }
    if (target == null) {
      retval.put("status", false);
      retval.put("message", "Target not found");
      return retval;
    }

    scheduleService.move(source, target);
    retval.put("status", true);
    retval.put("source", source);
    retval.put("target", target);

    return retval;
  }

  @RequestMapping(value="/switch", method=RequestMethod.POST)
  @ResponseBody
  public Map switch_(
      @RequestParam("source") Schedule source,
      @RequestParam("target") Schedule target) {
    Map<String, Object> retval = new HashMap<>();
    if (source == null) {
      retval.put("status", false);
      retval.put("message", "Source not found");
      return retval;
    }
    if (target == null) {
      retval.put("status", false);
      retval.put("message", "Target not found");
      return retval;
    }

    scheduleService.move(source, target);
    retval.put("status", true);
    retval.put("source", source);
    retval.put("target", target);

    return retval;
  }

  @RequestMapping(value="/generate", method=RequestMethod.POST)
  @ResponseBody
  public Map generate(
      @RequestParam("semesterId") Semester semester,
      @RequestParam(defaultValue="-99") int day) {
    Map<String, Object> retval = new HashMap<>();
    if (semester == null) {
      retval.put("status", false);
    }
    try {
      List<Schedule> scs = scheduleService.generate(semester, day);
      retval.put("status", true);
    } catch (Exception ex) {
      retval.put("status", false);
      retval.put("message", ex.getMessage());
    }
    retval.put("semester", semester);

    return retval;
  }

  @RequestMapping("/validate")
  @ResponseBody
  public List validate(
      @RequestParam("semesterId") Semester semester,
      @RequestParam(required=false) Integer day,
      @RequestParam(defaultValue="true") boolean validateRoom) {
    return scheduleService.validate(semester, day, validateRoom);
  }
}
