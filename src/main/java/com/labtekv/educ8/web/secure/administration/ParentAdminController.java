/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.labtekv.educ8.web.secure.administration;

import com.labtekv.educ8.ext.hibernate.HibernateUtil;
import com.labtekv.educ8.model.Address;
import com.labtekv.educ8.model.Parent;
import com.labtekv.educ8.service.ParentService;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

/**
 *
 * @author Andika
 */

@Controller
@RequestMapping(value = "/secure/administration/parent", method = RequestMethod.GET)
//@SessionAttributes("parent")
@SessionAttributes({"parent", "schools"})
public class ParentAdminController {
  @Autowired @Qualifier("autoFetchParentService_all")
  private ParentService parentService;

  @Autowired
  private HibernateUtil hibernateUtil;

  private final String defaultView = "/secure/administration/parent";

  public void setHibernateUtil(HibernateUtil hibernateUtil) {
    this.hibernateUtil = hibernateUtil;
  }

  public void setParentService(ParentService parentService) {
    this.parentService = parentService;
  }

  @InitBinder
  public void setAllowedFields(WebDataBinder dataBinder) {
    dataBinder.setDisallowedFields("id");
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    dateFormat.setLenient(false);
    dataBinder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false));
  }

  @RequestMapping(value = "/")
  public String setupSearchPage() {
    return defaultView + "/index";
  }

  @RequestMapping(value = "/search", method = RequestMethod.POST)
  public String searchParent(
      Model model,
      @RequestParam(defaultValue = "") String name,
      @RequestParam(defaultValue = "1") int page) {

    List parents = parentService.getByName(name, page);
    model.addAttribute("parents", parents);

    return defaultView + "/list";
  }

  @RequestMapping("/new")
  public String setupNewForm(Model model) {
    Parent parent = new Parent();
    parent.setAddress(new Address());

    model.addAttribute("parent", parent);
    return defaultView + "/form";
  }

  @RequestMapping("/{id}")
  public String setupDetailPage(Model model, @PathVariable("id") long id) {
    model.addAttribute("parent", parentService.getById(id));
    model.addAttribute("readOnly", true);
    return defaultView + "/form";
  }

  @RequestMapping("/edit-{id}")
  public String setupEditForm(Model model, @PathVariable("id") long id) {
    Parent parent = parentService.getById(id);
    Address address = parent.getAddress();
    address = (Address) hibernateUtil.initializeImplementation(address);
    parent.setAddress(address);

    model.addAttribute("parent", parent);
    return defaultView + "/form";
  }

  @RequestMapping(value = "/submit", method = RequestMethod.POST)
  public String updateParent(
      @ModelAttribute("parent") Parent parent,
      BindingResult result, SessionStatus status) {

    if (result.hasErrors()) {
      return defaultView + "/form";
    } else {
      if (parent.isNew()) {
        parentService.create(parent);
      } else
        parentService.update(parent);

      status.setComplete();
      return "redirect:" + defaultView + "/";
    }
  }
}
