package com.labtekv.educ8.web.secure.administration;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import com.labtekv.educ8.beans.propertyeditors.SchoolPropertyEditor;
import com.labtekv.educ8.ext.hibernate.HibernateUtil;
import com.labtekv.educ8.model.Address;
import com.labtekv.educ8.model.School;
import com.labtekv.educ8.model.Teacher;
import com.labtekv.educ8.service.SchoolService;
import com.labtekv.educ8.service.TeacherService;
import com.labtekv.educ8.util.SchoolLoader;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;

/**
 *
 * @author Andika
 */
@Controller
@RequestMapping(value = "/secure/administration/teacher")
//@SessionAttributes("teacher")
@SessionAttributes({"teacher", "schools"})
public class TeacherAdminController {

  @Autowired @Qualifier("autoFetchTeacherService_all")
  private TeacherService teacherService;

  @Autowired
  @Qualifier("simpleSchoolService")
  private SchoolService schoolService;

  @Autowired
  private SchoolLoader schoolLoader;

  @Autowired
  private HibernateUtil hibernateUtil;

  private final String defaultView = "/secure/administration/teacher";

  public void setHibernateUtil(HibernateUtil hibernateUtil) {
    this.hibernateUtil = hibernateUtil;
  }

  public void setSchoolLoader(SchoolLoader schoolLoader) {
    this.schoolLoader = schoolLoader;
  }

  public void setTeacherService(TeacherService teacherService) {
    this.teacherService = teacherService;
  }

  public void setSchoolService(SchoolService schoolService) {
    this.schoolService = schoolService;
  }

  @InitBinder
  public void setAllowedFields(WebDataBinder dataBinder) {
    dataBinder.setDisallowedFields("id");
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    dateFormat.setLenient(false);
    dataBinder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false));
    dataBinder.registerCustomEditor(School.class, new SchoolPropertyEditor(schoolService));
  }

  @RequestMapping(value = "/")
  public String setupSearchPage() {
    return defaultView + "/index";
  }

  @RequestMapping(value = "/search", method = RequestMethod.POST)
  public String searchTeacher(
      Model model,
      @RequestParam(defaultValue = "") String name,
      @RequestParam(defaultValue = "1") int page,
      HttpServletResponse res,
      HttpServletRequest req) throws IOException {

    Collection schools = schoolLoader.loadAdministrationSchools(req, res);
    List teachers = teacherService.getByName(name, schools, page);
    model.addAttribute("teachers", teachers);

    return defaultView + "/list";
  }

  @RequestMapping("/new")
  public String setupNewForm(Model model,
      HttpServletRequest req, HttpServletResponse res) throws IOException {
    Teacher teacher = new Teacher();
    teacher.setAddress(new Address());

    model.addAttribute("schools", schoolLoader.loadAdministrationSchools(req, res));
    model.addAttribute("teacher", teacher);
    return defaultView + "/form";
  }

  @RequestMapping("/{id}")
  public String setupDetailPage(Model model, @PathVariable("id") long id,
      HttpServletRequest req,
      HttpServletResponse res) throws IOException {
    //prepareDetailPage(model, id);
    model.addAttribute("schools", schoolLoader.loadAdministrationSchools(req, res));
    model.addAttribute("teacher", teacherService.getById(id));
    model.addAttribute("readOnly", true);
    return defaultView + "/form";
  }

  @RequestMapping("/edit-{id}")
  public String setupEditForm(Model model, @PathVariable("id") long id,
      HttpServletRequest req,
      HttpServletResponse res) throws IOException {
    //prepareDetailPage(model, id);
    Teacher teacher = teacherService.getById(id);
    Address address = teacher.getAddress();
    //School school = teacher.getSchool();
    address = (Address) hibernateUtil.initializeImplementation(address);
    //school = (School) hibernateUtil.initializeImplementation(school);
    teacher.setAddress(address);
    //teacher.setSchool(school);

    model.addAttribute("schools", schoolLoader.loadAdministrationSchools(req, res));
    model.addAttribute("teacher", teacher);
    return defaultView + "/form";
  }

  @RequestMapping(value = "/submit", method = RequestMethod.POST)
  public String updateTeacher(
      @Valid @ModelAttribute("teacher") Teacher teacher,
      BindingResult result, SessionStatus status) {

    if (result.hasErrors()) {
      //model.addAttribute("schools", schoolLoader.loadAdministrationSchools(req, res));
      return defaultView + "/form";
    } else {
      if (teacher.isNew()) {
        teacherService.create(teacher);
      } else
        teacherService.update(teacher);

      status.setComplete();
      return "redirect:" + defaultView + "/";
    }
  }

  @RequestMapping(value = "/delete-{id}", method = RequestMethod.DELETE)
  @ResponseBody
  public Map deleteTeacher(@PathVariable("id") long id) {
    Map map = new HashMap();
    try {
      teacherService.deleteById(id);
      map.put("status", true);
    } catch (Exception e) {
      map.put("status", false);
      map.put("message", e.getMessage());
    }
    return map;
  }

//  private void prepareDetailPage(Model model, long id) {
//    Teacher t = teacherService.getById(id);
//    model.addAttribute("teacher", t);
//    model.addAttribute("schools", schoolService.getAll());
//  }
}
