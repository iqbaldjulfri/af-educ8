package com.labtekv.educ8.web.secure.administration;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import com.labtekv.educ8.beans.propertyeditors.AddressPropertyEditor;
import com.labtekv.educ8.beans.propertyeditors.ParentPropertyEditor;
import com.labtekv.educ8.beans.propertyeditors.SchoolPropertyEditor;
import com.labtekv.educ8.beans.propertyeditors.SpecializationPropertyEditor;
import com.labtekv.educ8.ext.hibernate.HibernateUtil;
import com.labtekv.educ8.model.*;
import com.labtekv.educ8.service.*;
import com.labtekv.educ8.util.SchoolLoader;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;

/**
 *
 * @author Andika
 */
@Controller
@RequestMapping("/secure/administration/student")
@SessionAttributes({"student", "schools", "specializations"})
public class StudentAdminController {
  private static final String defaultView = "/secure/administration/student";

  @Autowired @Qualifier("autoFetchStudentService_all")
  private StudentService studentService;

  @Autowired
  private SchoolLoader schoolLoader;

  @Autowired @Qualifier("simpleSchoolService")
  private SchoolService schoolService;

  @Autowired @Qualifier("autoFetchParentService_address")
  private ParentService parentService;

  @Autowired @Qualifier("simpleAddressService")
  private AddressService addressService;

  @Autowired @Qualifier("simpleSpecializationService")
  private SpecializationService specializationService;

  @Autowired
  private HibernateUtil hibernateUtil;

  public void setHibernateUtil(HibernateUtil hibernateUtil) {
    this.hibernateUtil = hibernateUtil;
  }

  public void setParentService(ParentService parentService) {
    this.parentService = parentService;
  }

  public void setSchoolLoader(SchoolLoader schoolLoader) {
    this.schoolLoader = schoolLoader;
  }

  public void setSchoolService(SchoolService schoolService) {
    this.schoolService = schoolService;
  }

  public void setStudentService(StudentService studentService) {
    this.studentService = studentService;
  }

  public void setSpecializationService(SpecializationService specializationService) {
    this.specializationService = specializationService;
  }

  public void setAddressService(AddressService addressService) {
    this.addressService = addressService;
  }

  @InitBinder
  public void initBinder(WebDataBinder dataBinder) {
    dataBinder.setDisallowedFields("id");
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    dateFormat.setLenient(false);
    dataBinder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false));
    dataBinder.registerCustomEditor(School.class, new SchoolPropertyEditor(schoolService));
    dataBinder.registerCustomEditor(Parent.class, new ParentPropertyEditor(parentService));
    dataBinder.registerCustomEditor(Specialization.class, new SpecializationPropertyEditor(specializationService));
    dataBinder.registerCustomEditor(Address.class, new AddressPropertyEditor(addressService));
  }

  @RequestMapping("/")
  public String setupSearchPage() {
    return defaultView + "/index";
  }

  @RequestMapping(value = "/search", method = RequestMethod.POST)
  public String searchStudent(Model model,
      @RequestParam(defaultValue = "") String name,
      @RequestParam(defaultValue = "") String idNumber,
      @RequestParam(defaultValue = "1") int page,
      HttpServletResponse res,
      HttpServletRequest req) throws IOException {

    Collection schools = schoolLoader.loadAdministrationSchools(req, res);
    model.addAttribute("students", studentService.getByNameNumber(name, idNumber, schools, page));

    return defaultView + "/list";
  }

  @RequestMapping("/new")
  public String setupNewForm(Model model,
      HttpServletRequest req, HttpServletResponse res) throws IOException {
    Collection<School> schools = schoolLoader.loadAdministrationSchools(req, res);
    Student student = new Student();
    student.setAddress(new Address());

    model.addAttribute("student", student);
    model.addAttribute("specializations", specializationService.getAll(schools));
    model.addAttribute("schools", schools);
    return defaultView + "/form";
  }

  @RequestMapping("/{id}")
  public String setupDetailPage(Model model,
      @PathVariable("id") long id,
      HttpServletRequest req,
      HttpServletResponse res) throws IOException {
    Collection<School> schools = schoolLoader.loadAdministrationSchools(req, res);

    model.addAttribute("student", studentService.getById(id));
    model.addAttribute("specializations", specializationService.getAll(schools));
    model.addAttribute("schools", schools);
    model.addAttribute("readOnly", true);
    return defaultView + "/form";
  }

  @RequestMapping("/edit-{id}")
  public String setupEditForm(Model model,
      @PathVariable("id") long id,
      HttpServletRequest req,
      HttpServletResponse res) throws IOException {
    Collection<School> schools = schoolLoader.loadAdministrationSchools(req, res);

    model.addAttribute("student", studentService.getById(id));
    model.addAttribute("specializations", specializationService.getAll(schools));
    model.addAttribute("schools", schools);
    return defaultView + "/form";
  }

  @RequestMapping(value = "/submit", method = RequestMethod.POST)
  public String update(
      @Valid @ModelAttribute("student") Student student,
      BindingResult result,
      SessionStatus status) {

    if (result.hasErrors()) {
      return defaultView + "/form";
    } else {
      if (student.isNew())
        studentService.create(student);
      else
        studentService.update(student);

      status.setComplete();
      return "redirect:" + defaultView + "/";
    }
  }

  @RequestMapping(value = "/delete-{id}", method = RequestMethod.DELETE)
  @ResponseBody
  public Map delete(@PathVariable("id") long id) {
    Map map = new HashMap();
    try {
      studentService.deleteById(id);
      map.put("status", true);
    } catch (Exception e) {
      map.put("status", false);
      map.put("message", e.getMessage());
    }
    return map;
  }

  @RequestMapping("/popup/parent/")
  public String setupParentPopup(Model model,
      @RequestParam(required = false) Long id,
      @RequestParam int type,
      @RequestParam String elmId) {
    Parent p;
    if (id == null) p = new Parent();
    else p = parentService.getById(id);
    if (p == null) p = new Parent();
    if (p.isNew()) p.setAddress(new Address());
    p.setParentType(type);

    model.addAttribute("parent", p);
    model.addAttribute("elmId", elmId);

    return defaultView + "/popup/parent-form";
  }

  @RequestMapping(value = "/popup/parent/save", method = RequestMethod.POST)
  @ResponseBody
  public Parent saveParent(@RequestBody Parent p) throws IOException {
    if (p.isNew()) {
      parentService.create(p);
    } else {
      parentService.update(p);
    }

    return p;
  }


}
