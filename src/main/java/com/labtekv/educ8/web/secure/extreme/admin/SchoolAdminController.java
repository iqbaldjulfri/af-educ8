package com.labtekv.educ8.web.secure.extreme.admin;

import java.util.List;
import javax.validation.Valid;

import com.labtekv.educ8.beans.propertyeditors.AddressPropertyEditor;
import com.labtekv.educ8.model.Address;
import com.labtekv.educ8.model.School;
import com.labtekv.educ8.service.AddressService;
import com.labtekv.educ8.service.SchoolService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;

/**
 *
 * @author Iqbal Djulfri
 */
@Controller
@RequestMapping(value = "/secure/extreme/admin/school", method = RequestMethod.GET)
@SessionAttributes("school")
public class SchoolAdminController {

  private final String defaultView = "/secure/extreme/admin/school";

  @Autowired @Qualifier("simpleSchoolService")
  private SchoolService schoolService;

  @Autowired @Qualifier("simpleAddressService")
  private AddressService addressService;

  public void setAddressService(AddressService addressService) {
    this.addressService = addressService;
  }

  public void setSchoolService(SchoolService schoolService) {
    this.schoolService = schoolService;
  }

  @InitBinder
  public void setAllowedFields(WebDataBinder dataBinder) {
    dataBinder.setDisallowedFields("id");
    dataBinder.registerCustomEditor(Address.class, new AddressPropertyEditor(addressService));
  }

  @RequestMapping(value = "/")
  public String setupSearchPage() {
    return defaultView + "/index";
  }

  @RequestMapping(value = "/search", method = RequestMethod.POST)
  public String searchSchool(
      Model model,
      @RequestParam(defaultValue = "") String name,
      @RequestParam(defaultValue = "1") int page) {

    List schools = schoolService.getByName(name, page);
    model.addAttribute("schools", schools);

    return defaultView + "/list";
  }

  @RequestMapping("/new")
  public String setupNewForm(Model model) {
    School school = new School();
    school.setAddress(new Address());

    model.addAttribute("school", school);
    return defaultView + "/form";
  }

  @RequestMapping("/{id}")
  public String setupDetailPage(Model model, @PathVariable("id") long id) {
    model.addAttribute("school", schoolService.getById(id));
    model.addAttribute("readOnly", true);
    return defaultView + "/form";
  }

  @RequestMapping("/edit-{id}")
  public String setupEditForm(Model model, @PathVariable("id") long id) {
    model.addAttribute("school", schoolService.getById(id));
    return defaultView + "/form";
  }

  @RequestMapping(value = "/submit", method = RequestMethod.POST)
  public String updateSchool(
      @Valid @ModelAttribute("school") School school,
      BindingResult result, SessionStatus status) {

    if (result.hasErrors()) {
      return defaultView + "/form";
    } else {
      if (school.isNew()) {
        schoolService.create(school);
      } else
        schoolService.update(school);

      status.setComplete();
      return "redirect:" + defaultView + "/";
    }
  }

}
