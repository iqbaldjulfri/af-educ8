package com.labtekv.educ8.web.secure.user;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import com.labtekv.educ8.model.Message;
import com.labtekv.educ8.model.User;
import com.labtekv.educ8.service.MessageService;
import com.labtekv.educ8.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

/**
 *
 * @author Iqbal Djulfri
 */
@Controller
@RequestMapping(value="/secure/user/message", method=RequestMethod.GET)
@SessionAttributes("message")
public class MessageController {
  private final String defaultView = "/secure/user/message";

  @Autowired
  @Qualifier("autoFetchMessageService_all")
  private MessageService messageService;

  @Autowired
  @Qualifier("simpleUserService")
  private UserService userService;

  public void setMessageService(MessageService messageService) {
    this.messageService = messageService;
  }

  public void setUserService(UserService userService) {
    this.userService = userService;
  }

  @InitBinder
  public void setAllowedFields(WebDataBinder binder) {
    binder.setDisallowedFields("id", "time");
  }

  @RequestMapping("/")
  public String setupIndexPage(
      @RequestParam(required=false) String err) {
    return defaultView + "/index";
  }

  @RequestMapping(value = "/search", method=RequestMethod.POST)
  public String searchMessage(
      Model model,
      @RequestParam(defaultValue="") String query,
      @RequestParam(defaultValue="1") int page,
      @RequestParam(required=false) Boolean deleted,
      HttpServletRequest req) {

    User user = (User) req.getAttribute("USER");
    List messages = messageService.getByUserId(user.getId(), query, deleted, page);
    model.addAttribute("messages", messages);

    return defaultView + "/list";
  }

  @RequestMapping("/new")
  public String setupNewForm(
      Model model,
      HttpServletRequest req) {

    Message message = new Message();
    message.setIsDeleted(false);
    message.setIsRead(false);
    message.setSender((User) req.getAttribute("USER"));
    model.addAttribute("message", message);

    return defaultView + "/form";
  }

  @RequestMapping(value = "/submit", method = RequestMethod.POST)
  public String sendMessage(
      @Valid @ModelAttribute("message") Message message,
      @RequestParam("userIds") String usersIds,
      BindingResult result, SessionStatus status) {

    List<User> users = new ArrayList<>();

    if (usersIds.isEmpty()) {
      ObjectError err = new FieldError("message", "user", "You have to choose recipient(s)");
      result.addError(err);
    } else {
      String[] ids = usersIds.split(",");
      for (String id : ids) {
        try {
          User u = userService.getById(Long.valueOf(id));
          users.add(u);
        } catch (NumberFormatException e) {
          ObjectError err = new FieldError("message", "user", "No such user");
          result.addError(err);
        }
      }
    }

    if (result.hasErrors()) {
      return defaultView + "/form";
    } else {
      if (message.isNew()) {
        message.setTime(new Date());
        for (User u : users) {
          Message m = message.clone();
          m.setUser(u);
          messageService.create(m);
        }
      } else {
        messageService.update(message);
      }

      status.setComplete();
      return "redirect:" + defaultView + "/";
    }
  }

  @RequestMapping("/{id}")
  public String setupDetailPage(
      Model model,
      @PathVariable("id") long id,
      HttpServletRequest req,
      HttpServletResponse res) throws IOException {

    User user = (User) req.getAttribute("USER");
    Message message = messageService.getById(id);
    if (message == null) {
      res.sendRedirect("../message/index?err=No such message");
    }
    if (user.getId() != message.getUser().getId()) {
      res.sendRedirect("../message/index?err=You are not the owner of this message");
    }

    message.setIsRead(true);
    messageService.update(message);

    model.addAttribute("message", message);
    model.addAttribute("readOnly", true);
    return defaultView + "/form";
  }

  @RequestMapping(value="/delete-{id}", method=RequestMethod.DELETE)
  @ResponseBody
  public Map deleteMessage(
      Model model,
      @PathVariable("id") long id,
      HttpServletRequest req,
      HttpServletResponse res) throws IOException {

    Map map = new HashMap();
    try {
      User user = (User) req.getAttribute("USER");
      Message message = messageService.getById(id);

      if (message == null)
        throw new Exception("No such message");
      if (user.getId() != message.getUser().getId())
        throw new Exception("You are not the owner of this message");

      messageService.delete(message);
      map.put("status", true);
    } catch (Exception e) {
      map.put("status", false);
      map.put("message", e.getMessage());
    }
    return map;
  }
}
