package com.labtekv.educ8.web.secure.administration;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import com.labtekv.educ8.beans.propertyeditors.SchoolPropertyEditor;
import com.labtekv.educ8.model.School;
import com.labtekv.educ8.model.Semester;
import com.labtekv.educ8.service.SchoolService;
import com.labtekv.educ8.service.SemesterService;
import com.labtekv.educ8.util.SchoolLoader;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;

/**
 *
 * @author Iqbal Djulfri
 */
@Controller
@RequestMapping("/secure/administration/semester")
@SessionAttributes({"semester", "schools"})
public class SemesterAdminController {
  private static final String defaultView = "/secure/administration/semester";

  @Autowired @Qualifier("autoFetchSemesterService_school")
  private SemesterService semesterService;

  @Autowired
  private SchoolLoader schoolLoader;

  @Autowired @Qualifier("simpleSchoolService")
  private SchoolService schoolService;

  public void setSchoolService(SchoolService schoolService) {
    this.schoolService = schoolService;
  }

  public void setSemesterService(SemesterService semesterService) {
    this.semesterService = semesterService;
  }

  public void setSchoolLoader(SchoolLoader schoolLoader) {
    this.schoolLoader = schoolLoader;
  }

  @InitBinder
  public void initBinder(WebDataBinder dataBinder) {
    dataBinder.setDisallowedFields("id");

    SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
    df.setLenient(true);
    dataBinder.registerCustomEditor(Date.class, new CustomDateEditor(df, false));

    dataBinder.registerCustomEditor(School.class, new SchoolPropertyEditor(schoolService));
  }

  @RequestMapping("/")
  public String setupSearchPage() {
    return defaultView + "/index";
  }

  @RequestMapping(value = "/search", method = RequestMethod.POST)
  public String searchUser(Model model,
      @RequestParam(defaultValue = "") String name,
      @RequestParam(defaultValue = "1") int page,
      HttpServletResponse res,
      HttpServletRequest req) throws IOException {
    List semesters;

    Collection schools = schoolLoader.loadAdministrationSchools(req, res);
    semesters = semesterService.getByName(name, schools, page);

    model.addAttribute("semesters", semesters);

    return defaultView + "/list";
  }

  @RequestMapping("/new")
  public String setupNewForm(Model model,
      HttpServletRequest req, HttpServletResponse res) throws IOException {
    model.addAttribute("semester", new Semester());
    model.addAttribute("schools", schoolLoader.loadAdministrationSchools(req, res));
    return defaultView + "/form";
  }

  @RequestMapping("/{id}")
  public String setupDetailPage(Model model,
      @PathVariable("id") long id,
      HttpServletRequest req,
      HttpServletResponse res) throws IOException {
    model.addAttribute("semester", semesterService.getById(id));
    model.addAttribute("schools", schoolLoader.loadAdministrationSchools(req, res));
    model.addAttribute("readOnly", true);
    return defaultView + "/form";
  }

  @RequestMapping("/edit-{id}")
  public String setupEditForm(Model model,
      @PathVariable("id") long id,
      HttpServletRequest req,
      HttpServletResponse res) throws IOException {
    model.addAttribute("semester", semesterService.getById(id));
    model.addAttribute("schools", schoolLoader.loadAdministrationSchools(req, res));
    return defaultView + "/form";
  }

  @RequestMapping(value = "/submit", method = RequestMethod.POST)
  public String update(
      @Valid @ModelAttribute("semester") Semester semester,
      BindingResult result,
      SessionStatus status) {

    if (result.hasErrors()) {
      return defaultView + "/form";
    } else {
      if (semester.isNew())
        semesterService.create(semester);
      else
        semesterService.update(semester);

      status.setComplete();
      return "redirect:" + defaultView + "/";
    }
  }

  @RequestMapping(value = "/delete-{id}", method = RequestMethod.DELETE)
  @ResponseBody
  public Map delete(@PathVariable("id") long id) {
    Map map = new HashMap();
    try {
      semesterService.deleteById(id);
      map.put("status", true);
    } catch (Exception e) {
      map.put("status", false);
      map.put("message", e.getMessage());
    }
    return map;
  }
}
