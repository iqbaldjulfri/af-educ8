package com.labtekv.educ8.web.secure.extreme.admin;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;

import com.labtekv.educ8.beans.propertyeditors.RolePropertyEditor;
import com.labtekv.educ8.model.User;
import com.labtekv.educ8.service.RoleService;
import com.labtekv.educ8.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;

/**
 *
 * @author Iqbal Djulfri
 */
@Controller
@RequestMapping(value = "/secure/extreme/admin/user", method = RequestMethod.GET)
@SessionAttributes("user")
public class UserAdminController {

  @Autowired @Qualifier("autoFetchUserService_authorities")
  private UserService userService;

  @Autowired
  private RoleService roleService;

  private final String defaultView = "/secure/extreme/admin/user";

  public void setRoleService(RoleService roleService) {
    this.roleService = roleService;
  }

  public void setUserService(UserService userService) {
    this.userService = userService;
  }

  @InitBinder
  public void initBinder(WebDataBinder dataBinder) {
    dataBinder.setDisallowedFields("id");
    dataBinder.registerCustomEditor(GrantedAuthority.class, new RolePropertyEditor(roleService));
  }

  @RequestMapping("/")
  public String setupSearchPage() {
    return defaultView + "/index";
  }

  @RequestMapping(value = "/search", method = RequestMethod.POST)
  public String searchUser(Model model,
      @RequestParam(defaultValue = "") String username,
      @RequestParam(defaultValue = "") String name,
      @RequestParam(defaultValue = "1") int page) {

    List users = userService.getByNameOrUsername(name, username, page);
    model.addAttribute("users", users);

    return defaultView + "/list";
  }

  @RequestMapping("/new")
  public String setupNewForm(Model model) {
    model.addAttribute("user", new User());
    loadRoles(model);
    return defaultView + "/form";
  }

  @RequestMapping("/{id}")
  public String setupDetailPage(Model model, @PathVariable("id") long id) {
    model.addAttribute("user", userService.getById(id));
    model.addAttribute("readOnly", true);
    loadRoles(model);
    return defaultView + "/form";
  }

  @RequestMapping("/edit-{id}")
  public String setupEditForm(Model model, @PathVariable("id") long id) {
    model.addAttribute("user", userService.getById(id));
    loadRoles(model);
    return defaultView + "/form";
  }

  @RequestMapping(value = "/submit", method = RequestMethod.POST)
  public String updateUser(
      @Valid @ModelAttribute("user") User user,
      @RequestParam(defaultValue = "") String newPass,
      BindingResult result,
      SessionStatus status,
      Model model) {

    if (user.isNew() && newPass.isEmpty())
      result.addError(new ObjectError("password", new String[] {"NotBlank"}, new Object[] {},  "Must be filled"));

    if (!user.isNew() && !newPass.isEmpty() && newPass.length() < 6)
      result.addError(new ObjectError("password", new String[] {"Min"}, new Object[] {6}, "Min. 6 character"));

    if (result.hasErrors()) {
      loadRoles(model);
      return defaultView + "/form";
    } else {
      if (user.isNew()) {
        user.setPassword(newPass);
        userService.create(user);
      } else
        userService.update(user, newPass);

      status.setComplete();
      return "redirect:" + defaultView + "/";
    }
  }

  @RequestMapping(value = "/delete-{id}", method = RequestMethod.DELETE)
  @ResponseBody
  public Map deleteUser(@PathVariable("id") long id) {
    Map map = new HashMap();
    try {
      userService.deleteById(id);
      map.put("status", true);
    } catch (Exception e) {
      map.put("status", false);
      map.put("message", e.getMessage());
    }
    return map;
  }

  private void loadRoles(Model model) {
    model.addAttribute("roles", roleService.getAll());
  }
}
