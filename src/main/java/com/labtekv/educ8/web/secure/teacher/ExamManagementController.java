package com.labtekv.educ8.web.secure.teacher;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.apache.commons.io.IOUtils;

import com.labtekv.educ8.EntityNotSavedException;
import com.labtekv.educ8.beans.propertyeditors.*;
import com.labtekv.educ8.model.*;
import com.labtekv.educ8.service.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author Iqbal Djulfri
 */
@Controller
@RequestMapping("/secure/teacher/course/{course}/exam")
@SessionAttributes({"exam", "types", "clazzes", "files"})
public class ExamManagementController {

  private final String defaultView = "/secure/teacher/course/exam";
  @Autowired
  @Qualifier("simpleCourseService")
  private CourseService courseService;

  @Autowired
  @Qualifier("simpleTeacherService")
  private TeacherService teacherService;

  @Autowired
  @Qualifier("simpleSemesterService")
  private ActiveSemesterService activeSemesterService;

  @Autowired
  @Qualifier("simpleFromScheduleService")
  private ClazzFromScheduleService clazzFromScheduleService;

  @Autowired
  @Qualifier("simpleExamTypeService")
  private ExamTypeService examTypeService;

  @Autowired
  @Qualifier("simpleClazzService")
  private ClazzService clazzService;

  @Autowired
  @Qualifier("autoFetchExamService_type_clazzes_files")
  private ExamService examService;

  @Autowired
  @Qualifier("simpleExamFileService")
  private ExamFileService examFileService;

  public void setCourseService(CourseService courseService) {
    this.courseService = courseService;
  }

  public void setTeacherService(TeacherService teacherService) {
    this.teacherService = teacherService;
  }

  public void setActiveSemesterService(ActiveSemesterService activeSemesterService) {
    this.activeSemesterService = activeSemesterService;
  }

  public void setClazzFromScheduleService(ClazzFromScheduleService clazzFromScheduleService) {
    this.clazzFromScheduleService = clazzFromScheduleService;
  }

  public void setExamTypeService(ExamTypeService examTypeService) {
    this.examTypeService = examTypeService;
  }

  public void setClazzService(ClazzService clazzService) {
    this.clazzService = clazzService;
  }

  public void setExamService(ExamService examService) {
    this.examService = examService;
  }

  public void setExamFileService(ExamFileService examFileService) {
    this.examFileService = examFileService;
  }

  @InitBinder
  public void setAllowedFields(ServletRequestDataBinder binder) {
    binder.setDisallowedFields(new String[] {"id", "course", "semester", "remedialFor"});
    binder.registerCustomEditor(Course.class, new CoursePropertyEditor(courseService));
    binder.registerCustomEditor(Teacher.class, new TeacherPropertyEditor(teacherService));
    binder.registerCustomEditor(Clazz.class, new ClazzPropertyEditor(clazzService));
    binder.registerCustomEditor(ExamType.class, new ExamTypePropertyEditor(examTypeService));
    binder.registerCustomEditor(ExamFile.class, new ExamFilePropertyEditor(examFileService));
    binder.registerCustomEditor(Exam.class, new ExamPropertyEditor(examService));
    binder.registerCustomEditor(Date.class, new CustomDateEditor(new SimpleDateFormat("dd-MM-yyyy hh:mm"), true));
  }

  @RequestMapping("/new")
  public String setupNewForm(
      @PathVariable Course course,
      HttpServletRequest req,
      Model model) {

    User u = (User) req.getAttribute("USER");
    Teacher teacher = u.getTeacher();
    if (teacher == null) {
      model.addAttribute("err", "af.err.user.not-a-teacher");
    } else {
      Semester semester = activeSemesterService.get();
      Exam exam = new Exam();
      exam.setCourse(course);
      exam.setSemester(semester);
      exam.setFiles(new ArrayList<ExamFile>());

      List clazzes = clazzFromScheduleService.get(teacher, semester, course);
      List types = examTypeService.getAll();

      model.addAttribute("course", course);
      model.addAttribute("exam", exam);
      model.addAttribute("types", types);
      model.addAttribute("clazzes", clazzes);
      model.addAttribute("files", exam.getFiles());
    }

    return defaultView + "/form";
  }

  @RequestMapping("/edit-{exam}")
  public String setupEditForm(
      @PathVariable Exam exam,
      @PathVariable Course course,
      HttpServletRequest req,
      Model model) {

    User u = (User) req.getAttribute("USER");
    Teacher teacher = u.getTeacher();
    if (exam == null) {
      model.addAttribute("err", "af.err.exam.no-such-exam");
    }
    if (teacher == null) {
      model.addAttribute("err", "af.err.user.not-a-teacher");
    } else {
      if (false) {
        // TODO cek apakah teachernya valid
        model.addAttribute("err", "af.err.exam.not-a-valid-teacher");
      } else {
        Semester semester = activeSemesterService.get();

        List clazzes = clazzFromScheduleService.get(teacher, semester, course);
        List types = examTypeService.getAll();
        List files = examFileService.getAll(exam);

        model.addAttribute("course", course);
        model.addAttribute("exam", exam);
        model.addAttribute("types", types);
        model.addAttribute("clazzes", clazzes);
        model.addAttribute("files", files);
      }
    }

    return defaultView + "/form";
  }

  @RequestMapping("/{exam}")
  public String setupViewOnlyForm(
      @PathVariable Exam exam,
      @PathVariable Course course,
      HttpServletRequest req,
      Model model) {
    User u = (User) req.getAttribute("USER");
    Teacher teacher = u.getTeacher();
    if (exam == null) {
      model.addAttribute("err", "af.err.exam.no-such-exam");
    }
    if (teacher == null) {
      model.addAttribute("err", "af.err.user.not-a-teacher");
    } else {
      if (false) {
        // TODO cek apakah teachernya valid
        model.addAttribute("err", "af.err.exam.not-a-valid-teacher");
      } else {
        Semester semester = activeSemesterService.get();

        List clazzes = clazzFromScheduleService.get(teacher, semester, course);
        List types = examTypeService.getAll();
        List files = examFileService.getAll(exam);

        model.addAttribute("course", course);
        model.addAttribute("exam", exam);
        model.addAttribute("types", types);
        model.addAttribute("clazzes", clazzes);
        model.addAttribute("files", files);
        model.addAttribute("readOnly", true);
      }
    }

    return defaultView + "/form";
  }

  @RequestMapping(value = "/submit", method = RequestMethod.POST)
  public String save(
      @Valid @ModelAttribute Exam exam,
      @PathVariable Course course,
      BindingResult result,
      SessionStatus status) throws EntityNotSavedException, IOException {
    // TODO cek apakah teachernya valid
    if (result.hasErrors()) {
      return defaultView + "/form";
    } else {
      if (exam.isNew()) {
        List<ExamFile> files = exam.getFiles();
        exam.setFiles(null);
        examService.create(exam);
        for (ExamFile file : files) {
          file.setExam(exam);
          if (file.isNew()) {
            examFileService.create(file);
            file.saveFile();
          }
        }
      } else {
        List<ExamFile> files = exam.getFiles();
        for (ExamFile file : files) {
          file.setExam(exam);
          if (file.isNew()) {
            examFileService.create(file);
            file.saveFile();
          }
        }
        examService.update(exam);
      }

      final long courseId = course.getId();
      status.setComplete();
      return "redirect:../../" + courseId;
    }
  }

  @RequestMapping(value = "/file", method = RequestMethod.POST)
  public @ResponseBody
  List<ExamFile> upload(
      @ModelAttribute Exam exam,
      @RequestParam("file") MultipartFile file,
      HttpServletRequest req) throws EntityNotSavedException, IOException {

    // TODO cek apakah teachernya valid
    ExamFile examFile = new ExamFile();
    examFile.setExam(exam);
    examFile.setName(file.getOriginalFilename());
    examFile.setSize(file.getSize());

    File temp = new File(ExamFile.BASE_LOCATION + "temp" + File.separatorChar + file.getSize() + "_" + file.getOriginalFilename());
    if (!temp.exists()) {
      temp.mkdirs();
      temp.createNewFile();
    }
    file.transferTo(temp);
    examFile.setFile(temp);

    exam.getFiles().add(examFile);
    return exam.getFiles();
  }

  @RequestMapping(value = "/delete-file", method = RequestMethod.POST)
  public @ResponseBody
  ExamFile deleteFile(
      @ModelAttribute Exam exam,
      @RequestParam int index) {

    // TODO cek apakah teachernya valid
    ExamFile file = exam.getFiles().remove(index);
    if (!file.isNew()) {
      examFileService.delete(file);
    }
    file.getFile().delete();

    return file;
  }

  @RequestMapping(value = "/file/{file}/{name}", method=RequestMethod.GET)
  public void getExamFile(
      @PathVariable ExamFile file,
      HttpServletResponse res) throws EntityNotSavedException, FileNotFoundException, IOException {
    file.loadFile();
    InputStream is = new FileInputStream(file.getFile());
    IOUtils.copy(is, res.getOutputStream());
    res.flushBuffer();
  }
}
