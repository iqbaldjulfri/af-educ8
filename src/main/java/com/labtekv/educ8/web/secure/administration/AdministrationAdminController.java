
package com.labtekv.educ8.web.secure.administration;

import com.labtekv.educ8.beans.propertyeditors.SchoolPropertyEditor;
import com.labtekv.educ8.ext.hibernate.HibernateUtil;
import com.labtekv.educ8.model.Address;
import com.labtekv.educ8.model.Administration;
import com.labtekv.educ8.model.School;
import com.labtekv.educ8.service.AdministrationService;
import com.labtekv.educ8.service.RoleService;
import com.labtekv.educ8.service.SchoolService;
import com.labtekv.educ8.util.SchoolLoader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

/**
 *
 * @author Andika
 */

@Controller
@RequestMapping(value = "/secure/administration/administration", method = RequestMethod.GET)
//@SessionAttributes("administration")
@SessionAttributes({"administration", "schools"})
public class AdministrationAdminController {
  @Autowired
  @Qualifier("autoFetchAdministrationService_all")
  private AdministrationService administrationService;

  @Autowired
  @Qualifier("simpleSchoolService")
  private SchoolService schoolService;

  @Autowired
  private SchoolLoader schoolLoader;

  @Autowired
  private HibernateUtil hibernateUtil;

  private final String defaultView = "/secure/administration/administration";

  public void setHibernateUtil(HibernateUtil hibernateUtil) {
    this.hibernateUtil = hibernateUtil;
  }

  public void setSchoolLoader(SchoolLoader schoolLoader) {
    this.schoolLoader = schoolLoader;
  }

  public void setAdministrationService(AdministrationService administrationService) {
    this.administrationService = administrationService;
  }

  public void setSchoolService(SchoolService schoolService) {
    this.schoolService = schoolService;
  }

  @InitBinder
  public void setAllowedFields(WebDataBinder dataBinder) {
    dataBinder.setDisallowedFields("id");
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    dateFormat.setLenient(false);
    dataBinder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false));
    dataBinder.registerCustomEditor(School.class, new SchoolPropertyEditor(schoolService));
  }

  @RequestMapping(value = "/")
  public String setupSearchPage() {
    return defaultView + "/index";
  }

  @RequestMapping(value = "/search", method = RequestMethod.POST)
  public String searchAdministration(
      Model model,
      @RequestParam(defaultValue = "") String name,
      @RequestParam(defaultValue = "1") int page,
      HttpServletRequest req,
      HttpServletResponse res) throws IOException {

    Collection schools = schoolLoader.loadAdministrationSchools(req, res);
    List administrations = administrationService.getByName(name, schools, page);
    model.addAttribute("administrations", administrations);

    return defaultView + "/list";
  }

  @RequestMapping("/new")
  public String setupNewForm(Model model,
      HttpServletRequest req,
      HttpServletResponse res) throws IOException {
    Administration administration = new Administration();
    administration.setAddress(new Address());

    model.addAttribute("schools", schoolLoader.loadAdministrationSchools(req, res));
    //model.addAttribute("schools", schoolService.getAll());
    model.addAttribute("administration", administration);
    return defaultView + "/form";
  }

  @RequestMapping("/{id}")
  public String setupDetailPage(Model model, @PathVariable("id") long id,
      HttpServletRequest req,
      HttpServletResponse res) throws IOException {
    //prepareDetailPage(model, id);
    model.addAttribute("schools", schoolLoader.loadAdministrationSchools(req, res));
    model.addAttribute("administration", administrationService.getById(id));
    model.addAttribute("readOnly", true);
    return defaultView + "/form";
  }

  @RequestMapping("/edit-{id}")
  public String setupEditForm(Model model, @PathVariable("id") long id,
      HttpServletRequest req,
      HttpServletResponse res) throws IOException {
    //prepareDetailPage(model, id);
    Administration administration = administrationService.getById(id);
    Address address = administration.getAddress();
    address = (Address) hibernateUtil.initializeImplementation(address);
    administration.setAddress(address);

    //School school = teacher.getSchool();
    //school = (School) hibernateUtil.initializeImplementation(school);
    //teacher.setSchool(school);
    model.addAttribute("schools", schoolLoader.loadAdministrationSchools(req, res));
    model.addAttribute("administration", administration);
    return defaultView + "/form";
  }

  @RequestMapping(value = "/submit", method = RequestMethod.POST)
  public String updateAdministration(
      @Valid @ModelAttribute("administration") Administration administration,
      BindingResult result, SessionStatus status) {

    if (result.hasErrors()) {
      //model.addAttribute("schools", schoolLoader.loadAdministrationSchools(req, res));
      return defaultView + "/form";
    } else {
      if (administration.isNew()) {
        administrationService.create(administration);
      } else
        administrationService.update(administration);

      status.setComplete();
      return "redirect:" + defaultView + "/";
    }
  }

  @RequestMapping(value = "/delete-{id}", method = RequestMethod.DELETE)
  @ResponseBody
  public Map deleteAdministration(@PathVariable("id") long id) {
    Map map = new HashMap();
    try {
      administrationService.deleteById(id);
      map.put("status", true);
    } catch (Exception e) {
      map.put("status", false);
      map.put("message", e.getMessage());
    }
    return map;
  }

//  private void prepareDetailPage(Model model, long id) {
//    Administration a = administrationService.getById(id);
//    model.addAttribute("administration", a);
//    model.addAttribute("schools", schoolService.getAll());
//  }
}
