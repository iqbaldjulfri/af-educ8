package com.labtekv.educ8.web.secure.teacher;

import java.util.List;
import javax.servlet.http.HttpServletRequest;

import com.labtekv.educ8.beans.propertyeditors.CoursePropertyEditor;
import com.labtekv.educ8.model.Course;
import com.labtekv.educ8.model.Semester;
import com.labtekv.educ8.model.Teacher;
import com.labtekv.educ8.model.User;
import com.labtekv.educ8.service.ActiveSemesterService;
import com.labtekv.educ8.service.ClazzFromScheduleService;
import com.labtekv.educ8.service.CourseService;
import com.labtekv.educ8.service.ExamService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author Iqbal Djulfri
 */
@Controller
@RequestMapping("/secure/teacher/course")
public class CourseManagementController {
  private final String defaultView = "/secure/teacher/course";

  @Autowired
  @Qualifier("simpleSemesterService")
  private ActiveSemesterService activeSemesterService;

  @Autowired
  @Qualifier("simpleFromScheduleService")
  private ClazzFromScheduleService clazzFromScheduleService;

  @Autowired
  @Qualifier("autoFetchCourseService_school")
  private CourseService courseService;

  @Autowired
  @Qualifier("autoFetchExamService_clazzes_type_remedialFor")
  private ExamService examService;

  public void setActiveSemesterService(ActiveSemesterService activeSemesterService) {
    this.activeSemesterService = activeSemesterService;
  }

  public void setClazzFromScheduleService(ClazzFromScheduleService clazzFromScheduleService) {
    this.clazzFromScheduleService = clazzFromScheduleService;
  }

  public void setCourseService(CourseService courseService) {
    this.courseService = courseService;
  }

  public void setExamService(ExamService examService) {
    this.examService = examService;
  }

  @InitBinder
  public void setAllowedFields(ServletRequestDataBinder binder) {
    binder.registerCustomEditor(Course.class, new CoursePropertyEditor(courseService));
  }

  @RequestMapping("/{id}")
  public String setupIndexPage(
      @PathVariable("id") Course course,
      HttpServletRequest req,
      Model model) {
    // isinya ada:
    // course detail
    // class yang dapet course ini
    // task list

    User u = (User) req.getAttribute("USER");
    Teacher t = u.getTeacher();

    if (t == null) {
      model.addAttribute("err", "af.err.user.not-a-teacher");
    } else {
      Semester semester = activeSemesterService.get();
      if (semester == null) {
        model.addAttribute("err", "af.err.school.no-active-semester");
        return defaultView + "/index";
      }

      List clazzes = clazzFromScheduleService.get(t, semester, course);
      List exams = examService.getAll(course, semester);

      model.addAttribute("course", course);
      model.addAttribute("clazzes", clazzes);
      model.addAttribute("exams", exams);
    }

    return defaultView + "/index";
  }
}
