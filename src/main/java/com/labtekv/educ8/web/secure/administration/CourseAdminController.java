package com.labtekv.educ8.web.secure.administration;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import com.labtekv.educ8.beans.propertyeditors.RoomPropertyEditor;
import com.labtekv.educ8.beans.propertyeditors.SchoolPropertyEditor;
import com.labtekv.educ8.beans.propertyeditors.TeacherPropertyEditor;
import com.labtekv.educ8.model.Course;
import com.labtekv.educ8.model.Room;
import com.labtekv.educ8.model.School;
import com.labtekv.educ8.model.Teacher;
import com.labtekv.educ8.service.CourseService;
import com.labtekv.educ8.service.RoomService;
import com.labtekv.educ8.service.SchoolService;
import com.labtekv.educ8.service.TeacherService;
import com.labtekv.educ8.util.SchoolLoader;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;

/**
 *
 * @author Iqbal Djulfri
 */
@Controller
@RequestMapping("/secure/administration/course")
@SessionAttributes({"course", "schools", "rooms", "teachers"})
public class CourseAdminController {

  private static final String defaultView = "/secure/administration/course";

  @Autowired @Qualifier("autoFetchCourseService_all")
  private CourseService courseService;

  @Autowired @Qualifier("simpleSchoolService")
  private SchoolService schoolService;

  @Autowired
  private SchoolLoader schoolLoader;

  @Autowired @Qualifier("simpleTeacherService")
  private TeacherService teacherService;

  @Autowired @Qualifier("simpleRoomService")
  private RoomService roomService;

  public void setTeacherService(TeacherService teacherService) {
    this.teacherService = teacherService;
  }

  public void setCourseService(CourseService courseService) {
    this.courseService = courseService;
  }

  public void setSchoolLoader(SchoolLoader schoolLoader) {
    this.schoolLoader = schoolLoader;
  }

  public void setSchoolService(SchoolService schoolService) {
    this.schoolService = schoolService;
  }

  public void setRoomService(RoomService roomService) {
    this.roomService = roomService;
  }

  @InitBinder
  public void initBinder(WebDataBinder dataBinder) {
    dataBinder.setDisallowedFields("id");
    dataBinder.registerCustomEditor(School.class, new SchoolPropertyEditor(schoolService));
    dataBinder.registerCustomEditor(Teacher.class, new TeacherPropertyEditor(teacherService));
    dataBinder.registerCustomEditor(Room.class, new RoomPropertyEditor(roomService));
  }

  @RequestMapping("/")
  public String setupSearchPage() {
    return defaultView + "/index";
  }

  @RequestMapping(value = "/search", method = RequestMethod.POST)
  public String searchUser(Model model,
      @RequestParam(defaultValue = "") String name,
      @RequestParam(defaultValue = "1") int page,
      HttpServletResponse res,
      HttpServletRequest req) throws IOException {
    List courses;

    Collection schools = schoolLoader.loadAdministrationSchools(req, res);
    courses = courseService.getByName(name, schools, page);

    model.addAttribute("courses", courses);

    return defaultView + "/list";
  }

  @RequestMapping("/new")
  public String setupNewForm(Model model,
      HttpServletRequest req, HttpServletResponse res) throws IOException {
    Collection schools = schoolLoader.loadAdministrationSchools(req, res);
    model.addAttribute("course", new Course());
    model.addAttribute("schools", schools);
    model.addAttribute("rooms", roomService.getAll(schools));
    model.addAttribute("teachers", teacherService.getAll(schools));
    return defaultView + "/form";
  }

  @RequestMapping("/{id}")
  public String setupDetailPage(Model model,
      @PathVariable("id") long id,
      HttpServletRequest req,
      HttpServletResponse res) throws IOException {
    Collection schools = schoolLoader.loadAdministrationSchools(req, res);
    model.addAttribute("course", courseService.getById(id));
    model.addAttribute("schools", schools);
    model.addAttribute("rooms", roomService.getAll(schools));
    model.addAttribute("teachers", teacherService.getAll(schools));
    model.addAttribute("readOnly", true);
    return defaultView + "/form";
  }

  @RequestMapping("/edit-{id}")
  public String setupEditForm(Model model,
      @PathVariable("id") long id,
      HttpServletRequest req,
      HttpServletResponse res) throws IOException {
    Collection schools = schoolLoader.loadAdministrationSchools(req, res);
    model.addAttribute("course", courseService.getById(id));
    model.addAttribute("schools", schools);
    model.addAttribute("rooms", roomService.getAll(schools));
    model.addAttribute("teachers", teacherService.getAll(schools));
    return defaultView + "/form";
  }

  @RequestMapping(value = "/submit", method = RequestMethod.POST)
  public String update(
      @Valid @ModelAttribute("course") Course course,
      BindingResult result,
      SessionStatus status) {

    if (result.hasErrors()) {
      return defaultView + "/form";
    } else {
      if (course.isNew())
        courseService.create(course);
      else
        courseService.update(course);

      status.setComplete();
      return "redirect:" + defaultView + "/";
    }
  }

  @RequestMapping(value = "/delete-{id}", method = RequestMethod.DELETE)
  @ResponseBody
  public Map delete(@PathVariable("id") long id) {
    Map map = new HashMap();
    try {
      courseService.deleteById(id);
      map.put("status", true);
    } catch (Exception e) {
      map.put("status", false);
      map.put("message", e.getMessage());
    }
    return map;
  }

}
