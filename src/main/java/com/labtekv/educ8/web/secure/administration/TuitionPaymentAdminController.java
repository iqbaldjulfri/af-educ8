/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.labtekv.educ8.web.secure.administration;

import com.labtekv.educ8.beans.propertyeditors.SchoolPropertyEditor;
import com.labtekv.educ8.beans.propertyeditors.StudentPropertyEditor;
import com.labtekv.educ8.model.School;
import com.labtekv.educ8.model.Student;
import com.labtekv.educ8.model.TuitionPayment;
import com.labtekv.educ8.service.SchoolService;
import com.labtekv.educ8.service.StudentService;
import com.labtekv.educ8.service.TuitionPaymentService;
import com.labtekv.educ8.util.SchoolLoader;
import java.io.IOException;
import java.util.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Andika
 */
@Controller
@RequestMapping("/secure/administration/tuition-payment")
public class TuitionPaymentAdminController {
  private static final String defaultView = "/secure/administration/tuition-payment";

  @Autowired @Qualifier("autoFetchTuitionPaymentService_student")
  private TuitionPaymentService tuitionPaymentService;

  @Autowired @Qualifier("simpleSchoolService")
  private SchoolService schoolService;
  
  @Autowired @Qualifier("simpleStudentService")
  private StudentService studentService;

  @Autowired
  private SchoolLoader schoolLoader;

  public void setTuitionPaymentService(TuitionPaymentService tuitionPaymentService) {
    this.tuitionPaymentService = tuitionPaymentService;
  }

  public void setSchoolLoader(SchoolLoader schoolLoader) {
    this.schoolLoader = schoolLoader;
  }

  public void setSchoolService(SchoolService schoolService) {
    this.schoolService = schoolService;
  }
  
  public void setStudentService(StudentService studentService) {
    this.studentService = studentService;
  }

   @InitBinder
  public void initBinder(WebDataBinder dataBinder) {
    dataBinder.setDisallowedFields("id");
    dataBinder.registerCustomEditor(School.class, new SchoolPropertyEditor(schoolService));
    dataBinder.registerCustomEditor(Student.class, new StudentPropertyEditor(studentService));
  }
   
  @RequestMapping("/")
  public String setupSearchPage(Model model,
      HttpServletRequest req, HttpServletResponse res) throws IOException {
    List years = new ArrayList();
    Calendar date = new GregorianCalendar();
    int year = date.get(Calendar.YEAR);
    for(int i = (year-5); i <= (year+1); i++){
      years.add(i);
    }
    model.addAttribute("schools", schoolLoader.loadAdministrationSchools(req, res));
    model.addAttribute("years", years);
    return defaultView + "/index";
  }
  
  @RequestMapping(value = "/search", method = RequestMethod.POST)
  public String searchUser(Model model,
      @RequestParam(required=false) String studentId, //INI ID NUMBER STUDENT bukan id tabel student
      @RequestParam(required=false) Long schoolId,
      @RequestParam int month,
      @RequestParam int year,
      @RequestParam(defaultValue = "1") int page,
      HttpServletResponse res,
      HttpServletRequest req) throws IOException {
    List<TuitionPayment> tuitionPayments;
    Collection schools = null;
    
    School s = schoolId == null ? null : schoolService.getById(schoolId);
    if(schoolId == 0){
      schools = schoolLoader.loadAdministrationSchools(req, res);
      tuitionPayments = tuitionPaymentService.getByStudentAndMonthAndYear(studentId, month, year, schools, page);
    }else{
      schools = new ArrayList();
      schools.add(s);
      tuitionPayments = tuitionPaymentService.getByStudentAndMonthAndYear(studentId, month, year, schools, page);
    }
    Map studentPayment = new HashMap();
    List<Student> students = studentService.getByNameNumber("", studentId, schools, page);
    for(Student st : students){
      boolean f = false;
      for(TuitionPayment tp : tuitionPayments){        
        if(tp.getStudent().getId() == st.getId()){
          f = true;
          studentPayment.put(st, tp);
          tuitionPayments.remove(tp);
          break;
        }
      }
      if(!f){
        studentPayment.put(st, null);
      }
    }
    
    model.addAttribute("month", month);
    model.addAttribute("year", year);
    model.addAttribute("students", students);
    model.addAttribute("studentPayment", studentPayment);

    return defaultView + "/list";
  }
  
  @RequestMapping(value = "/pay", method = RequestMethod.POST)
  @ResponseBody
  public Map payTuition(Model model,
      @RequestParam(required=false) long studentId, //INI BARU ID TABEL STUDENT
      @RequestParam int month,
      @RequestParam int year,
      HttpServletResponse res,
      HttpServletRequest req) {
    Map map = new HashMap();
    try {
      TuitionPayment tp = new TuitionPayment();
      tp.setStudent(studentService.getById(studentId));
      tp.setMonth(month);
      tp.setYear(year);
      tp.setPaymentStatus(Boolean.TRUE);
      tuitionPaymentService.create(tp);
      map.put("status", true);
    } catch (Exception e) {
      map.put("status", false);
      map.put("message", e.getMessage());
    }
    return map;
  }
}
