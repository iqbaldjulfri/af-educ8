package com.labtekv.educ8.web.secure.administration;

import com.labtekv.educ8.beans.propertyeditors.ClazzPropertyEditor;
import com.labtekv.educ8.beans.propertyeditors.SchoolPropertyEditor;
import com.labtekv.educ8.beans.propertyeditors.SemesterPropertyEditor;
import com.labtekv.educ8.beans.propertyeditors.StudentPropertyEditor;
import java.io.IOException;
import java.util.Collection;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.labtekv.educ8.model.Clazz;
import com.labtekv.educ8.model.School;
import com.labtekv.educ8.model.Semester;
import com.labtekv.educ8.model.Student;
import com.labtekv.educ8.service.*;
import com.labtekv.educ8.util.SchoolLoader;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

/**
 *
 * @author Iqbal Djulfri
 */
@Controller
@RequestMapping("/secure/administration/clazz-assignment")
public class ClazzAssignmentController {

  private static final String defaultView = "/secure/administration/clazz-assignment";

  @Autowired @Qualifier("simpleClazzAssignmentService")
  private ClazzAssignmentService clazzAssignmentService;
  
  @Autowired @Qualifier("simpleStudentService")
  private StudentService studentService;

  @Autowired @Qualifier("simpleClazzService")
  private ClazzService clazzService;
  
  @Autowired @Qualifier("simpleSemesterService")
  private SemesterService semesterService;
  
  @Autowired @Qualifier("simpleSchoolService")
  private SchoolService schoolService;
  
  @Autowired
  private SchoolLoader schoolLoader;

  public void setClazzAssignmentService(ClazzAssignmentService clazzAssignmentService) {
    this.clazzAssignmentService = clazzAssignmentService;
  }
  
  public void setStudentService(StudentService studentService) {
    this.studentService = studentService;
  }

  public void setClazzService(ClazzService clazzService) {
    this.clazzService = clazzService;
  }
  
  public void setSemesterService(SemesterService semesterService) {
    this.semesterService = semesterService;
  }
  
  public void setSchoolLoader(SchoolLoader schoolLoader) {
    this.schoolLoader = schoolLoader;
  }

  public void setSchoolService(SchoolService schoolService) {
    this.schoolService = schoolService;
  }

  @InitBinder
  public void initBinder(WebDataBinder dataBinder) {
    dataBinder.setDisallowedFields("id");
    dataBinder.registerCustomEditor(School.class, new SchoolPropertyEditor(schoolService));
    dataBinder.registerCustomEditor(Student.class, new StudentPropertyEditor(studentService));
    dataBinder.registerCustomEditor(Semester.class, new SemesterPropertyEditor(semesterService));
    dataBinder.registerCustomEditor(Clazz.class, new ClazzPropertyEditor(clazzService));
  }

  @RequestMapping("/")
  public String setupIndexPage() {
    return defaultView + "/index";
  }
  
  @RequestMapping(value = "/search", method = RequestMethod.POST)
  public String searchClazz(Model model,
      @RequestParam(defaultValue = "") String name,
      @RequestParam(defaultValue = "1") int page,
      HttpServletResponse res,
      HttpServletRequest req) throws IOException {

    Collection schools = schoolLoader.loadAdministrationSchools(req, res);
    model.addAttribute("clazzes", clazzService.getByName(name, schools, page));

    return defaultView + "/list";
  }

}
