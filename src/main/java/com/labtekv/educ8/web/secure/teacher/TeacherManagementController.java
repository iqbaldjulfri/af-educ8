package com.labtekv.educ8.web.secure.teacher;

import java.util.List;
import javax.servlet.http.HttpServletRequest;

import com.labtekv.educ8.model.Teacher;
import com.labtekv.educ8.model.User;
import com.labtekv.educ8.service.CourseTeacherService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author Iqbal Djulfri
 */
@Controller
@RequestMapping("/secure/teacher")
public class TeacherManagementController {

  private final String defaultView = "/secure/teacher";

  @Autowired
  @Qualifier("simpleCourseTeacherService")
  private CourseTeacherService courseTeacherService;

  public void setCourseService(CourseTeacherService courseTeacherService) {
    this.courseTeacherService = courseTeacherService;
  }

  @RequestMapping("/")
  public String setupIndexPage(HttpServletRequest req,
      Model model) {
    // isinya ada:
    // list of courses
    // list of classes
    // list of classes (sbg wali)
    // list of extracurriculer

    User u = (User) req.getAttribute("USER");
    Teacher t = u.getTeacher();

    if (t == null) {
      model.addAttribute("err", "af.err.user.not-a-teacher");
    } else {
      List courses = courseTeacherService.getAll(t);

      model.addAttribute("courses", courses);
    }

    return defaultView + "/index";
  }
}
