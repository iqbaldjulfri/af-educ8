package com.labtekv.educ8.web.secure.administration;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import com.labtekv.educ8.beans.propertyeditors.SchoolPropertyEditor;
import com.labtekv.educ8.model.ScheduleSlot;
import com.labtekv.educ8.model.School;
import com.labtekv.educ8.service.ScheduleSlotService;
import com.labtekv.educ8.service.SchoolService;
import com.labtekv.educ8.util.SchoolLoader;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;

/**
 *
 * @author Iqbal Djulfri
 */
@Controller
@RequestMapping("/secure/administration/schedule-slot")
@SessionAttributes({"scheduleSlot", "schools"})
public class ScheduleSlotAdminController {

  private static final String defaultView = "/secure/administration/schedule-slot";

  @Autowired @Qualifier("autoFetchScheduleSlotService_school")
  private ScheduleSlotService scheduleSlotService;

  @Autowired
  private SchoolLoader schoolLoader;

  @Autowired @Qualifier("simpleSchoolService")
  private SchoolService schoolService;

  public void setSchoolService(SchoolService schoolService) {
    this.schoolService = schoolService;
  }

  public void setScheduleSlotService(ScheduleSlotService scheduleSlotService) {
    this.scheduleSlotService = scheduleSlotService;
  }

  public void setSchoolLoader(SchoolLoader schoolLoader) {
    this.schoolLoader = schoolLoader;
  }

  @InitBinder
  public void initBinder(WebDataBinder dataBinder) {
    dataBinder.setDisallowedFields("id");

    SimpleDateFormat df = new SimpleDateFormat("HH:mm");
    df.setLenient(true);
    dataBinder.registerCustomEditor(Date.class, new CustomDateEditor(df, false));

    dataBinder.registerCustomEditor(School.class, new SchoolPropertyEditor(schoolService));
  }

  @RequestMapping("/")
  public String setupSearchPage() {
    return defaultView + "/index";
  }

  @RequestMapping(value = "/search", method = RequestMethod.POST)
  public String searchUser(Model model,
      @RequestParam(defaultValue = "") String description,
      @RequestParam(defaultValue = "1") int page,
      HttpServletResponse res,
      HttpServletRequest req) throws IOException {
    List scheduleSlots;

    Collection schools = schoolLoader.loadAdministrationSchools(req, res);
    scheduleSlots = scheduleSlotService.getByDescription(description, schools, page);

    model.addAttribute("scheduleSlots", scheduleSlots);

    return defaultView + "/list";
  }

  @RequestMapping("/new")
  public String setupNewForm(Model model,
      HttpServletRequest req, HttpServletResponse res) throws IOException {
    model.addAttribute("scheduleSlot", new ScheduleSlot());
    model.addAttribute("schools", schoolLoader.loadAdministrationSchools(req, res));
    return defaultView + "/form";
  }

  @RequestMapping("/{id}")
  public String setupDetailPage(Model model,
      @PathVariable("id") long id,
      HttpServletRequest req,
      HttpServletResponse res) throws IOException {
    model.addAttribute("scheduleSlot", scheduleSlotService.getById(id));
    model.addAttribute("schools", schoolLoader.loadAdministrationSchools(req, res));
    model.addAttribute("readOnly", true);
    return defaultView + "/form";
  }

  @RequestMapping("/edit-{id}")
  public String setupEditForm(Model model,
      @PathVariable("id") long id,
      HttpServletRequest req,
      HttpServletResponse res) throws IOException {
    model.addAttribute("scheduleSlot", scheduleSlotService.getById(id));
    model.addAttribute("schools", schoolLoader.loadAdministrationSchools(req, res));
    return defaultView + "/form";
  }

  @RequestMapping(value = "/submit", method = RequestMethod.POST)
  public String update(
      @Valid @ModelAttribute("scheduleSlot") ScheduleSlot scheduleSlot,
      BindingResult result,
      SessionStatus status) {

    if (result.hasErrors()) {
      return defaultView + "/form";
    } else {
      if (scheduleSlot.isNew())
        scheduleSlotService.create(scheduleSlot);
      else
        scheduleSlotService.update(scheduleSlot);

      status.setComplete();
      return "redirect:" + defaultView + "/";
    }
  }

  @RequestMapping(value = "/delete-{id}", method = RequestMethod.DELETE)
  @ResponseBody
  public Map delete(@PathVariable("id") long id) {
    Map map = new HashMap();
    try {
      scheduleSlotService.deleteById(id);
      map.put("status", true);
    } catch (Exception e) {
      map.put("status", false);
      map.put("message", e.getMessage());
    }
    return map;
  }

}
