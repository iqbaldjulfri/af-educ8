package com.labtekv.educ8.web.secure.user;

import com.labtekv.educ8.dao.UserDao;
import com.labtekv.educ8.model.Message;
import com.labtekv.educ8.model.User;
import com.labtekv.educ8.service.UserService;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

/**
 *
 * @author madewiratama
 */
@Controller
@RequestMapping(value = "/secure/user/changepassword", method = RequestMethod.GET)
@SessionAttributes("user")
public class ChangePasswordController {
  private final String defaultView = "/secure/user/changepassword";
  
  @Autowired
  @Qualifier("simpleUserService")
  private UserService userService;

  public void setUserService(UserService userService) {
    this.userService = userService;
  }
  
  @InitBinder
  public void setAllowedFields(WebDataBinder binder) {
    binder.setDisallowedFields("id", "username");
  }

  @RequestMapping("/")
  public String setupIndexPage(Model model, 
      @RequestParam(required=false) String request,
      HttpServletRequest req)
  {
    User user = (User) req.getAttribute("USER");
    model.addAttribute("user", user);
    model.addAttribute("request", request);
    return defaultView + "/changepassword";
  }
  
  @RequestMapping(value = "/submit", method = RequestMethod.POST)
  public String sendMessage(
      @Valid @ModelAttribute("user") User user,
      @RequestParam String oldPassword,
      @RequestParam String newPassword,
      @RequestParam String retypePassword,
      BindingResult result, SessionStatus status) {

    oldPassword = new ShaPasswordEncoder().encodePassword(oldPassword, user.getUsername());
    if(oldPassword.equals(user.getPassword())){
      if(retypePassword.equals(newPassword)){
        userService.update(user, newPassword);
        status.setComplete();
        return "redirect:" + defaultView + "/changepassword?request=success";
      }
    }
    return "redirect:" + defaultView + "/changepassword?request=failed";
  }
}
