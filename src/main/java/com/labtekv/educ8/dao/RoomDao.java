package com.labtekv.educ8.dao;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.labtekv.educ8.model.Room;
import com.labtekv.educ8.model.School;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;

/**
 *
 * @author Iqbal Djulfri
 */
public interface RoomDao extends BaseDao<Long, Room> {
  public List<Room> getBySchool(List<Criterion> criterions, List<Order> orders, Collection<School> schools, int page);
  public List<Room> getAllBySchool(Collection<School> schools);
}
