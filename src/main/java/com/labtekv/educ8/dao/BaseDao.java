
package com.labtekv.educ8.dao;

import java.io.Serializable;
import java.util.List;

import com.labtekv.educ8.ext.hibernate.EntityExistsException;
import com.labtekv.educ8.ext.hibernate.EntityNotFoundException;
import com.labtekv.educ8.model.BaseModel;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;

/**
 *
 * @author Iqbal Djulfri
 */
public interface BaseDao<ID extends Serializable, MODEL extends BaseModel> {
  public static final int FETCH_SIZE = 10;

  public MODEL get(ID id) throws EntityNotFoundException;
  public List<MODEL> getAll();
  public List<MODEL> get(List<Criterion> criterions, List<Order> orders, int page);
  public List<MODEL> get(List<Criterion> criterions, List<Order> orders, int page, int fetchSize);

  public long countAll();
  public long count(List<Criterion> criterions);

  public ID save(MODEL model) throws EntityExistsException;
  public void update(MODEL model);
  public void delete(ID id) throws EntityNotFoundException;
  public void delete(MODEL model);
}
