/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.labtekv.educ8.dao;

import com.labtekv.educ8.model.ClazzAssignment;

/**
 *
 * @author Andika
 */
public interface ClazzAssignmentDao extends BaseDao<Long, ClazzAssignment> {
  
}
