package com.labtekv.educ8.dao;

import com.labtekv.educ8.model.Schedule;

/**
 *
 * @author Iqbal Djulfri
 */
public interface ScheduleDao extends BaseDao<Long, Schedule> {

}
