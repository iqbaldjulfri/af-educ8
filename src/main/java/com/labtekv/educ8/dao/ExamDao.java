package com.labtekv.educ8.dao;

import com.labtekv.educ8.model.Exam;

/**
 *
 * @author Iqbal Djulfri
 */
public interface ExamDao extends BaseDao<Long, Exam> {
  
}
