package com.labtekv.educ8.dao;

import java.util.Collection;
import java.util.List;
import com.labtekv.educ8.model.Administration;
import com.labtekv.educ8.model.School;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;

/**
 *
 * @author Andika
 */
public interface AdministrationDao extends BaseDao<Long, Administration> {

  public List<Administration> getBySchool(List<Criterion> criterions, List<Order> orders, Collection<School> schools, int page);
  public List<Administration> getAllBySchool(Collection<School> schools);

}
