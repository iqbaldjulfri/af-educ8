package com.labtekv.educ8.dao;

import com.labtekv.educ8.model.School;

/**
 *
 * @author Iqbal Djulfri
 */
public interface SchoolDao extends BaseDao<Long, School> {

}
