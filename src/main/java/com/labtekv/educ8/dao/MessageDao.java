package com.labtekv.educ8.dao;

import com.labtekv.educ8.model.Message;

/**
 *
 * @author Iqbal Djulfri
 */
public interface MessageDao extends BaseDao<Long, Message> {

}
