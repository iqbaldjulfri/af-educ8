package com.labtekv.educ8.dao;

import java.util.Collection;
import java.util.List;
import com.labtekv.educ8.model.Clazz;
import com.labtekv.educ8.model.School;

/**
 *
 * @author Iqbal Djulfri
 */
public interface ClazzDao extends BaseDao<Long, Clazz> {
}
