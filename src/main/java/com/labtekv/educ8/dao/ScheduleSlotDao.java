package com.labtekv.educ8.dao;

import com.labtekv.educ8.model.ScheduleSlot;

/**
 *
 * @author Iqbal Djulfri
 */
public interface ScheduleSlotDao extends BaseDao<Long, ScheduleSlot> {

}
