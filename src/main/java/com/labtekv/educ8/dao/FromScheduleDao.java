package com.labtekv.educ8.dao;

import java.util.List;

import com.labtekv.educ8.model.Clazz;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;

/**
 *
 * @author Iqbal Djulfri
 */
public interface FromScheduleDao {

  public List<Clazz> getClazz();
  public List<Clazz> getClazz(List<Criterion> criterions, List<Order> orders);
  public List<Clazz> getClazz(List<Criterion> criterions, List<Order> orders, int page);
  public List<Clazz> getClazz(List<Criterion> criterions, List<Order> orders, int page, int fetchSize);

  public List<Clazz> getClazzJoinCourse();
  public List<Clazz> getClazzJoinCourse(List<Criterion> criterions, List<Order> orders);
  public List<Clazz> getClazzJoinCourse(List<Criterion> criterions, List<Order> orders, int page);
  public List<Clazz> getClazzJoinCourse(List<Criterion> criterions, List<Order> orders, int page, int fetchSize);

}
