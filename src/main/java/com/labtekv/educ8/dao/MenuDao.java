package com.labtekv.educ8.dao;

import java.util.Collection;
import java.util.List;
import com.labtekv.educ8.model.Menu;
import com.labtekv.educ8.model.Role;

/**
 *
 * @author Iqbal Djulfri
 */
public interface MenuDao extends BaseDao<Long, Menu> {
  public List<Menu> getByRoles(Collection<Role> roles);
  public List<Menu> getForDisplay(Collection<Role> roles);
}
