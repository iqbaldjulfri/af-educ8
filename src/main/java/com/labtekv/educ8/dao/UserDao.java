
package com.labtekv.educ8.dao;

import com.labtekv.educ8.ext.hibernate.EntityNotFoundException;
import com.labtekv.educ8.model.User;

/**
 *
 * @author Iqbal Djulfri
 */
public interface UserDao extends BaseDao<Long, User> {
  public User getByUsername(String username) throws EntityNotFoundException;
}
