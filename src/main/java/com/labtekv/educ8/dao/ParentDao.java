/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.labtekv.educ8.dao;

import com.labtekv.educ8.model.Parent;

/**
 *
 * @author Andika
 */
public interface ParentDao extends BaseDao<Long, Parent> {
  
}
