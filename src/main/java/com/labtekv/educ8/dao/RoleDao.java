/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.labtekv.educ8.dao;

import com.labtekv.educ8.model.Role;
import com.labtekv.educ8.ext.hibernate.EntityNotFoundException;

/**
 *
 * @author Alienware
 */
public interface RoleDao extends BaseDao<Long, Role> {
	public Role getByAuthority(String authority) throws EntityNotFoundException;
}
