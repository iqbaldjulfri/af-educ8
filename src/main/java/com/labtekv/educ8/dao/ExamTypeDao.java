package com.labtekv.educ8.dao;

import com.labtekv.educ8.model.ExamType;

/**
 *
 * @author Iqbal Djulfri
 */
public interface ExamTypeDao extends BaseDao<Long, ExamType> {

}
