package com.labtekv.educ8.dao;

import com.labtekv.educ8.model.Specialization;

/**
 *
 * @author Iqbal Djulfri
 */
public interface SpecializationDao extends BaseDao<Long, Specialization> {

}
