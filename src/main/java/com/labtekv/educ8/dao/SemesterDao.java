package com.labtekv.educ8.dao;

import com.labtekv.educ8.model.Semester;

/**
 *
 * @author Iqbal Djulfri
 */
public interface SemesterDao extends BaseDao<Long, Semester> {

}
