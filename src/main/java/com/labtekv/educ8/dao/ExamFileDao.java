package com.labtekv.educ8.dao;

import com.labtekv.educ8.model.ExamFile;

/**
 *
 * @author Iqbal Djulfri
 */
public interface ExamFileDao extends BaseDao<Long, ExamFile> {
  
}
