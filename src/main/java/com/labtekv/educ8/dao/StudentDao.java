/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.labtekv.educ8.dao;

import com.labtekv.educ8.model.School;
import com.labtekv.educ8.model.Student;
import java.util.Collection;
import java.util.List;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;

/**
 *
 * @author Andika
 */
public interface StudentDao extends BaseDao<Long, Student> {
  public List<Student> getBySchool(List<Criterion> criterions, List<Order> orders, Collection<School> schools, int page);
  public List<Student> getAllBySchool(Collection<School> schools);
}
