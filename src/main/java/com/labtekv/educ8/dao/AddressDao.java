package com.labtekv.educ8.dao;

import com.labtekv.educ8.model.Address;

/**
 *
 * @author Iqbal Djulfri
 */
public interface AddressDao extends BaseDao<Long, Address> {

}
