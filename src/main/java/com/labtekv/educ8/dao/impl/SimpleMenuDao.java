package com.labtekv.educ8.dao.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.labtekv.educ8.dao.MenuDao;
import com.labtekv.educ8.model.Menu;
import com.labtekv.educ8.model.Role;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;

import org.springframework.stereotype.Repository;

/**
 *
 * @author Iqbal Djulfri
 */
@Repository("simpleMenuDao")
public class SimpleMenuDao extends SimpleDao<Long, Menu> implements MenuDao {

  public SimpleMenuDao() {
    super(Menu.class);
  }

  @Override
  public List<Menu> getByRoles(Collection<Role> roles) {
    Criteria c = sf.getCurrentSession().createCriteria(type);

    c.createAlias("roles", "role", JoinType.LEFT_OUTER_JOIN);
    c.setProjection(Projections.distinct(Projections.id()));

    if (roles == null || roles.isEmpty()) {
      c.add(Restrictions.isNull("role.id"));
    } else {
      List ids = new ArrayList();
      for (Role r : roles) {
        ids.add(r.getId());
      }

      c.add(Restrictions.or(
          Restrictions.in("role.id", ids),
          Restrictions.isNull("role.id")));
    }

    List mIds = c.list();

    c = sf.getCurrentSession().createCriteria(type);
    c.add(Restrictions.in("id", mIds));
    c.addOrder(Order.asc("name"));

    return c.list();
  }

  @Override
  public List<Menu> getForDisplay(Collection<Role> roles) {
    Criteria c = sf.getCurrentSession().createCriteria(type);

    c.createAlias("roles", "role", JoinType.LEFT_OUTER_JOIN);
    c.setProjection(Projections.distinct(Projections.id()));

    if (roles == null || roles.isEmpty()) {
      c.add(Restrictions.isNull("role.id"));
      c.add(Restrictions.isNull("parent"));
    } else {
      List ids = new ArrayList();
      for (Role r : roles) {
        ids.add(r.getId());
      }

      c.add(Restrictions.or(
          Restrictions.in("role.id", ids),
          Restrictions.isNull("role.id")));
      c.add(Restrictions.isNull("parent"));
    }

    List mIds = c.list();
    if (mIds == null || mIds.isEmpty()) return new ArrayList();

    c = sf.getCurrentSession().createCriteria(type);
    c.add(Restrictions.in("id", mIds));
    c.addOrder(Order.desc("priority"));
    c.addOrder(Order.asc("name"));

    List menus = c.list();

    initializeChildrenForDisplay(roles, mIds);

    return menus;
  }

  private void initializeChildrenForDisplay(Collection<Role> roles,
      List parentIds) {
    if (parentIds == null || parentIds.isEmpty()) return;

    Criteria c = sf.getCurrentSession().createCriteria(type);

    c.createAlias("roles", "role", JoinType.LEFT_OUTER_JOIN);
    c.setProjection(Projections.distinct(Projections.id()));

    if (roles == null || roles.isEmpty()) {
      c.add(Restrictions.isNull("role.id"));
    } else {
      List ids = new ArrayList();
      for (Role r : roles) {
        ids.add(r.getId());
      }

      c.add(Restrictions.or(
          Restrictions.in("role.id", ids),
          Restrictions.isNull("role.id")));
    }
    c.add(Restrictions.in("parent.id", parentIds));

    List mIds = c.list();
    if (mIds == null || mIds.isEmpty()) return;

    c = sf.getCurrentSession().createCriteria(type);
    c.add(Restrictions.in("id", mIds));
    c.addOrder(Order.desc("priority"));
    c.addOrder(Order.asc("name"));
    List menus = c.list();

    initializeChildrenForDisplay(roles, mIds);
  }

  private void initialize(Menu m) {

  }

}
