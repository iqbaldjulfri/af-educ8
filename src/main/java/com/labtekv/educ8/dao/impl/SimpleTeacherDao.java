/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.labtekv.educ8.dao.impl;

import com.labtekv.educ8.dao.TeacherDao;
import com.labtekv.educ8.model.School;
import com.labtekv.educ8.model.Teacher;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Andika
 */

@Repository("simpleTeacherDao")
public class SimpleTeacherDao extends SimpleDao<Long, Teacher> implements TeacherDao{

  public SimpleTeacherDao() {
    super(Teacher.class);
  }

  @Override
  public List<Teacher> getBySchool(List<Criterion> criterions, List<Order> orders, Collection<School> schools, int page) {
    Criteria c = sf.getCurrentSession().createCriteria(Teacher.class), c2;
    c.createAlias("schools", "school", JoinType.LEFT_OUTER_JOIN);
    c.setProjection(Projections.distinct(Projections.id()));

    if (page > 0) {
      c.setFirstResult(page * FETCH_SIZE - FETCH_SIZE);
    }

    if (criterions != null) for (Criterion criterion : criterions) c.add(criterion);
    List ids = new ArrayList();
    for (School s : schools) ids.add(s.getId());
    c.add(Restrictions.or(Restrictions.in("school.id", ids), Restrictions.isNull("school.id")));

    ids = c.list();

    c2 = sf.getCurrentSession().createCriteria(Teacher.class);
    if (ids != null && !ids.isEmpty()) c2.add(Restrictions.in("id", ids));
    if (orders != null) for (Order order : orders) c2.addOrder(order);

    return c2.list();
  }

  @Override
  public List<Teacher> getAllBySchool(Collection<School> schools) {
    List os = new ArrayList();
    os.add(Order.asc("name"));
    return getBySchool(null, os, schools, 0);
  }

}
