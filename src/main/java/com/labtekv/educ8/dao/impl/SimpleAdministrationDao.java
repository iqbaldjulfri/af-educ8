package com.labtekv.educ8.dao.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.labtekv.educ8.dao.AdministrationDao;
import com.labtekv.educ8.model.Administration;
import com.labtekv.educ8.model.School;

import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;

import org.springframework.stereotype.Repository;

/**
 *
 * @author Andika
 */

@Repository("simpleAdministrationDao")
public class SimpleAdministrationDao extends SimpleDao<Long, Administration> implements AdministrationDao {

  public SimpleAdministrationDao() {
    super(Administration.class);
  }

  @Override
  public List<Administration> getBySchool(List<Criterion> criterions, List<Order> orders, Collection<School> schools, int page) {
    Criteria c = sf.getCurrentSession().createCriteria(Administration.class), c2;
    c.createAlias("schools", "school", JoinType.LEFT_OUTER_JOIN);

    if (page >= 0) {
      c.setProjection(Projections.distinct(Projections.id()))
          .setFirstResult(page * FETCH_SIZE - FETCH_SIZE);
    }

    if (criterions != null) for (Criterion criterion : criterions) c.add(criterion);
    List ids = new ArrayList();
    for (School s : schools) ids.add(s.getId());
    c.add(Restrictions.or(Restrictions.in("school.id", ids), Restrictions.isNull("school.id")));

    ids = c.list();

    c2 = sf.getCurrentSession().createCriteria(Administration.class);
    if (ids != null && !ids.isEmpty()) c2.add(Restrictions.in("id", ids));
    if (orders != null) for (Order order : orders) c2.addOrder(order);

    return c2.list();
  }

  @Override
  public List<Administration> getAllBySchool(Collection<School> schools) {
    List os = new ArrayList();
    os.add(Order.asc("name"));
    return getBySchool(null, os, schools, 0);
  }

}
