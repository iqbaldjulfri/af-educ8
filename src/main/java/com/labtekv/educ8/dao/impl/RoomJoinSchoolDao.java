package com.labtekv.educ8.dao.impl;

import java.util.List;

import com.labtekv.educ8.model.Room;

import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Iqbal Djulfri
 */
@Repository
public class RoomJoinSchoolDao extends SimpleRoomDao {

  @Override
  public List<Room> getAll() {
    return get(null, null, 0);
  }

  @Override
  public List<Room> get(List<Criterion> criterions, List<Order> orders, int page) {
    return get(criterions, orders, page, FETCH_SIZE);
  }

  @Override
  public List<Room> get(List<Criterion> criterions, List<Order> orders, int page, int fetchSize) {
    Criteria c = sf.getCurrentSession().createCriteria(type).createAlias("schools", "school");
    if (criterions != null) for (Criterion cx : criterions) c.add(cx);
    if (orders != null) for (Order o : orders) c.addOrder(o);
    if (page > 0) {
      c.setMaxResults(fetchSize);
      c.setFirstResult(page * fetchSize - fetchSize);
    }
    return c.list();
  }

}
