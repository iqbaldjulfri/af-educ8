package com.labtekv.educ8.dao.impl;

import com.labtekv.educ8.dao.ScheduleDao;
import com.labtekv.educ8.model.Schedule;

import org.springframework.stereotype.Repository;

/**
 *
 * @author Iqbal Djulfri
 */
@Repository
public class SimpleScheduleDao extends SimpleDao<Long, Schedule> implements ScheduleDao {
  public SimpleScheduleDao() {
    super(Schedule.class);
  }
}
