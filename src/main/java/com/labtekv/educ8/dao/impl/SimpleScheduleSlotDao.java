package com.labtekv.educ8.dao.impl;

import com.labtekv.educ8.dao.ScheduleSlotDao;
import com.labtekv.educ8.model.ScheduleSlot;

import org.springframework.stereotype.Repository;

/**
 *
 * @author Iqbal Djulfri
 */
@Repository
public class SimpleScheduleSlotDao extends SimpleDao<Long, ScheduleSlot> implements ScheduleSlotDao {

  public SimpleScheduleSlotDao() {
    super(ScheduleSlot.class);
  }

}
