/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.labtekv.educ8.dao.impl;

import com.labtekv.educ8.dao.ClazzAssignmentDao;
import com.labtekv.educ8.model.ClazzAssignment;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Andika
 */
@Repository
public class SimpleClazzAssignmentDao extends SimpleDao<Long, ClazzAssignment> implements ClazzAssignmentDao{
  public SimpleClazzAssignmentDao(){
    super(ClazzAssignment.class);
  }
}
