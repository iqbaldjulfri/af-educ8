package com.labtekv.educ8.dao.impl;


import com.labtekv.educ8.dao.SchoolDao;
import com.labtekv.educ8.model.School;

import org.springframework.stereotype.Repository;

/**
 *
 * @author Iqbal Djulfri
 */
@Repository("simpleSchoolDao")
public class SimpleSchoolDao extends SimpleDao<Long, School> implements SchoolDao {

  public SimpleSchoolDao() {
    super(School.class);
  }

}
