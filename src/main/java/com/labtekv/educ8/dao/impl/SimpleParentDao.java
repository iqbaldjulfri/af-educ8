/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.labtekv.educ8.dao.impl;

import com.labtekv.educ8.dao.ParentDao;
import com.labtekv.educ8.model.Parent;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Andika
 */

@Repository("simpleParentDao")
public class SimpleParentDao extends SimpleDao<Long, Parent> implements ParentDao{
  
  public SimpleParentDao() {
    super(Parent.class);
  }
  
}
