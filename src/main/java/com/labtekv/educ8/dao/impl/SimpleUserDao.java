
package com.labtekv.educ8.dao.impl;

import com.labtekv.educ8.dao.UserDao;
import com.labtekv.educ8.ext.hibernate.EntityNotFoundException;
import com.labtekv.educ8.model.User;

import org.springframework.stereotype.Repository;

/**
 *
 * @author Iqbal Djulfri
 */
@Repository("simpleUserDao")
public class SimpleUserDao extends SimpleDao<Long, User> implements UserDao {

  public SimpleUserDao() {
    super(User.class);
  }

  @Override
  public User getByUsername(String username) throws EntityNotFoundException {
    return (User) sf.getCurrentSession()
        .createQuery("from User where username = '" + username + "'").uniqueResult();
  }

}
