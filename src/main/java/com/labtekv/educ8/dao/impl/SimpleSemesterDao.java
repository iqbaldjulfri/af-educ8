package com.labtekv.educ8.dao.impl;


import com.labtekv.educ8.dao.SemesterDao;
import com.labtekv.educ8.model.Semester;

import org.springframework.stereotype.Repository;

/**
 *
 * @author Iqbal Djulfri
 */
@Repository
public class SimpleSemesterDao extends SimpleDao<Long, Semester> implements SemesterDao {
  public SimpleSemesterDao() {
    super(Semester.class);
  }
}
