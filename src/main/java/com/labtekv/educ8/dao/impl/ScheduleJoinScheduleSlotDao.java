package com.labtekv.educ8.dao.impl;

import java.util.List;

import com.labtekv.educ8.model.Schedule;

import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;

import org.springframework.stereotype.Repository;

/**
 *
 * @author Iqbal Djulfri
 */
@Repository("scheduleJoinScheduleSlotDao")
public class ScheduleJoinScheduleSlotDao extends SimpleScheduleDao {

  @Override
  public List<Schedule> getAll() {
    return super.getAll();
  }

  @Override
  public List<Schedule> get(List<Criterion> criterions, List<Order> orders, int page) {
    return get(criterions, orders, page, FETCH_SIZE);
  }

  @Override
  public List<Schedule> get(List<Criterion> criterions, List<Order> orders, int page, int fetchSize) {
    Criteria c = sf.getCurrentSession().createCriteria(type).createAlias("slot", "slot");
    if (criterions != null) for (Criterion cr : criterions) c.add(cr);
    if (orders != null) for (Order o : orders) c.addOrder(o);
    if (page > 0) {
      c.setFirstResult(page * fetchSize - fetchSize);
      c.setMaxResults(fetchSize);
    }
    return c.list();
  }

}
