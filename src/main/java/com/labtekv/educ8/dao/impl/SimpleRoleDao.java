
package com.labtekv.educ8.dao.impl;

import com.labtekv.educ8.dao.RoleDao;
import com.labtekv.educ8.ext.hibernate.EntityNotFoundException;
import com.labtekv.educ8.model.Role;

import org.springframework.stereotype.Repository;

/**
 *
 * @author Alienware
 */

@Repository("simpleRoleDao")
public class SimpleRoleDao extends SimpleDao<Long, Role> implements RoleDao {

	public SimpleRoleDao() {
    super(Role.class);
  }

	@Override
  public Role getByAuthority(String authority) throws EntityNotFoundException {
    return (Role) sf.getCurrentSession()
        .createQuery("from Role where authority = '" + authority + "'").uniqueResult();
  }
}
