package com.labtekv.educ8.dao.impl;

import com.labtekv.educ8.dao.TuitionPaymentDao;
import com.labtekv.educ8.model.TuitionPayment;

import org.springframework.stereotype.Repository;

/**
 *
 * @author Andika
 */
@Repository
public class SimpleTuitionPaymentDao extends SimpleDao<Long, TuitionPayment> implements TuitionPaymentDao {
  public SimpleTuitionPaymentDao(){
    super(TuitionPayment.class);
  }
}
