package com.labtekv.educ8.dao.impl;

import java.util.List;

import com.labtekv.educ8.dao.FromScheduleDao;
import com.labtekv.educ8.model.Clazz;
import com.labtekv.educ8.model.Schedule;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;

import org.springframework.stereotype.Repository;

/**
 *
 * @author Iqbal Djulfri
 */
@Repository
public class SimpleFromScheduleDao extends SimpleDao<Long, Schedule> implements FromScheduleDao {

  public SimpleFromScheduleDao() {
    super(Schedule.class);
  }

  private List initializeList(List objects) {
    for (Object object : objects) {
      Hibernate.initialize(object);
    }
    return objects;
  }

  @Override
  public List<Clazz> getClazz() {
    return getClazz(null, null);
  }

  @Override
  public List<Clazz> getClazz(List<Criterion> criterions, List<Order> orders) {
    return getClazz(criterions, orders, 0);
  }

  @Override
  public List<Clazz> getClazz(List<Criterion> criterions, List<Order> orders, int page) {
    return getClazz(criterions, orders, page, FETCH_SIZE);
  }

  @Override
  public List<Clazz> getClazz(List<Criterion> criterions, List<Order> orders, int page, int fetchSize) {
    Criteria c = sf.getCurrentSession().createCriteria(type).createAlias("clazz", "clazz")
        .setProjection(Projections.distinct(Projections.property("clazz")));
    if (criterions != null) for (Criterion cx : criterions) c.add(cx);
    if (orders != null) for (Order o : orders) c.addOrder(o);
    if (page > 0) {
      c.setMaxResults(fetchSize);
      c.setFirstResult(page * fetchSize - fetchSize);
    }
    return initializeList(c.list());
  }

  @Override
  public List<Clazz> getClazzJoinCourse() {
    return getClazzJoinCourse(null, null);
  }

  @Override
  public List<Clazz> getClazzJoinCourse(List<Criterion> criterions, List<Order> orders) {
    return getClazzJoinCourse(criterions, orders, 0);
  }

  @Override
  public List<Clazz> getClazzJoinCourse(List<Criterion> criterions, List<Order> orders, int page) {
    return getClazzJoinCourse(criterions, orders, page, FETCH_SIZE);
  }

  @Override
  public List<Clazz> getClazzJoinCourse(List<Criterion> criterions, List<Order> orders, int page, int fetchSize) {
   Criteria c = sf.getCurrentSession().createCriteria(type).createAlias("course", "course").createAlias("clazz", "clazz")
        .setProjection(Projections.distinct(Projections.property("clazz")));
    if (criterions != null) for (Criterion cx : criterions) c.add(cx);
    if (orders != null) for (Order o : orders) c.addOrder(o);
    if (page > 0) {
      c.setMaxResults(fetchSize);
      c.setFirstResult(page * fetchSize - fetchSize);
    }
    return initializeList(c.list());
  }


}
