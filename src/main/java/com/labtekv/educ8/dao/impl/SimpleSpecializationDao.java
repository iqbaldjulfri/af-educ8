package com.labtekv.educ8.dao.impl;

import com.labtekv.educ8.dao.SpecializationDao;
import com.labtekv.educ8.model.Specialization;

import org.springframework.stereotype.Repository;

/**
 *
 * @author Iqbal Djulfri
 */
@Repository
public class SimpleSpecializationDao extends SimpleDao<Long, Specialization> implements SpecializationDao {

  public SimpleSpecializationDao() {
    super(Specialization.class);
  }

}
