package com.labtekv.educ8.dao.impl;

import com.labtekv.educ8.dao.CourseDao;
import com.labtekv.educ8.model.Course;

import org.springframework.stereotype.Repository;

/**
 *
 * @author Iqbal Djulfri
 */
@Repository
public class SimpleCourseDao extends SimpleDao<Long, Course> implements CourseDao {
  public SimpleCourseDao() {
    super(Course.class);
  }
}
