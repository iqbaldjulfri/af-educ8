package com.labtekv.educ8.dao.impl;


import com.labtekv.educ8.dao.MessageDao;
import com.labtekv.educ8.model.Message;

import org.springframework.stereotype.Repository;

/**
 *
 * @author Iqbal Djulfri
 */
@Repository("simpleMessageDao")
public class SimpleMessageDao extends SimpleDao<Long, Message> implements MessageDao {
  public SimpleMessageDao() {
    super(Message.class);
  }
}
