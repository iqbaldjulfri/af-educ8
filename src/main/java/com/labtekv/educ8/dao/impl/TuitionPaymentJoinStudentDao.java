/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.labtekv.educ8.dao.impl;

import com.labtekv.educ8.model.TuitionPayment;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Andika
 */
@Repository
public class TuitionPaymentJoinStudentDao extends SimpleTuitionPaymentDao{
  @Override
  public List<TuitionPayment> getAll() {
    return get(null, null, 0);
  }

  @Override
  public List<TuitionPayment> get(List<Criterion> criterions, List<Order> orders, int page) {
    return get(criterions, orders, page, FETCH_SIZE);
  }
  
  @Override
  public List<TuitionPayment> get(List<Criterion> criterions, List<Order> orders, int page, int fetchSize) {
    Criteria c = sf.getCurrentSession().createCriteria(type).createAlias("student", "student");
    if (criterions != null) for (Criterion cx : criterions) c.add(cx);
    if (orders != null) for (Order o : orders) c.addOrder(o);
    if (page > 0) {
      c.setMaxResults(fetchSize);
      c.setFirstResult(page * fetchSize - fetchSize);
    }
    return c.list();
  }
  
  @Override
  public long countAll() {
    return count(null);
  }

  @Override
  public long count(List<Criterion> criterions) {
    Criteria c = sf.getCurrentSession().createCriteria(type).createAlias("student", "student").setProjection(Projections.rowCount());
    if (criterions != null) for (Criterion cx : criterions) c.add(cx);
    return (long) c.uniqueResult();
  }
}
