package com.labtekv.educ8.dao.impl;

import com.labtekv.educ8.dao.ExamDao;
import com.labtekv.educ8.model.Exam;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Iqbal Djulfri
 */
@Repository
public class SimpleExamDao extends SimpleDao<Long, Exam> implements ExamDao {

  public SimpleExamDao() {
    super(Exam.class);
  }
  
}
