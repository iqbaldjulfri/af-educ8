
package com.labtekv.educ8.dao.impl;

import java.io.Serializable;
import java.util.List;

import com.labtekv.educ8.ext.hibernate.EntityExistsException;
import com.labtekv.educ8.ext.hibernate.EntityNotFoundException;
import com.labtekv.educ8.dao.BaseDao;
import com.labtekv.educ8.model.BaseModel;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;

import org.springframework.beans.factory.annotation.Autowired;


/**
 *
 * @author Iqbal Djulfri
 */
public class SimpleDao<ID extends Serializable, MODEL extends BaseModel> implements BaseDao<ID, MODEL> {

  @Autowired
  protected SessionFactory sf;
  protected Class type;

  public SimpleDao(Class type) {
    this.type = type;
  }

  public void setSf(SessionFactory sf) {
    this.sf = sf;
  }

  @Override
  public MODEL get(ID id) throws EntityNotFoundException {
    return (MODEL) sf.getCurrentSession().get(type, id);
  }

  @Override
  public List<MODEL> getAll() {
    return get(null, null, 0);
  }

  @Override
  public List<MODEL> get(List<Criterion> criterions, List<Order> orders, int page) {
    return get(criterions, orders, page, FETCH_SIZE);
  }

  @Override
  public List<MODEL> get(List<Criterion> criterions, List<Order> orders, int page, int fetchSize) {
    Criteria c = sf.getCurrentSession().createCriteria(type);
    if (criterions != null) for (Criterion cx : criterions) c.add(cx);
    if (orders != null) for (Order o : orders) c.addOrder(o);
    if (page > 0) {
      c.setMaxResults(fetchSize);
      c.setFirstResult(page * fetchSize - fetchSize);
    }
    return c.list();
  }

  @Override
  public long countAll() {
    return count(null);
  }

  @Override
  public long count(List<Criterion> criterions) {
    Criteria c = sf.getCurrentSession().createCriteria(type).setProjection(Projections.rowCount());
    if (criterions != null) for (Criterion cx : criterions) c.add(cx);
    return (long) c.uniqueResult();
  }

  @Override
  public ID save(MODEL model) throws EntityExistsException {
    return (ID) sf.getCurrentSession().save(model);
  }

  @Override
  public void update(MODEL model) {
    sf.getCurrentSession().saveOrUpdate(model);
  }

  @Override
  public void delete(ID id) throws EntityNotFoundException {
    MODEL m = (MODEL) sf.getCurrentSession().get(type, id);
    delete(m);
  }

  @Override
  public void delete(MODEL model) {
    sf.getCurrentSession().delete(model);
  }

}
