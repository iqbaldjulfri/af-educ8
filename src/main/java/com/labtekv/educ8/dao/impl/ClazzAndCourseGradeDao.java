package com.labtekv.educ8.dao.impl;

import java.util.ArrayList;
import java.util.List;

import com.labtekv.educ8.dao.BaseDao;
import com.labtekv.educ8.model.Clazz;
import com.labtekv.educ8.model.Course;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Iqbal Djulfri
 */
@Repository
public class ClazzAndCourseGradeDao {

  @Autowired
  private SessionFactory sf;

  public void setSf(SessionFactory sf) {
    this.sf = sf;
  }

  public List<String> getByNameAndSchool(String name, long schoolId) {
    List grades = new ArrayList();
    Criterion nm = Restrictions.ilike("grade", name, MatchMode.ANYWHERE),
        sc = Restrictions.eq("school.id", schoolId);
    Criteria c;

    c = sf.getCurrentSession().createCriteria(Clazz.class)
        .setProjection(Projections.distinct(Projections.property("grade")));
    c.add(nm).add(sc).addOrder(Order.asc("grade"));
    grades.addAll(c.list());

    c = sf.getCurrentSession().createCriteria(Course.class)
        .setProjection(Projections.distinct(Projections.property("grade")));
    c.add(nm).add(sc).addOrder(Order.asc("grade"));
    List<String> temp = c.list();
    for (String grade : temp) {
      if (!grades.contains(grade)) grades.add(grade);
    }

    return grades.size() > BaseDao.FETCH_SIZE ? grades.subList(0, BaseDao.FETCH_SIZE) : grades;
  }
}
