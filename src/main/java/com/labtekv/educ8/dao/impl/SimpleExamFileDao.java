package com.labtekv.educ8.dao.impl;

import com.labtekv.educ8.dao.ExamFileDao;
import com.labtekv.educ8.model.ExamFile;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Iqbal Djulfri
 */
@Repository
public class SimpleExamFileDao extends SimpleDao<Long, ExamFile> implements ExamFileDao {

  public SimpleExamFileDao() {
    super(ExamFile.class);
  }
  
}
