package com.labtekv.educ8.dao.impl;

import com.labtekv.educ8.dao.ExamTypeDao;
import com.labtekv.educ8.model.ExamType;

import org.springframework.stereotype.Repository;

/**
 *
 * @author Iqbal Djulfri
 */
@Repository
public class SimpleExamTypeDao extends SimpleDao<Long, ExamType> implements ExamTypeDao {
  public SimpleExamTypeDao(){
    super(ExamType.class);
  }
}
