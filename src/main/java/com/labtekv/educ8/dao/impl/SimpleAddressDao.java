package com.labtekv.educ8.dao.impl;

import com.labtekv.educ8.dao.AddressDao;
import com.labtekv.educ8.model.Address;

import org.springframework.stereotype.Repository;

/**
 *
 * @author Iqbal Djulfri
 */
@Repository("simpleAddressDao")
public class SimpleAddressDao extends SimpleDao<Long, Address> implements AddressDao {

  public SimpleAddressDao() {
    super(Address.class);
  }

}
