/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.labtekv.educ8.dao.impl;

import com.labtekv.educ8.dao.StudentDao;
import com.labtekv.educ8.model.School;
import com.labtekv.educ8.model.Student;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Andika
 */

@Repository("simpleStudentDao")
public class SimpleStudentDao extends SimpleDao<Long, Student> implements StudentDao {

  public SimpleStudentDao() {
    super(Student.class);
  }

  @Override
  public List<Student> getBySchool(List<Criterion> criterions, List<Order> orders, Collection<School> schools, int page) {
    Criteria c = sf.getCurrentSession().createCriteria(Student.class), c2;
    c.createAlias("school", "school", JoinType.LEFT_OUTER_JOIN);

    if (page >= 0) {
      c.setProjection(Projections.distinct(Projections.id()))
          .setFirstResult(page * FETCH_SIZE - FETCH_SIZE);
    }

    if (criterions != null) for (Criterion criterion : criterions) c.add(criterion);
    List ids = new ArrayList();
    if(schools != null){      
      for (School s : schools) ids.add(s.getId());    
      c.add(Restrictions.or(Restrictions.in("school.id", ids), Restrictions.isNull("school.id")));
    }

    ids = c.list();

    c2 = sf.getCurrentSession().createCriteria(Student.class);
    if (ids != null && !ids.isEmpty()) c2.add(Restrictions.in("id", ids));
    if (orders != null) for (Order order : orders) c2.addOrder(order);

    return c2.list();
  }

  @Override
  public List<Student> getAllBySchool(Collection<School> schools) {
    List os = new ArrayList();
    os.add(Order.asc("name"));
    return getBySchool(null, os, schools, 0);
  }
}
