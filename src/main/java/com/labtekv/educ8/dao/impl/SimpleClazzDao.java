package com.labtekv.educ8.dao.impl;

import com.labtekv.educ8.dao.ClazzDao;
import com.labtekv.educ8.model.Clazz;

import org.springframework.stereotype.Repository;

/**
 *
 * @author Iqbal Djulfri
 */
@Repository
public class SimpleClazzDao extends SimpleDao<Long, Clazz> implements ClazzDao {
  public SimpleClazzDao() {
    super(Clazz.class);
  }
}
