package com.labtekv.educ8.dao;

import com.labtekv.educ8.model.Course;

/**
 *
 * @author Iqbal Djulfri
 */
public interface CourseDao extends BaseDao<Long, Course> {
}

/**
 * clazz
 * exam type
 *
 * schedule
 *
 * student
 * parent
 *
 * attendance
 * exam
 * grade
 */