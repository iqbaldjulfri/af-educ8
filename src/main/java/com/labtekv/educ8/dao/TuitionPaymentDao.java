package com.labtekv.educ8.dao;

import com.labtekv.educ8.model.TuitionPayment;

/**
 *
 * @author Andika
 */
public interface TuitionPaymentDao extends BaseDao<Long, TuitionPayment> {

}
