package com.labtekv.educ8.ext.hibernate;

import com.labtekv.educ8.model.BaseModel;

/**
 *
 * @author Iqbal Djulfri
 */
public class EntityExistsException extends javax.persistence.EntityExistsException {

  public EntityExistsException(BaseModel entity) {
    super("entity: " + entity + " exists");
  }

}
