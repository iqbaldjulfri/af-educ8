package com.labtekv.educ8.ext.hibernate;

import com.labtekv.educ8.model.BaseModel;

/**
 *
 * @author Iqbal Djulfri
 */
public class EntityNotFoundException extends javax.persistence.EntityNotFoundException {

  public EntityNotFoundException(BaseModel entity) {
    super("entity: " + entity + " not found");
  }

}
