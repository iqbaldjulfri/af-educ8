package com.labtekv.educ8.ext.hibernate;

import org.hibernate.Hibernate;
import org.hibernate.proxy.HibernateProxy;
import org.springframework.stereotype.Component;

/**
 *
 * @author Iqbal Djulfri
 */
@Component
public class HibernateUtil {
  public Object initializeImplementation(Object o) {
    if (o instanceof HibernateProxy) {
      HibernateProxy proxy = (HibernateProxy) o;
      o = proxy.getHibernateLazyInitializer().getImplementation();
    }
    return o;
  }
}
