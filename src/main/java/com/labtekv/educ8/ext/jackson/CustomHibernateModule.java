package com.labtekv.educ8.ext.jackson;

import com.fasterxml.jackson.datatype.hibernate4.Hibernate4Module;
import com.fasterxml.jackson.databind.AnnotationIntrospector;

/**
 *
 * @author Iqbal Djulfri
 */
public class CustomHibernateModule extends Hibernate4Module {

  @Override
  public void setupModule(SetupContext context) {
    AnnotationIntrospector ai = annotationIntrospector();
    if (ai != null) {
      context.appendAnnotationIntrospector(ai);
    }
    context.addSerializers(new CustomHibernateSerializers(_moduleFeatures));
  }
}
