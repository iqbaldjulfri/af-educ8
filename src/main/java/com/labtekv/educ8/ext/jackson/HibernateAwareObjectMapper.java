package com.labtekv.educ8.ext.jackson;

import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.datatype.hibernate4.Hibernate4Module;
//import com.fasterxml.jackson.databind.SerializationConfig;


/**
 *
 * @author Iqbal Djulfri
 */
public class HibernateAwareObjectMapper extends ObjectMapper {
  public HibernateAwareObjectMapper() {
		Hibernate4Module hm = new Hibernate4Module();//new CustomHibernateModule();
		registerModule(hm);
//		configure(SerializationConfig.Feature.FAIL_ON_EMPTY_BEANS, false);
	}

	public void setPrettyPrint(boolean prettyPrint) {
//		configure(SerializationConfig.Feature.INDENT_OUTPUT, prettyPrint);
	}
}
