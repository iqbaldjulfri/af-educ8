package com.labtekv.educ8.ext.spring;

import com.labtekv.educ8.dao.UserDao;
import com.labtekv.educ8.model.User;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Iqbal Djulfri
 */
@Service
@Transactional(readOnly=true)
public class CustomUserDetailsService implements UserDetailsService {

  @Autowired
  private UserDao userDao;

  public void setUserDao(UserDao userDao) {
    this.userDao = userDao;
  }

  @Override
  public UserDetails loadUserByUsername(String string) throws UsernameNotFoundException {
    try {
      User user = userDao.getByUsername(string);
      Hibernate.initialize(user.getAuthorities());
      return user;
    } catch (UsernameNotFoundException e) {
      throw e;
    }
  }

}
