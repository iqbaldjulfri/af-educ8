package com.labtekv.educ8.ext.spring;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.labtekv.educ8.dao.MenuDao;
import com.labtekv.educ8.dao.UserDao;
import com.labtekv.educ8.ext.hibernate.HibernateUtil;
import com.labtekv.educ8.model.Menu;
import com.labtekv.educ8.model.User;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.filter.GenericFilterBean;

/**
 *
 * @author Iqbal Djulfri
 */
@Service("menuManagementFilter")
@Transactional
public class MenuManagementFilter extends GenericFilterBean {

  @Autowired @Qualifier("simpleMenuDao")
  private MenuDao menuDao;

  @Autowired @Qualifier("simpleUserDao")
  private UserDao userDao;

  @Autowired
  private HibernateUtil hibernateUtil;

  public void setHibernateUtil(HibernateUtil hibernateUtil) {
    this.hibernateUtil = hibernateUtil;
  }

  public void setMenuDao(MenuDao menuDao) {
    this.menuDao = menuDao;
  }

  public void setUserDao(UserDao userDao) {
    this.userDao = userDao;
  }

  @Override
  @Transactional(readOnly = true)
  public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
      throws IOException, ServletException {
    HttpServletRequest request = (HttpServletRequest) req;
    HttpServletResponse response = (HttpServletResponse) res;

    String username;
    User user;
    Set roles;
    try {
      username = request.getUserPrincipal().getName();
    } catch (Exception e) {
      username = null;
    }

    logger.debug("username: " + username);

    if (username != null) {
      user = userDao.getByUsername(username);
      Hibernate.initialize(user.getAuthorities());
      roles = (Set) hibernateUtil.initializeImplementation(user.getAuthorities());
    } else {
      user = null;
      roles = new HashSet();
    }

    List<Menu> menuItems = menuDao.getForDisplay(roles);
    for (Menu menuItem : menuItems) getMenuChildren(menuItem);
    req.setAttribute("MENU", menuItems);
    req.setAttribute("USER", user);
    req.setAttribute("ROLES", roles);

    chain.doFilter(request, response);
  }

  private void getMenuChildren(Menu menu) {
    List<Menu> children = menu.getChildren();
    if (children != null && !children.isEmpty())
      for (Menu child : children) getMenuChildren(child);
  }
}
