package com.labtekv.educ8;

/**
 *
 * @author Iqbal Djulfri
 */
public class EntityNotSavedException extends Exception {

  public EntityNotSavedException(String message) {
    super(message);
  }
  
}
