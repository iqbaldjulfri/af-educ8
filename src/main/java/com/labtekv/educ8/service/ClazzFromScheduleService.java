package com.labtekv.educ8.service;

import java.util.List;

import com.labtekv.educ8.model.Clazz;
import com.labtekv.educ8.model.Course;
import com.labtekv.educ8.model.Semester;
import com.labtekv.educ8.model.Teacher;

/**
 *
 * @author Iqbal Djulfri
 */
public interface ClazzFromScheduleService {
  public List<Clazz> get(Teacher t, Semester s);
  public List<Clazz> get(Teacher t, Semester s, int page);
  public List<Clazz> get(Teacher t, Semester s, Course c);
  public List<Clazz> get(Teacher t, Semester s, Course c, int page);
}
