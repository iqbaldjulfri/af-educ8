package com.labtekv.educ8.service;

import java.util.Collection;
import java.util.List;

import com.labtekv.educ8.model.Menu;
import com.labtekv.educ8.model.Role;

/**
 *
 * @author Iqbal Djulfri
 */
public interface MenuService {
  public Menu getById(long id);
  public List<Menu> getByName(String name, int page);
  public List<Menu> getByRoles(Collection<Role> roles);
  public void create(Menu menu);
  public void update(Menu menu);
  public void delete(Menu menu);
  public void deleteById(long id);

  public List<Menu> getAll();
}
