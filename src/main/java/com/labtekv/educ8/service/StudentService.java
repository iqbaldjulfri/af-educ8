/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.labtekv.educ8.service;

import com.labtekv.educ8.model.School;
import com.labtekv.educ8.model.Student;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Andika
 */
public interface StudentService {
  public Student getById(long id);
  
  public List<Student> getByName(String name, int page);
  public List<Student> getByNameNumber(String name, String number, int page);
  public List<Student> getByName(String name, Collection<School> schools, int page);
  public List<Student> getByNameNumber(String name, String number, Collection<School> schools, int page);
  public List<Student> getAll();
  public List<Student> getAll(Collection<School> schools);
  public long getPageCountByName(String name);
  public long getPageCountByNameNumber(String name, String number);
  public long getPageCountByName(String name, Collection<School> schools, int page);
  public long getPageCountByNameNumber(String name, String number, Collection<School> schools, int page);

  public long create(Student student);
  public void update(Student student);
  public void delete(Student student);
  public void deleteById(long id);
}
