package com.labtekv.educ8.service;

import java.util.Collection;
import java.util.List;

import com.labtekv.educ8.model.ExamType;
import com.labtekv.educ8.model.School;

/**
 *
 * @author Iqbal Djulfri
 */
public interface ExamTypeService {
  public ExamType getById(long id);
  public List<ExamType> getAll();
  public List<ExamType> getAll(Collection<School> schools);
  public List<ExamType> getByNameOrCode(String name, String code, int page);
  public List<ExamType> getByNameOrCode(String name, String code, Collection<School> schools, int page);

  public long create(ExamType et);
  public void update(ExamType et);
  public void delete(ExamType et);
  public void deleteById(long id);
}
