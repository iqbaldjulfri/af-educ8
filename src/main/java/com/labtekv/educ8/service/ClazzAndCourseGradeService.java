package com.labtekv.educ8.service;

import java.util.List;

/**
 *
 * @author Iqbal Djulfri
 */
public interface ClazzAndCourseGradeService {
  public List getClazzAndCourseGrade(String name, long schoolId);
}
