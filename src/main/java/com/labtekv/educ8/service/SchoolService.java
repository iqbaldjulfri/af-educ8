package com.labtekv.educ8.service;

import java.util.List;

import com.labtekv.educ8.model.School;

/**
 *
 * @author Iqbal Djulfri
 */
public interface SchoolService {

  public School getById(long id);
  public List getByName(String name, int page);
  public long getPageCountByName(String name);
  public List getAll();

  public long create(School school);
  public void update(School school);
  public void delete(School school);
  public void deleteById(long id);

}
