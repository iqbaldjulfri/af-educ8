package com.labtekv.educ8.service;

import java.util.List;

import com.labtekv.educ8.model.User;

/**
 *
 * @author Iqbal Djulfri
 */
public interface UserService {
  public User getById(Long id);
  public User getByUsername(String username);
  public List<User> getByName(String name, int page);
  public List<User> getByNameOrUsername(String name, String username, int page);
  public long countByName(String name);
  public void create(User user);
  public void update(User user, String newPass);
  public void delete(User user);
  public void deleteById(long id);
  public void changePassword(User user, String newPassword);
}
