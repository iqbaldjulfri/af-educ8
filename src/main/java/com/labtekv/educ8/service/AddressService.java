package com.labtekv.educ8.service;

import com.labtekv.educ8.model.Address;

/**
 *
 * @author Iqbal Djulfri
 */
public interface AddressService {
  public Address getById(long id);

  public long create(Address address);
  public void update(Address address);
  public void delete(Address address);
  public void deleteById(long id);
}
