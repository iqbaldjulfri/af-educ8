package com.labtekv.educ8.service.impl;

import com.labtekv.educ8.dao.ExamDao;
import com.labtekv.educ8.model.Exam;
import com.labtekv.educ8.service.ExamService;
import java.util.ArrayList;
import java.util.List;

import com.labtekv.educ8.model.Course;

import com.labtekv.educ8.model.Semester;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Iqbal Djulfri
 */
@Service
@Transactional
public class SimpleExamService implements ExamService {

  @Autowired
  @Qualifier("simpleExamDao")
  private ExamDao examDao;

  public void setExamDao(ExamDao examDao) {
    this.examDao = examDao;
  }

  @Override
  public Exam getById(long id) {
    return examDao.get(id);
  }

  @Override
  public List<Exam> getAll() {
    return examDao.getAll();
  }

  @Override
  public List<Exam> getAll(Course c, Semester s) {
    List cs = new ArrayList(), os = new ArrayList();
    cs.add(Restrictions.eq("course.id", c.getId()));
    cs.add(Restrictions.eq("semester.id", s.getId()));
    os.add(Order.desc("start"));
    return examDao.get(cs, os, 0);
  }

  @Override
  public List<Exam> getByName(String name, int page) {
    List cs = new ArrayList(), os = new ArrayList();
    cs.add(Restrictions.ilike("name", name, MatchMode.ANYWHERE));
    os.add(Order.asc("name"));
    return examDao.get(cs, os, page);
  }

  @Override
  public long create(Exam exam) {
    return examDao.save(exam);
  }

  @Override
  public void update(Exam exam) {
    examDao.update(exam);
  }

  @Override
  public void delete(Exam exam) {
    examDao.delete(exam);
  }

  @Override
  public void deleteById(long id) {
    examDao.delete(id);
  }

}
