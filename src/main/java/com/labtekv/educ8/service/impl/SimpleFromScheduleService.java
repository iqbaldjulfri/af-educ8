package com.labtekv.educ8.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.labtekv.educ8.dao.FromScheduleDao;
import com.labtekv.educ8.model.Clazz;
import com.labtekv.educ8.model.Course;
import com.labtekv.educ8.model.Semester;
import com.labtekv.educ8.model.Teacher;
import com.labtekv.educ8.service.ClazzFromScheduleService;

import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Iqbal Djulfri
 */
@Service
@Transactional(readOnly=true)
public class SimpleFromScheduleService implements ClazzFromScheduleService {

  @Autowired
  @Qualifier("simpleFromScheduleDao")
  private FromScheduleDao dao;

  public void setDao(FromScheduleDao dao) {
    this.dao = dao;
  }

  @Override
  public List<Clazz> get(Teacher t, Semester s) {
    return get(t, s, 0);
  }

  @Override
  public List<Clazz> get(Teacher t, Semester s, int page) {
    List cs = new ArrayList(), os = new ArrayList();
    cs.add(Restrictions.eq("teacher.id", t.getId()));
    cs.add(Restrictions.eq("semester.id", s.getId()));
    os.add(Order.asc("clazz.name"));
    return dao.getClazz(cs, os, page);
  }

  @Override
  public List<Clazz> get(Teacher t, Semester s, Course c) {
    return get(t, s, c, 0);
  }

  @Override
  public List<Clazz> get(Teacher t, Semester s, Course c, int page) {
    List cs = new ArrayList(), os = new ArrayList();
    cs.add(Restrictions.eq("teacher.id", t.getId()));
    cs.add(Restrictions.eq("semester.id", s.getId()));
    cs.add(Restrictions.eq("course.id", c.getId()));
    os.add(Order.asc("clazz.name"));
    return dao.getClazz(cs, os, page);
  }

}
