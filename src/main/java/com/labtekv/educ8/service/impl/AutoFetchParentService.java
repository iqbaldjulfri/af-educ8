/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.labtekv.educ8.service.impl;

import com.labtekv.educ8.model.Parent;
import java.util.List;
import org.hibernate.Hibernate;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Andika
 */
@Transactional(readOnly = true)
public class AutoFetchParentService extends SimpleParentService {
  private List<String> fetchAttributes;

  public AutoFetchParentService(List toBeFetchedAttributes) {
    this.fetchAttributes = toBeFetchedAttributes;
  }
  
  @Override
  public List<Parent> getAll() {
    List<Parent> parents = super.getAll();
    for (Parent parent : parents) fetchParent(parent);
    return parents;
  }
  
  @Override
  public Parent getById(long id) {
    Parent parent = super.getById(id);
    fetchParent(parent);
    return parent;
  }

  @Override
  public List<Parent> getByName(String name, int page) {
    List<Parent> parents = super.getByName(name, page);
    for (Parent parent : parents) {
      fetchParent(parent);
    }

    return parents;
  }

  private void fetchParent(Parent parent) {    
    if (fetchAttributes.contains("all")) {
      Hibernate.initialize(parent.getAddress());
    } else {
      if (fetchAttributes.contains("address")) Hibernate.initialize(parent.getAddress());
    }
  }
}
