/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.labtekv.educ8.service.impl;

import com.labtekv.educ8.dao.AddressDao;
import com.labtekv.educ8.dao.TeacherDao;
import com.labtekv.educ8.model.School;
import com.labtekv.educ8.model.Teacher;
import com.labtekv.educ8.service.TeacherService;
import com.labtekv.educ8.util.PageCounter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Andika
 */
@Service("simpleTeacherService")
@Transactional
public class SimpleTeacherService implements TeacherService {
  
  @Autowired @Qualifier("simpleTeacherDao")
  private TeacherDao teacherDao;

  @Autowired @Qualifier("simpleAddressDao")
  private AddressDao addressDao;

  @Autowired
  private PageCounter pageCounter;

  public void setAddressDao(AddressDao addressDao) {
    this.addressDao = addressDao;
  }

  public void setPageCounter(PageCounter pageCounter) {
    this.pageCounter = pageCounter;
  }

  public void setTeacherDao(TeacherDao teacherDao) {
    this.teacherDao = teacherDao;
  }

  @Override
  public Teacher getById(long id) {
    Teacher teacher = teacherDao.get(id);
    return teacher;
  }

  @Override
  public List<Teacher> getAll() {
    return teacherDao.getAll();
  }

  @Override
  public List<Teacher> getAll(Collection<School> schools) {
    return teacherDao.getAllBySchool(schools);
  }
  
  @Override
  public List<Teacher> getByName(String name, int page) {
    List cs = new ArrayList();
    cs.add(Restrictions.ilike("name", name, MatchMode.ANYWHERE));

    List os = new ArrayList();
    os.add(Order.asc("name"));

    List<Teacher> teachers;
    teachers = teacherDao.get(cs, null, page);
    return teachers;
  }
  
  @Override
  public List<Teacher> getByName(String name, Collection<School> schools, int page) {
    List cs = new ArrayList(), os = new ArrayList();
    cs.add(Restrictions.ilike("name", name, MatchMode.ANYWHERE));
    os.add(Order.asc("name"));
    return teacherDao.getBySchool(cs, os, schools, page);
  }

  @Override
  public long getPageCountByName(String name) {
    List cs = new ArrayList();
    cs.add(Restrictions.ilike("name", name, MatchMode.ANYWHERE));

    return pageCounter.getMaxPage(teacherDao.count(cs));
  }

  @Override
  public long create(Teacher teacher) {
    addressDao.save(teacher.getAddress());
    return teacherDao.save(teacher);
  }

  @Override
  public void update(Teacher teacher) {
    addressDao.update(teacher.getAddress());
    teacherDao.update(teacher);
  }

  @Override
  public void delete(Teacher teacher) {
    teacherDao.delete(teacher);
  }

  @Override
  public void deleteById(long id) {
    teacherDao.delete(id);
  }

  @Override
  public long getPageCountByName(String name, Collection<School> schools, int page) {
    throw new UnsupportedOperationException("Not supported yet.");
  }
}
