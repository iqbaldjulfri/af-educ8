package com.labtekv.educ8.service.impl;

import java.util.List;

import com.labtekv.educ8.model.Message;

import org.hibernate.Hibernate;

import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Iqbal Djulfri
 */
@Transactional(readOnly=true)
public class AutoFetchMessageService extends SimpleMessageService {
  private List fetchAttributes;

  public AutoFetchMessageService(List fetchAttributes) {
    this.fetchAttributes = fetchAttributes;
  }

  @Override
  public Message getById(long id) {
    Message m = super.getById(id);
    fetch(m);
    return m;
  }

  @Override
  public List<Message> getAll() {
    List<Message> ms = super.getAll();
    for (Message m : ms) fetch(m);
    return ms;
  }

  @Override
  public List<Message> getByUserId(long userId, Boolean deleted, int page) {
    return getByUserId(userId, null, deleted, page);
  }

  @Override
  public List<Message> getByUserId(long userId, String query, Boolean deleted, int page) {
    List<Message> ms = super.getByUserId(userId, query, deleted, page);
    for (Message m : ms) fetch(m);
    return ms;
  }

  private void fetch(Message m) {
    if (fetchAttributes.contains("all")) {
      Hibernate.initialize(m.getSender());
      Hibernate.initialize(m.getUser());
    } else {
      if (fetchAttributes.contains("sender")) Hibernate.initialize(m.getSender());
      if (fetchAttributes.contains("user")) Hibernate.initialize(m.getUser());
    }
  }
}
