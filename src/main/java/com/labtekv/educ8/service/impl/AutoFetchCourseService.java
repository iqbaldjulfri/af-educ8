package com.labtekv.educ8.service.impl;

import java.util.Collection;
import java.util.List;

import com.labtekv.educ8.model.Course;
import com.labtekv.educ8.model.School;

import org.hibernate.Hibernate;

import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Iqbal Djulfri
 */
@Transactional(readOnly=true)
public class AutoFetchCourseService extends SimpleCourseService {

  private List fetchAttributes;

  public AutoFetchCourseService(List fetchAttributes) {
    this.fetchAttributes = fetchAttributes;
  }

  @Override
  public List<Course> getAll() {
    return getByName(null, null, 0);
  }

  @Override
  public List<Course> getAll(Collection<School> schools) {
    return getByName(null, schools, 0);
  }

  @Override
  public Course getById(long id) {
    Course c = super.getById(id);
    fetch(c);
    return c;
  }

  @Override
  public List<Course> getByName(String name, int page) {
    return getByName(name, null, page);
  }

  @Override
  public List<Course> getByName(String name, Collection<School> schools, int page) {
    List<Course> courses = super.getByName(name, schools, page);
    for (Course course : courses) fetch(course);
    return courses;
  }

  private void fetch(Course c) {
    if (fetchAttributes.contains("all")) {
      Hibernate.initialize(c.getSchool());
      Hibernate.initialize(c.getTeachers());
    } else {
      if (fetchAttributes.contains("school")) Hibernate.initialize(c.getSchool());
      if (fetchAttributes.contains("teachers")) Hibernate.initialize(c.getTeachers());
    }
  }

}
