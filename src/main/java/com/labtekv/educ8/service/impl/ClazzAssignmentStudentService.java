package com.labtekv.educ8.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.labtekv.educ8.dao.StudentDao;
import com.labtekv.educ8.model.Clazz;
import com.labtekv.educ8.model.School;
import com.labtekv.educ8.model.Student;

import org.hibernate.Hibernate;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Iqbal Djulfri
 */
@Service
@Transactional(readOnly = true)
public class ClazzAssignmentStudentService extends AutoFetchStudentService {

  private static final List fetchAttributes;

  @Autowired @Qualifier("simpleStudentDao")
  private StudentDao studentDao;

  static {
    fetchAttributes = new ArrayList();
    fetchAttributes.add("school");
    fetchAttributes.add("clazz");
  }

  public ClazzAssignmentStudentService() {
    super(fetchAttributes);
  }

  public List<Student> getByNameOrSchoolOrClazz(String name, School school,
      Clazz clazz, int page) {
    return getByNameOrSchoolOrClazz(name, school, clazz, null, page);
  }

  public List<Student> getByNameOrSchoolOrClazz(String name, School school,
      Clazz clazz, Collection<School> accessibleSchools, int page) {
    List cs = new ArrayList(), os = new ArrayList();

    if (name != null) cs.add(Restrictions.ilike("name", name, MatchMode.ANYWHERE));
    if (school != null) cs.add(Restrictions.eq("school.id", school.getId()));
    if (clazz != null) cs.add(Restrictions.eq("clazz.id", clazz.getId()));
    if (accessibleSchools != null && !accessibleSchools.isEmpty()) {
      List ids = new ArrayList();
      for (School s : accessibleSchools) ids.add(s.getId());
      cs.add(Restrictions.in("school.id", ids));
    }

    os.add(Order.asc("school.id"));
    os.add(Order.asc("clazz.id"));
    os.add(Order.asc("name"));

    List<Student> res = studentDao.get(cs, os, page);
    for (Student s : res) {
      Hibernate.initialize(s.getSchool());
    }

    return res;
  }

}
