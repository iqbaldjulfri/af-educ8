/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.labtekv.educ8.service.impl;

import com.labtekv.educ8.dao.AddressDao;
import com.labtekv.educ8.dao.ParentDao;
import com.labtekv.educ8.dao.StudentDao;
import com.labtekv.educ8.model.School;
import com.labtekv.educ8.model.Student;
import com.labtekv.educ8.service.StudentService;
import com.labtekv.educ8.util.PageCounter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Andika
 */
@Service("simpleStudentService")
@Transactional
public class SimpleStudentService implements StudentService {
  @Autowired @Qualifier("simpleStudentDao")
  private StudentDao studentDao;

  @Autowired @Qualifier("simpleAddressDao")
  private AddressDao addressDao;
  
  @Autowired @Qualifier("simpleParentDao")
  private ParentDao parentDao;

  @Autowired
  private PageCounter pageCounter;

  public void setAddressDao(AddressDao addressDao) {
    this.addressDao = addressDao;
  }
  
  public void setParentDao(ParentDao parentDao) {
    this.parentDao = parentDao;
  }

  public void setPageCounter(PageCounter pageCounter) {
    this.pageCounter = pageCounter;
  }

  public void setStudentDao(StudentDao studentDao) {
    this.studentDao = studentDao;
  }

  @Override
  public Student getById(long id) {
    Student student = studentDao.get(id);
    return student;
  }

  @Override
  public List<Student> getAll() {
    return studentDao.getAll();
  }
  
  @Override
  public List<Student> getAll(Collection<School> schools) {
    return studentDao.getAllBySchool(schools);
  }
  
  @Override
  public List<Student> getByName(String name, int page) {
    List cs = new ArrayList();
    cs.add(Restrictions.ilike("name", name, MatchMode.ANYWHERE));

    List os = new ArrayList();
    os.add(Order.asc("name"));

    List<Student> students;
    students = studentDao.get(cs, null, page);
    return students;
  }
  
  @Override
  public List<Student> getByName(String name, Collection<School> schools, int page) {
    List cs = new ArrayList(), os = new ArrayList();
    cs.add(Restrictions.ilike("name", name, MatchMode.ANYWHERE));
    os.add(Order.asc("name"));
    return studentDao.getBySchool(cs, os, schools, page);
  }

  @Override
  public long getPageCountByName(String name) {
    List cs = new ArrayList();
    cs.add(Restrictions.ilike("name", name, MatchMode.ANYWHERE));

    return pageCounter.getMaxPage(studentDao.count(cs));
  }
  
  @Override
  public List<Student> getByNameNumber(String name, String number, int page) {
    return getByNameNumber(name, number, null, page);
  }
  
  @Override
  public List<Student> getByNameNumber(String name, String number, Collection<School> schools, int page) {
    List cs = new ArrayList(), os = new ArrayList();
    cs.add(Restrictions.ilike("name", name, MatchMode.ANYWHERE));
    cs.add(Restrictions.ilike("idNumber", number, MatchMode.ANYWHERE));
    os.add(Order.asc("name"));
    return studentDao.getBySchool(cs, os, schools, page);
  }

  @Override
  public long getPageCountByNameNumber(String name, String number) {
    List cs = new ArrayList();
    cs.add(Restrictions.ilike("name", name, MatchMode.ANYWHERE));
    cs.add(Restrictions.ilike("idNumber", number, MatchMode.ANYWHERE));

    return pageCounter.getMaxPage(studentDao.count(cs));
  }

  @Override
  public long create(Student student) {
    addressDao.save(student.getAddress());
    if(student.getFather()!=null){
      parentDao.save(student.getFather());
    }
    if(student.getMother()!=null){
      parentDao.save(student.getMother());
    }
    if(student.getGuardian()!=null){
      parentDao.save(student.getGuardian());
    }
    return studentDao.save(student);
  }

  @Override
  public void update(Student student) {
    addressDao.update(student.getAddress());
    parentDao.update(student.getFather());
    parentDao.update(student.getMother());
    parentDao.update(student.getGuardian());
    studentDao.update(student);
  }

  @Override
  public void delete(Student student) {
    studentDao.delete(student);
  }

  @Override
  public void deleteById(long id) {
    studentDao.delete(id);
  }
  
  @Override
  public long getPageCountByName(String name, Collection<School> schools, int page) {
    throw new UnsupportedOperationException("Not supported yet.");
  }
  
  @Override
  public long getPageCountByNameNumber(String name, String number, Collection<School> schools, int page) {
    throw new UnsupportedOperationException("Not supported yet.");
  }
}
