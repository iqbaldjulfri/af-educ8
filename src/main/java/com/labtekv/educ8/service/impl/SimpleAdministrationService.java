package com.labtekv.educ8.service.impl;

import com.labtekv.educ8.dao.AddressDao;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import com.labtekv.educ8.dao.AdministrationDao;
import com.labtekv.educ8.model.Administration;
import com.labtekv.educ8.model.School;
import com.labtekv.educ8.service.AdministrationService;
import com.labtekv.educ8.util.PageCounter;

import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Iqbal Djulfri
 */
@Service
@Transactional
public class SimpleAdministrationService implements AdministrationService {

  @Autowired @Qualifier("simpleAdministrationDao")
  private AdministrationDao administrationDao;

  @Autowired @Qualifier("simpleAddressDao")
  private AddressDao addressDao;

  @Autowired
  private PageCounter pageCounter;

  public void setPageCounter(PageCounter pageCounter) {
    this.pageCounter = pageCounter;
  }

  public void setAddressDao(AddressDao addressDao) {
    this.addressDao = addressDao;
  }


  public void setAdministrationDao(AdministrationDao administrationDao) {
    this.administrationDao = administrationDao;
  }

  @Override
  public Administration getById(long id) {
    return administrationDao.get(id);
  }

  @Override
  public List<Administration> getAll() {
    return administrationDao.getAll();
  }

  @Override
  public List<Administration> getAll(Collection<School> schools) {
    return administrationDao.getAllBySchool(schools);
  }

  @Override
  public List<Administration> getByName(String name, int page) {
    List cs = new ArrayList(), os = new ArrayList();
    cs.add(Restrictions.ilike("name", name, MatchMode.ANYWHERE));
    os.add(Order.asc("name"));
    return administrationDao.get(cs, os, page);
  }

  @Override
  public List<Administration> getByName(String name, Collection<School> schools, int page) {
    List cs = new ArrayList(), os = new ArrayList();
    cs.add(Restrictions.ilike("name", name, MatchMode.ANYWHERE));
    os.add(Order.asc("name"));
    return administrationDao.getBySchool(cs, os, schools, page);
  }

  @Override
  public long create(Administration a) {
    addressDao.save(a.getAddress());
    return administrationDao.save(a);
  }

  @Override
  public void update(Administration a) {
    addressDao.update(a.getAddress());
    administrationDao.update(a);
  }

  @Override
  public void delete(Administration a) {
    administrationDao.delete(a);
  }

  @Override
  public void deleteById(long id) {
    administrationDao.delete(id);
  }

  @Override
  public long getPageCountByName(String name) {
    List cs = new ArrayList();
    cs.add(Restrictions.ilike("name", name, MatchMode.ANYWHERE));

    return pageCounter.getMaxPage(administrationDao.count(cs));
  }

  @Override
  public long getPageCountByName(String name, Collection<School> schools, int page) {
    throw new UnsupportedOperationException("Not supported yet.");
  }

}
