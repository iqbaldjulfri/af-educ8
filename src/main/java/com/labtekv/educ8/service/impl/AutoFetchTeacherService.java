/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.labtekv.educ8.service.impl;

import com.labtekv.educ8.model.School;
import com.labtekv.educ8.model.Teacher;
import java.util.Collection;
import java.util.List;
import org.hibernate.Hibernate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Andika
 */
@Transactional(readOnly = true)
public class AutoFetchTeacherService extends SimpleTeacherService {
  
  private List<String> fetchAttributes;

  public AutoFetchTeacherService(List toBeFetchedAttributes) {
    this.fetchAttributes = toBeFetchedAttributes;
  }
  
  @Override
  public List<Teacher> getAll() {
    List<Teacher> teachers = super.getAll();
    for (Teacher teacher : teachers) fetchTeacher(teacher);
    return teachers;
  }

  @Override
  public List<Teacher> getAll(Collection<School> schools) {
    List<Teacher> teachers = super.getAll(schools);
    for (Teacher teacher : teachers) fetchTeacher(teacher);
    return teachers;
  }
  
  @Override
  public Teacher getById(long id) {
    Teacher teacher = super.getById(id);
    fetchTeacher(teacher);
    return teacher;
  }

  @Override
  public List<Teacher> getByName(String name, int page) {
    List<Teacher> teachers = super.getByName(name, page);
    for (Teacher teacher : teachers) {
      fetchTeacher(teacher);
    }

    return teachers;
  }
  
  @Override
  public List<Teacher> getByName(String name, Collection<School> schools, int page) {
    List<Teacher> teachers = super.getByName(name, schools, page);
    for (Teacher teacher : teachers) fetchTeacher(teacher);
    return teachers;
  }

  private void fetchTeacher(Teacher teacher) {    
    if (fetchAttributes.contains("all")) {
      Hibernate.initialize(teacher.getAddress());
      Hibernate.initialize(teacher.getSchools());
    } else {
      if (fetchAttributes.contains("address")) Hibernate.initialize(teacher.getAddress());
      if (fetchAttributes.contains("schools")) Hibernate.initialize(teacher.getSchools());
    }
  }
}
