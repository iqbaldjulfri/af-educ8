package com.labtekv.educ8.service.impl;

import java.util.Collection;
import java.util.List;

import com.labtekv.educ8.model.Administration;
import com.labtekv.educ8.model.School;

import org.hibernate.Hibernate;

import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Iqbal Djulfri
 */
@Transactional(readOnly = true)
public class AutoFetchAdministrationService extends SimpleAdministrationService {

  private List fetchAttributes;

  public AutoFetchAdministrationService(List fetchAttributes) {
    this.fetchAttributes = fetchAttributes;
  }

  @Override
  public List<Administration> getAll() {
    List<Administration> administrations = super.getAll();
    for (Administration administration : administrations) fetch(administration);
    return administrations;
  }

  @Override
  public List<Administration> getAll(Collection<School> schools) {
    List<Administration> administrations = super.getAll(schools);
    for (Administration administration : administrations) fetch(administration);
    return administrations;
  }

  @Override
  public Administration getById(long id) {
    Administration administration = super.getById(id);
    fetch(administration);
    return administration;
  }

  @Override
  public List<Administration> getByName(String name, int page) {
    List<Administration> administrations = super.getByName(name, page);
    for (Administration administration : administrations) fetch(administration);
    return administrations;
  }

  @Override
  public List<Administration> getByName(String name, Collection<School> schools, int page) {
    List<Administration> administrations = super.getByName(name, schools, page);
    for (Administration administration : administrations) fetch(administration);
    return administrations;
  }

  private void fetch(Administration a) {
    if (fetchAttributes.contains("all")) {
      Hibernate.initialize(a.getSchools());
      Hibernate.initialize(a.getAddress());
    } else {
      if (fetchAttributes.contains("schools")) Hibernate.initialize(a.getSchools());
      if (fetchAttributes.contains("address")) Hibernate.initialize(a.getAddress());
    }
  }

}
