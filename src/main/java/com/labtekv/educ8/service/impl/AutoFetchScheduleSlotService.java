package com.labtekv.educ8.service.impl;

import java.util.Collection;
import java.util.List;

import com.labtekv.educ8.model.ScheduleSlot;
import com.labtekv.educ8.model.School;

import org.hibernate.Hibernate;

import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Iqbal Djulfri
 */
@Transactional(readOnly=true)
public class AutoFetchScheduleSlotService extends SimpleScheduleSlotService {
  private List fetchAttributes;

  public AutoFetchScheduleSlotService(List fetchAttributes) {
    this.fetchAttributes = fetchAttributes;
  }

  @Override
  public List<ScheduleSlot> getAll() {
    List<ScheduleSlot> scheduleSlots = super.getAll();
    for (ScheduleSlot scheduleSlot : scheduleSlots) fetch(scheduleSlot);
    return scheduleSlots;
  }

  @Override
  public List<ScheduleSlot> getAll(Collection<School> schools) {
    return getByDescription(null, schools, 0);
  }

  @Override
  public List<ScheduleSlot> getByDescription(String desc, int page) {
    return getByDescription(desc, null, page);
  }

  @Override
  public List<ScheduleSlot> getByDescription(String desc, Collection<School> schools, int page) {
    List<ScheduleSlot> scheduleSlots = super.getByDescription(desc, schools, page);
    for (ScheduleSlot scheduleSlot : scheduleSlots) fetch(scheduleSlot);
    return scheduleSlots;
  }

  @Override
  public ScheduleSlot getById(long id) {
    ScheduleSlot ss = super.getById(id);
    fetch(ss);
    return ss;
  }

  private void fetch(ScheduleSlot ss) {
    if (fetchAttributes.contains("all")) {
      Hibernate.initialize(ss.getSchool());
    } else {
      if (fetchAttributes.contains("school")) Hibernate.initialize(ss.getSchool());
    }
  }

}
