package com.labtekv.educ8.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import com.labtekv.educ8.dao.ExamTypeDao;
import com.labtekv.educ8.model.ExamType;
import com.labtekv.educ8.model.School;
import com.labtekv.educ8.service.ExamTypeService;

import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Iqbal Djulfri
 */
@Service
@Transactional
public class SimpleExamTypeService implements ExamTypeService {
  @Autowired @Qualifier("simpleExamTypeDao")
  private ExamTypeDao examTypeDao;

  public void setExamTypeDao(ExamTypeDao examTypeDao) {
    this.examTypeDao = examTypeDao;
  }

  @Override
  public ExamType getById(long id) {
    return examTypeDao.get(id);
  }

  @Override
  public List<ExamType> getAll() {
    return getAll(null);
  }

  @Override
  public List<ExamType> getAll(Collection<School> schools) {
    return getByNameOrCode(null, null, schools, 0);
  }

  @Override
  public List<ExamType> getByNameOrCode(String name, String code, int page) {
    return getByNameOrCode(name, code, null, page);
  }

  @Override
  public List<ExamType> getByNameOrCode(String name, String code, Collection<School> schools, int page) {
    List cs = new ArrayList(), os = new ArrayList();
    if (name != null) cs.add(Restrictions.ilike("name", name, MatchMode.ANYWHERE));
    if (code != null) cs.add(Restrictions.ilike("code", code, MatchMode.ANYWHERE));
    if (schools != null) {
      List ids = new ArrayList();
      for (School school : schools) ids.add(school.getId());
      cs.add(Restrictions.in("school.id", ids));
    }

    os.add(Order.asc("code"));
    return examTypeDao.get(cs, os, page);
  }

  @Override
  public long create(ExamType et) {
    return examTypeDao.save(et);
  }

  @Override
  public void update(ExamType et) {
    examTypeDao.update(et);
  }

  @Override
  public void delete(ExamType et) {
    examTypeDao.delete(et);
  }

  @Override
  public void deleteById(long id) {
    examTypeDao.delete(id);
  }
}
