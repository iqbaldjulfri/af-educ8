package com.labtekv.educ8.service.impl;

import java.util.Collection;
import java.util.List;

import com.labtekv.educ8.model.Room;
import com.labtekv.educ8.model.School;

import org.hibernate.Hibernate;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Iqbal Djulfri
 */
@Transactional(readOnly = true)
public class AutoFetchRoomService extends SimpleRoomService {
  private List fetchAttributes;

  public AutoFetchRoomService(List fetchAttributes) {
    this.fetchAttributes = fetchAttributes;
  }

  @Override
  public List<Room> getAll() {
    List<Room> rooms = super.getAll();
    for (Room room : rooms) fetch(room);
    return rooms;
  }

  @Override
  public List<Room> getAll(Collection<School> schools) {
    List<Room> rooms = super.getAll(schools);
    for (Room room : rooms) fetch(room);
    return rooms;
  }

  @Override
  public Room getById(long id) {
    Room room = super.getById(id);
    fetch(room);
    return room;
  }

  @Override
  public List<Room> getByName(String name, int page) {
    List<Room> rooms = super.getByName(name, page);
    for (Room room : rooms) fetch(room);
    return rooms;
  }

  @Override
  public List<Room> getByName(String name, Collection<School> schools, int page) {
    List<Room> rooms = super.getByName(name, schools, page);
    for (Room room : rooms) fetch(room);
    return rooms;
  }

  private void fetch(Room r) {
    if (fetchAttributes.contains("all")) {
      Hibernate.initialize(r.getSchools());
    } else {
      if (fetchAttributes.contains("schools")) Hibernate.initialize(r.getSchools());
    }
  }
}
