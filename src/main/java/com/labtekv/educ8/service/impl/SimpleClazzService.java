package com.labtekv.educ8.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import com.labtekv.educ8.dao.ClazzDao;
import com.labtekv.educ8.model.Clazz;
import com.labtekv.educ8.model.School;
import com.labtekv.educ8.service.ClazzService;

import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Iqbal Djulfri
 */
@Service
@Transactional
public class SimpleClazzService implements ClazzService {

  @Autowired @Qualifier("simpleClazzDao")
  private ClazzDao clazzDao;

  public void setClazzDao(ClazzDao clazzDao) {
    this.clazzDao = clazzDao;
  }

  @Override
  @Transactional(readOnly = true)
  public Clazz getById(long id) {
    return clazzDao.get(id);
  }

  @Override
  @Transactional(readOnly = true)
  public List<Clazz> getAll() {
    return clazzDao.getAll();
  }

  @Override
  @Transactional(readOnly = true)
  public List<Clazz> getAll(Collection<School> schools) {
    return getByName(null, schools, 0);
  }

  @Override
  @Transactional(readOnly = true)
  public List<Clazz> getByName(String name, int page) {
    return getByName(name, null, page);
  }

  @Override
  @Transactional(readOnly = true)
  public List<Clazz> getByName(String name, Collection<School> schools, int page) {
    List cs = new ArrayList(), os = new ArrayList();
    if (schools != null) {
      List ids = new ArrayList();
      for (School s : schools) {
        ids.add(s.getId());
      }
      cs.add(Restrictions.in("school.id", ids));
    }
    if (name != null) {
      cs.add(Restrictions.ilike("name", name, MatchMode.ANYWHERE));
    }
    os.add(Order.asc("name"));
    return clazzDao.get(cs, os, page);
  }

  @Override
  public long create(Clazz clazz) {
    return clazzDao.save(clazz);
  }

  @Override
  public void update(Clazz clazz) {
    clazzDao.update(clazz);
  }

  @Override
  public void delete(Clazz clazz) {
    clazzDao.delete(clazz);
  }

  @Override
  public void deleteById(long id) {
    clazzDao.delete(id);
  }
}
