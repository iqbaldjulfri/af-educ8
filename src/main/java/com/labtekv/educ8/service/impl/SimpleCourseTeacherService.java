package com.labtekv.educ8.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.labtekv.educ8.dao.CourseDao;
import com.labtekv.educ8.model.Course;
import com.labtekv.educ8.model.Teacher;
import com.labtekv.educ8.service.CourseTeacherService;

import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Iqbal Djulfri
 */
@Service
@Transactional(readOnly=true)
public class SimpleCourseTeacherService implements CourseTeacherService {

  @Autowired
  @Qualifier("courseJoinTeacherDao")
  private CourseDao courseDao;

  public void setCourseDao(CourseDao courseDao) {
    this.courseDao = courseDao;
  }

  @Override
  public List<Course> getAll(Teacher t) {
    return get(t, 0);
  }

  @Override
  public List<Course> get(Teacher t, int page) {
    List cs = new ArrayList(), os = new ArrayList();
    if (t != null) cs.add(Restrictions.eq("teacher.id", t.getId()));
    cs.add(Restrictions.eq("isActive", true));
    os.add(Order.asc("name"));
    return courseDao.get(cs, os, page);
  }

}
