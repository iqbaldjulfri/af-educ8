package com.labtekv.educ8.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.labtekv.educ8.dao.UserDao;
import com.labtekv.educ8.model.User;
import com.labtekv.educ8.service.UserService;

import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Iqbal Djulfri
 */
@Service
@Transactional
public class SimpleUserService implements UserService {

  @Autowired
  @Qualifier("simpleUserDao")
  private UserDao userDao;

  public void setUserDao(UserDao userDao) {
    this.userDao = userDao;
  }

  @Override
  @Transactional(readOnly=true)
  public User getById(Long id) {
    return userDao.get(id);
  }

  @Override
  @Transactional(readOnly=true)
  public User getByUsername(String username) {
    return userDao.getByUsername(username);
  }

  @Override
  @Transactional(readOnly=true)
  public List<User> getByName(String name, int page) {
    List cs = new ArrayList();
    List os = new ArrayList();
    cs.add(Restrictions.ilike("name", name, MatchMode.ANYWHERE));
    os.add(Order.asc("username"));
    return userDao.get(cs, os, page);
  }

  @Override
  public List<User> getByNameOrUsername(String name, String username, int page) {
    List cs = new ArrayList();
    List os = new ArrayList();
    cs.add(Restrictions.or(
        Restrictions.ilike("name", name, MatchMode.ANYWHERE),
        Restrictions.ilike("username", username, MatchMode.ANYWHERE)
        ));
    os.add(Order.asc("username"));
    return userDao.get(cs, os, page);
  }

  @Override
  public long countByName(String name) {
    List cs = new ArrayList();
    cs.add(Restrictions.ilike("name", name, MatchMode.ANYWHERE));
    return userDao.count(cs);
  }

  @Override
  public void create(User user) {
    user.setPassword(new ShaPasswordEncoder().encodePassword(user.getPassword(), user.getUsername()));
    userDao.save(user);
  }

  @Override
  public void update(User user, String newPass) {
    if (newPass == null || newPass.isEmpty()) {
//      User temp = userDao.getByUsername(user.getUsername());
//      user.setPassword(temp.getPassword());
    } else {
      user.setPassword(new ShaPasswordEncoder().encodePassword(newPass, user.getUsername()));
    }
    userDao.update(user);
  }

  @Override
  public void delete(User user) {
    userDao.delete(user);
  }

  @Override
  public void deleteById(long id) {
    User user = userDao.get(id);
    delete(user);
  }

  @Override
  public void changePassword(User user, String newPassword) {
    user.setPassword(newPassword);
    update(user, newPassword);
  
  }
  
  

}
