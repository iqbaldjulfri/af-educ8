package com.labtekv.educ8.service.impl;

import java.util.List;

import com.labtekv.educ8.dao.impl.ClazzAndCourseGradeDao;
import com.labtekv.educ8.service.ClazzAndCourseGradeService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Iqbal Djulfri
 */
@Service("clazzAndRoomGradeService")
@Transactional(readOnly=true)
public class SimpleClazzAndCourseGradeService implements ClazzAndCourseGradeService {
  @Autowired
  private ClazzAndCourseGradeDao dao;

  public void setDao(ClazzAndCourseGradeDao dao) {
    this.dao = dao;
  }

  @Override
  public List getClazzAndCourseGrade(String name, long schoolId) {
    return dao.getByNameAndSchool(name, schoolId);
  }
}
