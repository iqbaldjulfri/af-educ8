package com.labtekv.educ8.service.impl;

import java.util.List;

import com.labtekv.educ8.model.User;

import org.hibernate.Hibernate;

import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Iqbal Djulfri
 */
@Transactional(readOnly = true)
public class AutoFetchUserService extends SimpleUserService {

  private List<String> fetchAttributes;

  public AutoFetchUserService(List toBeFetchedAttributes) {
    this.fetchAttributes = toBeFetchedAttributes;
  }

  @Override
  public User getById(Long id) {
    User user = super.getById(id);
    fetchUser(user);
    return user;
  }

  @Override
  public List<User> getByNameOrUsername(String name, String username, int page) {
    List<User> users = super.getByNameOrUsername(name, username, page);
    for (User user : users) {
      fetchUser(user);
    }

    return users;
  }

  @Override
  public User getByUsername(String username) {
    User user = super.getByUsername(username);
    fetchUser(user);
    return user;
  }

  private void fetchUser(User user) {
    if (fetchAttributes.contains("all")) {
      Hibernate.initialize(user.getAuthorities());
      Hibernate.initialize(user.getAdministration());
      Hibernate.initialize(user.getParent());
      Hibernate.initialize(user.getStudent());
      Hibernate.initialize(user.getTeacher());
    } else {
      if (fetchAttributes.contains("authorities")) Hibernate.initialize(user.getAuthorities());
      if (fetchAttributes.contains("administration")) Hibernate.initialize(user.getAdministration());
      if (fetchAttributes.contains("parent")) Hibernate.initialize(user.getParent());
      if (fetchAttributes.contains("student")) Hibernate.initialize(user.getStudent());
      if (fetchAttributes.contains("teacher")) Hibernate.initialize(user.getTeacher());
    }
  }
}
