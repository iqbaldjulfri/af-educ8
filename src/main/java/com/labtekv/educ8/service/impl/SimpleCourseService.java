package com.labtekv.educ8.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.labtekv.educ8.dao.CourseDao;
import com.labtekv.educ8.model.Course;
import com.labtekv.educ8.model.School;
import com.labtekv.educ8.service.CourseService;

import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Iqbal Djulfri
 */
@Service
@Transactional
public class SimpleCourseService implements CourseService {

  @Autowired @Qualifier("simpleCourseDao")
  private CourseDao courseDao;

  public void setCourseDao(CourseDao courseDao) {
    this.courseDao = courseDao;
  }

  @Override
  @Transactional(readOnly = true)
  public Course getById(long id) {
    return courseDao.get(id);
  }

  @Override
  @Transactional(readOnly = true)
  public List<Course> getAll() {
    return courseDao.getAll();
  }

  @Override
  @Transactional(readOnly = true)
  public List<Course> getAll(Collection<School> schools) {
    return getByName(null, schools, 0);
  }

  @Override
  @Transactional(readOnly = true)
  public List<Course> getByName(String name, int page) {
    return getByName(name, null, page);
  }

  @Override
  @Transactional(readOnly = true)
  public List<Course> getByName(String name, Collection<School> schools, int page) {
    List cs = new ArrayList(), os = new ArrayList();
    if (schools != null) {
      List ids = new ArrayList();
      for (School s : schools) ids.add(s.getId());
      cs.add(Restrictions.in("school.id", ids));
    }
    if (name != null) cs.add(Restrictions.ilike("name", name, MatchMode.ANYWHERE));
    os.add(Order.asc("name"));
    return courseDao.get(cs, os, page);
  }

  @Override
  public long create(Course c) {
    return courseDao.save(c);
  }

  @Override
  public void update(Course c) {
    courseDao.update(c);
  }

  @Override
  public void delete(Course c) {
    courseDao.delete(c);
  }

  @Override
  public void deleteById(long id) {
    courseDao.delete(id);
  }

}
