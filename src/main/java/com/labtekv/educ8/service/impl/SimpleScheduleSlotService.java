package com.labtekv.educ8.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.labtekv.educ8.dao.ScheduleSlotDao;
import com.labtekv.educ8.model.ScheduleSlot;
import com.labtekv.educ8.model.School;
import com.labtekv.educ8.service.ScheduleSlotService;

import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Iqbal Djulfri
 */
@Service
@Transactional
public class SimpleScheduleSlotService implements ScheduleSlotService {

  @Autowired @Qualifier("simpleScheduleSlotDao")
  private ScheduleSlotDao scheduleSlotDao;

  public void setScheduleSlotDao(ScheduleSlotDao scheduleSlotDao) {
    this.scheduleSlotDao = scheduleSlotDao;
  }

  @Override
  @Transactional(readOnly = true)
  public ScheduleSlot getById(long id) {
    return scheduleSlotDao.get(id);
  }

  @Override
  @Transactional(readOnly = true)
  public List<ScheduleSlot> getAll() {
    return scheduleSlotDao.getAll();
  }

  @Override
  @Transactional(readOnly = true)
  public List<ScheduleSlot> getAll(Collection<School> schools) {
    return getByDescription(null, schools, 0);
  }

  @Override
  @Transactional(readOnly = true)
  public List<ScheduleSlot> getByDescription(String desc, int page) {
    return getByDescription(desc, null, page);
  }

  @Override
  @Transactional(readOnly = true)
  public List<ScheduleSlot> getByDescription(String desc, Collection<School> schools, int page) {
    List cs = new ArrayList(), os = new ArrayList();
    if (schools != null) {
      List ids = new ArrayList();
      for (School s : schools) ids.add(s.getId());
      cs.add(Restrictions.in("school.id", ids));
    }
    if (desc != null) cs.add(Restrictions.ilike("description", desc, MatchMode.ANYWHERE));
    os.add(Order.asc("day"));
    os.add(Order.asc("start"));
    os.add(Order.asc("end"));
    return scheduleSlotDao.get(cs, os, page);
  }

  @Override
  public long create(ScheduleSlot ss) {
    return scheduleSlotDao.save(ss);
  }

  @Override
  public void update(ScheduleSlot ss) {
    scheduleSlotDao.update(ss);
  }

  @Override
  public void delete(ScheduleSlot ss) {
    scheduleSlotDao.delete(ss);
  }

  @Override
  public void deleteById(long id) {
    scheduleSlotDao.delete(id);
  }

}
