package com.labtekv.educ8.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import com.labtekv.educ8.dao.ClazzDao;
import com.labtekv.educ8.dao.CourseDao;
import com.labtekv.educ8.dao.RoomDao;
import com.labtekv.educ8.dao.ScheduleDao;
import com.labtekv.educ8.dao.ScheduleSlotDao;
import com.labtekv.educ8.model.Clazz;
import com.labtekv.educ8.model.Course;
import com.labtekv.educ8.model.Room;
import com.labtekv.educ8.model.Schedule;
import com.labtekv.educ8.model.ScheduleSlot;
import com.labtekv.educ8.model.Semester;
import com.labtekv.educ8.model.Teacher;
import com.labtekv.educ8.service.ScheduleService;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Iqbal Djulfri
 */
@Service("simpleScheduleService")
@Transactional
public class SimpleScheduleService implements ScheduleService {

  @Autowired
  @Qualifier("simpleScheduleDao")
  private ScheduleDao scheduleDao;

  @Autowired
  @Qualifier("simpleClazzDao")
  private ClazzDao clazzDao;

  @Autowired
  @Qualifier("simpleCourseDao")
  private CourseDao courseDao;

  @Autowired
  @Qualifier("roomJoinSchoolDao")
  private RoomDao roomDao;

  @Autowired
  @Qualifier("simpleScheduleSlotDao")
  private ScheduleSlotDao scheduleSlotDao;

  public void setScheduleDao(ScheduleDao scheduleDao) {
    this.scheduleDao = scheduleDao;
  }

  public void setClazzDao(ClazzDao clazzDao) {
    this.clazzDao = clazzDao;
  }

  public void setCourseDao(CourseDao courseDao) {
    this.courseDao = courseDao;
  }

  public void setRoomDao(RoomDao roomDao) {
    this.roomDao = roomDao;
  }

  public void setScheduleSlotDao(ScheduleSlotDao scheduleSlotDao) {
    this.scheduleSlotDao = scheduleSlotDao;
  }

  @Override
  public Schedule getById(long id) {
    return scheduleDao.get(id);
  }

  @Override
  public List<Schedule> getAll() {
    return scheduleDao.getAll();
  }

  @Override
  public List<Schedule> getByClazz(long id) {
    List cs = new ArrayList();
    cs.add(Restrictions.eq("clazz.id", id));
    return scheduleDao.get(cs, null, 0);
  }

  @Override
  public List<Schedule> getBySemester(long id) {
    List cs = new ArrayList();
    cs.add(Restrictions.eq("semester.id", id));
    return scheduleDao.get(cs, null, 0);
  }

  @Override
  public List<Schedule> getByRoom(long id) {
    List cs = new ArrayList();
    cs.add(Restrictions.eq("room.id", id));
    return scheduleDao.get(cs, null, 0);
  }

  @Override
  public List<Schedule> getByCourse(long id) {
    List cs = new ArrayList();
    cs.add(Restrictions.eq("course.id", id));
    return scheduleDao.get(cs, null, 0);
  }

  @Override
  public List<Schedule> getBySlot(long id) {
    List cs = new ArrayList();
    cs.add(Restrictions.eq("slot.id", id));
    return scheduleDao.get(cs, null, 0);
  }

  @Override
  public List<Schedule> getBySemesterClazz(long semesterId, Long clazzId) {
    List cs = new ArrayList();
    cs.add(Restrictions.eq("semester.id", semesterId));
    if (clazzId != null) cs.add(Restrictions.eq("clazz.id", clazzId));
    return scheduleDao.get(cs, null, 0);
  }

  @Override
  public long create(Schedule s) {
    return scheduleDao.save(s);
  }

  @Override
  public void update(Schedule s) {
    scheduleDao.update(s);
  }

  @Override
  public void delete(Schedule s) {
    scheduleDao.delete(s);
  }

  @Override
  public void deleteById(long id) {
    scheduleDao.delete(id);
  }

  @Override
  public void createAllForSemesterClazz(Semester semester, Clazz clazz) {
    Date now = new Date();
    if (semester.getEnd().before(now)) return;

    List<Schedule> listedSchedules = this.getBySemesterClazz(semester.getId(), clazz == null ? null : clazz.getId());

    List<Clazz> clazzes;
    if (clazz == null) {
      List cs = new ArrayList();
      cs.add(Restrictions.eq("school.id", semester.getSchool().getId()));
      clazzes = clazzDao.get(cs, null, 0);
    } else {
      clazzes = new ArrayList();
      clazzes.add(clazz);
    }
    List cs = new ArrayList();
    cs.add(Restrictions.eq("school.id", semester.getSchool().getId()));
    cs.add(Restrictions.eq("isActive", true));
    List<ScheduleSlot> slots = scheduleSlotDao.get(cs, null, 0);

    for (Clazz c : clazzes) { // untuk setiap clazz
      for (ScheduleSlot slot : slots) { // untuk setiap schedule slot
        boolean found = false;
        for (Schedule ls : listedSchedules) { // cari di semua listedSchedules
          if (ls.getClazz().getId() == c.getId() && ls.getSlot().getId() == slot.getId()) {
            found = true; // cari yang clazz sama slotnya sama dengan 2 for sebelumnya
          }
        }

        if (!found) { // kalau ga ketemu berarti ga ada
          // kalau ga ada buat buat baru
          Schedule s = new Schedule();
          s.setClazz(c);
          s.setCourse(null);
          s.setIsLocked(false);
          s.setRoom(null);
          s.setTeacher(null);
          s.setSemester(semester);
          s.setSlot(slot);

          scheduleDao.save(s);
        }
      }
    }
  }

  @Override
  public void update(Schedule s, Course c, Room r, Teacher teacher, boolean lock) {
    s.setCourse(c);
    s.setRoom(r);
    s.setIsLocked(lock);
    s.setTeacher(teacher);

    update(s);
  }

  @Override
  public void clear(Schedule s) {
    s.setCourse(null);
    s.setRoom(null);
    s.setTeacher(null);
    s.setIsLocked(false);
    update(s);
  }

  @Override
  public void copy(Schedule source, Schedule target) {
    target.setCourse(source.getCourse());
    target.setRoom(source.getRoom());
    target.setTeacher(source.getTeacher());
    target.setIsLocked(source.getIsLocked());
    update(target);
  }

  @Override
  public void move(Schedule source, Schedule target) {
    copy(source, target);
    clear(source);
  }

  @Override
  public void switch_(Schedule source, Schedule target) {
    Schedule temp = target.clone();

    move(source, target);
    move(temp, source);
  }

  @Override
  public List<Schedule> generate(Semester s) throws Exception {
    return generate(s, -99);
  }

  @Override
  public List<Schedule> generate(Semester s, int day) throws Exception {
    List cs = new ArrayList(), os = new ArrayList();
    cs.add(Restrictions.eq("school.id", s.getSchool().getId()));
    cs.add(Restrictions.eq("isActive", true));

    os.clear();
    os.add(Order.asc("grade"));
    os.add(Order.asc("name"));
    List<Clazz> cls = clazzDao.get(cs, os, 0);

    os.clear();
    os.add(Order.asc("grade"));
    os.add(Order.desc("maxSchedulePerWeek"));
    List<Course> crs = courseDao.get(cs, os, 0);

    os.clear();
    os.add(Order.asc("day"));
    os.add(Order.asc("start"));
    List<ScheduleSlot> sls = scheduleSlotDao.get(cs, os, 0);

    List<Schedule> scs = getBySemester(s.getId());

    cs.clear();
    cs.add(Restrictions.in("school.id", new Long[] {s.getSchool().getId()}));
    List<Room> rms = roomDao.get(cs, null, 0);

    // clear all schedule but the locked ones
    for (Schedule sc : scs) {
      if (sc.getIsLocked()) continue;

      sc.setCourse(null);
      sc.setRoom(null);
      sc.setTeacher(null);
    }
    Stack<Schedule> stack = new Stack<>();
    Stack<Schedule> tempStack = new Stack<>();

    for (ScheduleSlot sl : sls) {
      if ((day >= 0 && sl.getDay() == day) || day < 0) {
        for (Clazz cl : cls) {
          tempStack.push(findSchedule(scs, sl, cl));
        }
      }
    }

    // reverse the stack
    do {
      Schedule sc = tempStack.pop();
      if (!sc.getIsLocked()) stack.push(sc);
    } while (!tempStack.isEmpty());

    // call the recursive method
    if (!generateRecursive(stack, crs, rms, sls, scs, cls, day < 0 ? null : day)) {
      throw new Exception("Cannot generate schedule");
    }
    for (Schedule sc : scs) update(sc);
    return scs;

  }

  private boolean generateRecursive(Stack<Schedule> stack, List<Course> crs,
      List<Room> rms, List<ScheduleSlot> sls, List<Schedule> scs,
      List<Clazz> cls, Integer day) throws Exception {
    // base!
    if (stack.isEmpty()) return true;

    // pop it
    Schedule s = stack.pop();
    System.out.println("pop:  " + s.getId() + ", name: " + s.getClazz().getName() + " ; size: " + stack.size());

    boolean valid = false;

    // workaround for no grade found for a class in all courses
    boolean nograde = true;

    // check for every course
    for (Course cr : crs) {
      // if grade is not equals, ignore
      if (cr.getGrade().equals(s.getClazz().getGrade())) nograde = false;
      else continue;
      s.setCourse(cr);

      // check if course valid; this way, we can skip teacher & room tests if
      // course is invalid
      if (!validate(sls, scs, cls, crs, day, false).isEmpty()) continue;

      boolean isTeacherValid = false;
      // check every teacher assigned to course
      for (Teacher tc : cr.getTeachers()) {
        s.setTeacher(tc);
        if (!validate(sls, scs, cls, crs, day, false).isEmpty()) continue;
        else isTeacherValid = true;
      }

      // only when teacher is valid we can continue to room tests
      if (isTeacherValid) {

        // check room here
        // check whether course had default room
        if (cr.getDefaultRoom() != null) {
          s.setRoom(cr.getDefaultRoom());

          // validate the schedule
          if (validate(sls, scs, cls, crs, day, true).isEmpty()) {
            // if validation OK
            // call next recursive step
            valid = generateRecursive(stack, crs, rms, sls, scs, cls, day);
          }
        }

        // check whether clazz had default room (skip if valid)
        if (!valid && s.getClazz().getDefaultRoom() != null) {
          s.setRoom(s.getClazz().getDefaultRoom());

          // validate the schedule
          if (validate(sls, scs, cls, crs, day, true).isEmpty()) {
            // if validation OK
            // call next recursive step
            valid = generateRecursive(stack, crs, rms, sls, scs, cls, day);
          }
        }

        // check wheter the course and class default room suffice
        if (!valid && cr.getDefaultRoom() == null && s.getClazz().getDefaultRoom() == null) {
          // if not get every room on board :)
          for (Room rm : rms) {
            s.setRoom(rm);

            // validate the schedule
            if (validate(sls, scs, cls, crs, day, true).isEmpty()) {
              // if validation OK
              // call next recursive step
              valid = generateRecursive(stack, crs, rms, sls, scs, cls, day);
            }

            // if valid (a room found), then we don't need to search for another one
            if (valid) break;
          }
        }
      }

      // if valid (course and room found), then we're good to go
      if (valid) break;
    }

    if (nograde) {
      valid = generateRecursive(stack, crs, rms, sls, scs, cls, day);
    }

    // if every single course and room won't match, then we have to fix the previous step
    if (!valid) {
      // reset & put it back to where it's belong
      s.setCourse(null);
      s.setRoom(null);
      s.setTeacher(null);
      stack.push(s);
      System.out.println("push: " + s.getId() + ", name: " + s.getClazz().getName() + " ; size: " + stack.size());
    }

    return valid;
  }

  /**
   * result:
   * [
   *  {
   *    type: "maxWeekCount",
   *    course: Course,
   *    clazz: Clazz,
   *    count: actualCount,
   *    max: maxCount,
   *    schedules: [Schedules...]
   *  },
   *  {
   *    type: "maxDayCount" | "maxConsecutive",
   *    course: Course,
   *    clazz: Clazz,
   *    day: int
   *    count: actualCount,
   *    max: maxCount,
   *    schedules: [Schedules...]
   *  },
   *  {
   *    type: "course",
   *    course: Course,
   *    schedules: [Schedules...]
   *  },
   *  {
   *    type: "room",
   *    room: Room,
   *    schedules: [Schedules...]
   *  },
   *  {
   *    type: "teacher",
   *    teacher: Teacher,
   *    schedules: [Schedules...]
   *  }
   * ]
   */

  @Override
  public List validate(Semester s, Integer singleDay, boolean validateRoom) {
    List cs = new ArrayList();
    cs.add(Restrictions.eq("school.id", s.getSchool().getId()));
    cs.add(Restrictions.eq("isActive", true));
    List<Clazz> cls = clazzDao.get(cs, null, 0);
    List<Course> crs = courseDao.get(cs, null, 0);
    List<ScheduleSlot> sls = scheduleSlotDao.get(cs, null, 0);
    List<Schedule> scs = getBySemester(s.getId());

    return validate(sls, scs, cls, crs, singleDay, validateRoom);
  }

  private List validate(List<ScheduleSlot> sls, List<Schedule> scs,
      List<Clazz> cls, List<Course> crs, Integer singleDay, boolean validateRoom) {
    List errors = new ArrayList();
    Map<Teacher, List<Schedule>> tcMap = new HashMap<>();
    Map<Room, List<Schedule>> rmMap = new HashMap<>();

    // look for conflicting schedule
    // for every day
    for (int day = 0; day < 7; day++) {
      // ignodre unmatched day
      if (singleDay != null && day != singleDay) continue;

      // for every slot
      for (ScheduleSlot sl : sls) {
        // ignore different day
        if (sl.getDay() != day) continue;

        // for every schedule
        for (Schedule sc : scs) {
          // ignore different slot
          if (sc.getSlot().getId() != sl.getId()) continue;

          // conflict checking preparation starts

          // the idea is to make search in 1 slot in every class for duplicates
          // courses and rooms by put a List of schedules in a Map with the
          // schedule's course or room as its key. If a schedule is duplicated,
          // then the list will have more than 1 item, and the conflict cause
          // can be returned also

          // if schedule has no teacher, skip this
          if (sc.getTeacher() != null) {
            // check whether the teacher map already has a similar key
            if (tcMap.containsKey(sc.getTeacher())) {
              // if yes, then add schedule to its value
              tcMap.get(sc.getTeacher()).add(sc);
            } else {
              // if no, create new list and add schedule to its value
              List temp = new ArrayList();
              temp.add(sc);
              tcMap.put(sc.getTeacher(), temp);
            }
          }

          // if schedule has no room, skip this
          if (validateRoom && sc.getRoom() != null) {
            // check whether the room map already has a similar key
            if (rmMap.containsKey(sc.getRoom())) {
              // if yes, then add schedule to its value
              rmMap.get(sc.getRoom()).add(sc);
            } else {
              // if no, create new list and add schedule to its value
              List temp = new ArrayList();
              temp.add(sc);
              rmMap.put(sc.getRoom(), temp);
            }
          }

          // conflict checking preparation ends
        }

        // the real conflict checking starts

        // check conflicts in map by checking an element value (which is List)
        // if the list contains more than 1 schedule, then it's a conflict
        // if it's a conflict, put an error to errors list
        for (Iterator<Teacher> it = tcMap.keySet().iterator(); it.hasNext();) {
          Teacher key = it.next();
          List temp = tcMap.get(key);
          if (temp.size() > 1)
            errors.add(new ScheduleError("teacher", key).addAll(temp));
        }
        for (Iterator<Room> it = rmMap.keySet().iterator(); it.hasNext();) {
          Room key = it.next();
          List temp = rmMap.get(key);
          if (temp.size() > 1)
            errors.add(new ScheduleError("room", key).addAll(temp));
        }

        // the real conflict checking ends

        // flush maps data
        tcMap.clear();
        rmMap.clear();
      }
    }

    // checking for count violation. see course's attirbutes: maxSchedulePerWeek,
    // maxSchedulePerDay, maxConsecutivePerDay. As per 28 Sep 2012, this still
    // is not supporting consecutiveCount.
    for (Clazz cl : cls) {
      for (Course cr : crs) {
        int consecutiveCount = 0;
        List weekScs = new ArrayList();

        for (int day = 0; day < 7; day++) {

          List dayScs = new ArrayList();

          for (Schedule sc : scs) {
            if (sc.getCourse() != null
                && sc.getCourse().getId() == cr.getId()
                && sc.getClazz().getId() == cl.getId()
                && sc.getSlot().getDay() == day) {
              weekScs.add(sc);
              if (singleDay == null || day == singleDay) dayScs.add(sc);
            }
          }
          if (dayScs.size() > cr.getMaxSchedulePerDay()) {
            errors.add(new ScheduleError("maxDayCount", cl, cr, dayScs.size(),
                cr.getMaxSchedulePerDay(), day).addAll(dayScs));
          }
        }
        if (weekScs.size() > cr.getMaxSchedulePerWeek()) {
          errors.add(new ScheduleError("maxWeekCount", cl, cr, weekScs.size(),
              cr.getMaxSchedulePerWeek()).addAll(weekScs));
        }
      }
    }

    for (Object error : errors) {
      System.out.println("---");
      System.out.println(error);
    }

    return errors;
  }

  private Schedule findSchedule(List<Schedule> scs, ScheduleSlot slot, Clazz clazz) {
    for (Schedule sc : scs) {
      if (sc.getSlot().getId() == slot.getId() && sc.getClazz().getId() == clazz.getId())
        return sc;
    }
    return null;
  }

  private class ScheduleError {
    protected String type;
    protected List<Schedule> schedules = new ArrayList();
    protected Clazz clazz;
    protected Course course;
    protected Room room;
    protected Teacher teacher;
    protected int count;
    protected int max;
    protected int day;

    private ScheduleError(String type, Clazz clazz, Course course,
        Room room, Teacher teacher, int count, int max, int day) {
      this.type = type;
      this.clazz = clazz;
      this.course = course;
      this.room = room;
      this.teacher = teacher;
      this.count = count;
      this.max = max;
      this.day = day;
    }

    public ScheduleError(String type, Clazz clazz, Course course, int count, int max, int day) {
      this(type, clazz, course, null, null, count, max, day);
    }

    public ScheduleError(String type, Clazz clazz, Course course, int count, int max) {
      this(type, clazz, course, count, max, -99);
    }

    public ScheduleError(String type, Course course) {
      this(type, null, course, null, null, 0, 0, 0);
    }

    public ScheduleError(String type, Room room) {
      this(type, null, null, room, null, 0, 0, 0);
    }

    public ScheduleError(String type, Teacher teacher) {
      this(type, null, null, null, teacher, 0, 0, 0);
    }

    public ScheduleError add(Schedule s) {
      schedules.add(s);
      return this;
    }

    public ScheduleError addAll(List schedules) {
      this.schedules.addAll(schedules);
      return this;
    }

    public String getType() {
      return type;
    }

    public List<Schedule> getSchedules() {
      return schedules;
    }

    public Clazz getClazz() {
      return clazz;
    }

    public Course getCourse() {
      return course;
    }

    public Room getRoom() {
      return room;
    }

    public Teacher getTeacher() {
      return teacher;
    }

    public int getCount() {
      return count;
    }

    public int getMax() {
      return max;
    }

    public int getDay() {
      return day;
    }

    @Override
    public String toString() {
      return "ScheduleError{" + "type=" + type + ", "
          + "clazz=" + (clazz != null ? clazz.getId() : null)
          + ", course=" + (course != null ? course.getId() : null)
          + ", room=" + (room != null ? room.getId() : null)
          + ", teacher=" + (teacher != null ? teacher.getId() : null)
          + ", count=" + count
          + ", max=" + max
          + ", day=" + day + '}';
    }

  }

}
