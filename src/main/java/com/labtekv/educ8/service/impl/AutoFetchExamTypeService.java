package com.labtekv.educ8.service.impl;

import java.util.Collection;
import java.util.List;

import com.labtekv.educ8.model.ExamType;
import com.labtekv.educ8.model.School;

import org.hibernate.Hibernate;

/**
 *
 * @author Iqbal Djulfri
 */
public class AutoFetchExamTypeService extends SimpleExamTypeService {
  private List fetchAttributes;

  public AutoFetchExamTypeService(List fetchAttributes) {
    this.fetchAttributes = fetchAttributes;
  }

  @Override
  public List<ExamType> getAll() {
    return getByNameOrCode(null, null, 0);
  }

  @Override
  public List<ExamType> getAll(Collection<School> schools) {
    return getByNameOrCode(null, null, schools, 0);
  }

  @Override
  public ExamType getById(long id) {
    ExamType et = super.getById(id);
    fetch(et);
    return et;
  }

  @Override
  public List<ExamType> getByNameOrCode(String name, String code, int page) {
    return getByNameOrCode(name, code, null, page);
  }

  @Override
  public List<ExamType> getByNameOrCode(String name, String code, Collection<School> schools, int page) {
    List<ExamType> ets = super.getByNameOrCode(name, code, schools, page);
    for (ExamType et : ets) fetch(et);

    return ets;
  }

  private void fetch(ExamType et) {
    if (fetchAttributes.contains("all")) {
      Hibernate.initialize(et.getSchool());
    } else {
      if (fetchAttributes.contains("school")) Hibernate.initialize(et.getSchool());
    }
  }

}
