/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.labtekv.educ8.service.impl;

import com.labtekv.educ8.dao.TuitionPaymentDao;
import com.labtekv.educ8.model.School;
import com.labtekv.educ8.model.TuitionPayment;
import com.labtekv.educ8.service.TuitionPaymentService;
import com.labtekv.educ8.util.PageCounter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Andika
 */

@Service
@Transactional
public class SimpleTuitionPaymentService implements TuitionPaymentService {
  @Autowired @Qualifier("tuitionPaymentJoinStudentDao")
  private TuitionPaymentDao tuitionPaymentDao;
  
  @Autowired
  private PageCounter pageCounter;

  public void setTuitionPaymentDao(TuitionPaymentDao tuitionPaymentDao) {
    this.tuitionPaymentDao = tuitionPaymentDao;
  }
  
  public void setPageCounter(PageCounter pageCounter) {
    this.pageCounter = pageCounter;
  }

  @Override
  public TuitionPayment getById(long id) {
    return tuitionPaymentDao.get(id);
  }

  @Override
  public List<TuitionPayment> getAll() {
    return getAll(null);
  }

  @Override
  public List<TuitionPayment> getAll(Collection<School> schools) {
    return getByStudentAndMonthAndYear(null, null, null, schools, 0);
  }

  @Override
  public List<TuitionPayment> getByStudentAndMonthAndYear(String studentId, Integer month, Integer year, int page) {
    return getByStudentAndMonthAndYear(studentId, month, year, null, page);
  }

  @Override
  public List<TuitionPayment> getByStudentAndMonthAndYear(String studentId, Integer month, Integer year, Collection<School> schools, int page) {
    List cs = new ArrayList(), os = new ArrayList();
    if (studentId != null) cs.add(Restrictions.ilike("student.idNumber", studentId, MatchMode.ANYWHERE));
    if (month != null) cs.add(Restrictions.eq("month", month));
    if (year != null) cs.add(Restrictions.eq("year", year));
    if (schools != null) {
      List ids = new ArrayList();
      for (School school : schools) ids.add(school.getId());
      cs.add(Restrictions.in("student.school.id", ids));
    }

    os.add(Order.asc("student.idNumber"));
    return tuitionPaymentDao.get(cs, os, page);
  }
  
  @Override
  public long getPageCountByStudentAndMonthAndYear(String studentId, Integer month, Integer year) {
    List cs = new ArrayList();
    if (studentId != null) cs.add(Restrictions.ilike("student.idNumber", studentId, MatchMode.ANYWHERE));
    if (month != null) cs.add(Restrictions.eq("month", month));
    if (year != null) cs.add(Restrictions.eq("year", year));

    return pageCounter.getMaxPage(tuitionPaymentDao.count(cs));
  }
  
  @Override
  public long getPageCountByStudentAndMonthAndYear(String studentId, Integer month, Integer year, Collection<School> schools) {
    List cs = new ArrayList();
    if (studentId != null) cs.add(Restrictions.ilike("student.idNumber", studentId, MatchMode.ANYWHERE));
    if (month != null) cs.add(Restrictions.eq("month", month));
    if (year != null) cs.add(Restrictions.eq("year", year));
    if (schools != null) {
      List ids = new ArrayList();
      for (School school : schools) ids.add(school.getId());
      cs.add(Restrictions.in("student.school.id", ids));
    }

    return pageCounter.getMaxPage(tuitionPaymentDao.count(cs));
  }

  @Override
  public long create(TuitionPayment tp) {
    return tuitionPaymentDao.save(tp);
  }

  @Override
  public void update(TuitionPayment tp) {
    tuitionPaymentDao.update(tp);
  }

  @Override
  public void delete(TuitionPayment tp) {
    tuitionPaymentDao.delete(tp);
  }

  @Override
  public void deleteById(long id) {
    tuitionPaymentDao.delete(id);
  }
}
