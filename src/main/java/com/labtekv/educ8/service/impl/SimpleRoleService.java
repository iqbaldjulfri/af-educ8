
package com.labtekv.educ8.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.labtekv.educ8.dao.RoleDao;
import com.labtekv.educ8.model.Role;
import com.labtekv.educ8.service.RoleService;
import org.hibernate.criterion.MatchMode;

import org.hibernate.criterion.Restrictions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Alienware
 */

@Service("simpleRoleService")
@Transactional
public class SimpleRoleService implements RoleService {

	@Autowired
  @Qualifier("simpleRoleDao")
  private RoleDao roleDao;

  public void setRoleDao(RoleDao roleDao) {
    this.roleDao = roleDao;
  }

	@Override
  public Role getById(long id) {
    return roleDao.get(id);
  }

	@Override
  public void create(Role role) {
    roleDao.save(role);
  }

  @Override
  public void update(Role role) {
    roleDao.update(role);
  }

  @Override
  public void delete(Role role) {
    roleDao.delete(role);
  }

  @Override
  public void deleteById(long id) {
    roleDao.delete(id);
  }

  @Override
  public List<Role> getByAuthority(String authority, int page) {
    List cs = new ArrayList();
    cs.add(Restrictions.ilike("authority", authority, MatchMode.ANYWHERE));
    return roleDao.get(cs, null, page);
  }

	@Override
  public List<Role> getByDescription(String description, int page) {
    List cs = new ArrayList();
    cs.add(Restrictions.ilike("description", description, MatchMode.ANYWHERE));
    return roleDao.get(cs, null, page);
  }

	@Override
  @Transactional(readOnly=true)
  public Role getByAuthority(String authority) {
    return roleDao.getByAuthority(authority);
  }

  @Override
  public List<Role> getAll() {
    return roleDao.getAll();
  }
}
