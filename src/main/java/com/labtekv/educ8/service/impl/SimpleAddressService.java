package com.labtekv.educ8.service.impl;

import com.labtekv.educ8.dao.AddressDao;
import com.labtekv.educ8.model.Address;
import com.labtekv.educ8.service.AddressService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Iqbal Djulfri
 */
@Service("simpleAddressService")
@Transactional
public class SimpleAddressService implements AddressService {

  @Autowired @Qualifier("simpleAddressDao")
  private AddressDao addressDao;

  public void setAddressDao(AddressDao addressDao) {
    this.addressDao = addressDao;
  }

  @Override
  public Address getById(long id) {
    return addressDao.get(id);
  }

  @Override
  public long create(Address address) {
    return addressDao.save(address);
  }

  @Override
  public void update(Address address) {
    addressDao.update(address);
  }

  @Override
  public void delete(Address address) {
    addressDao.delete(address);
  }

  @Override
  public void deleteById(long id) {
    addressDao.delete(id);
  }

}
