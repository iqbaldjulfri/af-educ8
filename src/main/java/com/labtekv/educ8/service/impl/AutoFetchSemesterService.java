package com.labtekv.educ8.service.impl;

import java.util.Collection;
import java.util.List;

import com.labtekv.educ8.model.School;
import com.labtekv.educ8.model.Semester;

import org.hibernate.Hibernate;

import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Iqbal Djulfri
 */
@Transactional(readOnly = true)
public class AutoFetchSemesterService extends SimpleSemesterService {
  private List fetchAttributes;

  public AutoFetchSemesterService(List fetchAttributes) {
    this.fetchAttributes = fetchAttributes;
  }

  @Override
  public List<Semester> getAll() {
    List<Semester> scheduleSlots = super.getAll();
    for (Semester scheduleSlot : scheduleSlots) fetch(scheduleSlot);
    return scheduleSlots;
  }

  @Override
  public List<Semester> getAll(Collection<School> schools) {
    return getByName(null, schools, 0);
  }

  @Override
  public List<Semester> getByName(String name, int page) {
    return getByName(name, null, page);
  }

  @Override
  public List<Semester> getByName(String name, Collection<School> schools, int page) {
    List<Semester> ss = super.getByName(name, schools, page);
    for (Semester scheduleSlot : ss) fetch(scheduleSlot);
    return ss;
  }

  @Override
  public Semester getById(long id) {
    Semester ss = super.getById(id);
    fetch(ss);
    return ss;
  }

  private void fetch(Semester s) {
    if (fetchAttributes.contains("all")) {
      Hibernate.initialize(s.getSchool());
    } else {
      if (fetchAttributes.contains("school")) Hibernate.initialize(s.getSchool());
    }
  }
}
