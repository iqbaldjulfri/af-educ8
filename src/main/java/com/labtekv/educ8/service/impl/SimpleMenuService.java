package com.labtekv.educ8.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.labtekv.educ8.dao.MenuDao;
import com.labtekv.educ8.model.Menu;
import com.labtekv.educ8.model.Role;
import com.labtekv.educ8.service.MenuService;

import org.hibernate.Hibernate;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Iqbal Djulfri
 */
@Service("simpleMenuService")
@Transactional
public class SimpleMenuService implements MenuService {

  @Autowired
  @Qualifier("simpleMenuDao")
  private MenuDao menuDao;

  public void setMenuDao(MenuDao menuDao) {
    this.menuDao = menuDao;
  }

  @Override
  public Menu getById(long id) {
    Menu m = menuDao.get(id);
    Hibernate.initialize(m.getRoles());
    return m;
  }

  @Override
  public List<Menu> getByRoles(Collection<Role> roles) {
    return menuDao.getByRoles(roles);
  }

  @Override
  public void create(Menu menu) {
    menuDao.save(menu);
  }

  @Override
  public void update(Menu menu) {
    menuDao.update(menu);
  }

  @Override
  public void delete(Menu menu) {
    menuDao.delete(menu);
  }

  @Override
  public void deleteById(long id) {
    menuDao.delete(id);
  }

  @Override
  public List<Menu> getByName(String name, int page) {
    List cs = new ArrayList();
    cs.add(Restrictions.ilike("name", name, MatchMode.ANYWHERE));
    return menuDao.get(cs, null, page);
  }

  @Override
  public List<Menu> getAll() {
    return menuDao.get(null, null, 0);
  }

}
