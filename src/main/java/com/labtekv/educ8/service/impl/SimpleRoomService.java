package com.labtekv.educ8.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.labtekv.educ8.dao.RoomDao;
import com.labtekv.educ8.model.Room;
import com.labtekv.educ8.model.School;
import com.labtekv.educ8.service.RoomService;

import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Iqbal Djulfri
 */
@Service
@Transactional
public class SimpleRoomService implements RoomService {

  @Autowired @Qualifier("simpleRoomDao")
  private RoomDao roomDao;

  public void setRoomDao(RoomDao roomDao) {
    this.roomDao = roomDao;
  }

  @Override
  @Transactional(readOnly = true)
  public Room getById(long id) {
    return roomDao.get(id);
  }

  @Override
  @Transactional(readOnly = true)
  public List<Room> getByName(String name, int page) {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  @Transactional(readOnly = true)
  public List<Room> getByName(String name, Collection<School> schools, int page) {
    List cs = new ArrayList(), os = new ArrayList();
    cs.add(Restrictions.ilike("name", name, MatchMode.ANYWHERE));
    os.add(Order.asc("name"));

    return roomDao.getBySchool(cs, os, schools, page);
  }

  @Override
  @Transactional(readOnly = true)
  public List<Room> getAll() {
    return roomDao.getAll();
  }

  @Override
  @Transactional(readOnly = true)
  public List<Room> getAll(Collection<School> schools) {
    return roomDao.getAllBySchool(schools);
  }

  @Override
  public long create(Room room) {
    return roomDao.save(room);
  }

  @Override
  public void update(Room room) {
    roomDao.update(room);
  }

  @Override
  public void delete(Room room) {
    roomDao.delete(room);
  }

  @Override
  public void deleteById(long id) {
    roomDao.delete(id);
  }

}
