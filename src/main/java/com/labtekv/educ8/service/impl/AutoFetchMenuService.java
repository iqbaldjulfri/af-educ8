package com.labtekv.educ8.service.impl;

import java.util.Collection;
import java.util.List;

import com.labtekv.educ8.model.Menu;
import com.labtekv.educ8.model.Role;

import org.hibernate.Hibernate;

import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Iqbal Djulfri
 */
@Transactional(readOnly=true)
public class AutoFetchMenuService extends SimpleMenuService {
  private List fetchAttributes;

  public AutoFetchMenuService(List fetchAttributes) {
    this.fetchAttributes = fetchAttributes;
  }

  @Override
  public List<Menu> getAll() {
    List<Menu> menus = super.getAll();
    for (Menu m : menus) fetch(m);
    return menus;
  }

  @Override
  public Menu getById(long id) {
    Menu m = super.getById(id);
    fetch(m);
    return m;
  }

  @Override
  public List<Menu> getByName(String name, int page) {
    List<Menu> menus = super.getByName(name, page);
    for (Menu m : menus) fetch(m);
    return menus;
  }

  @Override
  public List<Menu> getByRoles(Collection<Role> roles) {
    List<Menu> menus = super.getByRoles(roles);
    for (Menu menu : menus) fetch(menu);
    return menus;
  }

  private void fetch(Menu m) {
    if (fetchAttributes.contains("all")) {
      Hibernate.initialize(m.getChildren());
      Hibernate.initialize(m.getParent());
      Hibernate.initialize(m.getRoles());
    } else {
      if (fetchAttributes.contains("children")) Hibernate.initialize(m.getChildren());
      if (fetchAttributes.contains("parent")) Hibernate.initialize(m.getParent());
      if (fetchAttributes.contains("roles")) Hibernate.initialize(m.getRoles());
    }
  }
}
