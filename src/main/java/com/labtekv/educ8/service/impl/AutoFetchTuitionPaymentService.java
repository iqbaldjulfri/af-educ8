/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.labtekv.educ8.service.impl;

import com.labtekv.educ8.model.School;
import com.labtekv.educ8.model.TuitionPayment;
import java.util.Collection;
import java.util.List;
import org.hibernate.Hibernate;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Andika
 */
@Transactional(readOnly = true)
public class AutoFetchTuitionPaymentService extends SimpleTuitionPaymentService {
  private List fetchAttributes;

  public AutoFetchTuitionPaymentService(List fetchAttributes) {
    this.fetchAttributes = fetchAttributes;
  }

  @Override
  public List<TuitionPayment> getAll() {
    List<TuitionPayment> tuitionPayments = super.getAll();
    for (TuitionPayment tuitionPayment : tuitionPayments) fetch(tuitionPayment);
    return tuitionPayments;
  }

  @Override
  public List<TuitionPayment> getAll(Collection<School> schools) {
    List<TuitionPayment> tuitionPayments = super.getAll(schools);
    for (TuitionPayment tuitionPayment : tuitionPayments) fetch(tuitionPayment);
    return tuitionPayments;
  }

  @Override
  public TuitionPayment getById(long id) {
    TuitionPayment tuitionPayment = super.getById(id);
    fetch(tuitionPayment);
    return tuitionPayment;
  }

  @Override
  public List<TuitionPayment> getByStudentAndMonthAndYear(String studentId, Integer month, Integer year, int page) {
    List<TuitionPayment> tuitionPayments = super.getByStudentAndMonthAndYear(studentId, month, year, page);
    for (TuitionPayment tuitionPayment : tuitionPayments) fetch(tuitionPayment);
    return tuitionPayments;
  }

  @Override
  public List<TuitionPayment> getByStudentAndMonthAndYear(String studentId, Integer month, Integer year, Collection<School> schools, int page) {
    List<TuitionPayment> tuitionPayments = super.getByStudentAndMonthAndYear(studentId, month, year, schools, page);
    for (TuitionPayment tuitionPayment : tuitionPayments) fetch(tuitionPayment);
    return tuitionPayments;
  }

  private void fetch(TuitionPayment tp) {
    if (fetchAttributes.contains("all")) {
      Hibernate.initialize(tp.getStudent());
    } else {
      if (fetchAttributes.contains("student")) Hibernate.initialize(tp.getStudent());
    }
  }
}
