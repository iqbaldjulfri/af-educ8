/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.labtekv.educ8.service.impl;

import com.labtekv.educ8.dao.AddressDao;
import com.labtekv.educ8.dao.ParentDao;
import com.labtekv.educ8.model.Parent;
import com.labtekv.educ8.service.ParentService;
import com.labtekv.educ8.util.PageCounter;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Andika
 */
@Service("simpleParentService")
@Transactional
public class SimpleParentService implements ParentService {
  
  @Autowired @Qualifier("simpleParentDao")
  private ParentDao parentDao;

  @Autowired @Qualifier("simpleAddressDao")
  private AddressDao addressDao;

  @Autowired
  private PageCounter pageCounter;

  public void setAddressDao(AddressDao addressDao) {
    this.addressDao = addressDao;
  }

  public void setPageCounter(PageCounter pageCounter) {
    this.pageCounter = pageCounter;
  }

  public void setParentDao(ParentDao parentDao) {
    this.parentDao = parentDao;
  }

  @Override
  public Parent getById(long id) {
    Parent parent = parentDao.get(id);
    return parent;
  }

  @Override
  public List<Parent> getAll() {
    return parentDao.getAll();
  }
  
  @Override
  public List<Parent> getByName(String name, int page) {
    List cs = new ArrayList();
    cs.add(Restrictions.ilike("name", name, MatchMode.ANYWHERE));

    List os = new ArrayList();
    os.add(Order.asc("name"));

    List<Parent> parents;
    parents = parentDao.get(cs, null, page);
    return parents;
  }

  @Override
  public long getPageCountByName(String name) {
    List cs = new ArrayList();
    cs.add(Restrictions.ilike("name", name, MatchMode.ANYWHERE));

    return pageCounter.getMaxPage(parentDao.count(cs));
  }

  @Override
  public long create(Parent parent) {
    addressDao.save(parent.getAddress());
    return parentDao.save(parent);
  }

  @Override
  public void update(Parent parent) {
    addressDao.update(parent.getAddress());
    parentDao.update(parent);
  }

  @Override
  public void delete(Parent parent) {
    parentDao.delete(parent);
  }

  @Override
  public void deleteById(long id) {
    parentDao.delete(id);
  }

}
