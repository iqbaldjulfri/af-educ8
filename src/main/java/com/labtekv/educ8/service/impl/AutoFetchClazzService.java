package com.labtekv.educ8.service.impl;

import java.util.Collection;
import java.util.List;

import com.labtekv.educ8.model.Clazz;
import com.labtekv.educ8.model.School;

import org.hibernate.Hibernate;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Iqbal Djulfri
 */
@Transactional(readOnly=true)
public class AutoFetchClazzService extends SimpleClazzService {
  private List fetchAttributes;

  public AutoFetchClazzService(List fetchAttributes) {
    this.fetchAttributes = fetchAttributes;
  }

  @Override
  public List<Clazz> getAll() {
    return getAll(null);
  }

  @Override
  public List<Clazz> getAll(Collection<School> schools) {
    return getByName(null, schools, 0);
  }

  @Override
  public Clazz getById(long id) {
    Clazz c = super.getById(id);
    fetch(c);
    return c;
  }

  @Override
  public List<Clazz> getByName(String name, int page) {
    return getByName(name, null, page);
  }

  @Override
  public List<Clazz> getByName(String name, Collection<School> schools, int page) {
    List<Clazz> clazzes = super.getByName(name, schools, page);
    for (Clazz clazz : clazzes) fetch(clazz);
    return clazzes;
  }

  private void fetch(Clazz c) {
    if (fetchAttributes.contains("all")) {
      Hibernate.initialize(c.getSchool());
      Hibernate.initialize(c.getHomeroomTeacher());
    } else {
      if (fetchAttributes.contains("school")) Hibernate.initialize(c.getSchool());
      if (fetchAttributes.contains("homeroomTeacher")) Hibernate.initialize(c.getHomeroomTeacher());
    }
  }

}
