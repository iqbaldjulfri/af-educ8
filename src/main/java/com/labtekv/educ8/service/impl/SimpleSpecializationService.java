package com.labtekv.educ8.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.labtekv.educ8.dao.SpecializationDao;
import com.labtekv.educ8.model.School;
import com.labtekv.educ8.model.Specialization;
import com.labtekv.educ8.service.SpecializationService;

import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Iqbal Djulfri
 */
@Service
@Transactional
public class SimpleSpecializationService implements SpecializationService {
  @Autowired @Qualifier("simpleSpecializationDao")
  private SpecializationDao specializationDao;

  public void setSpecializationDao(SpecializationDao specializationDao) {
    this.specializationDao = specializationDao;
  }

  @Override
  @Transactional(readOnly = true)
  public Specialization getById(long id) {
    return specializationDao.get(id);
  }

  @Override
  @Transactional(readOnly = true)
  public List<Specialization> getAll() {
    return specializationDao.getAll();
  }

  @Override
  @Transactional(readOnly = true)
  public List<Specialization> getAll(Collection<School> schools) {
    return getByName(null, schools, 0);
  }

  @Override
  @Transactional(readOnly = true)
  public List<Specialization> getByName(String name, int page) {
    return getByName(name, null, page);
  }

  @Override
  @Transactional(readOnly = true)
  public List<Specialization> getByName(String name, Collection<School> schools, int page) {
    List cs = new ArrayList(), os = new ArrayList();
    if (schools != null) {
      List ids = new ArrayList();
      for (School s : schools) ids.add(s.getId());
      cs.add(Restrictions.in("school.id", ids));
    }
    if (name != null) cs.add(Restrictions.ilike("name", name, MatchMode.ANYWHERE));
    os.add(Order.asc("name"));
    return specializationDao.get(cs, os, page);
  }

  @Override
  public long create(Specialization s) {
    return specializationDao.save(s);
  }

  @Override
  public void update(Specialization s) {
    specializationDao.update(s);
  }

  @Override
  public void delete(Specialization s) {
    specializationDao.delete(s);
  }

  @Override
  public void deleteById(long id) {
    specializationDao.delete(id);
  }
}
