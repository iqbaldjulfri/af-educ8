package com.labtekv.educ8.service.impl;

import java.util.Collection;
import java.util.List;

import com.labtekv.educ8.model.School;
import com.labtekv.educ8.model.Specialization;

import org.hibernate.Hibernate;

import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Iqbal Djulfri
 */
@Transactional(readOnly = true)
public class AutoFetchSpecializationService extends SimpleSpecializationService {
  private List fetchAttributes;

  public AutoFetchSpecializationService(List fetchAttributes) {
    this.fetchAttributes = fetchAttributes;
  }

  @Override
  public List<Specialization> getAll() {
    List<Specialization> scheduleSlots = super.getAll();
    for (Specialization scheduleSlot : scheduleSlots) fetch(scheduleSlot);
    return scheduleSlots;
  }

  @Override
  public List<Specialization> getAll(Collection<School> schools) {
    return getByName(null, schools, 0);
  }

  @Override
  public List<Specialization> getByName(String name, int page) {
    return getByName(name, null, page);
  }

  @Override
  public List<Specialization> getByName(String name, Collection<School> schools, int page) {
    List<Specialization> ss = super.getByName(name, schools, page);
    for (Specialization scheduleSlot : ss) fetch(scheduleSlot);
    return ss;
  }

  @Override
  public Specialization getById(long id) {
    Specialization ss = super.getById(id);
    fetch(ss);
    return ss;
  }

  private void fetch(Specialization s) {
    if (fetchAttributes.contains("all")) {
      Hibernate.initialize(s.getSchool());
    } else {
      if (fetchAttributes.contains("school")) Hibernate.initialize(s.getSchool());
    }
  }

}
