package com.labtekv.educ8.service.impl;

import java.util.List;

import com.labtekv.educ8.model.Course;
import com.labtekv.educ8.model.Exam;
import com.labtekv.educ8.model.Semester;

import org.hibernate.Hibernate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Iqbal Djulfri
 */
@Transactional(readOnly=true)
public class AutoFetchExamService extends SimpleExamService {
  private List<String> fetchAttributes;
  
  public AutoFetchExamService(List fetchAttributes) {
    this.fetchAttributes = fetchAttributes;
  }

  private Exam fetch(Exam e) {
    if (fetchAttributes.contains("all")) {
      Hibernate.initialize(e.getClazzes());
      Hibernate.initialize(e.getCourse());
      Hibernate.initialize(e.getFiles());
      Hibernate.initialize(e.getRemedialFor());
      Hibernate.initialize(e.getRemedials());
      Hibernate.initialize(e.getSemester());
      Hibernate.initialize(e.getType());
    } else {
      if (fetchAttributes.contains("clazzes")) Hibernate.initialize(e.getClazzes());
      if (fetchAttributes.contains("course")) Hibernate.initialize(e.getCourse());
      if (fetchAttributes.contains("files")) Hibernate.initialize(e.getFiles());
      if (fetchAttributes.contains("remedialFor")) Hibernate.initialize(e.getRemedialFor());
      if (fetchAttributes.contains("remedials")) Hibernate.initialize(e.getRemedials());
      if (fetchAttributes.contains("semester")) Hibernate.initialize(e.getSemester());
      if (fetchAttributes.contains("type")) Hibernate.initialize(e.getType());
    }

    return e;
  }

  private List<Exam> fetch(List<Exam> es) {
    for (Exam e : es) {
      fetch(e);
    }
    return es;
  }

  @Override
  public Exam getById(long id) {
    return fetch(super.getById(id));
  }

  @Override
  public List<Exam> getAll() {
    return fetch(super.getAll());
  }

  @Override
  public List<Exam> getAll(Course c, Semester s) {
    return fetch(super.getAll(c, s));
  }

  @Override
  public List<Exam> getByName(String name, int page) {
    return fetch(super.getByName(name, page));
  }
}
