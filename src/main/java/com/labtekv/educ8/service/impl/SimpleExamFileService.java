package com.labtekv.educ8.service.impl;

import com.labtekv.educ8.dao.ExamFileDao;
import com.labtekv.educ8.model.Exam;
import com.labtekv.educ8.model.ExamFile;
import com.labtekv.educ8.service.ExamFileService;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Iqbal Djulfri
 */
@Service
@Transactional
public class SimpleExamFileService implements ExamFileService {

  @Autowired
  @Qualifier("simpleExamFileDao")
  private ExamFileDao examFileDao;

  public void setExamFileDao(ExamFileDao examFileDao) {
    this.examFileDao = examFileDao;
  }

  @Override
  public ExamFile getById(long id) {
    return fetchExam(examFileDao.get(id));
  }

  @Override
  public List<ExamFile> getAll() {
    return fetchExam(examFileDao.getAll());
  }

  @Override
  public List<ExamFile> getAll(Exam exam) {
    List cs = new ArrayList();
    cs.add(Restrictions.eq("exam.id", exam.getId()));
    return fetchExam(examFileDao.get(cs, null, 0));
  }

  @Override
  public long create(ExamFile examFile) {
    return examFileDao.save(examFile);
  }

  @Override
  public void update(ExamFile examFile) {
    examFileDao.update(examFile);
  }

  @Override
  public void delete(ExamFile examFile) {
    examFileDao.delete(examFile);
  }

  @Override
  public void deleteById(long id) {
    examFileDao.delete(id);
  }

  private ExamFile fetchExam(ExamFile file) {
    Hibernate.initialize(file.getExam());
    return file;
  }

  private List<ExamFile> fetchExam(List<ExamFile> files) {
    for (ExamFile file : files) {
      fetchExam(file);
    }
    return files;
  }

}
