package com.labtekv.educ8.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.labtekv.educ8.dao.MessageDao;
import com.labtekv.educ8.model.Message;
import com.labtekv.educ8.service.MessageService;

import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Iqbal Djulfri
 */
@Service("simpleMessageService")
@Transactional
public class SimpleMessageService implements MessageService {

  @Autowired
  @Qualifier("simpleMessageDao")
  private MessageDao messageDao;

  public void setMessageDao(MessageDao messageDao) {
    this.messageDao = messageDao;
  }

  @Override
  @Transactional(readOnly=true)
  public Message getById(long id) {
    return messageDao.get(id);
  }

  @Override
  @Transactional(readOnly=true)
  public List<Message> getAll() {
    return messageDao.getAll();
  }

  @Override
  @Transactional(readOnly=true)
  public List<Message> getByUserId(long userId, Boolean deleted, int page) {
    return getByUserId(userId, null, deleted, page);
  }

  @Override
  @Transactional(readOnly=true)
  public List<Message> getByUserId(long userId, String query, Boolean deleted, int page) {
    List cs = new ArrayList(), os = new ArrayList();
    cs.add(Restrictions.eq("user.id", userId));
    if (query != null) {
      cs.add(Restrictions.or(
          Restrictions.ilike("subject", query, MatchMode.ANYWHERE),
          Restrictions.ilike("message", query, MatchMode.ANYWHERE)
          ));
    }
    if (deleted != null) {
      cs.add(Restrictions.eq("isDeleted", deleted));
    }
    os.add(Order.desc("time"));
    return messageDao.get(cs, os, page);
  }

  @Override
  public long create(Message m) {
    return messageDao.save(m);
  }

  @Override
  public void update(Message m) {
    messageDao.update(m);
  }

  @Override
  public void delete(Message m) {
    messageDao.delete(m);
  }

  @Override
  public void deleteById(long id) {
    messageDao.delete(id);
  }

}
