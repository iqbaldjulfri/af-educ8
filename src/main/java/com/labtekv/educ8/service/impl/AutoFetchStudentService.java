/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.labtekv.educ8.service.impl;

import com.labtekv.educ8.model.School;
import com.labtekv.educ8.model.Student;
import java.util.Collection;
import java.util.List;
import org.hibernate.Hibernate;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Andika
 */
@Transactional(readOnly = true)
public class AutoFetchStudentService extends SimpleStudentService {
  private List<String> fetchAttributes;

  public AutoFetchStudentService(List toBeFetchedAttributes) {
    this.fetchAttributes = toBeFetchedAttributes;
  }
  
  @Override
  public List<Student> getAll() {
    List<Student> students = super.getAll();
    for (Student student : students) fetchStudent(student);
    return students;
  }
  
  @Override
  public List<Student> getAll(Collection<School> schools) {
    List<Student> students = super.getAll(schools);
    for (Student student : students) fetchStudent(student);
    return students;
  }
  
  @Override
  public Student getById(long id) {
    Student student = super.getById(id);
    fetchStudent(student);
    return student;
  }

  @Override
  public List<Student> getByName(String name, int page) {
    List<Student> students = super.getByName(name, page);
    for (Student student : students) {
      fetchStudent(student);
    }

    return students;
  }
  
  @Override
  public List<Student> getByNameNumber(String name, String number, int page) {
    List<Student> students = super.getByNameNumber(name, number, page);
    for (Student student : students) {
      fetchStudent(student);
    }

    return students;
  }
  
  @Override
  public List<Student> getByName(String name, Collection<School> schools, int page) {
    List<Student> students = super.getByName(name, schools, page);
    for (Student student : students) fetchStudent(student);
    return students;
  }
  
  @Override
  public List<Student> getByNameNumber(String name, String number, Collection<School> schools, int page) {
    List<Student> students = super.getByNameNumber(name, number, schools, page);
    for (Student student : students) fetchStudent(student);
    return students;
  }

  private void fetchStudent(Student student) {    
    if (fetchAttributes.contains("all")) {
      Hibernate.initialize(student.getAddress());
      Hibernate.initialize(student.getSpecialization());
      Hibernate.initialize(student.getSchool());
      Hibernate.initialize(student.getFather());
      Hibernate.initialize(student.getMother());
      Hibernate.initialize(student.getGuardian());
    } else {
      if (fetchAttributes.contains("address")) Hibernate.initialize(student.getAddress());
      if (fetchAttributes.contains("specialization")) Hibernate.initialize(student.getSpecialization());
      if (fetchAttributes.contains("school")) Hibernate.initialize(student.getSchool());
      if (fetchAttributes.contains("father")) Hibernate.initialize(student.getFather());
      if (fetchAttributes.contains("mother")) Hibernate.initialize(student.getMother());
      if (fetchAttributes.contains("guardian")) Hibernate.initialize(student.getGuardian());
    }
  }
}
