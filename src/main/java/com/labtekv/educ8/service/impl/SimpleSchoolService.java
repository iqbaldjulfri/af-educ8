package com.labtekv.educ8.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.labtekv.educ8.dao.AddressDao;
import com.labtekv.educ8.dao.SchoolDao;
import com.labtekv.educ8.model.School;
import com.labtekv.educ8.service.SchoolService;
import com.labtekv.educ8.util.PageCounter;

import org.hibernate.Hibernate;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Iqbal Djulfri
 */
@Service
@Transactional
public class SimpleSchoolService implements SchoolService {

  @Autowired @Qualifier("simpleSchoolDao")
  private SchoolDao schoolDao;

  @Autowired @Qualifier("simpleAddressDao")
  private AddressDao addressDao;

  @Autowired
  private PageCounter pageCounter;

  public void setAddressDao(AddressDao addressDao) {
    this.addressDao = addressDao;
  }

  public void setPageCounter(PageCounter pageCounter) {
    this.pageCounter = pageCounter;
  }

  public void setSchoolDao(SchoolDao schoolDao) {
    this.schoolDao = schoolDao;
  }

  @Override
  public School getById(long id) {
    School school = schoolDao.get(id);
    if(school != null) Hibernate.initialize(school.getAddress());
    return school;
  }

  @Override
  public List getByName(String name, int page) {
    List cs = new ArrayList();
    cs.add(Restrictions.ilike("name", name, MatchMode.ANYWHERE));

    List os = new ArrayList();
    os.add(Order.asc("isActive"));
    os.add(Order.asc("name"));

    List<School> schools;
    schools = schoolDao.get(cs, null, page);
    for (School school : schools)
      Hibernate.initialize(school.getAddress().getStreet());

    return schools;
  }

  @Override
  public long getPageCountByName(String name) {
    List cs = new ArrayList();
    cs.add(Restrictions.ilike("name", name, MatchMode.ANYWHERE));

    return pageCounter.getMaxPage(schoolDao.count(cs));
  }

  @Override
  public long create(School school) {
    addressDao.save(school.getAddress());
    return schoolDao.save(school);
  }

  @Override
  public void update(School school) {
    addressDao.update(school.getAddress());
    schoolDao.update(school);
  }

  @Override
  public void delete(School school) {
    schoolDao.delete(school);
  }

  @Override
  public void deleteById(long id) {
    schoolDao.delete(id);
  }

  @Override
  public List getAll() {
    return schoolDao.getAll();
  }

}
