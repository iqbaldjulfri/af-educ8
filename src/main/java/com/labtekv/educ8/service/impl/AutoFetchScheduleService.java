package com.labtekv.educ8.service.impl;

import java.util.List;

import com.labtekv.educ8.model.Schedule;
import com.labtekv.educ8.model.Schedule;

import org.hibernate.Hibernate;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Iqbal Djulfri
 */
@Transactional(readOnly=true)
public class AutoFetchScheduleService extends SimpleScheduleService {
  private List fetchAttributes;

  public AutoFetchScheduleService(List fetchAttributes) {
    this.fetchAttributes = fetchAttributes;
  }

  @Override
  public Schedule getById(long id) {
    Schedule s = super.getById(id);
    fetch(s);
    return s;
  }

  @Override
  public List<Schedule> getAll() {
    List<Schedule> ss = super.getAll();
    for (Schedule s : ss) fetch(s);
    return ss;
  }

  @Override
  public List<Schedule> getByClazz(long id) {
    List<Schedule> ss = super.getByClazz(id);
    for (Schedule s : ss) fetch(s);
    return ss;
  }

  @Override
  public List<Schedule> getBySemester(long id) {
    List<Schedule> ss = super.getBySemester(id);
    for (Schedule s : ss) fetch(s);
    return ss;
  }

  @Override
  public List<Schedule> getByRoom(long id) {
    List<Schedule> ss = super.getByRoom(id);
    for (Schedule s : ss) fetch(s);
    return ss;
  }

  @Override
  public List<Schedule> getByCourse(long id) {
    List<Schedule> ss = super.getByCourse(id);
    for (Schedule s : ss) fetch(s);
    return ss;
  }

  @Override
  public List<Schedule> getBySlot(long id) {
    List<Schedule> ss = super.getBySlot(id);
    for (Schedule s : ss) fetch(s);
    return ss;
  }

  @Override
  public List<Schedule> getBySemesterClazz(long semesterId, Long clazzId) {
    List<Schedule> ss = super.getBySemesterClazz(semesterId, clazzId);
    for (Schedule s : ss) fetch(s);
    return ss;
  }

  private void fetch(Schedule s) {
    if (fetchAttributes.contains("all")) {
      Hibernate.initialize(s.getClazz());
      Hibernate.initialize(s.getCourse());
      Hibernate.initialize(s.getTeacher());
      Hibernate.initialize(s.getRoom());
      Hibernate.initialize(s.getSemester());
      Hibernate.initialize(s.getSlot());
    } else {
      if (fetchAttributes.contains("clazz")) Hibernate.initialize(s.getClazz());
      if (fetchAttributes.contains("course")) Hibernate.initialize(s.getCourse());
      if (fetchAttributes.contains("room")) Hibernate.initialize(s.getRoom());
      if (fetchAttributes.contains("semester")) Hibernate.initialize(s.getSemester());
      if (fetchAttributes.contains("slot")) Hibernate.initialize(s.getSlot());
      if (fetchAttributes.contains("teacher")) Hibernate.initialize(s.getTeacher());
    }
  }
}
