/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.labtekv.educ8.service.impl;

import com.labtekv.educ8.dao.ClazzAssignmentDao;
import com.labtekv.educ8.model.ClazzAssignment;
import com.labtekv.educ8.service.ClazzAssignmentService;
import com.labtekv.educ8.util.PageCounter;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Andika
 */

@Service
@Transactional
public class SimpleClazzAssignmentService implements ClazzAssignmentService {
  @Autowired @Qualifier("simpleClazzAssignmentDao")
  private ClazzAssignmentDao clazzAssignmentDao;
  
  @Autowired
  private PageCounter pageCounter;
  
  public void setClazzAssignmentDao(ClazzAssignmentDao clazzAssignmentDao) {
    this.clazzAssignmentDao = clazzAssignmentDao;
  }

  public void setPageCounter(PageCounter pageCounter) {
    this.pageCounter = pageCounter;
  }

  @Override
  public ClazzAssignment getById(long id) {
    return clazzAssignmentDao.get(id);
  }

  @Override
  public List<ClazzAssignment> getAll() {
    return getByClazzAndSemester(null, null, 0);
  }

  @Override
  public List<ClazzAssignment> getByClazzAndSemester(Long clazzId, Long semesterId, int page) {
    List cs = new ArrayList(), os = new ArrayList();
    if (clazzId != null) cs.add(Restrictions.eq("clazz.id", clazzId));
    if (semesterId != null) cs.add(Restrictions.eq("semester.id", semesterId));

    os.add(Order.asc("student.id"));
    return clazzAssignmentDao.get(cs, os, page);
  }
  
  @Override
  public long getPageCountAll() {
    return getPageCountByClazzAndSemester(null, null);
  }
  
  @Override
  public long getPageCountByClazzAndSemester(Long clazzId, Long semesterId) {
    List cs = new ArrayList();
    if (clazzId != null) cs.add(Restrictions.eq("clazz.id", clazzId));
    if (semesterId != null) cs.add(Restrictions.eq("semester.id", semesterId));

    return pageCounter.getMaxPage(clazzAssignmentDao.count(cs));
  }

  @Override
  public long create(ClazzAssignment clazzAssignment) {
    return clazzAssignmentDao.save(clazzAssignment);
  }

  @Override
  public void update(ClazzAssignment clazzAssignment) {
    clazzAssignmentDao.update(clazzAssignment);
  }

  @Override
  public void delete(ClazzAssignment clazzAssignment) {
    clazzAssignmentDao.delete(clazzAssignment);
  }

  @Override
  public void deleteById(long id) {
    clazzAssignmentDao.delete(id);
  }
}
