package com.labtekv.educ8.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.labtekv.educ8.dao.SemesterDao;
import com.labtekv.educ8.model.School;
import com.labtekv.educ8.model.Semester;
import com.labtekv.educ8.service.ActiveSemesterService;
import com.labtekv.educ8.service.SemesterService;

import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Iqbal Djulfri
 */
@Service
@Transactional
public class SimpleSemesterService implements SemesterService, ActiveSemesterService {

  @Autowired @Qualifier("simpleSemesterDao")
  private SemesterDao semesterDao;

  public void setSemesterDao(SemesterDao semesterDao) {
    this.semesterDao = semesterDao;
  }

  @Override
  @Transactional(readOnly = true)
  public Semester getById(long id) {
    return semesterDao.get(id);
  }

  @Override
  @Transactional(readOnly = true)
  public List<Semester> getAll() {
    return semesterDao.getAll();
  }

  @Override
  @Transactional(readOnly = true)
  public List<Semester> getAll(Collection<School> schools) {
    return getByName(null, schools, 0);
  }

  @Override
  @Transactional(readOnly = true)
  public List<Semester> getByName(String name, int page) {
    return getByName(name, null, page);
  }

  @Override
  @Transactional(readOnly = true)
  public List<Semester> getByName(String name, Collection<School> schools, int page) {
    List cs = new ArrayList(), os = new ArrayList();
    if (schools != null) {
      List ids = new ArrayList();
      for (School s : schools) ids.add(s.getId());
      cs.add(Restrictions.in("school.id", ids));
    }
    if (name != null) cs.add(Restrictions.ilike("name", name, MatchMode.ANYWHERE));
    os.add(Order.desc("start"));
    os.add(Order.desc("end"));
    return semesterDao.get(cs, os, page);
  }

  @Override
  public long create(Semester s) {
    return semesterDao.save(s);
  }

  @Override
  public void update(Semester s) {
    semesterDao.update(s);
  }

  @Override
  public void delete(Semester s) {
    semesterDao.delete(s);
  }

  @Override
  public void deleteById(long id) {
    semesterDao.delete(id);
  }

  @Override
  public Semester get() {
    return get(new Date());
  }

  @Override
  public Semester get(Date date) {
    List cs = new ArrayList();
    cs.add(Restrictions.le("start", date));
    cs.add(Restrictions.ge("end", date));
    try {
      return semesterDao.get(cs, null, 1, 1).get(0);
    } catch (IndexOutOfBoundsException e) {
      return null;
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

}
