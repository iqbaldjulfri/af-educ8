package com.labtekv.educ8.service;

import java.util.List;

import com.labtekv.educ8.model.Clazz;
import com.labtekv.educ8.model.Course;
import com.labtekv.educ8.model.Room;
import com.labtekv.educ8.model.Schedule;
import com.labtekv.educ8.model.Semester;
import com.labtekv.educ8.model.Teacher;

/**
 *
 * @author Iqbal Djulfri
 */
public interface ScheduleService {

  public Schedule getById(long id);
  public List<Schedule> getAll();
  public List<Schedule> getByClazz(long id);
  public List<Schedule> getBySemester(long id);
  public List<Schedule> getByRoom(long id);
  public List<Schedule> getByCourse(long id);
  public List<Schedule> getBySlot(long id);
  public List<Schedule> getBySemesterClazz(long semesterId, Long clazzId);

  public long create(Schedule s);
  public void update(Schedule s);
  public void delete(Schedule s);
  public void deleteById(long id);

  public void createAllForSemesterClazz(Semester semester, Clazz clazz);
  public void update(Schedule s, Course course, Room room, Teacher teacher, boolean lock);
  public void clear(Schedule s);
  public void copy(Schedule source, Schedule target);
  public void move(Schedule source, Schedule target);
  public void switch_(Schedule source, Schedule target);

  public List<Schedule> generate(Semester s) throws Exception;
  public List<Schedule> generate(Semester s, int day) throws Exception;
  public List validate(Semester s, Integer day, boolean validateRoom);
}
