package com.labtekv.educ8.service;

import java.util.Collection;
import java.util.List;
import com.labtekv.educ8.model.Room;
import com.labtekv.educ8.model.School;

/**
 *
 * @author Iqbal Djulfri
 */
public interface RoomService {

  public Room getById(long id);
  public List<Room> getByName(String name, int page);
  public List<Room> getByName(String name, Collection<School> schools, int page);
  public List<Room> getAll();
  public List<Room> getAll(Collection<School> schools);

  public long create(Room room);
  public void update(Room room);
  public void delete(Room room);
  public void deleteById(long id);

}
