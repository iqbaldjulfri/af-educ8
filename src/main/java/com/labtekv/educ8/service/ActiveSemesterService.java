
package com.labtekv.educ8.service;

import java.util.Date;

import com.labtekv.educ8.model.Semester;

/**
 *
 * @author Iqbal Djulfri
 */
public interface ActiveSemesterService {
  public Semester get();
  public Semester get(Date date);
}
