
package com.labtekv.educ8.service;

import java.util.List;

import com.labtekv.educ8.model.Role;
/**
 *
 * @author Alienware
 */
public interface RoleService {
	public Role getById(long id);
	public Role getByAuthority(String authority);
  public List<Role> getByAuthority(String authority, int page);
  public List<Role> getByDescription(String description, int page);
  public void create(Role role);
  public void update(Role role);
  public void delete(Role role);
  public void deleteById(long id);

  public List<Role> getAll();
}
