/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.labtekv.educ8.service;

import com.labtekv.educ8.model.Parent;
import java.util.List;

/**
 *
 * @author Andika
 */
public interface ParentService {
  public Parent getById(long id);
  
  public List<Parent> getByName(String name, int page);
  //public List<Parent> getByName(String name, Collection<Student> students, int page);
  public List<Parent> getAll();
  //public List<Parent> getAll(Collection<Student> students);
  public long getPageCountByName(String name);
  //public long getPageCountByName(String name, Collection<Student> students, int page);

  public long create(Parent parent);
  public void update(Parent parent);
  public void delete(Parent parent);
  public void deleteById(long id);
}
