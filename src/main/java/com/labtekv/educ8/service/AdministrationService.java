
package com.labtekv.educ8.service;

import com.labtekv.educ8.model.Administration;
import com.labtekv.educ8.model.School;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Andika
 */
public interface AdministrationService {
  public Administration getById(long id);
  public List<Administration> getByName(String name, int page);
  public List<Administration> getByName(String name, Collection<School> schools, int page);
  public List<Administration> getAll();
  public List<Administration> getAll(Collection<School> schools);
  public long getPageCountByName(String name);
  public long getPageCountByName(String name, Collection<School> schools, int page);

  public long create(Administration administration);
  public void update(Administration administration);
  public void delete(Administration administration);
  public void deleteById(long id);

}
