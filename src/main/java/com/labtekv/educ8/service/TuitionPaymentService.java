/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.labtekv.educ8.service;

import com.labtekv.educ8.model.School;
import com.labtekv.educ8.model.TuitionPayment;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Andika
 */
public interface TuitionPaymentService {
  public TuitionPayment getById(long id);
  public List<TuitionPayment> getAll();
  public List<TuitionPayment> getAll(Collection<School> schools);
  public List<TuitionPayment> getByStudentAndMonthAndYear(String studentId, Integer month, Integer year, int page);
  public List<TuitionPayment> getByStudentAndMonthAndYear(String studentId, Integer month, Integer year, Collection<School> schools, int page);
  
  public long getPageCountByStudentAndMonthAndYear(String studentId, Integer month, Integer year);
  public long getPageCountByStudentAndMonthAndYear(String studentId, Integer month, Integer year, Collection<School> schools);

  public long create(TuitionPayment tp);
  public void update(TuitionPayment tp);
  public void delete(TuitionPayment tp);
  public void deleteById(long id);
  
}
