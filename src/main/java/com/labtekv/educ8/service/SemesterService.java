package com.labtekv.educ8.service;

import java.util.Collection;
import java.util.List;

import com.labtekv.educ8.model.School;
import com.labtekv.educ8.model.Semester;

/**
 *
 * @author Iqbal Djulfri
 */
public interface SemesterService {
  public Semester getById(long id);
  public List<Semester> getAll();
  public List<Semester> getAll(Collection<School> schools);
  public List<Semester> getByName(String desc, int page);
  public List<Semester> getByName(String desc, Collection<School> schools, int page);

  public long create(Semester ss);
  public void update(Semester ss);
  public void delete(Semester ss);
  public void deleteById(long id);
}
