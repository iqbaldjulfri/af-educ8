package com.labtekv.educ8.service;

import java.util.Collection;
import java.util.List;

import com.labtekv.educ8.model.School;
import com.labtekv.educ8.model.Specialization;

/**
 *
 * @author Iqbal Djulfri
 */
public interface SpecializationService {
  public Specialization getById(long id);
  public List<Specialization> getAll();
  public List<Specialization> getAll(Collection<School> schools);
  public List<Specialization> getByName(String desc, int page);
  public List<Specialization> getByName(String desc, Collection<School> schools, int page);

  public long create(Specialization s);
  public void update(Specialization s);
  public void delete(Specialization s);
  public void deleteById(long id);
}
