package com.labtekv.educ8.service;

import java.util.Collection;
import java.util.List;
import com.labtekv.educ8.model.Clazz;
import com.labtekv.educ8.model.School;

/**
 *
 * @author Iqbal Djulfri
 */
public interface ClazzService {
  public Clazz getById(long id);
  public List<Clazz> getAll();
  public List<Clazz> getAll(Collection<School> schools);
  public List<Clazz> getByName(String name, int page);
  public List<Clazz> getByName(String name, Collection<School> schools, int page);

  public long create(Clazz clazz);
  public void update(Clazz clazz);
  public void delete(Clazz clazz);
  public void deleteById(long id);

}
