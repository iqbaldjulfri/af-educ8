package com.labtekv.educ8.service;

import java.util.List;

import com.labtekv.educ8.model.Course;
import com.labtekv.educ8.model.Teacher;

/**
 *
 * @author Iqbal Djulfri
 */
public interface CourseTeacherService {
  public List<Course> getAll(Teacher t);
  public List<Course> get(Teacher t, int page);
}
