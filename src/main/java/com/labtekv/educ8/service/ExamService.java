package com.labtekv.educ8.service;

import com.labtekv.educ8.model.Exam;
import java.util.List;

import com.labtekv.educ8.model.Course;
import com.labtekv.educ8.model.Semester;

/**
 *
 * @author Iqbal Djulfri
 */
public interface ExamService {
  public Exam getById(long id);
  public List<Exam> getAll();
  public List<Exam> getAll(Course c, Semester s);
  public List<Exam> getByName(String name, int page);

  public long create(Exam exam);
  public void update(Exam exam);
  public void delete(Exam exam);
  public void deleteById(long id);
}
