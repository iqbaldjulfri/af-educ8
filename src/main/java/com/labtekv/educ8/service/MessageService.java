package com.labtekv.educ8.service;

import java.util.List;

import com.labtekv.educ8.model.Message;

/**
 *
 * @author Iqbal Djulfri
 */
public interface MessageService {
  public Message getById(long id);
  public List<Message> getAll();
  public List<Message> getByUserId(long userId, Boolean deleted, int page);
  public List<Message> getByUserId(long userId, String query, Boolean deleted, int page);

  public long create(Message m);
  public void update(Message m);
  public void delete(Message m);
  public void deleteById(long id);
}
