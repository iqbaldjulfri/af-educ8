/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.labtekv.educ8.service;

import com.labtekv.educ8.model.ClazzAssignment;
import java.util.List;

/**
 *
 * @author Andika
 */
public interface ClazzAssignmentService {
  public ClazzAssignment getById(long id);
  public List<ClazzAssignment> getAll();
  public List<ClazzAssignment> getByClazzAndSemester(Long clazzId, Long semesterId, int page);
  public long getPageCountAll();
  public long getPageCountByClazzAndSemester(Long clazzId, Long semesterId);

  public long create(ClazzAssignment clazzAssignment);
  public void update(ClazzAssignment clazzAssignment);
  public void delete(ClazzAssignment clazzAssignment);
  public void deleteById(long id);
}
