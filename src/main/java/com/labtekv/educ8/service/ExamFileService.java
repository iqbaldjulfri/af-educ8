package com.labtekv.educ8.service;

import com.labtekv.educ8.model.Exam;
import com.labtekv.educ8.model.ExamFile;
import java.util.List;

/**
 *
 * @author Iqbal Djulfri
 */
public interface ExamFileService {
  public ExamFile getById(long id);
  public List<ExamFile> getAll();
  public List<ExamFile> getAll(Exam exam);

  public long create(ExamFile examFile);
  public void update(ExamFile examFile);
  public void delete(ExamFile examFile);
  public void deleteById(long id);
}
