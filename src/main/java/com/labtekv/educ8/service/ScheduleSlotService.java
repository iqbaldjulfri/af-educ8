package com.labtekv.educ8.service;

import java.util.Collection;
import java.util.List;

import com.labtekv.educ8.model.ScheduleSlot;
import com.labtekv.educ8.model.School;

/**
 *
 * @author Iqbal Djulfri
 */
public interface ScheduleSlotService {

  public ScheduleSlot getById(long id);
  public List<ScheduleSlot> getAll();
  public List<ScheduleSlot> getAll(Collection<School> schools);
  public List<ScheduleSlot> getByDescription(String desc, int page);
  public List<ScheduleSlot> getByDescription(String desc, Collection<School> schools, int page);

  public long create(ScheduleSlot ss);
  public void update(ScheduleSlot ss);
  public void delete(ScheduleSlot ss);
  public void deleteById(long id);

}
