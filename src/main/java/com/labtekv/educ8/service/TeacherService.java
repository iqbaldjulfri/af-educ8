/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.labtekv.educ8.service;

import com.labtekv.educ8.model.School;
import com.labtekv.educ8.model.Teacher;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Andika
 */
public interface TeacherService {
  public Teacher getById(long id);
  
  public List<Teacher> getByName(String name, int page);
  public List<Teacher> getByName(String name, Collection<School> schools, int page);
  public List<Teacher> getAll();
  public List<Teacher> getAll(Collection<School> schools);
  public long getPageCountByName(String name);
  public long getPageCountByName(String name, Collection<School> schools, int page);

  public long create(Teacher teacher);
  public void update(Teacher teacher);
  public void delete(Teacher teacher);
  public void deleteById(long id);
}
