package com.labtekv.educ8.service;

import java.util.Collection;
import java.util.List;

import com.labtekv.educ8.model.Course;
import com.labtekv.educ8.model.School;

/**
 *
 * @author Iqbal Djulfri
 */
public interface CourseService {
  public Course getById(long id);
  public List<Course> getAll();
  public List<Course> getAll(Collection<School> schools);
  public List<Course> getByName(String name, int page);
  public List<Course> getByName(String name, Collection<School> schools, int page);

  public long create(Course c);
  public void update(Course c);
  public void delete(Course c);
  public void deleteById(long id);
}
