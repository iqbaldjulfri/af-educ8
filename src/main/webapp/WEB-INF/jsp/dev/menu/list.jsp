<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<table width="100%" cellpadding="3px" cellspacing="1px">
  <thead>
    <tr>
      <td><spring:message code="af.model.menu.name"/></td>
      <td><spring:message code="af.model.menu.url"/></td>
      <td><spring:message code="af.model.menu.description"/></td>
      <td></td>
    </tr>
  </thead>
  <tbody><% int c = 0;%>
    <c:forEach items="${menus}" var="m">
      <tr class="<%= ++c % 2 == 0 ? "af-table-row-alt" : "" %>">
        <td><a href="<c:url value="${m.id}"/>" title="${m.name}">${m.name}</a></td>
        <td>${m.url}</td>
        <td>${m.description}</td>
        <td><a href="#" onclick="deleteMenu(${m.id});"><spring:message code="af.delete"/></a></td>
      </tr>
    </c:forEach>
  </tbody>
</table>