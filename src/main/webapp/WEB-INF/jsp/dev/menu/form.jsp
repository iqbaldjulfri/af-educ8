<%--
    Document   : form
    Created on : May 10, 2012, 11:25:15 AM
    Author     : Iqbal Djulfri
--%>


<%@include file="/WEB-INF/jsp/includes.jsp" %>
<c:set var="title" value="Menu Details" scope="request"/>
<c:set var="titleCode" value="af.title.menu-details" scope="request"/>
<%@include file="/WEB-INF/jsp/header.jsp" %>
<script type="text/javascript" src="<c:url value="/static/js/dev/menu/form.js"/>"></script>

<form:form modelAttribute="menu" method="post" action="submit">
  <div>
    <spring:message code="af.model.menu.name"/>:
    <form:errors path="name" cssClass="errors"/><br/>
    <form:input path="name" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.menu.url"/>:
    <form:errors path="url" cssClass="errors"/><br/>
    <form:input path="url" disabled="${readOnly}" size="100"/>
  </div>
  <div>
    <spring:message code="af.model.menu.description"/>:
    <form:errors path="description" cssClass="errors"/><br/>
    <form:input path="description" disabled="${readOnly}" size="100"/>
  </div>
  <div>
    <spring:message code="af.model.menu.priority"/>:
    <form:errors path="priority" cssClass="errors"/><br/>
    <form:input path="priority" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.menu.parent"/>:
    <form:errors path="parent" cssClass="errors"/><br/>
    <form:hidden path="parent" id="parentId"/>
    <input type="text" id="parentName" ${readOnly ? "disabled=\"true\"" : ""} value="${menu.parent.name}"/>
  </div>
  <div>
    <spring:message code="af.model.menu.roles"/>:
    <form:errors path="roles" cssClass="errors"/><br/>
    <form:checkboxes path="roles" items="${roles}" itemLabel="description" itemValue="id" disabled="${readOnly}"/>
  </div>
  <div>
    <c:choose>
      <c:when test="${readOnly}">
        <a href="<c:url value="edit-${menu.id}"/>"><spring:message code="af.edit"/></a>
      </c:when>
      <c:otherwise>
        <input type="submit" value="<spring:message code="af.save"/>"/>
      </c:otherwise>
    </c:choose>
    <a href="<c:url value="../menu/"/>"><spring:message code="af.cancel"/></a>
  </div>
</form:form>

<%@include file="/WEB-INF/jsp/footer.jsp" %>
