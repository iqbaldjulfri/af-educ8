<%--
    Document   : search
    Created on : May 10, 2012, 12:20:44 PM
    Author     : Alienware
--%>

<%@include file="/WEB-INF/jsp/includes.jsp" %>
<c:set var="title" value="Role Administration" scope="request"/>
<c:set var="titleCode" value="af.title.role-administration" scope="request"/>
<%@include file="/WEB-INF/jsp/header.jsp" %>
<script type="text/javascript" src="<c:url value="/static/js/dev/role/search.js"/>"></script>

<label for="authority"><spring:message code="af.model.role.authority"/>:</label><br/>
<input type="text" id="authority"/><br/>
<br/>
<div id="action">
  <div class="af-left">
    <button id="search"><spring:message code="af.search"/></button>
    <a href="<c:url value="new"/>"><spring:message code="af.create-new"/></a>
  </div>
  <div class="af-right">
    <button id="prev"><spring:message code="af.prev"/></button>
    <input type="text" id="page" size="2"/>
    <button id="next"><spring:message code="af.next"/></button>
  </div>
  <div class="af-clear"></div>
</div>
  
<div id="list">
</div>
<%@include file="/WEB-INF/jsp/footer.jsp" %>
