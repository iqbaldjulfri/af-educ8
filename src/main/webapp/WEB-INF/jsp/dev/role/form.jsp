<%--
    Document   : form
    Created on : May 10, 2012, 12:30:09 PM
    Author     : Alienware
--%>

<%@include file="/WEB-INF/jsp/includes.jsp" %>
<c:set var="title" value="Role Details" scope="request"/>
<c:set var="titleCode" value="af.title.role-details" scope="request"/>
<%@include file="/WEB-INF/jsp/header.jsp" %>
<form:form modelAttribute="role" method="post" action="submit">
  <div>
    <spring:message code="af.model.role.authority"/>:
    <form:errors path="authority" cssClass="errors"/><br/>
    <form:input path="authority" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.role.description"/>:
    <form:errors path="description" cssClass="errors"/><br/>
    <form:input path="description" disabled="${readOnly}"/>
  </div>
  <div>
    <c:choose>
      <c:when test="${readOnly}">
        <a href="<c:url value="edit-${role.id}"/>"><spring:message code="af.edit"/></a>
      </c:when>
      <c:otherwise>
        <input type="submit" value="<spring:message code="af.save"/>"/>
      </c:otherwise>
    </c:choose>
    <a href="<c:url value="../role/"/>"><spring:message code="af.cancel"/></a>
  </div>
</form:form>
<%@include file="/WEB-INF/jsp/footer.jsp" %>
