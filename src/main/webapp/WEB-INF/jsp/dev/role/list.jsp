<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<table width="100%" cellpadding="3px" cellspacing="1px">
  <thead>
    <tr>
      <td><spring:message code="af.model.role.authority"/></td>
      <td><spring:message code="af.model.role.description"/></td>
      <td></td>
    </tr>
  </thead>
  <tbody><% int c = 0;%>
    <c:forEach items="${roles}" var="r">
      <tr class="<%= ++c % 2 == 0 ? "af-table-row-alt" : "" %>">
        <td><a href="<c:url value="${r.id}"/>" title="${r.authority}">${r.authority}</a></td>
        <td>${r.description}</td>
        <td><a href="#" onclick="deleteRole(${r.id});"><spring:message code="af.delete"/></a></td>
      </tr>
    </c:forEach>
  </tbody>
</table>