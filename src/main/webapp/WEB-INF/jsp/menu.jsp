<%@page import="org.springframework.web.servlet.tags.RequestContextAwareTag"%>
<%@page import="org.springframework.web.servlet.support.RequestContext"%>
<%@page import="org.springframework.context.MessageSource"%>
<%@page import="java.io.IOException"%>
<%@page import="com.labtekv.educ8.model.Menu"%>
<%@page import="java.util.List"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%!
void parseMenu(List<Menu> menus, HttpServletRequest req, JspWriter out, PageContext pageContext) throws IOException {
  if (menus == null || menus.isEmpty()) return;

  RequestContext requestContext = (RequestContext) pageContext.getAttribute(RequestContextAwareTag.REQUEST_CONTEXT_PAGE_ATTRIBUTE);
  MessageSource messageSource = requestContext.getMessageSource();

  for (Menu m : menus) {
    if (m.getChildren() == null || m.getChildren().isEmpty()) {
      out.println("<li><a href=\"" + req.getContextPath() + m.getUrl() + "\" title=\"" + m.getDescription() + "\">");
      out.print(messageSource.getMessage("af.menu.[" + m.getUrl() + "]", null, m.getName(), requestContext.getLocale()));
      out.println("</a></li>");
    } else {
      out.println("<li>");
      out.println("<a href=\"" + req.getContextPath() + m.getUrl() + "\" title=\"" + m.getDescription() + "\" onclick=\"return false;\">");
      out.print(messageSource.getMessage("af.menu.[" + m.getUrl() + "]", null, m.getName(), requestContext.getLocale()));
      out.println("</a>");
      out.println("<ul>");
      parseMenu(m.getChildren(), req, out, pageContext);
      out.println("</ul>");
    }
  }
}
%>