<%--
    Document   : index
    Created on : Jun 5, 2012, 10:12:17 AM
    Author     : Andika
--%>

<%@include file="/WEB-INF/jsp/includes.jsp" %>
<c:set var="title" value="Schedule Management" scope="request"/>
<c:set var="titleCode" value="af.title.schedule-management" scope="request"/>
<%@include file="/WEB-INF/jsp/header.jsp" %>
<script type="text/javascript" src="<c:url value="/static/js/secure/headmaster/schedule/index.js"/>"></script>
<label for="semester"><spring:message code="af.model.semester"/>:</label><br/>
<select id="semester">
  <c:forEach items="${semesters}" var="s">
    <option value="${s.id}">${s.name}</option>
  </c:forEach>
</select>
<br/>
<br/>
<div id="action">
  <div class="af-left">
    <button id="search"><spring:message code="af.manage"/></button>
  </div>
  <div class="af-right">
  </div>
  <div class="af-clear"></div>
</div>
<iframe id="list" frameborder="0" width="100%">

</iframe>
<%@include file="/WEB-INF/jsp/footer.jsp" %>