<%--
    Document   : index
    Created on : Jun 5, 2012, 10:12:17 AM
    Author     : Andika
--%>

<%@include file="/WEB-INF/jsp/includes.jsp" %>
<c:set var="title" value="Schedule Management" scope="request"/>
<c:set var="titleCode" value="af.title.schedule-management" scope="request"/>
<%@include file="/WEB-INF/jsp/header.jsp" %>
<script type="text/javascript" src="<c:url value="/static/js/secure/headmaster/schedule/details.js"/>"></script>
<style type="text/css">
.sch-main-cont { overflow-x: auto; max-width: 100%; }
.sch-clazz-cont { overflow: auto; width: 1400px; }
.sch-day-cont { float: left; border: 1px solid black; width: 198px; }
.sch-day-name { font-weight: bold; font-size: 14px; padding: 5px; }
.sch-slot-cont { border: 1px solid grey; margin: 5px 5px 10px; }
.sch-slot-title { border-bottom: 1px dotted black; padding: 2px 5px; }
.sch-cont { margin: 5px; border: 1px solid black; text-align: right; }
.sch-title { height: 10px; background-color: royalblue; border-bottom: 1px dotted black; cursor: move; }
.sch-error { height: 10px; background-color: red; border-bottom: 1px dotted black; cursor: move; }
.sch-details { padding: 5px; background-color: white; }
.sch-details .ui-button-text  { padding: 0.1em 0.5em 0.1em 2em; }
.sch-textfield { width: 105px; }
.sch-gen-dlg-cont { position: fixed; top: 50%; left: 50%; margin: -50px -150px; }
</style>

<h4 semesterId="${semester.id}">Semester: ${semester.name}</h4>
<input type="hidden" value="${semester.school.id}" id="schoolId"/>
<button id="toggle-tabs" onclick="toggleTabs()">Toggle Tabs</button>
<div id="clz-tabs">
  <ul>
    <c:forEach items="${clazzes}" var="clazz">
      <li><a href="#clz-${clazz.id}">${clazz.name}</a></li>
    </c:forEach>
  </ul>

  <c:forEach items="${clazzes}" var="clazz">
    <div id="clz-${clazz.id}">
      <h5>${clazz.name}</h5>
      <div class="sch-main-cont">
        <div class="sch-clazz-cont">
          <c:forEach items="${days}" var="day">
            <div class="sch-day-cont">
              <div class="sch-day-name">
                <spring:message code="af.days.${day}"/>
              </div>
                <c:forEach items="${holder[clazz][day]['slots']}" var="slot">
                  <div class="sch-slot-cont ui-corner-all">
                    <div class="sch-slot-title">
                      <b>
                        <fmt:formatDate value="${slot.start}" pattern="HH:mm"/> -
                        <fmt:formatDate value="${slot.end}" pattern="HH:mm"/>
                      </b>
                      (${slot.description})
                    </div>
                    <div class="sch-cont ui-corner-all sch-day-${day}" id="sch-${holder[clazz][day][slot].id}">
                      <div class="sch-title"></div>
                      <div class="sch-details">
                        Room:
                        <input type="hidden"
                               value="${holder[clazz][day][slot].room.id}"
                               id="roomid-${holder[clazz][day][slot].id}"/>
                        <input type="text"
                               value="${holder[clazz][day][slot].room.name}"
                               id="roomname-${holder[clazz][day][slot].id}"
                               class="sch-textfield"
                               onfocus="createRoomLookup(this);"
                               <c:if test="${holder[clazz][day][slot].isLocked == true}">disabled="true"</c:if>/>
                        <br/>
                        Course:
                        <input type="hidden"
                               value="${holder[clazz][day][slot].course.id}"
                               id="courseid-${holder[clazz][day][slot].id}"/>
                        <input type="text"
                               value="${holder[clazz][day][slot].course.name}"
                               id="coursename-${holder[clazz][day][slot].id}"
                               class="sch-textfield"
                               onfocus="createCourseLookup(this);"
                               <c:if test="${holder[clazz][day][slot].isLocked == true}">disabled="true"</c:if>/><br/>
                        Teacher:
                        <input type="hidden"
                               value="${holder[clazz][day][slot].teacher.id}"
                               id="teacherid-${holder[clazz][day][slot].id}"/>
                        <input type="text"
                               id="teachername-${holder[clazz][day][slot].id}"
                               value="${holder[clazz][day][slot].teacher.name}"
                               onfocus="createTeacherLookup(this);"
                               class="sch-textfield"
                               <c:if test="${holder[clazz][day][slot].isLocked == true}">disabled="true"</c:if>/><br/>
                        <input type="checkbox"
                               name="lock-${holder[clazz][day][slot].id}"
                               id="lock-${holder[clazz][day][slot].id}-y"
                               onchange="update($(this).parent().parent());"
                               <c:if test="${holder[clazz][day][slot].isLocked == true}">checked="true"</c:if>/>
                        <label for="lock-${holder[clazz][day][slot].id}-y"
                               class="sch-lock-lbl">Locked?</label>
                        <button class="sch-btn-clear ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only"
                                onclick="clearSchedule($(this).parent().parent())" role="button" aria-disabled="false" title="Clear">
                          <span class="ui-button-icon-primary ui-icon ui-icon-trash"></span>
                          <span class="ui-button-text">Clear</span>
                        </button>
                      </div>
                    </div>
                  </div>
                </c:forEach>
            </div>
          </c:forEach>
        </div>
      </div>
    </div>
  </c:forEach>
</div>
<div>
  <div>
    <button id="validate">Validate</button>

    <button id="generate" style="float: right;">Generate</button>
    <button id="lockday" style="float: right;">Lock Day</button>
    <button id="unlockday" style="float: right;">Unlock Day</button>
    <button id="clearday" style="float: right;">Clear Day</button>
    <select id="day" style="float: right;">
      <option value="0"><spring:message code="af.days.0"/></option>
      <option value="1"><spring:message code="af.days.1"/></option>
      <option value="2"><spring:message code="af.days.2"/></option>
      <option value="3"><spring:message code="af.days.3"/></option>
      <option value="4"><spring:message code="af.days.4"/></option>
      <option value="5"><spring:message code="af.days.5"/></option>
      <option value="6"><spring:message code="af.days.6"/></option>
    </select>
  </div>
  <div id="errList">
    <ol>

    </ol>
  </div>
</div>
<div id="generateDialog">
  <p>
    Your request is being generated please be patient.
    It may take a long time depending on the data provided.
    You will be notified when the process is complete.
  </p>
  <br/><br/>
  <div id="progressbar" class="af-progressbar-animated"></div>
</div>

<%@include file="/WEB-INF/jsp/footer.jsp" %>