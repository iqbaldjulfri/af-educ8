<%--
    Document   : form
    Created on : Apr 23, 2012, 11:14:24 AM
    Author     : Iqbal Djulfri
--%>

<%@include file="/WEB-INF/jsp/includes.jsp" %>
<c:set var="title" value="Course Details" scope="request"/>
<c:set var="titleCode" value="af.title.course-details" scope="request"/>
<%@include file="/WEB-INF/jsp/header.jsp" %>
<script type="text/javascript" src="<c:url value="/static/js/secure/administration/course/form.js"/>"></script>
<form:form modelAttribute="course" method="post" action="submit">
  <div>
    <spring:message code="af.model.course.code"/>:
    <form:errors path="code" cssClass="af-errors"/><br/>
    <form:input path="code" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.course.name"/>:
    <form:errors path="name" cssClass="af-errors"/><br/>
    <form:input path="name" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.course.description"/>:
    <form:errors path="description" cssClass="af-errors"/><br/>
    <form:textarea path="description" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.course.maxSchedulePerWeek"/>:
    <form:errors path="maxSchedulePerWeek" cssClass="af-errors"/><br/>
    <form:input path="maxSchedulePerWeek" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.course.maxSchedulePerDay"/>:
    <form:errors path="maxSchedulePerDay" cssClass="af-errors"/><br/>
    <form:input path="maxSchedulePerDay" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.course.maxConsecutivePerDay"/>:
    <form:errors path="maxConsecutivePerDay" cssClass="af-errors"/><br/>
    <form:input path="maxConsecutivePerDay" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.course.syllabus"/>:
    <form:errors path="syllabus" cssClass="af-errors"/><br/>
    <form:textarea path="syllabus" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.course.grade"/>:
    <form:errors path="grade" cssClass="af-errors"/><br/>
    <form:input path="grade" disabled="${readOnly}" id="grade"/>
  </div>
  <div>
    <spring:message code="af.model.course.defaultRoom"/>:
    <form:errors path="defaultRoom" cssClass="af-errors"/><br/>
    <form:select path="defaultRoom" disabled="${readOnly}">
      <form:option value="">---</form:option>
      <form:options items="${rooms}" itemLabel="name" itemValue="id"/>
    </form:select>
  </div>
  <div>
    <spring:message code="af.model.course.school"/>:
    <form:errors path="school" cssClass="af-errors"/><br/>
    <form:select path="school" disabled="${readOnly}">
      <form:options items="${schools}" itemLabel="name" itemValue="id"/>
    </form:select>
  </div>
  <div>
    <spring:message code="af.model.course.teacher"/>:
    <form:errors path="teachers" cssClass="af-errors"/><br/>
    <form:checkboxes path="teachers" disabled="${readOnly}" items="${teachers}" itemValue="id" itemLabel="name"/>
  </div>
  <div>
    <spring:message code="af.model.course.isActive"/>:
    <form:errors path="isActive" cssClass="af-errors"/><br/>
    <form:select path="isActive" disabled="${readOnly}">
      <form:option value="true"><spring:message code="af.yes"/></form:option>
      <form:option value="false"><spring:message code="af.no"/></form:option>
    </form:select>
  </div>
  <div>
    <c:choose>
      <c:when test="${readOnly}">
        <a href="<c:url value="edit-${course.id}"/>"><spring:message code="af.edit"/></a>
        &nbsp;&nbsp;
      </c:when>
      <c:otherwise>
        <input type="submit" value="<spring:message code="af.save"/>"/>
      </c:otherwise>
    </c:choose>
    <a href="<c:url value="../course/"/>"><spring:message code="af.cancel"/></a>
  </div>
</form:form>
<%@include file="/WEB-INF/jsp/footer.jsp" %>
