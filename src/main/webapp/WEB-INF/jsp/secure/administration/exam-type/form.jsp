<%--
    Document   : form
    Created on : Apr 23, 2012, 11:14:24 AM
    Author     : Iqbal Djulfri
--%>

<%@include file="/WEB-INF/jsp/includes.jsp" %>
<c:set var="title" value="Exam Type Details" scope="request"/>
<c:set var="titleCode" value="af.title.examType-details" scope="request"/>
<%@include file="/WEB-INF/jsp/header.jsp" %>
<form:form modelAttribute="examType" method="post" action="submit">
  <div>
    <spring:message code="af.model.examType.name"/>:
    <form:errors path="name" cssClass="af-errors"/><br/>
    <form:input path="name" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.examType.code"/>:
    <form:errors path="code" cssClass="af-errors"/><br/>
    <form:input path="code" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.examType.description"/>:
    <form:errors path="description" cssClass="af-errors"/><br/>
    <form:textarea path="description" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.examType.isActive"/>:
    <form:errors path="isActive" cssClass="af-errors"/><br/>
    <form:select path="isActive" disabled="${readOnly}">
      <form:option value="true"><spring:message code="af.yes"/></form:option>
      <form:option value="false"><spring:message code="af.no"/></form:option>
    </form:select>
  </div>
  <div>
    <spring:message code="af.model.examType.isVisible"/>:
    <form:errors path="isVisible" cssClass="af-errors"/><br/>
    <form:select path="isVisible" disabled="${readOnly}">
      <form:option value="true"><spring:message code="af.yes"/></form:option>
      <form:option value="false"><spring:message code="af.no"/></form:option>
    </form:select>
  </div>
  <div>
    <spring:message code="af.model.examType.maxRetry"/>:
    <form:errors path="maxRetry" cssClass="af-errors"/><br/>
    <form:input path="maxRetry" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.examType.school"/>:
    <form:errors path="school" cssClass="af-errors"/><br/>
    <form:select path="school" disabled="${readOnly}">
      <form:options items="${schools}" itemLabel="name" itemValue="id"/>
    </form:select>
  </div>
  <div>
    <c:choose>
      <c:when test="${readOnly}">
        <a href="<c:url value="edit-${examType.id}"/>"><spring:message code="af.edit"/></a>
        &nbsp;&nbsp;
      </c:when>
      <c:otherwise>
        <input type="submit" value="<spring:message code="af.save"/>"/>
      </c:otherwise>
    </c:choose>
    <a href="<c:url value="../exam-type/"/>"><spring:message code="af.cancel"/></a>
  </div>
</form:form>
<%@include file="/WEB-INF/jsp/footer.jsp" %>
