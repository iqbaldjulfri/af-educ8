<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<table width="100%" cellpadding="3px" cellspacing="1px">
  <thead>
    <tr>
      <td><spring:message code="af.model.examType.code"/></td>
      <td><spring:message code="af.model.examType.name"/></td>
      <td><spring:message code="af.model.examType.description"/></td>
      <td><spring:message code="af.model.examType.isActive"/></td>
      <td><spring:message code="af.model.examType.isVisible"/></td>
      <td><spring:message code="af.model.examType.school"/></td>
    </tr>
  </thead>
  <tbody><% int c = 0; %>
    <c:forEach items="${examTypes}" var="item">
      <tr class="<%= ++c % 2 == 0 ? "af-table-row-alt" : "" %>">
        <td><a href="<c:url value="${item.id}"/>">${item.code}</a></td>
        <td>${item.name}</td>
        <td>${item.description}</td>
        <td>
          <c:if test="${item.isActive}"><spring:message code="af.yes"/></c:if>
          <c:if test="${!item.isActive}"><spring:message code="af.no"/></c:if>
        </td>
        <td>
          <c:if test="${item.isVisible}"><spring:message code="af.yes"/></c:if>
          <c:if test="${!item.isVisible}"><spring:message code="af.no"/></c:if>
        </td>
        <td>${item.school.name}</td>
      </tr>
    </c:forEach>
  </tbody>
</table>