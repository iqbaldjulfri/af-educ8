<%--
    Document   : form
    Created on : May 29, 2012, 8:46:54 PM
    Author     : Andika
--%>

<%@include file="/WEB-INF/jsp/includes.jsp" %>
<c:set var="title" value="Teacher Details" scope="request"/>
<c:set var="titleCode" value="af.title.teacher-details" scope="request"/>
<%@include file="/WEB-INF/jsp/header.jsp" %>
<script type="text/javascript" src="<c:url value="/static/js/secure/administration/teacher/form.js"/>"></script>
<form:form modelAttribute="teacher" method="post" action="submit">
  <div>
    <spring:message code="af.model.teacher.name"/>:
    <form:errors path="name" cssClass="af-errors"/><br/>
    <form:input path="name" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.teacher.birthPlace"/>:
    <form:errors path="birthPlace" cssClass="af-errors"/><br/>
    <form:input path="birthPlace" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.teacher.birthDate"/>:
    <form:errors path="birthDate" cssClass="af-errors"/><br/>
    <form:input path="birthDate" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.teacher.gender"/>:
    <form:errors path="gender" cssClass="af-errors"/><br/>
    <form:select path="gender" disabled="${readOnly}">
      <form:option value="1"><spring:message code="af.male"/></form:option>
      <form:option value="0"><spring:message code="af.female"/></form:option>
    </form:select>
  </div>
  <div>
    <spring:message code="af.model.teacher.religion"/>:
    <form:errors path="religion" cssClass="af-errors"/><br/>
    <form:input path="religion" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.teacher.nationality"/>:
    <form:errors path="nationality" cssClass="af-errors"/><br/>
    <form:input path="nationality" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.teacher.bloodType"/>:
    <form:errors path="bloodType" cssClass="af-errors"/><br/>
    <form:input path="bloodType" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.address.street"/>:
    <form:errors path="address.street" cssClass="af-errors"/><br/>
    <form:input path="address.street" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.address.city"/>:
    <form:errors path="address.city" cssClass="af-errors"/><br/>
    <form:input path="address.city" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.address.region"/>:
    <form:errors path="address.region" cssClass="af-errors"/><br/>
    <form:input path="address.region" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.address.country"/>:
    <form:errors path="address.country" cssClass="af-errors"/><br/>
    <form:input path="address.country" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.address.postalCode"/>:
    <form:errors path="address.postalCode" cssClass="af-errors"/><br/>
    <form:input path="address.postalCode" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.teacher.phone"/> 1:
    <form:errors path="phone" cssClass="af-errors"/><br/>
    <form:input path="phone" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.teacher.phone"/> 2:
    <form:errors path="phone2" cssClass="af-errors"/><br/>
    <form:input path="phone2" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.teacher.email"/>:
    <form:errors path="email" cssClass="af-errors"/><br/>
    <form:input path="email" disabled="${readOnly}"/>
  </div>
<%--
  <div>
    <spring:message code="af.model.teacher.school"/>:
    <form:errors path="school.id" cssClass="af-errors"/><br/>
    <form:hidden path="school.id" id="schoolId"/>
    <input type="text" id="schoolName" value="${teacher.school.name}"/>
  </div>
--%>
  <div>
    <spring:message code="af.model.teacher.school"/>:
    <form:errors path="schools" cssClass="errors"/><br/>
    <form:checkboxes path="schools" items="${schools}" itemLabel="name" itemValue="id" disabled="${readOnly}"/>
  </div>
  <div>
    <c:choose>
      <c:when test="${readOnly}">
        <a href="<c:url value="edit-${teacher.id}"/>"><spring:message code="af.edit"/></a>
        &nbsp;&nbsp;
      </c:when>
      <c:otherwise>
        <input type="submit" value="<spring:message code="af.save"/>"/>
      </c:otherwise>
    </c:choose>
    <a href="<c:url value="../teacher/"/>"><spring:message code="af.cancel"/></a>
  </div>
</form:form>
<%@include file="/WEB-INF/jsp/footer.jsp" %>
