<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<table width="100%" cellpadding="3px" cellspacing="1px">
  <thead>
    <tr>
      <td><spring:message code="af.model.teacher.name"/></td>
      <td><spring:message code="af.model.teacher.birthPlaceDate"/></td>
      <td><spring:message code="af.model.teacher.address"/></td>
      <td><spring:message code="af.model.teacher.phone"/></td>
      <td><spring:message code="af.model.teacher.phone"/> 2</td>
      <td><spring:message code="af.model.teacher.email"/></td>
      <td></td>
    </tr>
  </thead>
  <tbody><% int c = 0;%>
    <c:forEach items="${teachers}" var="t">
      <tr class="<%= ++c % 2 == 0 ? "af-table-row-alt" : "" %>">
        <td><a href="<c:url value="${t.id}"/>" title="${t.name}">${t.name}</a></td>
        <td>${t.birthPlace}, ${t.birthDate}</td>
        <td>${t.address.street}, ${t.address.city}, ${t.address.country}, ${t.address.postalCode}</td>
        <td>${t.phone}</td>
        <td>${t.phone2}</td>
        <td>${t.email}</td>
        <td><a href="#" onclick="deleteTeacher(${t.id});"><spring:message code="af.delete"/></a></td>
      </tr>
    </c:forEach>
  </tbody>
</table>