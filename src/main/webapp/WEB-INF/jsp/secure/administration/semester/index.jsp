<%--
    Document   : index
    Created on : June 11, 2012, 8:46:54 PM
    Author     : Iqbal
--%>

<%@include file="/WEB-INF/jsp/includes.jsp" %>
<c:set var="title" value="Semester Administration" scope="request"/>
<c:set var="titleCode" value="af.title.semester-administration" scope="request"/>
<%@include file="/WEB-INF/jsp/header.jsp" %>
<script type="text/javascript" src="<c:url value="/static/js/secure/administration/semester/search.js"/>"></script>
<label for="name"><spring:message code="af.model.semester.name"/>:</label><br/>
<input type="text" id="name"/><br/>
<br/>
<div id="action">
  <div class="af-left">
    <button id="search"><spring:message code="af.search"/></button>
    <a href="<c:url value="new"/>"><spring:message code="af.create-new"/></a>
  </div>
  <div class="af-right">
    <button id="prev"><spring:message code="af.prev"/></button>
    <input type="text" id="page" size="2"/>
    <button id="next"><spring:message code="af.next"/></button>
  </div>
  <div class="af-clear"></div>
</div>
<div id="list">
</div>
<%@include file="/WEB-INF/jsp/footer.jsp" %>