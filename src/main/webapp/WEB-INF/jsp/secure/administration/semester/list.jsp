<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<table width="100%" cellpadding="3px" cellspacing="1px">
  <thead>
    <tr>
      <td><spring:message code="af.model.semester.name"/></td>
      <td><spring:message code="af.model.semester.start"/></td>
      <td><spring:message code="af.model.semester.end"/></td>
      <td><spring:message code="af.model.semester.school"/></td>
    </tr>
  </thead>
  <tbody><% int c = 0;%>
    <c:forEach items="${semesters}" var="item">
      <tr class="<%= ++c % 2 == 0 ? "af-table-row-alt" : "" %>">
        <td><a href="<c:url value="${item.id}"/>" title="${item.name}">${item.name}</a></td>
        <td><fmt:formatDate value="${item.start}" pattern="dd MMM yyyy"/></td>
        <td><fmt:formatDate value="${item.end}" pattern="dd MMM yyyy"/></td>
        <td>${item.school.name}</td>
      </tr>
    </c:forEach>
  </tbody>
</table>