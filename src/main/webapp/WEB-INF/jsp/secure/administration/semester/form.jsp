<%--
    Document   : form
    Created on : June 11, 2012, 8:46:54 PM
    Author     : Iqbal
--%>

<%@include file="/WEB-INF/jsp/includes.jsp" %>
<c:set var="title" value="Semester Details" scope="request"/>
<c:set var="titleCode" value="af.title.semester-details" scope="request"/>
<%@include file="/WEB-INF/jsp/header.jsp" %>
<script type="text/javascript" src="<c:url value="/static/js/secure/administration/semester/form.js"/>"></script>
<form:form modelAttribute="semester" method="post" action="submit">
  <div>
    <spring:message code="af.model.semester.name"/>:
    <form:errors path="name" cssClass="af-errors"/><br/>
    <form:input path="name" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.semester.start"/> (dd-mm-yyyy):
    <form:errors path="start" cssClass="af-errors"/><br/>
    <form:input path="start" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.semester.end"/> (dd-mm-yyyy):
    <form:errors path="end" cssClass="af-errors"/><br/>
    <form:input path="end" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.semester.school" text="School"/>:
    <form:errors path="school" cssClass="af-errors"/><br/>
    <form:select path="school" disabled="${readOnly}">
      <form:options items="${schools}" itemLabel="name" itemValue="id"/>
    </form:select>
  </div><br/>
  <div>
    <c:choose>
      <c:when test="${readOnly}">
        <a href="<c:url value="edit-${semester.id}"/>"><spring:message code="af.edit"/></a>
        &nbsp;&nbsp;
      </c:when>
      <c:otherwise>
        <input type="submit" value="<spring:message code="af.save"/>"/>
      </c:otherwise>
    </c:choose>
    <a href="<c:url value="../semester/"/>"><spring:message code="af.cancel"/></a>
  </div>
</form:form>
<%@include file="/WEB-INF/jsp/footer.jsp" %>
