<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<table width="100%" cellpadding="3px" cellspacing="1px">
  <thead>
    <tr>
      <td><spring:message code="af.model.parent.name"/></td>
      <td><spring:message code="af.model.parent.birthPlaceDate"/></td>
      <td><spring:message code="af.model.parent.address"/></td>
      <td><spring:message code="af.model.parent.phone"/></td>
      <td><spring:message code="af.model.parent.phone"/> 2</td>
      <td><spring:message code="af.model.parent.email"/></td>
      <td></td>
    </tr>
  </thead>
  <tbody><% int c = 0;%>
    <c:forEach items="${parents}" var="p">
      <tr class="<%= ++c % 2 == 0 ? "af-table-row-alt" : "" %>">
        <td><a href="<c:url value="${p.id}"/>" title="${p.name}">${p.name}</a></td>
        <td>${p.birthPlace}, ${p.birthDate}</td>
        <td>${p.address.street}, ${p.address.city}, ${p.address.country}, ${p.address.postalCode}</td>
        <td>${p.phone}</td>
        <td>${p.phone2}</td>
        <td>${p.email}</td>
        <td><a href="#" onclick="deleteParent(${p.id});"><spring:message code="af.delete"/></a></td>
      </tr>
    </c:forEach>
  </tbody>
</table>