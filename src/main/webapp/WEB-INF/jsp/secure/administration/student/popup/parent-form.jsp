<%--
    Document   : form
    Created on : Jun 6, 2012, 1:33:02 AM
    Author     : Andika
--%>

<%@include file="/WEB-INF/jsp/includes.jsp" %>
<c:set var="title" value="Parent Details" scope="request"/>
<c:set var="titleCode" value="af.title.parent-details" scope="request"/>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><spring:message code="${titleCode}" text="${title}"/> | AirFrame</title>
    <script type="text/javascript" src="<c:url value="/static/js/jquery-1.7.2.min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/static/js/jquery-ui-1.8.20.custom.min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/static/js/json2.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/static/js/airframe-jquery-1.0.0.js"/>"></script>
    <link rel="stylesheet" href="<c:url value="/static/css/blu/jquery-ui-1.8.20.custom.css"/>"/>
    <link rel="stylesheet" href="<c:url value="/static/css/default.css"/>"/>
    <script type="text/javascript" src="<c:url value="/static/js/secure/administration/parent/form.js"/>"></script>
    <script type="text/javascript">
      $(function() {
        $('#save').click(function() {
          if (!opener) {
            alert('No opener window, closing');
            window.close();
          }

          var obj = $('form').serializeObject();
          obj.birthDate = parseDate(obj.birthDate);
          obj.deathDate = parseDate(obj.deathDate);

          $.ajax({
            type: 'POST',
            url: 'save',
            data: JSON.stringify(obj),
            success: function(parent) {
              var elmId = $('#elm').val();
              $('#' + elmId + '_id', opener.document).val(parent.id);
              $('#' + elmId + '_name', opener.document).val(parent.name);
              window.close();
            },
            dataType: 'json',
            contentType: 'application/json'
          });

          return false;
        });
      });

function parseDate(d) {
  if (!d) return '';

  try {
    var x = d.split('-');
    x = x.reverse();
    return Date.parse(x.join('/'));
  } catch (e) {
    return '';
  }
}
    </script>
    <style>
      body {overflow: visible;}
    </style>
  </head>
  <body>
    <input type="hidden" id="elm" value="${elmId}"/>
    <form:form modelAttribute="parent" method="post" action="submit">
      <form:hidden path="parentType"/>
      <div>
        <spring:message code="af.model.parent.name"/>:
        <form:errors path="name" cssClass="af-errors"/><br/>
        <form:input path="name" disabled="${readOnly}"/>
      </div>
      <div>
        <spring:message code="af.model.parent.birthPlace"/>:
        <form:errors path="birthPlace" cssClass="af-errors"/><br/>
        <form:input path="birthPlace" disabled="${readOnly}"/>
      </div>
      <div>
        <spring:message code="af.model.parent.birthDate"/>:
        <form:errors path="birthDate" cssClass="af-errors"/><br/>
        <form:input path="birthDate" disabled="${readOnly}"/>
      </div>
      <div>
        <spring:message code="af.model.parent.gender"/>:
        <form:errors path="gender" cssClass="af-errors"/><br/>
        <form:select path="gender" disabled="${readOnly}">
          <form:option value="1"><spring:message code="af.male"/></form:option>
          <form:option value="0"><spring:message code="af.female"/></form:option>
        </form:select>
      </div>
      <div>
        <spring:message code="af.model.parent.religion"/>:
        <form:errors path="religion" cssClass="af-errors"/><br/>
        <form:input path="religion" disabled="${readOnly}"/>
      </div>
      <div>
        <spring:message code="af.model.parent.nationality"/>:
        <form:errors path="nationality" cssClass="af-errors"/><br/>
        <form:input path="nationality" disabled="${readOnly}"/>
      </div>
      <div>
        <spring:message code="af.model.parent.bloodType"/>:
        <form:errors path="bloodType" cssClass="af-errors"/><br/>
        <form:input path="bloodType" disabled="${readOnly}"/>
      </div>
      <div>
        <spring:message code="af.model.address.street"/>:
        <form:errors path="address.street" cssClass="af-errors"/><br/>
        <form:input path="address.street" disabled="${readOnly}"/>
      </div>
      <div>
        <spring:message code="af.model.address.city"/>:
        <form:errors path="address.city" cssClass="af-errors"/><br/>
        <form:input path="address.city" disabled="${readOnly}"/>
      </div>
      <div>
        <spring:message code="af.model.address.region"/>:
        <form:errors path="address.region" cssClass="af-errors"/><br/>
        <form:input path="address.region" disabled="${readOnly}"/>
      </div>
      <div>
        <spring:message code="af.model.address.country"/>:
        <form:errors path="address.country" cssClass="af-errors"/><br/>
        <form:input path="address.country" disabled="${readOnly}"/>
      </div>
      <div>
        <spring:message code="af.model.address.postalCode"/>:
        <form:errors path="address.postalCode" cssClass="af-errors"/><br/>
        <form:input path="address.postalCode" disabled="${readOnly}"/>
      </div>
      <div>
        <spring:message code="af.model.parent.phone"/> 1:
        <form:errors path="phone" cssClass="af-errors"/><br/>
        <form:input path="phone" disabled="${readOnly}"/>
      </div>
      <div>
        <spring:message code="af.model.parent.phone"/> 2:
        <form:errors path="phone2" cssClass="af-errors"/><br/>
        <form:input path="phone2" disabled="${readOnly}"/>
      </div>
      <div>
        <spring:message code="af.model.parent.email"/>:
        <form:errors path="email" cssClass="af-errors"/><br/>
        <form:input path="email" disabled="${readOnly}"/>
      </div>
      <div>
        <spring:message code="af.model.parent.education"/>:
        <form:errors path="education" cssClass="af-errors"/><br/>
        <form:input path="education" disabled="${readOnly}"/>
      </div>
      <div>
        <spring:message code="af.model.parent.job"/>:
        <form:errors path="job" cssClass="af-errors"/><br/>
        <form:input path="job" disabled="${readOnly}"/>
      </div>
      <div>
        <spring:message code="af.model.parent.monthlyIncome"/>:
        <form:errors path="monthlyIncome" cssClass="af-errors"/><br/>
        <form:input path="monthlyIncome" disabled="${readOnly}"/>
      </div>
      <div>
        <spring:message code="af.model.parent.isDeceased"/>:
        <form:errors path="isDeceased" cssClass="af-errors"/><br/>
        <form:select path="isDeceased" disabled="${readOnly}">
          <form:option value="true"><spring:message code="af.yes"/></form:option>
          <form:option value="false"><spring:message code="af.no"/></form:option>
        </form:select><br/>
        <spring:message code="af.model.parent.deathDate"/>:
        <form:errors path="deathDate" cssClass="af-errors"/><br/>
        <form:input path="deathDate" disabled="${readOnly}"/>
      </div>
      <div>
        <button id="save"><spring:message code="af.save"/></button>
        <a href="#" onclick="window.close();"><spring:message code="af.cancel"/></a>
      </div>
    </form:form>
  </body>
</html>
