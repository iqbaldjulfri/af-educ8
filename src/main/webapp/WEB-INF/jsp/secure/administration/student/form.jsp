<%--
    Document   : form
    Created on : Jun 12, 2012, 12:13:16 PM
    Author     : Andika
--%>

<%@include file="/WEB-INF/jsp/includes.jsp" %>
<c:set var="title" value="Student Details" scope="request"/>
<c:set var="titleCode" value="af.title.student-details" scope="request"/>
<%@include file="/WEB-INF/jsp/header.jsp" %>
<script type="text/javascript" src="<c:url value="/static/js/secure/administration/student/form.js"/>"></script>
<form:form modelAttribute="student" method="post" action="submit">
  <div>
    <spring:message code="af.model.student.idNumber"/>:
    <form:errors path="idNumber" cssClass="af-errors"/><br/>
    <form:input path="idNumber" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.student.testNumber"/>:
    <form:errors path="testNumber" cssClass="af-errors"/><br/>
    <form:input path="testNumber" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.student.name"/>:
    <form:errors path="name" cssClass="af-errors"/><br/>
    <form:input path="name" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.student.nickname"/>:
    <form:errors path="nickname" cssClass="af-errors"/><br/>
    <form:input path="nickname" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.student.birthPlace"/>:
    <form:errors path="birthPlace" cssClass="af-errors"/><br/>
    <form:input path="birthPlace" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.student.birthDate"/>:
    <form:errors path="birthDate" cssClass="af-errors"/><br/>
    <form:input path="birthDate" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.student.gender"/>:
    <form:errors path="gender" cssClass="af-errors"/><br/>
    <form:select path="gender" disabled="${readOnly}">
      <form:option value="1"><spring:message code="af.male"/></form:option>
      <form:option value="0"><spring:message code="af.female"/></form:option>
    </form:select>
  </div>
  <div>
    <spring:message code="af.model.student.religion"/>:
    <form:errors path="religion" cssClass="af-errors"/><br/>
    <form:input path="religion" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.student.nationality"/>:
    <form:errors path="nationality" cssClass="af-errors"/><br/>
    <form:input path="nationality" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.student.bloodType"/>:
    <form:errors path="bloodType" cssClass="af-errors"/><br/>
    <form:input path="bloodType" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.student.kidsOrder"/>:
    <form:errors path="kidsOrder" cssClass="af-errors"/><br/>
    <form:input path="kidsOrder" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.student.bloodBrother"/>:
    <form:errors path="bloodBrother" cssClass="af-errors"/><br/>
    <form:input path="bloodBrother" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.student.stepBrother"/>:
    <form:errors path="stepBrother" cssClass="af-errors"/><br/>
    <form:input path="stepBrother" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.student.fosterBrother"/>:
    <form:errors path="fosterBrother" cssClass="af-errors"/><br/>
    <form:input path="fosterBrother" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.student.fatherDeceased"/>:
    <form:errors path="fatherDeceased" cssClass="af-errors"/><br/>
    <form:select path="fatherDeceased" disabled="${readOnly}">
      <form:option value="true"><spring:message code="af.yes"/></form:option>
      <form:option value="false"><spring:message code="af.no"/></form:option>
    </form:select>
  </div>
  <div>
    <spring:message code="af.model.student.motherDeceased"/>:
    <form:errors path="motherDeceased" cssClass="af-errors"/><br/>
    <form:select path="motherDeceased" disabled="${readOnly}">
      <form:option value="true"><spring:message code="af.yes"/></form:option>
      <form:option value="false"><spring:message code="af.no"/></form:option>
    </form:select>
  </div>
  <div>
    <spring:message code="af.model.student.liveWith"/>:
    <form:errors path="liveWith" cssClass="af-errors"/><br/>
    <form:select path="liveWith" disabled="${readOnly}">
      <form:option value="0"><spring:message code="af.withParent"/></form:option>
      <form:option value="1"><spring:message code="af.withFamily"/></form:option>
      <form:option value="2"><spring:message code="af.inDorm"/></form:option>
      <form:option value="3"><spring:message code="af.inKost"/></form:option>
    </form:select>
  </div>
  <div>
    <spring:message code="af.model.student.dailyLanguage"/>:
    <form:errors path="dailyLanguage" cssClass="af-errors"/><br/>
    <form:input path="dailyLanguage" disabled="${readOnly}"/>
  </div>
  
  <%--EDUCATION DETAIL--%>
  <div>
    <spring:message code="af.model.student.graduateFrom"/>:
    <form:errors path="graduateFrom" cssClass="af-errors"/><br/>
    <form:input path="graduateFrom" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.student.sttbDateNumberFrom"/>:
    <form:errors path="sttbDateNumberFrom" cssClass="af-errors"/><br/>
    <form:input path="sttbDateNumberFrom" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.student.lengthOfStudy"/>:
    <form:errors path="lengthOfStudy" cssClass="af-errors"/><br/>
    <form:input path="lengthOfStudy" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.student.transferFrom"/>:
    <form:errors path="transferFrom" cssClass="af-errors"/><br/>
    <form:input path="transferFrom" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.student.transferReason"/>:
    <form:errors path="transferReason" cssClass="af-errors"/><br/>
    <form:input path="transferReason" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.student.acceptanceLevel"/>:
    <form:errors path="acceptanceLevel" cssClass="af-errors"/><br/>
    <form:input path="acceptanceLevel" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.student.acceptanceGroup"/>:
    <form:errors path="acceptanceGroup" cssClass="af-errors"/><br/>
    <form:input path="acceptanceGroup" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.student.specialization"/>:
    <form:errors path="specialization" cssClass="af-errors"/><br/>
    <form:select path="specialization" disabled="${readOnly}">
      <form:options items="${specializations}" itemLabel="name" itemValue="id"/>
    </form:select>
  </div>
  <div>
    <spring:message code="af.model.student.father"/>:
    <form:errors path="father" cssClass="af-errors"/><br/>
    <form:hidden path="father" id="father_id"/>
    <input type="text" id="father_name" disabled="true"/>
    <button id="newFather">XXXX NEW XXXX</button>
  </div>
  <div>
    <spring:message code="af.model.address.street"/>:
    <form:errors path="address.street" cssClass="af-errors"/><br/>
    <form:input path="address.street" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.address.city"/>:
    <form:errors path="address.city" cssClass="af-errors"/><br/>
    <form:input path="address.city" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.address.region"/>:
    <form:errors path="address.region" cssClass="af-errors"/><br/>
    <form:input path="address.region" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.address.country"/>:
    <form:errors path="address.country" cssClass="af-errors"/><br/>
    <form:input path="address.country" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.address.postalCode"/>:
    <form:errors path="address.postalCode" cssClass="af-errors"/><br/>
    <form:input path="address.postalCode" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.student.homeDistance"/>:
    <form:errors path="homeDistance" cssClass="af-errors"/><br/>
    <form:input path="homeDistance" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.student.phone"/> 1:
    <form:errors path="phone" cssClass="af-errors"/><br/>
    <form:input path="phone" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.student.phone"/> 2:
    <form:errors path="phone2" cssClass="af-errors"/><br/>
    <form:input path="phone2" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.student.email"/>:
    <form:errors path="email" cssClass="af-errors"/><br/>
    <form:input path="email" disabled="${readOnly}"/>
  </div>
  
  <%--HEALTH--%>
  <div>
    <spring:message code="af.model.student.diseaseSuffered"/>:
    <form:errors path="diseaseSuffered" cssClass="af-errors"/><br/>
    <form:input path="diseaseSuffered" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.student.physicalAbnormalities"/>:
    <form:errors path="physicalAbnormalities" cssClass="af-errors"/><br/>
    <form:input path="physicalAbnormalities" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.student.heightWeight"/>:
    <form:errors path="heightWeight" cssClass="af-errors"/><br/>
    <form:input path="heightWeight" disabled="${readOnly}"/>
  </div>  
  <%--HEALTH END--%>
  
  <%--HOBBIES--%>
  <div>
    <spring:message code="af.model.student.art"/>:
    <form:errors path="art" cssClass="af-errors"/><br/>
    <form:input path="art" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.student.sport"/>:
    <form:errors path="sport" cssClass="af-errors"/><br/>
    <form:input path="sport" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.student.social"/>:
    <form:errors path="social" cssClass="af-errors"/><br/>
    <form:input path="social" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.student.otherHobby"/>:
    <form:errors path="otherHobby" cssClass="af-errors"/><br/>
    <form:textarea path="otherHobby" disabled="${readOnly}"/>
  </div>
  <%--HOBBIES END--%>
  
  <%--STUDENT DEVELOPMENT--%>
  <div>
    <spring:message code="af.model.student.scholarship"/>:
    <form:errors path="scholarship" cssClass="af-errors"/><br/>
    <form:textarea path="scholarship" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.student.yearLeaveSchool"/>:
    <form:errors path="yearLeaveSchool" cssClass="af-errors"/><br/>
    <form:textarea path="yearLeaveSchool" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.student.reasonLeaveSchool"/>:
    <form:errors path="reasonLeaveSchool" cssClass="af-errors"/><br/>
    <form:textarea path="reasonLeaveSchool" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.student.yearGraduate"/>:
    <form:errors path="yearGraduate" cssClass="af-errors"/><br/>
    <form:textarea path="yearGraduate" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.student.sttbDateNumberGraduate"/>:
    <form:errors path="sttbDateNumberGraduate" cssClass="af-errors"/><br/>
    <form:textarea path="sttbDateNumberGraduate" disabled="${readOnly}"/>
  </div>
  <%--STUDENT DEVELOPMENT END--%>
  
  <%--AFTER GRADUATE--%>
  <div>
    <spring:message code="af.model.student.continueAt"/>:
    <form:errors path="continueAt" cssClass="af-errors"/><br/>
    <form:textarea path="continueAt" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.student.workAt"/>:
    <form:errors path="workAt" cssClass="af-errors"/><br/>
    <form:textarea path="workAt" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.student.dateStartWork"/>:
    <form:errors path="dateStartWork" cssClass="af-errors"/><br/>
    <form:input path="dateStartWork" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.student.salary"/>:
    <form:errors path="salary" cssClass="af-errors"/><br/>
    <form:textarea path="salary" disabled="${readOnly}"/>
  </div>
  <%--AFTER GRADUATE END--%>
  
  <div>
    <spring:message code="af.model.student.school"/>:
    <form:errors path="school" cssClass="af-errors"/><br/>
    <form:select path="school" disabled="${readOnly}">
      <form:options items="${schools}" itemLabel="name" itemValue="id"/>
    </form:select>
  </div>
<%--
  <div>
    <spring:message code="af.model.clazz.father"/>:
    <form:errors path="father" cssClass="af-errors"/><br/>
    <form:hidden path="father"/>
    <input type="text" id="fatherName" ${readOnly ? "disabled=\"true\"" : ""} value="${student.father.name}"/>
  </div>
--%>
  <div>
    <spring:message code="af.model.student.isActive"/>:
    <form:errors path="isActive" cssClass="af-errors"/><br/>
    <form:select path="isActive" disabled="${readOnly}">
      <form:option value="true"><spring:message code="af.yes"/></form:option>
      <form:option value="false"><spring:message code="af.no"/></form:option>
    </form:select>
  </div>
  <div>
    <c:choose>
      <c:when test="${readOnly}">
        <a href="<c:url value="edit-${student.id}"/>"><spring:message code="af.edit"/></a>
        &nbsp;&nbsp;
      </c:when>
      <c:otherwise>
        <input type="submit" value="<spring:message code="af.save"/>"/>
      </c:otherwise>
    </c:choose>
    <a href="<c:url value="../student/"/>"><spring:message code="af.cancel"/></a>
  </div>
</form:form>
<%@include file="/WEB-INF/jsp/footer.jsp" %>
