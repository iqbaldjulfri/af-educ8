<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<table width="100%" cellpadding="3px" cellspacing="1px">
  <thead>
    <tr>
      <td><spring:message code="af.model.student.name"/></td>
      <td><spring:message code="af.model.student.idNumber"/></td>
      <td><spring:message code="af.model.student.birthPlaceDate"/></td>
      <td><spring:message code="af.model.student.school"/></td>
      <td><spring:message code="af.model.student.phone"/></td>
      <td><spring:message code="af.model.student.phone"/> 2</td>
      <td><spring:message code="af.model.student.email"/></td>
      <td></td>
    </tr>
  </thead>
  <tbody><% int c = 0; %>
    <c:forEach items="${students}" var="item">
      <tr class="<%= ++c % 2 == 0 ? "af-table-row-alt" : "" %>">
        <td><a href="<c:url value="${item.id}"/>">${item.name}</a></td>
        <td>${item.idNumber}</td>
        <td>${item.birthPlace}, ${item.birthDate}</td>
        <td>${item.school.name}</td>
        <td>${item.phone}</td>
        <td>${item.phone2}</td>
        <td>${item.email}</td>
        <td><a href="#" onclick="del(${item.id});"><spring:message code="af.delete"/></a></td>
      </tr>
    </c:forEach>
  </tbody>
</table>