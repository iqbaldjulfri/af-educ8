<%--
    Document   : form
    Created on : May 29, 2012, 8:46:54 PM
    Author     : Andika
--%>

<%@include file="/WEB-INF/jsp/includes.jsp" %>
<c:set var="title" value="Schedule Slot Details" scope="request"/>
<c:set var="titleCode" value="af.title.scheduleSlot-details" scope="request"/>
<%@include file="/WEB-INF/jsp/header.jsp" %>
<form:form modelAttribute="scheduleSlot" method="post" action="submit">
  <div>
    <spring:message code="af.model.scheduleSlot.description"/>:
    <form:errors path="description" cssClass="af-errors"/><br/>
    <form:input path="description" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.scheduleSlot.day"/>:
    <form:errors path="day" cssClass="af-errors"/><br/>
    <form:select path="day" disabled="${readOnly}">
      <form:option value="0"><spring:message code="af.days.mon"/></form:option>
      <form:option value="1"><spring:message code="af.days.tue"/></form:option>
      <form:option value="2"><spring:message code="af.days.wed"/></form:option>
      <form:option value="3"><spring:message code="af.days.thu"/></form:option>
      <form:option value="4"><spring:message code="af.days.fri"/></form:option>
      <form:option value="5"><spring:message code="af.days.sat"/></form:option>
      <form:option value="6"><spring:message code="af.days.sun"/></form:option>
    </form:select>
  </div>
  <div>
    <spring:message code="af.model.scheduleSlot.start"/> (hh:mm):
    <form:errors path="start" cssClass="af-errors"/><br/>
    <form:input path="start" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.scheduleSlot.end"/> (hh:mm):
    <form:errors path="end" cssClass="af-errors"/><br/>
    <form:input path="end" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.scheduleSlot.school" text="School"/>:
    <form:errors path="school" cssClass="af-errors"/><br/>
    <form:select path="school" disabled="${readOnly}">
      <form:options items="${schools}" itemLabel="name" itemValue="id"/>
    </form:select>
  </div><br/>
  <div>
    <c:choose>
      <c:when test="${readOnly}">
        <a href="<c:url value="edit-${scheduleSlot.id}"/>"><spring:message code="af.edit"/></a>
        &nbsp;&nbsp;
      </c:when>
      <c:otherwise>
        <input type="submit" value="<spring:message code="af.save"/>"/>
      </c:otherwise>
    </c:choose>
    <a href="<c:url value="../schedule-slot/"/>"><spring:message code="af.cancel"/></a>
  </div>
</form:form>
<%@include file="/WEB-INF/jsp/footer.jsp" %>
