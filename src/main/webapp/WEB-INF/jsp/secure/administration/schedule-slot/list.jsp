<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<table width="100%" cellpadding="3px" cellspacing="1px">
  <thead>
    <tr>
      <td><spring:message code="af.model.scheduleSlot.description"/></td>
      <td><spring:message code="af.model.scheduleSlot.day"/></td>
      <td><spring:message code="af.model.scheduleSlot.start"/></td>
      <td><spring:message code="af.model.scheduleSlot.end"/></td>
      <td></td>
    </tr>
  </thead>
  <tbody><% int c = 0;%>
    <c:forEach items="${scheduleSlots}" var="item">
      <tr class="<%= ++c % 2 == 0 ? "af-table-row-alt" : "" %>">
        <td><a href="<c:url value="${item.id}"/>" title="${item.description}">${item.description}</a></td>
        <td><spring:message code="af.days.${item.day}"/></td>
        <td><fmt:formatDate value="${item.start}" pattern="HH:mm"/></td>
        <td><fmt:formatDate value="${item.end}" pattern="HH:mm"/></td>
        <td><a href="#" onclick="del(${item.id});"><spring:message code="af.delete"/></a></td>
      </tr>
    </c:forEach>
  </tbody>
</table>