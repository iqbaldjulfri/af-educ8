<%-- 
    Document   : index
    Created on : Sep 25, 2012, 7:25:22 PM
    Author     : Andika
--%>

<%@include file="/WEB-INF/jsp/includes.jsp" %>
<c:set var="title" value="Tuition Payment Administration" scope="request"/>
<c:set var="titleCode" value="af.title.tuitionPayment-administration" scope="request"/>
<%@include file="/WEB-INF/jsp/header.jsp" %>
<script type="text/javascript" src="<c:url value="/static/js/secure/administration/tuition-payment/search.js"/>"></script>

<label for="studentId"><spring:message code="af.model.tuitionPayment.studentId"/></label><br/>
<input type="text" id="studentId"/><br/>

<label for="school"><spring:message code="af.model.tuitionPayment.school"/></label><br/>
<select id="school">
  <option value="0">all</option>
  <c:forEach items="${schools}" var="item">
    <option value="${item.id}">${item.name}</option>
  </c:forEach>
</select><br/>

<label for="month"><spring:message code="af.model.tuitionPayment.month"/></label><br/>
<select id="month">
  <option value="1">1</option>
  <option value="2">2</option>
  <option value="3">3</option>
  <option value="4">4</option>
  <option value="5">5</option>
  <option value="6">6</option>
  <option value="7">7</option>
  <option value="8">8</option>
  <option value="9">9</option>
  <option value="10">10</option>
  <option value="11">11</option>
  <option value="12">12</option>
</select><br/>

<label for="year"><spring:message code="af.model.tuitionPayment.year"/></label><br/>
<select id="year">
  <c:forEach items="${years}" var="item">
    <option value="${item}">${item}</option>
  </c:forEach>
</select><br/>

<br/>
</form>


<br/>
<div id="action">
  <div class="af-left">
    <button id="search"><spring:message code="af.search"/></button>
  </div>
  <div class="af-right">
    <button id="prev"><spring:message code="af.prev"/></button>
    <input type="text" id="page" size="2"/>
    <button id="next"><spring:message code="af.next"/></button>
  </div>
  <div class="af-clear"></div>
</div>
<div id="list">
</div>
<%@include file="/WEB-INF/jsp/footer.jsp" %>