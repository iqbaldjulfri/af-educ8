<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<table width="100%" cellpadding="3px" cellspacing="1px">
  <thead>
    <tr>
      <td><spring:message code="af.model.student.name"/></td>
      <td><spring:message code="af.model.tuitionPayment.month"/></td>
      <td><spring:message code="af.model.tuitionPayment.year"/></td>
      <td><spring:message code="af.model.tuitionPayment.paymentStatus"/></td>
    </tr>
  </thead>
  <tbody><% int c = 0; %>
    <c:forEach items="${students}" var="item">
      <tr class="<%= ++c % 2 == 0 ? "af-table-row-alt" : "" %>">
        <td>${item.name}</td>
        <td>${month}</td>
        <td>${year}</td>
        <td>
          <c:if test="${studentPayment[item] == null}"><button id="pay_${item.id}" onclick="pay(${item.id}, ${month}, ${year});">Pay</button></c:if>
          <c:if test="${studentPayment[item] != null}">Paid</c:if>
        </td>
      </tr>
    </c:forEach>
  </tbody>
</table>