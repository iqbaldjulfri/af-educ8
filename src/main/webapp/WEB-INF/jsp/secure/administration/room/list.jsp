<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<table width="100%" cellpadding="3px" cellspacing="1px">
  <thead>
    <tr>
      <td><spring:message code="af.model.room.name"/></td>
      <td><spring:message code="af.model.room.location"/></td>
      <td><spring:message code="af.model.room.schools"/></td>
      <td></td>
    </tr>
  </thead>
  <tbody><% int c = 0; %>
    <c:forEach items="${rooms}" var="item">
      <tr class="<%= ++c % 2 == 0 ? "af-table-row-alt" : "" %>">
        <td><a href="<c:url value="${item.id}"/>">${item.name}</a></td>
        <td>${item.location}</td>
        <td><% request.setAttribute("count", 0); %>
          <c:forEach items="${item.schools}" var="school">
            <% Integer i = (Integer) request.getAttribute("count"); request.setAttribute("count", ++i); %>
            ${school.name}<c:if test="${count < item.schools.size()}">,</c:if>
          </c:forEach>
        </td>
        </td>
        <td><a href="#" onclick="del(${item.id});"><spring:message code="af.delete"/></a></td>
      </tr>
    </c:forEach>
  </tbody>
</table>