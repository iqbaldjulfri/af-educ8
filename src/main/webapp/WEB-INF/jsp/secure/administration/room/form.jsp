<%--
    Document   : form
    Created on : Apr 23, 2012, 11:14:24 AM
    Author     : Iqbal Djulfri
--%>

<%@include file="/WEB-INF/jsp/includes.jsp" %>
<c:set var="title" value="Room Details" scope="request"/>
<c:set var="titleCode" value="af.title.room-details" scope="request"/>
<%@include file="/WEB-INF/jsp/header.jsp" %>
<form:form modelAttribute="room" method="post" action="submit">
  <div>
    <spring:message code="af.model.room.name"/>:
    <form:errors path="name" cssClass="af-errors"/><br/>
    <form:input path="name" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.room.location"/>:
    <form:errors path="location" cssClass="af-errors"/><br/>
    <form:input path="location" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.room.schools"/>:
    <form:errors path="schools" cssClass="af-errors"/><br/>
    <form:checkboxes path="schools" disabled="${readOnly}" items="${schools}" itemValue="id" itemLabel="name"/>
  </div>
  <div>
    <c:choose>
      <c:when test="${readOnly}">
        <a href="<c:url value="edit-${room.id}"/>"><spring:message code="af.edit"/></a>
        &nbsp;&nbsp;
      </c:when>
      <c:otherwise>
        <input type="submit" value="<spring:message code="af.save"/>"/>
      </c:otherwise>
    </c:choose>
    <a href="<c:url value="../room/"/>"><spring:message code="af.cancel"/></a>
  </div>
</form:form>
<%@include file="/WEB-INF/jsp/footer.jsp" %>
