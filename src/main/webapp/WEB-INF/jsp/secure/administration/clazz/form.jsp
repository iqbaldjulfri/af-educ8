<%--
    Document   : form
    Created on : Apr 23, 2012, 11:14:24 AM
    Author     : Iqbal Djulfri
--%>

<%@include file="/WEB-INF/jsp/includes.jsp" %>
<c:set var="title" value="Clazz Details" scope="request"/>
<c:set var="titleCode" value="af.title.clazz-details" scope="request"/>
<%@include file="/WEB-INF/jsp/header.jsp" %>
<script type="text/javascript" src="<c:url value="/static/js/secure/administration/clazz/form.js"/>"></script>
<form:form modelAttribute="clazz" method="post" action="submit">
  <div>
    <spring:message code="af.model.clazz.name"/>:
    <form:errors path="name" cssClass="af-errors"/><br/>
    <form:input path="name" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.clazz.grade"/>:
    <form:errors path="grade" cssClass="af-errors"/><br/>
    <form:input path="grade" disabled="${readOnly}" id="grade"/>
  </div>
  <div>
    <spring:message code="af.model.clazz.school"/>:
    <form:errors path="school" cssClass="af-errors"/><br/>
    <form:select path="school" disabled="${readOnly}">
      <form:options items="${schools}" itemLabel="name" itemValue="id"/>
    </form:select>
  </div>
  <div>
    <spring:message code="af.model.clazz.homeroomTeacher"/>:
    <form:errors path="homeroomTeacher" cssClass="af-errors"/><br/>
    <form:hidden path="homeroomTeacher"/>
    <input type="text" id="teacherName" ${readOnly ? "disabled=\"true\"" : ""} value="${clazz.homeroomTeacher.name}"/>
  </div>
  <div>
    <spring:message code="af.model.clazz.isActive"/>:
    <form:errors path="isActive" cssClass="af-errors"/><br/>
    <form:select path="isActive" disabled="${readOnly}">
      <form:option value="true"><spring:message code="af.yes"/></form:option>
      <form:option value="false"><spring:message code="af.no"/></form:option>
    </form:select>
  </div>
  <div>
    <c:choose>
      <c:when test="${readOnly}">
        <a href="<c:url value="edit-${clazz.id}"/>"><spring:message code="af.edit"/></a>
        &nbsp;&nbsp;
      </c:when>
      <c:otherwise>
        <input type="submit" value="<spring:message code="af.save"/>"/>
      </c:otherwise>
    </c:choose>
    <a href="<c:url value="../clazz/"/>"><spring:message code="af.cancel"/></a>
  </div>
</form:form>
<%@include file="/WEB-INF/jsp/footer.jsp" %>
