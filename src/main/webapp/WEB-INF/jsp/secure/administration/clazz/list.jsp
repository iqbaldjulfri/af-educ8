<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<table width="100%" cellpadding="3px" cellspacing="1px">
  <thead>
    <tr>
      <td><spring:message code="af.model.clazz.name"/></td>
      <td><spring:message code="af.model.clazz.homeroomTeacher"/></td>
      <td><spring:message code="af.model.clazz.school"/></td>
      <td></td>
    </tr>
  </thead>
  <tbody><% int c = 0; %>
    <c:forEach items="${clazzes}" var="item">
      <tr class="<%= ++c % 2 == 0 ? "af-table-row-alt" : "" %>">
        <td><a href="<c:url value="${item.id}"/>">${item.name}</a></td>
        <td>${item.homeroomTeacher.name}</td>
        <td>${item.school.name}</td>
        <td><a href="#" onclick="del(${item.id});"><spring:message code="af.delete"/></a></td>
      </tr>
    </c:forEach>
  </tbody>
</table>