<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<table width="100%" cellpadding="3px" cellspacing="1px">
  <thead>
    <tr>
      <td><spring:message code="af.model.specialization.name"/></td>
      <td><spring:message code="af.model.specialization.description"/></td>
      <td><spring:message code="af.model.specialization.school"/></td>
    </tr>
  </thead>
  <tbody><% int c = 0;%>
    <c:forEach items="${specializations}" var="item">
      <tr class="<%= ++c % 2 == 0 ? "af-table-row-alt" : "" %>">
        <td><a href="<c:url value="${item.id}"/>" title="${item.name}">${item.name}</a></td>
        <td>${item.description}</td>
        <td>${item.school.name}</td>
      </tr>
    </c:forEach>
  </tbody>
</table>