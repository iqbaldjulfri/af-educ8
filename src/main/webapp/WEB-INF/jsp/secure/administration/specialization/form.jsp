<%--
    Document   : form
    Created on : June 11, 2012, 8:46:54 PM
    Author     : Iqbal
--%>

<%@include file="/WEB-INF/jsp/includes.jsp" %>
<c:set var="title" value="Specialization Details" scope="request"/>
<c:set var="titleCode" value="af.title.specialization-details" scope="request"/>
<%@include file="/WEB-INF/jsp/header.jsp" %>
<script>
$(function() {
  $('#test').click(function() {

//          $.postJSON('save', $('form').serialize(), function(parent) {
//            var elmId = $('#elm').val();
//            $('#' + elmId + '_id', opener.document).val(parent.id);
//            $('#' + elmId + '_name', opener.document).val(parent.name);
//            window.close();
//          });

    $.ajax({
      type: 'POST',
      url: 'test',
      data: '{"name": "IPA", "description": "TEST", "isActive": "true", "school": { "id": "2" }}',
      success: function(parent) {
        console.log(parent)
      },
      dataType: 'json',
      contentType: 'application/json'
    });

    return false;
  });
})
</script>
<form:form modelAttribute="specialization" method="post" action="submit">
  <div>
    <spring:message code="af.model.specialization.name"/>:
    <form:errors path="name" cssClass="af-errors"/><br/>
    <form:input path="name" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.specialization.description"/>:
    <form:errors path="description" cssClass="af-errors"/><br/>
    <form:input path="description" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.specialization.isActive"/>:
    <form:errors path="isActive" cssClass="af-errors"/><br/>
    <form:select path="isActive" disabled="${readOnly}">
      <form:option value="true"><spring:message code="af.yes"/></form:option>
      <form:option value="false"><spring:message code="af.no"/></form:option>
    </form:select>
  </div>
  <div>
    <spring:message code="af.model.specialization.school" text="School"/>:
    <form:errors path="school" cssClass="af-errors"/><br/>
    <form:select path="school" disabled="${readOnly}">
      <form:options items="${schools}" itemLabel="name" itemValue="id"/>
    </form:select>
  </div><br/>
  <div>
    <button id="test">TEST</button>
    <c:choose>
      <c:when test="${readOnly}">
        <a href="<c:url value="edit-${specialization.id}"/>"><spring:message code="af.edit"/></a>
        &nbsp;&nbsp;
      </c:when>
      <c:otherwise>
        <input type="submit" value="<spring:message code="af.save"/>"/>
      </c:otherwise>
    </c:choose>
    <a href="<c:url value="../specialization/"/>"><spring:message code="af.cancel"/></a>
  </div>
</form:form>
<%@include file="/WEB-INF/jsp/footer.jsp" %>
