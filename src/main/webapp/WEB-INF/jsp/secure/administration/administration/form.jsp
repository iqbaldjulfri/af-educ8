<%-- 
    Document   : form
    Created on : Jun 5, 2012, 10:14:55 AM
    Author     : Andika
--%>

<%@include file="/WEB-INF/jsp/includes.jsp" %>
<c:set var="title" value="Administration Details" scope="request"/>
<c:set var="titleCode" value="af.title.administration-details" scope="request"/>
<%@include file="/WEB-INF/jsp/header.jsp" %>
<script type="text/javascript" src="<c:url value="/static/js/secure/administration/administration/form.js"/>"></script>
<form:form modelAttribute="administration" method="post" action="submit">
  <div>
    <spring:message code="af.model.administration.name"/>:
    <form:errors path="name" cssClass="af-errors"/><br/>
    <form:input path="name" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.administration.birthPlace"/>:
    <form:errors path="birthPlace" cssClass="af-errors"/><br/>
    <form:input path="birthPlace" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.administration.birthDate"/>:
    <form:errors path="birthDate" cssClass="af-errors"/><br/>
    <form:input path="birthDate" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.administration.gender"/>:
    <form:errors path="gender" cssClass="af-errors"/><br/>
    <form:select path="gender" disabled="${readOnly}">
      <form:option value="1"><spring:message code="af.male"/></form:option>
      <form:option value="0"><spring:message code="af.female"/></form:option>
    </form:select>
  </div>
  <div>
    <spring:message code="af.model.administration.religion"/>:
    <form:errors path="religion" cssClass="af-errors"/><br/>
    <form:input path="religion" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.administration.nationality"/>:
    <form:errors path="nationality" cssClass="af-errors"/><br/>
    <form:input path="nationality" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.administration.bloodType"/>:
    <form:errors path="bloodType" cssClass="af-errors"/><br/>
    <form:input path="bloodType" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.address.street"/>:
    <form:errors path="address.street" cssClass="af-errors"/><br/>
    <form:input path="address.street" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.address.city"/>:
    <form:errors path="address.city" cssClass="af-errors"/><br/>
    <form:input path="address.city" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.address.region"/>:
    <form:errors path="address.region" cssClass="af-errors"/><br/>
    <form:input path="address.region" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.address.country"/>:
    <form:errors path="address.country" cssClass="af-errors"/><br/>
    <form:input path="address.country" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.address.postalCode"/>:
    <form:errors path="address.postalCode" cssClass="af-errors"/><br/>
    <form:input path="address.postalCode" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.administration.phone"/> 1:
    <form:errors path="phone" cssClass="af-errors"/><br/>
    <form:input path="phone" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.administration.phone"/> 2:
    <form:errors path="phone2" cssClass="af-errors"/><br/>
    <form:input path="phone2" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.administration.email"/>:
    <form:errors path="email" cssClass="af-errors"/><br/>
    <form:input path="email" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.administration.school"/>:
    <form:errors path="schools" cssClass="errors"/><br/>
    <form:checkboxes path="schools" items="${schools}" itemLabel="name" itemValue="id" disabled="${readOnly}"/>
  </div>
  <div>
    <c:choose>
      <c:when test="${readOnly}">
        <a href="<c:url value="edit-${administration.id}"/>"><spring:message code="af.edit"/></a>
        &nbsp;&nbsp;
      </c:when>
      <c:otherwise>
        <input type="submit" value="<spring:message code="af.save"/>"/>
      </c:otherwise>
    </c:choose>
    <a href="<c:url value="../administration/"/>"><spring:message code="af.cancel"/></a>
  </div>
</form:form>
<%@include file="/WEB-INF/jsp/footer.jsp" %>
