<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<table width="100%" cellpadding="3px" cellspacing="1px">
  <thead>
    <tr>
      <td><spring:message code="af.model.administration.name"/></td>
      <td><spring:message code="af.model.administration.birthPlaceDate"/></td>
      <td><spring:message code="af.model.administration.address"/></td>
      <td><spring:message code="af.model.administration.phone"/></td>
      <td><spring:message code="af.model.administration.phone"/> 2</td>
      <td><spring:message code="af.model.administration.email"/></td>
      <td></td>
    </tr>
  </thead>
  <tbody><% int c = 0;%>
    <c:forEach items="${administrations}" var="a">
      <tr class="<%= ++c % 2 == 0 ? "af-table-row-alt" : "" %>">
        <td><a href="<c:url value="${a.id}"/>" title="${a.name}">${a.name}</a></td>
        <td>${a.birthPlace}, ${a.birthDate}</td>
        <td>${a.address.street}, ${a.address.city}, ${a.address.country}, ${a.address.postalCode}</td>
        <td>${a.phone}</td>
        <td>${a.phone2}</td>
        <td>${a.email}</td>
        <td><a href="#" onclick="deleteAdministration(${a.id});"><spring:message code="af.delete"/></a></td>
      </tr>
    </c:forEach>
  </tbody>
</table>