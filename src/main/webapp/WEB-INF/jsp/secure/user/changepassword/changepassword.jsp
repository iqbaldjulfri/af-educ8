<%-- 
    Document   : changepassword
    Created on : Sep 22, 2012, 10:54:57 AM
    Author     : madewiratama
--%>

<%@include file="/WEB-INF/jsp/includes.jsp" %>
<c:set var="title" value="Change Password" scope="request"/>
<c:set var="titleCode" value="af.title.change-password" scope="request"/>

<%@include file="/WEB-INF/jsp/header.jsp" %>
<script type="text/javascript" src="<c:url value="/static/js/secure/user/changepassword.js"/>"></script>
<script type="text/javascript" src="<c:url value="/static/js/jquery.validate.js"/>"></script>
<style type="text/css">
  #userList {margin: 10px 0; }
</style>
<div>${request}</div>
<form:form modelAttribute="user" method="post" action="submit" name="formChangePassword" id="formChangePassword">
  <div>
    <spring:message code="af.model.user.password"/>:
    <br/>
    <input type="password" name="oldPassword" id="oldPassword">
  </div>
  <div>
    <spring:message code="af.model.user.new-password"/>:
    <br/>
    <input type="password" name="newPassword" id="newPassword">
  </div>
  <div>
    <spring:message code="af.model.user.retype-password"/>:
    <br/>
    <input type="password" name="retypePassword" id="retypePassword">
  </div>
  
  
  
  <div>
    <c:choose>
      <c:when test="${readOnly}">
        <a href="<c:url value="changepassword"/>"><spring:message code="af.back"/></a>
      </c:when>
      <c:otherwise>
        <input type="submit" value="<spring:message code="af.save"/>"/>
        <a href="<c:url value="changepassword"/>"><spring:message code="af.cancel" text="Cancel"/></a>
      </c:otherwise>
    </c:choose>
  </div>
</form:form>
<%@include file="/WEB-INF/jsp/footer.jsp" %>
