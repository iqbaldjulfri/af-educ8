<%--
    Document   : form
    Created on : Sep 19, 2012, 10:14:55 AM
    Author     : Iqbal Djulfri
--%>

<%@include file="/WEB-INF/jsp/includes.jsp" %>
<c:set var="title" value="Message Details" scope="request"/>
<c:set var="titleCode" value="af.title.message-details" scope="request"/>

<%@include file="/WEB-INF/jsp/header.jsp" %>
<script type="text/javascript" src="<c:url value="/static/js/secure/user/message/form.js"/>"></script>
<style type="text/css">
  #userList {margin: 10px 0; }
</style>
<form:form modelAttribute="message" method="post" action="submit">
  <div>
    <spring:message code="af.model.message.subject"/>:
    <form:errors path="subject" cssClass="af-errors"/><br/>
    <form:input path="subject" disabled="${readOnly}"/>
  </div>
  <c:if test="${readOnly}">
    <div>
      From:
      <form:errors path="sender" cssClass="af-errors"/><br/>
      <form:input path="sender.name" disabled="true"/>
    </div>
  </c:if>
  <c:if test="${readOnly == null || readOnly == false}">
    <div>
      To:
      <form:errors path="user" cssClass="af-errors"/><br/>
      <input type="text" id="users" style="display:none;"/>
      <input type="hidden" id="userIds" name="userIds"/>
      <div id="userList"></div>
    </div>
  </c:if>
  <div>
    <spring:message code="af.model.message.message"/>:
    <form:errors path="message" cssClass="af-errors"/><br/>
    <form:textarea path="message" disabled="${readOnly}"/>
  </div>
  <div>
    <c:choose>
      <c:when test="${readOnly}">
        <a href="<c:url value="../message/"/>"><spring:message code="af.back"/></a>
      </c:when>
      <c:otherwise>
        <input type="submit" value="<spring:message code="af.send" text="Send"/>"/>
        <a href="<c:url value="../message/"/>"><spring:message code="af.cancel" text="Cancel"/></a>
      </c:otherwise>
    </c:choose>
  </div>
</form:form>
<%@include file="/WEB-INF/jsp/footer.jsp" %>
