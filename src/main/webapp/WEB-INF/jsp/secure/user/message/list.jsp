<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<table width="100%" cellpadding="3px" cellspacing="1px">
  <thead>
    <tr>
      <td><spring:message code="af.model.message.subject"/></td>
      <td><spring:message code="af.model.message.sender"/></td>
      <td><spring:message code="af.model.message.time"/></td>
      <td></td>
    </tr>
  </thead>
  <tbody><% int c = 0;%>
    <c:forEach items="${messages}" var="item">
      <tr class="<%= ++c % 2 == 0 ? "af-table-row-alt" : "" %>">
        <td><a href="<c:url value="${item.id}"/>" title="${item.subject}" style="font-weight: ${item.isRead ? "normal" : "bold"}">${item.subject}</a></td>
        <td>${item.sender.name}</td>
        <td><fmt:formatDate value="${item.time}" pattern="dd-MM-yyyy hh:mm" /></td>
        <td><a href="#" onclick="deleteItem(${item.id});"><spring:message code="af.delete"/></a></td>
      </tr>
    </c:forEach>
  </tbody>
</table>