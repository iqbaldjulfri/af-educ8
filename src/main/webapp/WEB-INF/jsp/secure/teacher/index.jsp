<%--
    Document   : index
    Created on : Sep 19, 2012, 9:24:03 AM
    Author     : Iqbal Djulfri
--%>

<%@include file="/WEB-INF/jsp/includes.jsp" %>
<c:set var="title" value="Course & Exam Management" scope="request"/>
<c:set var="titleCode" value="af.title.teacher-dashboard" scope="request"/>
<%@include file="/WEB-INF/jsp/header.jsp" %>
<script type="text/javascript" src="<c:url value="/static/js/secure/teacher/index.js"/>"></script>
<div id="err">${err}</div>
<c:forEach items="${courses}" var="item">
  <a href="course/${item.id}">${item.name}</a><br/>
</c:forEach>
<%@include file="/WEB-INF/jsp/footer.jsp" %>
