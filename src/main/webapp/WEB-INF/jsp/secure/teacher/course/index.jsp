<%--
    Document   : index
    Created on : Sep 19, 2012, 9:24:03 AM
    Author     : Iqbal Djulfri
--%>

<%@include file="/WEB-INF/jsp/includes.jsp" %>
<c:set var="title" value="Course & Exam Management" scope="request"/>
<c:set var="titleCode" value="af.title.course-management" scope="request"/>
<%@include file="/WEB-INF/jsp/header.jsp" %>
<script type="text/javascript" src="<c:url value="/static/js/secure/teacher/course/index.js"/>"></script>
<div id="err">${err}</div>
<h3>Classes</h3>
<c:forEach items="${clazzes}" var="item">
  ${item.name}<br/>
</c:forEach>
<h3>Exams</h3>
<a href="<c:url value="${course.id}/exam/new"/>"><spring:message code="af.create-new"/></a>
<table width="100%" cellpadding="3px" cellspacing="1px">
  <thead>
    <tr>
      <td><spring:message code="af.model.exam.name"/></td>
      <td><spring:message code="af.model.exam.type"/></td>
      <td><spring:message code="af.model.exam.start"/></td>
      <td><spring:message code="af.model.exam.deadline"/></td>
      <td><spring:message code="af.model.exam.remedialFor"/></td>
      <td><spring:message code="af.model.exam.clazzes"/></td>
    </tr>
  </thead>
  <tbody><% int c = 0;%>
    <c:forEach items="${exams}" var="item">
      <tr class="<%= ++c % 2 == 0 ? "af-table-row-alt" : "" %>" id="exam-${item.id}">
        <td><a href="<c:url value="${course.id}/exam/${item.id}"/>" title="${item.name}">${item.name}</a></td>
        <td>${item.type.name}</td>
        <td><fmt:formatDate value="${item.start}" pattern="dd-MM-yyyy hh:mm" /></td>
        <td><fmt:formatDate value="${item.deadline}" pattern="dd-MM-yyyy hh:mm" /></td>
        <td><a href="<c:url value="#${item.remedialFor.id}"/>" title="${item.remedialFor.name}">${item.remedialFor.name}</a></td>
        <td>
          <c:forEach items="${item.clazzes}" var="clazz" varStatus="status">
            ${clazz.name}${!status.last ? ", " : ""}
          </c:forEach>
        </td>
      </tr>
    </c:forEach>
  </tbody>
</table>

<%@include file="/WEB-INF/jsp/footer.jsp" %>
