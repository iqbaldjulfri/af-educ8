<%--
    Document   : form
    Created on : Oct 23, 2012, 9:42:32 AM
    Author     : Iqbal Djulfri
--%>

<%@include file="/WEB-INF/jsp/includes.jsp" %>
<c:set var="title" value="Exam Details" scope="request"/>
<c:set var="titleCode" value="af.title.exam-details" scope="request"/>
<%@include file="/WEB-INF/jsp/header.jsp" %>
<script type="text/javascript" src="<c:url value="/static/js/jquery.iframe-transport.js"/>"></script>
<script type="text/javascript" src="<c:url value="/static/js/jquery.fileupload.js"/>"></script>
<script type="text/javascript" src="<c:url value="/static/js/jquery-ui-timepicker-addon.js"/>"></script>
<script type="text/javascript" src="<c:url value="/static/js/secure/teacher/course/exam/form.js"/>"></script>
<style>
  .filelist-del { padding: 0 10px; font-weight: bold; cursor: pointer; }
  .filelist-del:hover { background-color: red; color: white; }
  .filelist-item { margin: 5px 0 }
  .filelist-progress { position: absolute; height: 2px; width: 100%; z-index: -10; margin: 15px 0 0 0; background-color: orange; }
  .dropbox { background-color: #ffe0d0; color: orangered; text-align: center; vertical-align: middle; border: 1px dotted orangered; cursor: pointer; padding: 10px; margin: 5px 0;}
  .dropbox:hover { text-decoration: underline; }
</style>
<form:form modelAttribute="exam" method="post" action="submit">
  <div>
    <spring:message code="af.model.exam.type"/>:
    <form:errors path="type" cssClass="af-errors"/><br/>
    <form:select path="type" disabled="${readOnly}">
      <form:option value="">---</form:option>
      <form:options items="${types}" itemValue="id" itemLabel="name"/>
    </form:select>
  </div>
  <div>
    <spring:message code="af.model.exam.name"/>:
    <form:errors path="name" cssClass="af-errors"/><br/>
    <form:input path="name" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.exam.description"/>:
    <form:errors path="description" cssClass="af-errors"/><br/>
    <form:textarea path="description" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.exam.start"/>:
    <form:errors path="start" cssClass="af-errors"/><br/>
    <form:input path="start" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.exam.deadline"/>:
    <form:errors path="deadline" cssClass="af-errors"/><br/>
    <form:input path="deadline" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.exam.isSubmitable"/>:
    <form:errors path="isSubmitable" cssClass="af-errors"/><br/>
    <form:select path="isSubmitable" disabled="${readOnly}">
      <form:option value="true"><spring:message code="af.yes"/></form:option>
      <form:option value="false"><spring:message code="af.no"/></form:option>
    </form:select>
  </div>
  <div>
    <spring:message code="af.model.exam.isActive"/>:
    <form:errors path="isActive" cssClass="af-errors"/><br/>
    <form:select path="isActive" disabled="${readOnly}">
      <form:option value="true"><spring:message code="af.yes"/></form:option>
      <form:option value="false"><spring:message code="af.no"/></form:option>
    </form:select>
  </div>

  <div>
    <spring:message code="af.model.exam.clazzes"/>:
    <form:errors path="clazzes" cssClass="af-errors"/><br/>
    <form:checkboxes path="clazzes" disabled="${readOnly}" items="${clazzes}" itemValue="id" itemLabel="name"/>
  </div>
  <div>
    <spring:message code="af.model.exam.files"/>:<br/>
    <c:if test="${!readOnly}">
      <div class="dropbox">
        Click this box or drag and drop any file to this page to add
      </div>
      <input type="file" name="file" id="examFile" multiple="true" data-url="<c:url value="file"/>" style="display: none;"/>
    </c:if>
    <div id="fileList"><% int fcount = 0; %>
      <c:forEach items="${files}" var="item">
        <div class="filelist-item" data-id="${item.id}" data-count="<%= fcount++ %>">
          <span class="filelist-progress"></span>
          <c:if test="${!readOnly}"><span class="filelist-del" onclick="deleteFile($(this).parent());">x</span></c:if>
          <span class="filelist-name">
            <a href="<c:url value="file/${item.id}/${item.name}"/>" title="${item.name}">${item.name}</a>
          </span>
        </div>
      </c:forEach>
    </div>
  </div>
  <div>
    <c:choose>
      <c:when test="${readOnly}">
        <a href="<c:url value="edit-${exam.id}"/>"><spring:message code="af.edit"/></a>
        &nbsp;&nbsp;
      </c:when>
      <c:otherwise>
        <input type="submit" value="<spring:message code="af.save"/>"/>
      </c:otherwise>
    </c:choose>
    <a href="<c:url value="../../${course.id}"/>"><spring:message code="af.cancel"/></a>
  </div>
</form:form>
<%@include file="/WEB-INF/jsp/footer.jsp" %>
