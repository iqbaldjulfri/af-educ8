<%--
    Document   : form
    Created on : May 29, 2012, 8:46:54 PM
    Author     : Iqbal Djulfri
--%>

<%@include file="/WEB-INF/jsp/includes.jsp" %>
<c:set var="title" value="School Details" scope="request"/>
<c:set var="titleCode" value="af.title.school-details" scope="request"/>
<%@include file="/WEB-INF/jsp/header.jsp" %>
<form:form modelAttribute="school" method="post" action="submit">
  <div>
    <spring:message code="af.model.school.name"/>:
    <form:errors path="name" cssClass="af-errors"/><br/>
    <form:input path="name" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.school.level"/>:
    <form:errors path="level" cssClass="af-errors"/><br/>
    <form:input path="level" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.school.grade"/>:
    <form:errors path="grade" cssClass="af-errors"/><br/>
    <form:input path="grade" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.address.street"/>:
    <form:errors path="address.street" cssClass="af-errors"/><br/>
    <form:input path="address.street" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.address.city"/>:
    <form:errors path="address.city" cssClass="af-errors"/><br/>
    <form:input path="address.city" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.address.region"/>:
    <form:errors path="address.region" cssClass="af-errors"/><br/>
    <form:input path="address.region" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.address.country"/>:
    <form:errors path="address.country" cssClass="af-errors"/><br/>
    <form:input path="address.country" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.address.postalCode"/>:
    <form:errors path="address.postalCode" cssClass="af-errors"/><br/>
    <form:input path="address.postalCode" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.school.phone"/> 1:
    <form:errors path="phone" cssClass="af-errors"/><br/>
    <form:input path="phone" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.school.phone"/> 2:
    <form:errors path="phone2" cssClass="af-errors"/><br/>
    <form:input path="phone2" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.school.fax"/>:
    <form:errors path="fax" cssClass="af-errors"/><br/>
    <form:input path="fax" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.school.email"/>:
    <form:errors path="email" cssClass="af-errors"/><br/>
    <form:input path="email" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.school.website"/>:
    <form:errors path="website" cssClass="af-errors"/><br/>
    <form:input path="website" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.school.isActive"/>:
    <form:errors path="isActive" cssClass="af-errors"/><br/>

    <form:select path="isActive" disabled="${readOnly}">
      <form:option value="true"><spring:message code="af.yes"/></form:option>
      <form:option value="false"><spring:message code="af.no"/></form:option>
    </form:select>

  </div>
  <div>
    <c:choose>
      <c:when test="${readOnly}">
        <a href="<c:url value="edit-${school.id}"/>"><spring:message code="af.edit"/></a>
        &nbsp;&nbsp;
      </c:when>
      <c:otherwise>
        <input type="submit" value="<spring:message code="af.save"/>"/>
      </c:otherwise>
    </c:choose>
    <a href="<c:url value="../school/"/>"><spring:message code="af.cancel"/></a>
  </div>
</form:form>
<%@include file="/WEB-INF/jsp/footer.jsp" %>
