<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<table width="100%" cellpadding="3px" cellspacing="1px">
  <thead>
    <tr>
      <td><spring:message code="af.model.user.username"/></td>
      <td><spring:message code="af.model.user.name"/></td>
      <td><spring:message code="af.model.user.email"/></td>
      <td><spring:message code="af.model.user.authorities"/></td>
      <td><spring:message code="af.model.user.accountNonExpired"/></td>
      <td><spring:message code="af.model.user.accountNonLocked"/></td>
      <td><spring:message code="af.model.user.credentialsNonExpired"/></td>
      <td><spring:message code="af.model.user.enabled"/></td>
      <td></td>
    </tr>
  </thead>
  <tbody><% int c = 0; %>
    <c:forEach items="${users}" var="u">
      <tr class="<%= ++c % 2 == 0 ? "af-table-row-alt" : "" %>">
        <td><a href="<c:url value="${u.id}"/>">${u.username}</a></td>
        <td>${u.name}</td>
        <td>${u.email}</td>
        <td><% request.setAttribute("roleCount", 0); %>
          <c:forEach items="${u.authorities}" var="role">
            <% Integer i = (Integer) request.getAttribute("roleCount"); request.setAttribute("roleCount", ++i); %>
            ${role.authority}<c:if test="${roleCount < u.authorities.size()}">,</c:if>
          </c:forEach>
        </td>
        <td>
          <c:if test="${u.accountNonExpired}"><spring:message code="af.yes"/></c:if>
          <c:if test="${!u.accountNonExpired}"><spring:message code="af.no"/></c:if>
        </td>
        <td>
          <c:if test="${u.accountNonLocked}"><spring:message code="af.yes"/></c:if>
          <c:if test="${!u.accountNonLocked}"><spring:message code="af.no"/></c:if>
        </td>
        <td>
          <c:if test="${u.credentialsNonExpired}"><spring:message code="af.yes"/></c:if>
          <c:if test="${!u.credentialsNonExpired}"><spring:message code="af.no"/></c:if>
        </td>
        <td>
          <c:if test="${u.enabled}"><spring:message code="af.yes"/></c:if>
          <c:if test="${!u.enabled}"><spring:message code="af.no"/></c:if>
        </td>
        <td><a href="#" onclick="deleteUser(${u.id});"><spring:message code="af.delete"/></a></td>
      </tr>
    </c:forEach>
  </tbody>
</table>