<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<table width="100%" cellpadding="3px" cellspacing="1px">
  <thead>
    <tr>
      <td><spring:message code="af.model.school.name"/></td>
      <td><spring:message code="af.model.school.level"/></td>
      <td><spring:message code="af.model.school.grade"/></td>
      <td><spring:message code="af.model.school.isActive"/></td>
    </tr>
  </thead>
  <tbody><% int c = 0;%>
    <c:forEach items="${schools}" var="s">
      <tr class="<%= ++c % 2 == 0 ? "af-table-row-alt" : "" %>">
        <td><a href="<c:url value="${s.id}"/>" title="${s.name}">${s.name}</a></td>
        <td>${s.level}</td>
        <td>${s.grade}</td>
        <td>
          <c:if test="${s.isActive}"><spring:message code="af.yes"/></c:if>
          <c:if test="${!s.isActive}"><spring:message code="af.no"/></c:if>
        </td>
      </tr>
    </c:forEach>
  </tbody>
</table>