<%--
    Document   : form
    Created on : Apr 23, 2012, 11:14:24 AM
    Author     : Iqbal Djulfri
--%>

<%@include file="/WEB-INF/jsp/includes.jsp" %>
<c:set var="title" value="User Details" scope="request"/>
<c:set var="titleCode" value="af.title.user-details" scope="request"/>
<%@include file="/WEB-INF/jsp/header.jsp" %>
<form:form modelAttribute="user" method="post" action="submit">
  <div>
    <spring:message code="af.model.user.username"/>:
    <form:errors path="username" cssClass="af-errors"/><br/>
    <form:input path="username" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.user.password"/>:
    <form:errors path="password" cssClass="af-errors"/><br/>
    <input type="password" id="newPass" name="newPass" ${readOnly ? "disabled=\"true\"" : ""}/>
    <form:hidden path="password" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.user.name"/>:
    <form:errors path="name" cssClass="af-errors"/><br/>
    <form:input path="name" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.user.email"/>:
    <form:errors path="email" cssClass="af-errors"/><br/>
    <form:input path="email" disabled="${readOnly}"/>
  </div>
  <div>
    <spring:message code="af.model.user.authorities"/>:
    <form:errors path="authorities" cssClass="af-errors"/><br/>
    <form:checkboxes path="authorities" disabled="${readOnly}" items="${roles}" itemValue="id" itemLabel="authority"/>
  </div>
  <div>
    <c:choose>
      <c:when test="${readOnly}">
        <a href="<c:url value="edit-${user.id}"/>"><spring:message code="af.edit"/></a>
        &nbsp;&nbsp;
      </c:when>
      <c:otherwise>
        <input type="submit"/>
      </c:otherwise>
    </c:choose>
    <a href="<c:url value="../user/"/>"><spring:message code="af.cancel"/></a>
  </div>
</form:form>
<%@include file="/WEB-INF/jsp/footer.jsp" %>
