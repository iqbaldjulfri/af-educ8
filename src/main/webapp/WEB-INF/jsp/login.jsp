<%--
    Document   : login.jsp
    Created on : May 2, 2012 at 10:16 AM
    Author     : Iqbal Djulfri
--%>
<%@include file="/WEB-INF/jsp/includes.jsp" %>
<c:set var="title" value="Login" scope="request"/>
<%@include file="/WEB-INF/jsp/header.jsp" %>
<script type="text/javascript">
$(function() {
  $('#j_username').focus();
  $('[type="submit"]').button();
});
</script>
<div style="font-size: 12px; color: red; font-weight: bold;">${err}</div><br/>
<form action="j_spring_security_check" id="f" name="f" method="post">
  <label for="j_username">Username: </label><br/>
  <input id="idTxtUsername" type="text" name="j_username" id="j_username"/><br/>
  <label for="j_password">Password: </label><br/>
  <input type="password" name="j_password" id="j_password"/><br/><br/>
  <input type="submit" value="Login"/>
</form>
<%@include file="/WEB-INF/jsp/footer.jsp" %>
