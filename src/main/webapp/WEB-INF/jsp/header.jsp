<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@include file="menu.jsp" %>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><spring:message code="${titleCode}" text="${title}"/> | AirFrame</title>
    <script type="text/javascript" src="<c:url value="/static/js/jquery-1.8.2.min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/static/js/jquery-ui-1.9.0.custom.min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/static/js/json2.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/static/js/airframe-jquery-1.0.0.js"/>"></script>
    <script type="text/javascript">$.AF_CONTEXT = '<%= request.getContextPath() %>';</script>
    <link rel="stylesheet" href="<c:url value="/static/css/blu/jquery-ui-1.8.20.custom.css"/>"/>
    <link rel="stylesheet" href="<c:url value="/static/css/960-fluid.css"/>"/>
    <link rel="stylesheet" href="<c:url value="/static/css/default.css"/>"/>
  </head>
  <body>
    <div class="container_16">
      <div class="grid_16">
        <div class="af-cont-header">
          <div class="af-header-bar ui-widget-header">
            <div class="af-left">
              <a href="<c:url value="/"/>"><spring:message code="af.menu.[/]"/></a>
            </div>
            <div class="af-right">
              <c:choose>
                <c:when test="${USER != null}">
                  <spring:message code="af.welcome"/>, ${USER.name} <a href="<c:url value="/logout"/>">Logout</a>
                </c:when>
                <c:otherwise>
                  <a href="<c:url value="/login"/>">Login</a>
                </c:otherwise>
              </c:choose>
            </div>
            <div class="af-clear"></div>
          </div>
          <div class="af-header-title">
            <h1>AirFrame by Labtek V</h1>
          </div>
        </div>
      </div>
      <div class="grid_16">
        <div class="grid_3 alpha">
          <div class="af-cont-menu">
<%
            List<Menu> menus = (List<Menu>) request.getAttribute("MENU");
            for (Menu m : menus) {
              request.setAttribute("MENU_ITEM", m);
%>
            <div class="af-menu-bar"><spring:message code="af.menu.[${MENU_ITEM.url}]" text="${MENU_ITEM.name}"/></div>
            <div class="af-menu-items">
              <ul>
                <% parseMenu(((Menu) request.getAttribute("MENU_ITEM")).getChildren(), request,  out, pageContext); %>
              </ul>
            </div>
<%
            }
%>
            <div class="af-menu-items">
              <ul>
                <li><a href="?language=en" title="English"><spring:message code="af.lang.en" text="English"/></a></li>
                <li><a href="?language=id" title="Bahasa Indonesia"><spring:message code="af.lang.in" text="Bahasa Indonesia"/></a></li>
              </ul>
            </div>

          </div>
        </div>
        <div class="grid_13 omega">
          <div class="af-cont-content">
            <h2><spring:message code="${titleCode}" text="${title}"/></h2>
