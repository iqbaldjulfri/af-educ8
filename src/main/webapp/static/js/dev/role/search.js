//$(function() {
//  $('#search').click(function() {
//    $.post('search', {authority: ''}, function(roles) {
//      $('#list').html(roles.length == 0 ? 'no role' : '');
//      for (var i = 0; i < roles.length; i++){
//        var r = roles[i];
//        $('#list')
//          .append(
//            $('<a></a>')
//              .text(r.authority)
//              .attr('href', r.id)
//          ).append(' - ').append(
//            $('<a href="#list">Delete</a>')
//              .click(function() {
//                if (confirm('Are you sure?')) {
//                  $.delJSON('delete-' + r.id, {}, function(map) {
//                    if (map.status) alert('Done!');
//                    else alert(map.message);
//                  })
//                }
//
//                $('#search').click();
//
//                return false;
//              })
//          ).append('<br/>')
//      }
//    }, 'json')
//  })
//});

$(function() {
  $('#search').button({ icons: {primary: 'ui-icon-search' }}).click(function() {
    $('#page').val(1);
    search();
  });

  $('#prev').button({ icons: { primary: 'ui-icon-triangle-1-w' }, text: false }).click(function() {
    var page;
    try {
      page = parseInt($('#page').val());
      if (--page < 1) page = 1;
    } catch (e) { page = 1; }
    $('#page').val(page);
    search();
  });

  $('#next').button({ icons: { secondary: 'ui-icon-triangle-1-e' }, text: false }).click(function() {
    var page;
    try {
      page = parseInt($('#page').val());
      if (++page < 1) page = 1;
    } catch (e) { page = 1; }
    $('#page').val(page);
    search();
  });

  $('#page').val(1).keyup(function(e) {
    if (e.keyCode == 13) search();
  })
});

function search() {
  $('#list').html('loading...').load('search', {authority: $('#authority').val(), page: $('#page').val()});
}

function deleteRole(id) {
  if (confirm('Are you sure?')) {
    $.delJSON('delete-' + id, {}, function(map) {
      if (map.status) $.displayWarning('Done!');
      else $.displayError(map.message);
    })
  }

  search();
}