$(function() {
  $('#parentName:not(:disabled)').lookup({
    button: true,
    autocomplete: true,
    url: '/lookups/menu/by-name',
    keys: [
      { key: 'name', display: 'Name', select: true }
    ]
  }, function(data) {
    var parent = data[0];
    if (!parent) return;
    $('#parentId').val(parent.id);
    $('#parentName').val(parent.name);
  });
})