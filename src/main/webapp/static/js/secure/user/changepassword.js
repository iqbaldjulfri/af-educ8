$(document).ready(function(){
  $("#formChangePassword").validate({
    rules : {
      oldPassword:"required",
      newPassword:"required",
      retypePassword:{
        equalTo: "#newPassword"
      }
    }
  })
});