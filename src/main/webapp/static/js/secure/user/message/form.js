$(function() {
  $('#userIds:not(:disabled)').lookup({
    button: true,
    autocomplete: true,
    url: '/lookups/user/by-name',
    multiple: true,
    keys: [
      { id: true, key: 'id', display: false },
      { key: 'name', display: 'Name', select: true }
    ]
  }, function(data) {
    $('#userList').html('');

    var user = data[0];
    if (!user) return;

    for (var i = 0; i < data.length; i++) {
      user = data[i];
      var div = $('<div class="af-lookup-select-cont"></div>').data('userId', user.id);

      div.append('<span class="af-lookup-select-text">' + user.name + '</span>')
        .append('<span class="af-lookup-select-del">x</span>')
        .appendTo('#userList');

      div.children().last().click(function() {
        $(this).parent().hide(200, function() { $(this).remove(); updateUserList(); });
      });

      updateUserList();
    }
  });
});

function updateUserList() {
  var ids = [];
  $('#userList').children().each(function() {
    ids.push($(this).data('userId'));
  });

  $('#userIds').val(ids.join(','));
}