$(function() {
  var startDateTextBox = $('#start');
  var endDateTextBox = $('#deadline');

  startDateTextBox.datetimepicker({
    onClose: function(dateText, inst) {
      if (endDateTextBox.val() != '') {
        var testStartDate = startDateTextBox.datetimepicker('getDate');
        var testEndDate = endDateTextBox.datetimepicker('getDate');
        if (testStartDate > testEndDate)
          endDateTextBox.datetimepicker('setDate', testStartDate);
      }
      else {
        endDateTextBox.val(dateText);
      }
    },
    onSelect: function (selectedDateTime){
      endDateTextBox.datetimepicker('option', 'minDate', startDateTextBox.datetimepicker('getDate') );
    },
    dateFormat: 'dd-mm-yy',
    timeFormat: 'hh:mm',
    hourGrid: 4,
    minuteGrid: 10
  });
  endDateTextBox.datetimepicker({
    onClose: function(dateText, inst) {
      if (startDateTextBox.val() != '') {
        var testStartDate = startDateTextBox.datetimepicker('getDate');
        var testEndDate = endDateTextBox.datetimepicker('getDate');
        if (testStartDate > testEndDate)
          startDateTextBox.datetimepicker('setDate', testEndDate);
      }
      else {
        startDateTextBox.val(dateText);
      }
    },
    onSelect: function (selectedDateTime){
      startDateTextBox.datetimepicker('option', 'maxDate', endDateTextBox.datetimepicker('getDate') );
    },
    dateFormat: 'dd-mm-yy',
    timeFormat: 'hh:mm',
    hourGrid: 4,
    minuteGrid: 10
  });

  $('#examFile').fileupload({
    dataType: 'json',
    done: function (e, data) {
      for (var i = 0; i < data.elms.length; i++) {
        var elm = data.elms[i];
        elm.children('.filelist-progress').css('width', '100%');
      }
    },
    add: function(e, data) {
      data.elms = [];
      var flist = $('#fileList');
      var count = flist.children().length;
      for (var i = 0; i < data.files.length; i++) {
        var cont = $('<div class="filelist-item"></div>').data('count', count++);
        var prg = $('<span class="filelist-progress" style="width: 0"></span>').appendTo(cont);
        var del = $('<span class="filelist-del">x</span>').appendTo(cont);
        var name = $('<span class="filelist-name">' + data.files[i].name + '</span>').appendTo(cont);
        flist.append(cont);
        del.click(function() {
          deleteFile($(this).parent());
        });
        data.elms.push(cont);
      }
      data.submit();
    },
    progress: function(e, data) {
      var progress = parseInt(data.loaded / data.total * 100, 10);
      for (var i = 0; i < data.elms.length; i++) {
        var elm = data.elms[i];
        elm.children('.filelist-progress').css('width', '' + progress + '%');
      }
    },
    fail: function(e, data) {
      for (var i = 0; i < data.elms.length; i++) {
        var elm = data.elms[i];
        elm.remove();
      }
    }
  });

  $('.dropbox').click(function() {
    $(this).next().trigger('click');
  })
});

function deleteFile(item) {
  var id = item.data('id');
  var files = $('#fileList').children();
  for (var index = 0; index < files.length; index++) {
    if (item[0] == files[index]) break;
  }
  $.postJSON('delete-file', {index: index}, function() {
    item.remove();
  });
}