$(function() {
  // init tabs
  $('#clz-tabs').tabs();
  $('#toggle-tabs').button();
  // init button
  $('#validate').button().click(function() {
    $.getJSON('validate', { semesterId: $('[semesterId]').attr('semesterId') }, function(list) {
      clearErrors();
      parseErrors(list);
    })
  });
  $('#generate').button().click(function() {
    $('#generateDialog').dialog('open');
    $.postJSON('generate', { semesterId: $('[semesterId]').attr('semesterId'), day: $('#day').val() }, function(map) {
      if (map.status == true) {
        alert('Schedule generation completed. Press OK to reload page');
        window.location.reload(true);
      } else {
        alert('Schedule generation failed with a following error: \n' + map.message);
        $('#generateDialog').dialog('close');
      }
    })
  });
  $('#lockday').button().click(function() {
    var day = $('#day').val();
//    $('.sch-day-' + day).data('schedule').isLocked = true;
    $('.sch-day-' + day).each(function() {
      $(this).find(':checkbox')[0].checked = true;
      update($(this));
    });
  });
  $('#unlockday').button().click(function() {
    var day = $('#day').val();
//    $('.sch-day-' + day).data('schedule').isLocked = false;
    $('.sch-day-' + day).each(function() {
      $(this).find(':checkbox')[0].checked = false;
      update($(this));
    });
  });
  $('#clearday').button().click(function() {
    var day = $('#day').val();
//    $('.sch-day-' + day).data('schedule').isLocked = false;
    $('.sch-day-' + day).each(function() {
      clearSchedule($(this));
    });
  });
  // init drag
  $('.sch-cont').draggable({
    handle: '.sch-title',
    helper: 'clone',
    revert: 'invalid',
    scroll: true,
    cursor: 'move',
    cursorAt: { left: 87 },
    opacity: 0.7
  });
  // init drop
  $('.sch-slot-cont').droppable({
    accept: '.sch-cont',
    hoverClass: 'ui-state-hover',
    drop: function(e, ui) {
      var source = $(ui.draggable);
      var target = $(this).find('div.sch-cont');
      if (source.attr('id') == target.attr('id')) return;

      switch_(source, target);
    }
  });
  // init data
  $('.sch-cont').each(function() {
    copyUIToData($(this));
  });
  $('.sch-title').data('errors', []);

  // init dialog
  $('#generateDialog').dialog({
    modal: true,
    autoOpen: false,
    closeOnEscape: false,
    draggable: false,
    resizable: false,
    title: false
  }).prev().hide().parent().addClass('sch-gen-dlg-cont');
  $('#progressbar').progressbar({value: 100});
});

function switchUI(source, target) {
  var temp = $(target).clone();
  copyUIToData(temp);
  copyUI(source, target);
  copyUI(temp, source);
}

function moveUI(source, target) {
  copyUI(source, target);
  clearUI(source);
}

function copyUI(source, target) {
  var src = $(source).data('schedule');
  var tgt = $(target).data('schedule');
  tgt.isLocked = src.isLocked;
  tgt.room.id = src.room.id;
  tgt.room.name = src.room.name;
  tgt.course.id = src.course.id;
  tgt.course.name = src.course.name;
  tgt.teacher.id = src.teacher.id;
  tgt.teacher.name = src.teacher.name;

  loadUIFromObject($(target).data('schedule', tgt).data('schedule'));
}

function clearUI(sch) {
  var data = $(sch).data('schedule');
  data.room.id = data.room.name = data.course.id = data.course.name =
    data.teacher.id = data.teacher.name = '';
  data.isLocked = false;
  loadUIFromObject(data);
}

function loadUIFromObject(sch) {
  if (!sch.id) return;
  var dtl = $('#sch-' + sch.id).find('.sch-details');
  dtl.find('input[id^="roomid"]').val(sch.room.id);
  dtl.find('input[id^="roomname"]').val(sch.room.name)[0].disabled = sch.isLocked;
  dtl.find('input[id^="courseid"]').val(sch.course.id);
  dtl.find('input[id^="coursename"]').val(sch.course.name)[0].disabled = sch.isLocked;
  dtl.find('input[id^="teacherid"]').val(sch.teacher.id);
  dtl.find('input[id^="teachername"]').val(sch.teacher.name)[0].disabled = sch.isLocked;
  var rad = dtl.find('[input[type="checkbox"]');
  rad[0].checked = sch.isLocked;
}

function copyUIToData(sch) {
  var dtl = $(sch).find('.sch-details');
  var schedule = {
    room: {
      id: dtl.find('input[id^="roomid"]').val(),
      name: dtl.find('input[id^="roomname"]').val()
    },
    course: {
      id: dtl.find('input[id^="courseid"]').val(),
      name: dtl.find('input[id^="coursename"]').val()
    },
    teacher: {
      id: dtl.find('input[id^="teacherid"]').val(),
      name: dtl.find('input[id^="teachername"]').val()
    },
    id: dtl.parent().attr('id').replace('sch-', ''),
    isLocked: dtl.find('input[type="checkbox"]')[0].checked
  };
  $(sch).data('schedule', schedule);
}

function update(sch) {
  copyUIToData(sch);
  var data = $(sch).data('schedule');
  var schedule = {
    id: data.id,
    courseId: data.course.id ? data.course.id : '',
    roomId: data.room.id ? data.room.id : '',
    teacherId: data.teacher.id ? data.teacher.id : '',
    lock: data.isLocked
  };

  $.postJSON('update', schedule, function(map) {
    if (map.status) {
      loadUIFromObject(data);
    } else {
      $.displayError(map.message)
    }
  });
}

function switch_(source, target) {
  copyUIToData(source);
  copyUIToData(target);
  var datsrc = $(source).data('schedule');
  var dattgt = $(target).data('schedule');
  var dat = {
    source: datsrc.id,
    target: dattgt.id
  };

  $.postJSON('switch', dat, function(map) {
    if (map.status) {
      switchUI(source, target);
    } else {
      $.displayError(map.message)
    }
  });
}

function clearSchedule(sch) {
  $.postJSON('clear', { id: $(sch).data('schedule').id }, function(map) {
    if (map.status) {
      clearUI(sch);
    } else {
      $.displayError(map.message)
    }
  });
}

function clearErrors() {
  $('.sch-error').data('errors', []).removeClass('sch-error');
  $('#errList').html('<ol></ol>')
}

function parseErrors(list) {
  if (!list || list.length == 0) $('#errList').find('ol').append('<li>You\'re doing great! No error found :)</li>');
  for (var i = 0; i < list.length; i++) {
    var err = list[i];
    var li = $('<li></li>');

    if (err.type == 'maxWeekCount' || err.type == 'maxDayCount') {
      if (err.type == 'maxWeekCount') {
        li.append('Max count per week violated (max: ' + err.max + ', actual: ' + err.count + ') ');
      } else {
        li.append('Max count per day violated (max: ' + err.max + ', actual: ' + err.count + ') ');
        li.append('on ');
        switch (err.day) {
          case 0:
            li.append('Monday ');
            break;
          case 1:
            li.append('Tuesday ');
            break;
          case 2:
            li.append('Wednesday ');
            break;
          case 3:
            li.append('Thursday ');
            break;
          case 4:
            li.append('Friday ');
            break;
          case 5:
            li.append('Saturday ');
            break;
          default:
            li.append('Sunday ');
            break;
        }
      }
      li.append('by course <b>' + err.course.name + '</b> ');
      li.append('in class ').append('<a href="#clz-' + err.clazz.id + '">' + err.clazz.name + '</a> ');
    } else if (err.type == 'course') {
      li.append('Course <b>' + err.course.name + '</b> conflicts ');
    } else if (err.type == 'teacher') {
      li.append('Teacher <b>' + err.teacher.name + '</b> conflicts ');
    } else {
      li.append('Room <b>' + err.room.name + '</b> conflicts ');
    }
    li.append('in schedules: ')

    for (var j = 0; j < err.schedules.length; j++) {
      var sch = err.schedules[j];

      li.append('<a href="#sch-' + sch.id + '">' + sch.slot.description +' </a>');
      if (j < err.schedules.length - 1) li.append(',');
      li.append(' ');

      var schTitle = $('#sch-' + sch.id).find('.sch-title');
      schTitle.data('errors').push(err);
      if (!schTitle.hasClass('sch-error')) {
        schTitle.addClass('sch-error');
      }
    }

    $('#errList').find('ol').append(li);
  }
}

function createRoomLookup(obj) {
  $(obj).lookup({
    button: false,
    autocomplete: true,
    url: '/lookups/room/by-name',
    keys: [ { key: 'name', display: 'Name', select: true } ],
    addData: function() {
      this.schoolId = $('#schoolId').val();
    }
  }, function(data) {
    var room = data[0];
    if (!room) return;
    $(this).val(room.name).prev().val(room.id);
    update($(this).parent().parent());
  });
}

function createCourseLookup(obj) {
  $(obj).lookup({
    button: false,
    autocomplete: true,
    url: '/lookups/course/by-name',
    keys: [ { key: 'name', display: 'Name', select: true } ],
    addData: function() {
      this.schoolId = $('#schoolId').val();
    }
  }, function(data) {
    var course = data[0];
    if (!course) return;
    $(this).val(course.name).prev().val(course.id);
    update($(this).parent().parent());
  });
}

function createTeacherLookup(obj) {
  $(obj).lookup({
    button: false,
    autocomplete: true,
    url: '/lookups/teacher/by-name',
    keys: [ { key: 'name', display: 'Name', select: true } ],
    addData: function() {
      this.schoolId = $('#schoolId').val();
    }
  }, function(data) {
    var course = data[0];
    if (!course) return;
    $(this).val(course.name).prev().val(course.id);
    update($(this).parent().parent());
  });
}

function toggleTabs() {
  if ($('#clz-tabs').hasClass('ui-tabs')) {
    $('#clz-tabs').tabs('destroy').find('ul').hide();
  } else {
    $('#clz-tabs').tabs().find('ul').show();
  }
}