$(function() {
  $('#teacherName:not(:disabled)').lookup({
    button: true,
    autocomplete: true,
    url: '/lookups/teacher/by-name',
    keys: [
      { key: 'name', display: 'Name', select: true }
    ],
    addData: function() {
      this.schoolId = $('#school').val();
    }
  }, function(data) {
    var teacher = data[0];
    if (!teacher) return;
    $('#homeroomTeacher').val(teacher.id);
    $('#teacherName').val(teacher.name);
  });

  $('#grade:not(:disabled)').lookup({
    button: false,
    autocomplete: true,
    url: '/lookups/clazz-and-course-grades/by-name',
    keys: [
      { key: 'value', display: 'Value', select: true}
    ],
    addData: function() {
      this.schoolId = $('#school').val();
    }
  }, function(item) {
    $(this).val(item[0].value);
  });

  $('#school').change(function() { $('#homeroomTeacher, #teacherName').val(''); });
})