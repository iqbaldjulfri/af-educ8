$(function() {
  $('#start, #end').datepicker({
    dateFormat: 'dd-mm-yy',
    changeMonth: true,
    changeYear: true,
    onSelect: function(dateText) {
      var isStart = $(this).attr('id') == 'start';
      if (isStart) $('#end').datepicker('option', 'minDate', dateText);
      else $('#start').datepicker('option', 'maxDate', dateText);
    }
  });
});