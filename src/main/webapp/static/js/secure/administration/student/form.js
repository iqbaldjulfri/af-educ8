$(function() {
  $('#birthDate').datepicker({dateFormat: "dd-mm-yy"});

  $('#newFather').click(function() {
    var url = 'popup/parent/?';
    url += 'id=' + $('#father_id').val();
    url += '&type=0&elmId=father';
    var win = window.open(url, 'Add Father', 'height=400,width=600,scrollbars');

    if (window.focus) win.focus();

    return false;
  });
});