// jQuery AJAX override

//$.getJSON = function(url, data, callback) {
//  $.ajax({type: 'GET', url: url, data: data, success: callback, dataType: 'json', contentType: 'application/json'});
//};
//
//$.get = function(url, data, callback, type) {
//  if (type == 'json') $.getJSON(url, data, callback);
//  else $.ajax({type: 'GET', url: url, data: data, success: callback, dataType: type});
//};
//
$.postJSON = function(url, data, callback) {
  $.post(url, data, callback, 'json');
}
//
//$.post = function(url, data, callback, type) {
//  if (type == 'json') $.postJSON(url, data, callback);
//  else $.ajax({type: 'POST', url: url, data: data, success: callback, dataType: type});
//};
//
$.delJSON = function(url, data, callback) {
  $.ajax({type: 'DELETE', url: url, data: data, success: callback, dataType: 'json', contentType: 'application/json'});
};

$.del = function(url, data, callback, type) {
  if (type == 'json') $.delJSON(url, data, callback);
  else $.ajax({type: 'DELETE', url: url, data: data, success: callback, dataType: type});
};

$.putJSON = function(url, data, callback) {
  $.ajax({type: 'PUT', url: url, data: data, success: callback, dataType: 'json', contentType: 'application/json'});
}

$.put = function(url, data, callback, type) {
  if (type == 'json') $.putJSON(url, data, callback);
  else $.ajax({type: 'PUT', url: url, data: data, success: callback, dataType: type});
}

$.fn.serializeObject = function() {
  var arrayData, objectData;
  arrayData = this.serializeArray();
  objectData = {};

  $.each(arrayData, function() {
    var value;

    if (this.value != null) {
      value = this.value;
    } else {
      value = '';
    }

    // 0.1.2.3.4
    // a.b.c.d.e = z
    // a.b.c.d.f = ZZZ
    if (this.name.indexOf('.') != -1) {
      var attrs = this.name.split('.');
      var tx = objectData;

      for (var i = 0; i < attrs.length - 1; i++) {
        if (objectData[attrs[i]] == undefined)
          objectData[attrs[i]] = {};
        tx = objectData[attrs[i]];
      }
      tx[attrs[attrs.length - 1]] = value;
    } else {
      if (objectData[this.name] != null) {
        if (!objectData[this.name].push) {
          objectData[this.name] = [objectData[this.name]];
        }

        objectData[this.name].push(value);
      } else {
        objectData[this.name] = value;
      }
    }
  });

  return objectData;
};

// end of jQuery AJAX override

// AirFrame's jQuery extension

/**
 * Format item with type to string<br/><br/>
 *
 * <b>type:</b>
 * <ul>
 *  <li>string</li>
 *  <li>
 *    number<br/>
 *    options:
 *    <ul>
 *      <li><b>digitGrouping</b>: boolean (digit grouping; default: true)</li>
 *      <li><b>decimalDigits</b>: int (decimal digits; default: 2)</li>
 *      <li><b>prefix</b>: string (prefix; default: '')</li>
 *      <li><b>suffix</b>: string (suffix; default: '')</li>
 *    </ul>
 *  </li>
 *  <li>
 *    boolean<br/>
 *    options:
 *    <ul>
 *      <li><b>trueValue</b>: string (true value; default: 'YES')</li>
 *      <li><b>falseValue</b>: string (false value; default: 'NO')</li>
 *    </ul>
 *  </li>
 *  <li>
 *    date<br/>
 *    options:
 *    <ul>
 *      <li><b>pattern</b>: string (pattern; default: 'yyyy-MM-dd')</li>
 *    </ul>
 *  </li>
 * </ul>
 *
 * <b>form:</b>
 * <ul>
 *  <li>
 *    a<br/>
 *    options:
 *    <ul>
 *      <li><b>url</b>: string (URL; default: '')</li>
 *      <li><b>title</b>: string (title attribute; default: '')</li>
 *    </ul>
 *  </li>
 *  <li>
 *    button<br/>
 *    options:
 *    <ul>
 *      <li><b>text</b>: boolean (use text?; default: true)</li>
 *      <li><b>primaryIcon</b>: string (primary icon; default: undefined)</li>
 *      <li><b>secondaryIcon</b>: string (secondary icon; default: undefined)</li>
 *      <li><b>click</b>: string (onclick function name; default: undefined)</li>
 *    </ul>
 *  </li>
 * </ul>
 *
 **/
$.formatType = function(item, format, useForm) {
  if (item == undefined) return '';
  if (format == undefined) return item;

  var text = '';
  var typeofFormat = typeof format;

  if (typeofFormat == 'string') {
    format = format.toLowerCase();
    if (format == 'boolean') text = item ? 'YES' : 'NO';
    else if (format == 'number') text = $.toDecimalText(item);
    else if (format == 'date') text = $.toDateText(item);
    else text = item;
  } else if (typeofFormat == 'object') {
    var type = format.type ? format.type.toLowerCase() : 'string';
    var form = format.form && format.form.type ? format.form.type.toLowerCase() : '';

    // format text
    if (type == 'boolean')
      text = $.toBooleanText(item, format.trueValue, format.falseValue);
    else if (type == 'number')
      text = $.toDecimalText(item, format.digitGrouping, format.decimalDigits, format.prefix, format.suffix);
    else if (type =='date')
      text = $.toDateText(item, format.pattern);
    else text = item;

    // format form
    if (useForm == undefined || useForm == true)
      if (form == 'a')
        text = $('<a>' + text + '</a>')
          .attr('href', format.form.url ? format.form.url : '')
          .attr('title', format.form.title ? format.form.title : (format.form.url ? format.form.url : ''));
      else if (form == 'button')
        text = $('<button>' + text + '</button>')
          .attr('onclick', format.form.click ? format.form.click + '(event)' : '')
          .button({
            text: format.form.text,
            icons: {
              primary: format.form.primaryIcon,
              secondary: format.form.secondaryIcon
            }
          });

  } else {
    text = item;
  }

  return text;
}

$.toBooleanText = function(item, trueValue, falseValue) {
  if (typeof item != 'boolean') return 'NOT A BOOLEAN';
  return item ? (trueValue != undefined ? trueValue : 'YES') : (falseValue != undefined ? falseValue : 'NO');
}

$.toDecimalText = function(item, digitGrouping, decimalDigits, prefix, suffix) {
  var num, text, tmp,
    dg = digitGrouping != undefined ? digitGrouping : true,
    dd = decimalDigits != undefined ? decimalDigits : 2,
    pf = prefix != undefined ? prefix : '',
    sf = suffix != undefined ? suffix : '';
  if (typeof item == 'string') try {num = parseFloat(item);} catch (e) {return NaN;}
  else if (typeof item == 'number') num = item;
  else return NaN;

  num = Math.round(num * Math.pow(10, dd)) / Math.pow(10, dd);
  text = '' + num;
  tmp = text.split('.');
  text = '';
  for (var i = 0; i < tmp[0].length; i++) {
    text = tmp[0][tmp[0].length - i - 1] + text;
    if ((i + 1) % 3 == 0 && tmp[0].length != i + 1) text = ',' + text;
  }

  if (dd > 0) {
    if (!tmp[1]) tmp[1] = '';
    while (tmp[1].length < dd) tmp[1] += '0';
  }

  text = pf + text + (dd > 0 ? '.' + tmp[1] : '') + sf;


  return text;
}

$.toDateText = function (milisec, fmt) {
	var res = fmt ? fmt : 'yyyy-MM-dd';
	var date = new Date(milisec);
	var temp1, temp2 = [], temp3 = [];
	var i, j, c = -1;
  var _M_S = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
  var _M_L = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  var _D_S = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
  var _D_L = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

	if (res.indexOf('\'\'') != -1) {
		res = res.replace('\'\'', '#0');
		temp3.push('\'');
	}
	temp1 = res.split('\'');
	for (i = 0; i < temp1.length; i = i + 2) {
		if (temp1[i].indexOf('yyyy') != -1) {
			temp1[i] = temp1[i].replace('yyyy', '#' + temp3.length);
			temp3.push(date.getFullYear());
		}
		if (temp1[i].indexOf('yy') != -1) {
			temp1[i] = temp1[i].replace('yy', '#' + temp3.length);
			temp3.push(('' + date.getFullYear()).substr(2, 2));
		}
		if (temp1[i].indexOf('MMM') != -1) {
			temp1[i] = temp1[i].replace('MMM', '#' + temp3.length);
			temp3.push(_M_L[date.getMonth()]);
		}
		if (temp1[i].indexOf('MM') != -1) {
			temp1[i] = temp1[i].replace('MM', '#' + temp3.length);
			temp3.push(_M_S[date.getMonth()]);
		}
		if (temp1[i].indexOf('M') != -1) {
			temp1[i] = temp1[i].replace('M', '#' + temp3.length);
			temp3.push(date.getMonth() < 9 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1);
		}
		if (temp1[i].indexOf('EE') != -1) {
			temp1[i] = temp1[i].replace('EE', '#' + temp3.length);
			temp3.push(_D_L[date.getDay()]);
		}
		if (temp1[i].indexOf('E') != -1) {
			temp1[i] = temp1[i].replace('E', '#' + temp3.length);
			temp3.push(_D_S[date.getDay()]);
		}
		if (temp1[i].indexOf('dd') != -1) {
			temp1[i] = temp1[i].replace('dd', '#' + temp3.length);
			temp3.push(date.getDate() < 10 ? '0' + date.getDate() : date.getDate());
		}
		if (temp1[i].indexOf('HH') != -1) {
			temp1[i] = temp1[i].replace('HH', '#' + temp3.length);
			temp3.push(date.getHours() < 10 ? '0' + date.getHours() : date.getHours());
		}
		if (temp1[i].indexOf('H') != -1) {
			temp1[i] = temp1[i].replace('H', '#' + temp3.length);
			temp3.push(date.getHours());
		}
		if (temp1[i].indexOf('hh') != -1) {
			temp1[i] = temp1[i].replace('hh', '#' + temp3.length);
			temp3.push(date.getHours() < 10 ? '0' + date.getHours() : date.getHours());
		}
		if (temp1[i].indexOf('h') != -1) {
			temp1[i] = temp1[i].replace('h', '#' + temp3.length);
			temp3.push(date.getHours());
		}
		if (temp1[i].indexOf('mm') != -1) {
			temp1[i] = temp1[i].replace('mm', '#' + temp3.length);
			temp3.push(date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes());
		}
		if (temp1[i].indexOf('m') != -1) {
			temp1[i] = temp1[i].replace('m', '#' + temp3.length);
			temp3.push(date.getMinutes());
		}
		if (temp1[i].indexOf('ss') != -1) {
			temp1[i] = temp1[i].replace('ss', '#' + temp3.length);
			temp3.push(date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds());
		}
		if (temp1[i].indexOf('s') != -1) {
			temp1[i] = temp1[i].replace('s', '#' + temp3.length);
			temp3.push(date.getSeconds());
		}
		if (temp1[i].indexOf('SS') != -1) {
			temp1[i] = temp1[i].replace('SS', '#' + temp3.length);
			temp3.push(date.getMilliseconds() < 10 ? '00' + date.getMilliseconds()
				: date.getMilliseconds() < 100 ? '0' + date.getMilliseconds()
				: date.getMilliseconds());
		}
		if (temp1[i].indexOf('S') != -1) {
			temp1[i] = temp1[i].replace('S', '#' + temp3.length);
			temp3.push(date.getMilliseconds());
		}
	}

	res = temp1.join('');
	for (i = 0; i < temp3.length; i++) {
		res = res.replace('#' + i, temp3[i]);
	}

	return res;
}

$.displayError = function(msg) {
  if (msg == undefined) msg = 'Unspecified Error';

  var cont = $('<div class="ui-state-error ui-corner-all" style="z-index: 9999999999999999999;"></div>')
    .append('<p style="padding: 0 10px; float: left; width: 440px;"></p>');
  cont.children()
    .append('<span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>')
    .append('<b>Error: </b>').append(msg);

  var btn = $('<button class="af-right">Close</button>');
  cont.append(btn
      .addClass('ui-state-default')
      .css('margin', '5px')
      .hide()
      .button({text: false, icons: {primary: 'ui-icon-close'}})
    )
    .append('<div class="af-clear"></div>')
    .addClass('af-flash-dialog').appendTo($('body')).show(200, function() {
      $(this).children('button').show().click(function() {
        $(this).parent().hide(200, function() {
          $(this).remove();
        })
      })
    });
}

$.displayWarning = function(msg) {
  if (msg == undefined) msg = 'Unspecified Warning';

  var cont = $('<div class="ui-state-highlight ui-corner-all" style="z-index: 9999999999999999999;"></div>')
    .append('<p style="padding: 0 10px; float: left; width: 440px;"></p>');
  cont.children()
    .append('<span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>')
    .append(msg);

  var btn = $('<button class="af-right">Close</button>');
  cont.append(btn
      .addClass('ui-state-default')
      .css('margin', '5px')
      .hide()
      .button({text: false, icons: {primary: 'ui-icon-close'}})
    )
    .append('<div class="af-clear"></div>')
    .addClass('af-flash-dialog').appendTo($('body')).show(200, function() {
      $(this).children('button').show().click(function() {
        $(this).parent().hide(200, function() {
          $(this).remove();
        })
      })
    });
}

$.fn.extend({

  /**
   * Lookup
   *
   * opt.url: string
   * opt.multiple: boolean
   * opt.autocomplete: boolean
   * opt.button: boolean
   * opt.keys: array of objects
   * opt.lookup: object for lookup dialog
   * opt.addData: function for adding additional GET data
   **/
  lookup: function(opt, callback) {
    function parseLookupTable(data) {
      var tab = $('<table width="100%" cellpadding="3px" cellspacing="1px"></table>');
      var cont = $($('#lookup_' + opt.id).children('div')[2]);
      var selCont =  $($('#lookup_' + opt.id).children('div')[1]);
      var th = $('<thead></thead>');
      var trh = $('<tr></tr>').appendTo(th);
      var tb = $('<tbody></tbody>');

      cont.html('Result: <br/>');

      $.each(opt.keys, function() {
        if (this.display)
          trh.append('<td>' + (this.display ? this.display : this.key) + '</td>');
      });

      $.each(data, function(i) {
        var trb = $('<tr></tr>').addClass('af-table-row-clickable');
        if ((i + 1) % 2 == 0) trb.addClass('af-table-row-alt');
        var item = this;
        $.each(opt.keys, function() {
          if (this.display) {
            trb.append($('<td></td>').append($.formatType(item[this.key], this.format)));
            if (this.format && this.format.type && this.format.type.toLowerCase() == 'number')
              trb.children().last().css('text-align', 'right');
          }
        });

        trb.appendTo(tb).data('item', this).click(function() {
          var div = $('<div class="af-lookup-select-cont"></div>');
          var span = $('<span class="af-lookup-select-text"></span>').appendTo(div);
          var del = $('<span class="af-lookup-select-del">x</span>').appendTo(div);
          var item = $(this).data('item');

          $.each(opt.keys, function() {
            if (this.select) span.append($.formatType(item[this.key], this.format, false)).append('<br/>');
          });

          if (opt.multiple) {
            var idKey = [];
            var found = false;

            $.each(opt.keys, function() {
              if (this.id) idKey.push(this.key);
            });

            $.each(selCont.children('div'), function() {
              if (!found) {
                var tmp = $(this).data('item');
                var same = true;

                $.each(idKey, function() {
                  if (item[this] != tmp[this]) same = false;
                });

                if (same) {
                  found = true;
                  $(this)
                    .animate({opacity: 'toggle'}, 100).animate({opacity: 'toggle'}, 100)
                    .animate({opacity: 'toggle'}, 100).animate({opacity: 'toggle'}, 100);
                }
              }
            });

            if (!found) selCont.append(div);
          } else {
            selCont.children('div').remove();
            selCont.append(div);
          }

          div.data('item', item);

          del.click(function() {
            $(this).parent().remove();
          })
        });
      });

      tab.append(th).append(tb).appendTo(cont);
    }

    if (this.length < 1) return;
    if ($(this[0]).data('AF_LOOKUP')) return;
    $(this[0]).data('AF_LOOKUP', true);
    opt.id = $(this[0]).attr('id');
    opt.url = opt.url ? (opt.url.indexOf('/') == 0 ? $.AF_CONTEXT + opt.url : opt.url) : '';

    // if using lookup build the lookup structure
    if (opt.button) {
      // dialog template
      var tpl =
        '<div>' +
          '<div>' +
            '<div class="af-left">' +
              '<input type="text" class="af-lookup-term"/>' +
              '<button>Search</button>' +
            '</div>' +
            '<div class="af-right">' +
              '<button>Prev</button>' +
              '<input type="text" class="af-lookup-page"/>' +
              '<button>Next</button>' +
            '</div>' +
            '<div class="af-clear"></div>' +
          '</div>' +
          '<div>Selected:<br/></div>' +
          '<div>Result:<br/></div>' +
        '</div>';

      // create dialog
      var dia = $(tpl).attr('id', 'lookup_' + opt.id);
      // create button
      var btn =  $('<button>Search</button>')
        .button({text: false, icons: {primary: 'ui-icon-search'}});
      // append lookup button
      $(this[0]).after(btn);
      // append dialog and bind click event on button
      btn.append(dia).click(function() {
        $('#lookup_' + $(this).prev().attr('id')).dialog('open');
        return false;
      });
      // init key enter pressed
      $.each(dia.children().children().children('input'), function(x) {
        if (x == 0) {
          $(this).keyup(function(e) {
            if (e.keyCode == 13) $(this).next().click();
          });
        } else if (x == 1) {
          $(this).keyup(function(e) {
            if (e.keyCode == 13) {
              var term = $(this).parent().prev().children('input').first().val();
              var pageInput = $(this).parent().next().children('input').first();
              var page = $(this).val();
              var max = $(this).data('pageCount') == undefined ? 20 : $(this).data('pageCount');
              try {
                page = parseInt(page);
                if (page <= 1) page = 1;
                else if (page > max) page = max;
              } catch (e) {page = 1;}
              $(this).val(page);

              var data = {term: term, page: page};
              if (opt.addData != undefined) opt.addData.call(data);

              $.getJSON(opt.url, data, function(result) {
                var pageCount = result.pageCount == undefined ? 20 : result.pageCount;
                var list = result.list == undefined ? result : result.list;

                pageInput.data('pageCount', pageCount);
                parseLookupTable(list);
              });
            }
          });
        }
      });
      // init dialog buttons
      $.each(dia.children().children().children('button'), function(x) {
        // button search
        if (x == 0)
          $(this)
            .button({text: false, icons: {primary: 'ui-icon-search'}})
            .click(function() {
              var term = $(this).prev().val();
              var pageInput = $(this).parent().next().children('input').first();
              var page = pageInput.val(1).val();

              var data = {term: term, page: page};
              if (opt.addData != undefined) opt.addData.call(data);

              $.getJSON(opt.url, data, function(result) {
                var pageCount = result.pageCount == undefined ? 20 : result.pageCount;
                var list = result.list == undefined ? result : result.list;

                pageInput.data('pageCount', pageCount);
                parseLookupTable(list);
              });
            });
        // button prev
        else if (x == 1)
          $(this)
            .button({text: false, icons: {primary: 'ui-icon-triangle-1-w'}})
            .click(function() {
              var term = $(this).parent().prev().children('input').first().val();
              var pageInput = $(this).next();
              var page = pageInput.val();
              try {page = parseInt(page);if (--page <= 1) page = 1;} catch (e) {page = 1;}
              $(this).next().val(page);

              var data = {term: term, page: page};
              if (opt.addData != undefined) opt.addData.call(data);

              $.getJSON(opt.url, data, function(result) {
                var pageCount = result.pageCount == undefined ? 20 : result.pageCount;
                var list = result.list == undefined ? result : result.list;

                pageInput.data('pageCount', pageCount);
                parseLookupTable(list);
              });
            });
        // button next
        else if (x == 2)
          $(this)
            .button({text: false, icons: {primary: 'ui-icon-triangle-1-e'}})
            .click(function() {
              var term = $(this).parent().prev().children('input').first().val();
              var pageInput = $(this).prev();
              var page = pageInput.val();
              var max = pageInput.data('pageCount') == undefined ? 999999999 : pageInput.data('pageCount');
              try {page = parseInt(page);if (++page >= max) page = max;} catch (e) {page = max;}
              $(this).prev().val(page);

              var data = {term: term, page: page};
              if (opt.addData != undefined) opt.addData.call(data);

              $.getJSON(opt.url, data, function(result) {
                var pageCount = result.pageCount == undefined ? 20 : result.pageCount;
                var list = result.list == undefined ? result : result.list;

                pageInput.data('pageCount', pageCount);
                parseLookupTable(list);
              });
            });
      });

      // create dialog
      dia.dialog({
        autoOpen: opt.lookup && opt.lookup.autoOpen ? opt.lookup.autoOpen : false,
        title: opt.lookup && opt.lookup.title ? opt.lookup.title : 'Lookup',
        modal: opt.lookup && opt.lookup.modal ? opt.lookup.modal : false,
        minWidth: opt.lookup && opt.lookup.minWidth ? opt.lookup.minWidth : 500,
        minHeight: opt.lookup && opt.lookup.minHeight ? opt.lookup.minHeight : 200,
        buttons: {
          OK: function() {
            var data = [];
            $.each($($('#lookup_' + opt.id).children()[1]).children('div'), function() {
              data.push($(this).data('item'));
            });
            if (callback) callback.call(this, data);
            $(this).dialog('close');
          }
        }
      })
    }

    // create autocomplete
    if (opt.autocomplete) {
      $(this[0]).autocomplete({
        minLength: 0,
        source: function(req, res) {
          if (opt.addData != undefined) opt.addData.call(req);

          $.getJSON(opt.url ? opt.url : '', req, function(result) {
            var list = result.list == undefined ? result : result.list;

            res(list);
          });
        },
        focus: function(event, ui) {
          return false;
        },
        select: function(event, ui) {
          if (callback) callback.call(this, [ui.item]);
          return false;
        }
      }).data( "autocomplete" )._renderItem = function( ul, item ) {
        var li = $('<li></li>').data('item.autocomplete', item);
        var text = '<a>';
        if (opt.keys) {
          $.each(opt.keys, function() {
            if (this.select) text += $.formatType(item[this.key], this.format, false) + '<br/>';
          });
        } else {
          text += item.toString();
        }
        text += '</a>';

        li.append(text).appendTo(ul);
        return li;
      }
    }
  }
});

// end of AirFrame's jQuery extension

// AirFrame commons

$(function() {
  $(document).ajaxError(function(e, xhr) {
    $.displayError('<b>(' + xhr.status + ')</b> ' + xhr.statusText);
	});

  $('input[type="submit"]').button()
});

// end of AirFrame commons
